# version
## 0.3.3
現状のパッチを適用したら Fiber 周りの問題がなく動いた. しばらくの間動作確認をしてから正式採用するか決める.

## 0.3.2
2023年4月11日時点での最新版だが Fiber が利用できないので不採用. 根本的にこちらの実装が悪い可能性もあり、その場合は修正が必要.
```
resume: can't cross C function boundary (FiberError)
```
別件でビルド途中に対象物は不明だがリンクエラーがおきる. リンクコマンド末尾に `-ldl` があるのが直接の原因. このリンク前に libmruby.a はできていたので放置したところ `Fiber` が動かないことが判明し不採用となったので根本的原因は不明.

`-ldl` について `libdl.a` が必要かと予測し下記のパッケージを入れてみたが改善はしていない.
https://packages.msys2.org/package/mingw-w64-x86_64-dlfcn

## 0.3.1
それ以前は version 0.3.0 を利用していたが2022年5月12日公開の version 0.3.1 を使用する.

### require
`require` は git で最新ソースを checkout の仕組みらしく、 version 0.3.2 で追加された `mruby/internal.h` の include で失敗する. このファイルはなくてもいいらしい. (別のヘッダの関数を `mruby/internal.h` へ移動しただけかもしれない)

require (と mruby-io) には `fopen()` や WindowsAPI で末尾に `A` がつくものを利用していてマルチバイト文字列を含むファイル名が動作しないのでパッチをあてている.

# ビルドの手順
## 自動化
下記コマンドを打てばダウンロード、パッチ、コンパイルまで自動にやる.
```
make -f mruby.mak lib
```

これ以下の記述調整用のメモとなる.

## build_config/hamo.rb
DLL 化のためにマクロ定義が必要で、後述の DLL 化を参照.
他は `default` のままでよい. 試行錯誤の過程で complex と rational を削除してしまったが、今回の目的ではそれらはなくてもよいのでこのままにする.

# DLL 化
## マクロ定義 
`MRB_BUILD_AS_DLL` と `MRB_CORE` の定義をすると `include/mruby/common.h` によって API 関数に export 属性がつく. `MRB_LIB` と `MRB_CORE` の違いは未調査. この h ファイルは変更しなくていい.
```
include/mruby/common.h
#ifndef MRB_API
  #if defined(MRB_BUILD_AS_DLL)
    #if defined(MRB_CORE) || defined(MRB_LIB)
      # define MRB_API __declspec(dllexport)
    #else
      # define MRB_API __declspec(dllimport)
    #endif
  #else
    # define MRB_API extern
  #endif
#endif
```

## リンク
`build_config/host-shared.rb` に例があるが古いのか機能しない.

`libmruby.a` から dll へのリンクは `-Wl,--whole-archive ` のオプションを使えば `task/libmruby.rake` に手をいれる必要はない.

# エラー出力
`mrb_print_backtrace()` の出力先は `stderr` のみで不便なのでそれ以外を選べる fprint 版を追加. (API から取れる関数は微妙に情報が少なくデバッグに使えない)

`mrb_print_backtrace()` と `mrb_print_error()` は同じ処理なので `mrb_print_backtrace()` だけの対応とする. 修正箇所は `include/mruby.h` と `src/backtrace.c` の2つである.
