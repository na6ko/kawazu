#include <assert.h> //myasert は禁止. このソースでは myasert は無限再帰呼び出しになる.
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>
#include <sys/time.h>
#include <hamo.h>
#include "klass_manager_local.h"
#include "message_local.h"
#include "textencoding.h"

struct message{
	void *klass;
	void (*print)(void *t, const char *str);
	FILE *log;
	int message_enable;
	time_t logging_start;
	struct timeval timeval;
	char buf[0x10000];
	struct{
		char *buf;
		int left;
	}using;
	const struct errorlog_callback *errorlog_callback;
};
static void textarea_print(struct message *t, const char *str)
{
	fputs(str, t->log);

	if(t->print && t->message_enable){
		t->print(t->klass, str);
	}
	const size_t l = strlen(str);
	if(t->using.left < l + 1){
		t->using.buf = t->buf;
		t->using.left = sizeof(t->buf);
	}
	memcpy(t->using.buf, str, l);
	t->using.buf += l;
	*t->using.buf = '\0';
	t->using.left -= l;
}

/* message */
void message_new(struct mrb_state *m, void *klass, void (*callback)(void *t, const char *str), const char *logfilename, const char *mode, const struct errorlog_callback *errorlog_callback)
{
	static struct message tt;
	struct message *const t = &tt;
	klass_message_register(m, t);
	t->klass = klass;
	t->print = callback;
	t->errorlog_callback = errorlog_callback;
	t->message_enable = 1;
	assert(
		strncmp(mode, "w", 2) == 0 ||
		strncmp(mode, "a", 2) == 0
	);
	t->log = FOPEN(logfilename, mode);
	t->logging_start = time(NULL);
	gettimeofday(&t->timeval, NULL);
	t->using.buf = t->buf;
	t->using.left = sizeof(t->buf);
}
static void xx_printf(struct message *t, const char *format, va_list l)
{
	size_t n = vsnprintf(NULL, 0, format, l) + 1;
	char buf[n]; //C99 VLA
	vsnprintf(buf, n, format, l);
	textarea_print(t, buf);
}

#define XX_PRINTF(T, PREV_PARAM, FORMAT) \
{ \
	va_list l; \
	va_start(l, PREV_PARAM); \
	xx_printf(T, FORMAT, l); \
	va_end(l); \
}
#undef DECLARE_MESSAGE
#define DECLARE_MESSAGE(NAME, M) struct message *const NAME = klass_message_ref(M); assert(NAME->log);
void myassert_printf(struct mrb_state *m, const char *format, ...)
{
	DECLARE_MESSAGE(t, m);
	XX_PRINTF(t, format, format);
	fclose(t->log);
	memset(t, 0, sizeof(*t));
}

void message_delete(struct mrb_state *m)
{
	DECLARE_MESSAGE(t, m);
	fclose(t->log);
	klass_message_register(m, NULL);
	memset(t, 0, sizeof(*t));
}
static void error_save(void *f, void (*out_printf)(struct message *t, void *f, const char *format, ...), void (*out_puts)(struct message *t, void *tt, const char *str), struct message *t, const char *note)
{
	const char timeformat[] = "%Y-%m-%d %H:%M:%S";
	struct tm *tm = localtime(&t->logging_start);
	char timestamp[0x20];
	int l = sizeof(timestamp);
	strftime(timestamp, l, timeformat, tm);
	
	(*out_printf)(t, f, "---- %s.%02d; start ----\n", timestamp, t->timeval.tv_usec / 10000);
	(*out_printf)(t, f, "*%s*\n", note);
	(*out_puts)(t, f, t->buf);
	time_t timee = time(NULL);
	strftime(timestamp, l, timeformat, localtime(&timee));
	struct timeval timeval;
	(*out_printf)(t, f, "---- %s.%02d; end ----\n\n", timestamp, timeval.tv_usec / 10000);
}
static void out_fprintf(struct message *t, void *tt, const char *format, ...)
{
	va_list l;
	va_start(l, format);
	FILE *f = tt;
	vfprintf(f, format, l);
	va_end(l);
}
static void out_fputs(struct message *t, void *tt, const char *str)
{
	FILE *f = tt;
	fputs(str, f);
}
static void send_fprintf(struct message *t, void *tt, const char *format, ...)
{
	va_list l;
	va_start(l, format);
	size_t n = vsnprintf(NULL, 0, format, l) + 1;
	char buf[n];
	vsnprintf(buf, n, format, l);
	va_end(l);
	t->errorlog_callback->puts(tt, buf);
}
static void send_fputs(struct message *t, void *tt, const char *str)
{
	t->errorlog_callback->puts(tt, str);
}
void message_errorlog_save(struct mrb_state *m, const char *note)
{
	DECLARE_MESSAGE(t, m);
	const char filename[] = "kawazu_error.log";
	int l = program_dir_concat(filename, '/', NULL, 0);
	char program_dir[l]; //C99 VLA
	program_dir_concat(filename, '/', program_dir, l);
	
	{
		FILE *f = FOPEN(program_dir, "a");
		error_save(f, out_fprintf, out_fputs, t, note);
		fclose(f);
	}
	if(t->errorlog_callback == NULL){
		return;
	}
	{
		void *f = t->errorlog_callback->open(t->klass);
		error_save(f, send_fprintf, send_fputs, t, note);
		t->errorlog_callback->close(f);
	}
}

void message_printf(struct mrb_state *m, const char *format, ...)
{
	DECLARE_MESSAGE(t, m);
	XX_PRINTF(t, format, format);
}
void message_puts(struct mrb_state *m, const char *str)
{
	DECLARE_MESSAGE(t, m);
	textarea_print(t, str);
	textarea_print(t, "\n");
}
void log_printf(struct mrb_state *m, const char *format, ...)
{
	DECLARE_MESSAGE(t, m);
	int flag = t->message_enable;
	t->message_enable = 0;
	XX_PRINTF(t, format, format);
	t->message_enable = flag;
}
void log_puts(struct mrb_state *m, const char *str)
{
	DECLARE_MESSAGE(t, m);
	int flag = t->message_enable;
	t->message_enable = 0;
	message_puts(m, str);
	t->message_enable = flag;
}
#undef XX_PRINTF

/* message */
#include <mruby.h>
#include <mruby/string.h>
#include "mruby_driver.h"

void messege_backtrace_print(struct mrb_state *m)
{
	DECLARE_MESSAGE(t, m);
	assert(t);
	mrb_fprint_backtrace(m, t->log);
	mrb_fprint_backtrace(m, stdout);
	fflush(stdout);
	if(t->print){
		t->print(t->klass, RSTRING_PTR(mrb_inspect(m, mrb_obj_value(m->exc))));
		t->print(t->klass, "\n");
	}
}

static mrb_value m_message_print(mrb_state *m, mrb_value self)
{
	DECLARE_MESSAGE(t, m);
	assert(t);
	mrb_value str;
	mrb_bool logonly;
	mrb_get_args(m, "Sb", &str, &logonly);
	if(logonly){
		t->message_enable = 0;
	}
	textarea_print(t, RSTRING_PTR(str));
	t->message_enable = 1;
	return mrb_nil_value();
}
static mrb_value m_errorlog_save(mrb_state *m, mrb_value self)
{
	mrb_value str;
	mrb_get_args(m, "S", &str);
	message_errorlog_save(m, RSTRING_PTR(str));
	return mrb_nil_value();
}
static const struct script_function_list sbp_list[] = {
#define M(NAME, ARGC) {#NAME, m_##NAME, MRB_ARGS_REQ(ARGC)}
	M(message_print, 2), M(errorlog_save, 1),
	{NULL, NULL, 0}
#undef M
};
void sbp_message_method_define(struct mrb_state *m, struct RClass *mm)
{
	hamo_module_define(m, mm, "Textarea", sbp_list);
}
