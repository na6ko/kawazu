#pragma once
struct mrb_state;
void cui_textarea_new(struct mrb_state *m, const char *filename, const char *mode);
void cui_textarea_delete(struct mrb_state *m);
int cui_title_to_driver(struct mrb_state *m, int c, char **v);
