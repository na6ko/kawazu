CC = clang

SRCDIR = ../source_core
MRUBY_DIR = ../mruby-3.1.0
MRUBY_TARGET = $(MRUBY_DIR)/build/host
MRUBY_LIB = $(MRUBY_TARGET)/lib/libmruby.a
SBP_DIR = 
SBP_LIB = 
-include path.mk

CFLAGS = -Wall -Werror -std=c99 -ferror-limit=2
CFLAGS += -MMD -MP -MF $(OBJDIR)/$(@F).d
