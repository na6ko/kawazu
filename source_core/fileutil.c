/*
ファイル名として default codepage を期待するファイル操作関数に対して
unicode で処理するための mruby wrapper.
*/
#include <hamo.h>
#include <mruby.h>
#include <mruby/string.h>
#include <mruby/array.h>
#include <windows.h>
#if defined(_WIN32) || defined(_WIN64)
  #include "textencoding.h"
#else
  #include <unistd.h>
  #include <sys/stat.h>
#endif
#include "mruby_driver.h"
#include "fileutil.h"

static mrb_value m_unlink(mrb_state *m, mrb_value self)
{
	mrb_value s;
	mrb_get_args(m, "S", &s);
#if defined(_WIN32) || defined(_WIN64)
	my_unlink(mrb_str_to_cstr(m, s));
#else
	unlink(mrb_str_to_cstr(m, s));
#endif
	return mrb_nil_value();
}
static mrb_value m_mkdir(mrb_state *m, mrb_value self)
{
	mrb_value s;
	mrb_get_args(m, "S", &s);
#if defined(_WIN32) || defined(_WIN64)
	my_mkdir(mrb_str_to_cstr(m, s));
#else
	mkdir(mrb_str_to_cstr(m, s), 0755);
#endif
	return mrb_nil_value();
}
struct fileutil_find{
	mrb_state *m;
	mrb_value array;
};
static void find_callback(void *k, const char *str)
{
	struct fileutil_find *t = k;
	mrb_ary_push(t->m, t->array, mrb_str_new_cstr(t->m, str));
}
static mrb_value m_find(mrb_state *m, mrb_value self)
{
	struct fileutil_find t = {.m = m, .array = mrb_ary_new(m)};
	mrb_value s;
	mrb_get_args(m, "S", &s);
	file_find(&t, find_callback, mrb_str_to_cstr(m, s));
	return t.array;
}
static const struct script_function_list list[] = {
#define M(NAME, ARGC) {#NAME, m_##NAME, MRB_ARGS_REQ(ARGC)}
	M(unlink, 1), M(mkdir, 1), M(find, 1),
	{NULL, NULL, 0}
#undef M
};
void fileutil_method_define(struct mrb_state *m, struct RClass *mm)
{
	hamo_module_define(m, mm, "File", list);
}

