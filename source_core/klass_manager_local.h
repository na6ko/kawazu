/* 
THIS FILE IS AUTO-GENERATED
by klass_manager.rb
*/

#pragma once
struct mrb_state;

void klass_manager_init(struct mrb_state *m);
void klass_manager_close(struct mrb_state *m);
struct RClass;
int klass_manager_simulator_load(struct mrb_state *m, struct RClass *mm, const char *filename);
struct mruby_serialport_s;
void klass_mruby_serialport_s_register(struct mrb_state *m, struct mruby_serialport_s *t);
struct mruby_serialport_s *klass_mruby_serialport_s_ref(struct mrb_state *m);

struct progressbar;
void klass_progressbar_register(struct mrb_state *m, struct progressbar *t);
struct progressbar *klass_progressbar_ref(struct mrb_state *m);

struct message;
void klass_message_register(struct mrb_state *m, struct message *t);
struct message *klass_message_ref(struct mrb_state *m);

struct textarea;
void klass_list_driver_register(struct mrb_state *m, struct textarea *t);
struct textarea *klass_list_driver_ref(struct mrb_state *m);

void klass_list_action_register(struct mrb_state *m, struct textarea *t);
struct textarea *klass_list_action_ref(struct mrb_state *m);

void klass_list_flashdevice_register(struct mrb_state *m, struct textarea *t);
struct textarea *klass_list_flashdevice_ref(struct mrb_state *m);

void klass_title_to_driver_register(struct mrb_state *m, struct textarea *t);
struct textarea *klass_title_to_driver_ref(struct mrb_state *m);

void klass_list_config_register(struct mrb_state *m, struct textarea *t);
struct textarea *klass_list_config_ref(struct mrb_state *m);

