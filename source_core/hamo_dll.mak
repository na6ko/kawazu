TARGET = ../bin/hamo.dll
OBJDIR = ../bin/obj_hamo_dll
include base.mk
CFLAGS += -O2 -D_HAMO_BUILD_ -D_SIM_LOAD_ -DMRB_BUILD_AS_DLL
CFLAGS += -I$(MRUBY_DIR)/include -I../include
OBJ = $(addprefix $(OBJDIR)/, hamo.o klass_manager.o serialport_windows.o progressbar.o textencoding_windows.o fileutil.o message.o textarea.o)
OBJ += $(addprefix $(OBJDIR)/mruby_, loader.o serialport.o array_converter.o)
DEPEND_DLL = $(addprefix ../bin/, mruby.dll mcu_simulator.dll)

.PHONY: all clean

all: $(OBJDIR)
	make -C $(SBP_DIR)/busniki/simulator
	make -f hamo_dll.mak $(TARGET) $(DEPEND_DLL)
clean:
	rm -rf $(TARGET) $(OBJDIR) 
$(TARGET): $(OBJ) $(DEPEND_DLL)
	$(CC) -shared -static -o $@ $^ -lz -lsetupapi
	strip $@
	ldd $@ | grep -i -v System32
include $(MRUBY_TARGET)/objs.mk
../bin/mruby.dll: $(LIBMRUBY_OBJS)
	$(CC) -shared -static -o $@ $^ -lws2_32
	strip $@
include objdir.mk
