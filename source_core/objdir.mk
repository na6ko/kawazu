$(OBJDIR):
	mkdir $@
$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) -c -o $@ $<
$(OBJDIR)/mruby_loader.o: $(SRCDIR)/mruby_loader.c
	$(CC) $(CFLAGS) -I$(SBP_DIR)/include -DSYSTEM_SIM -c -o $@ $<
klass_manager.c: klass_manager.rb
	ruby $<
-include $(OBJDIR)/*.d
