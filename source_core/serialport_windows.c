/*
注意: これは Windows API 依存のソース.
*/
#include "myassert.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <windows.h>
#include <process.h>
#include <setupapi.h>
#include "memory_allocator.h"
#include "serialport.h"

#define RECEIVE_DATA_NUM (0x20)
struct serialport{
	struct mrb_state *m;
	HANDLE port;
	int receive_timeout_ms;
	struct{
		void *klass;
		const struct serialport_callbacks *callbacks;
	}parent;
	struct {
		HANDLE thread, fifo_ready, io_error;
		CRITICAL_SECTION section;
		uint8_t buffer[0x10020];
		struct receive_data{
			uint8_t *data;
			uint32_t length;
		}data[RECEIVE_DATA_NUM];
		volatile struct receive_data *w, *r;
	}receiver;
	DWORD error_code;
};

static uint32_t thead_readfile(void *tt);

struct serialport *serialport_create(struct mrb_state *m, const char *portname, uint32_t baud, enum serialport_parity parity, int byteperbit)
{
	int receiver_use_thread = 1;
	DECLARE_STRUCT_MALLOC(struct serialport, t);
	myassert(m, t);
	memset(t, 0, sizeof(struct serialport));
	t->m = m;
	t->receive_timeout_ms = 480*2;

	{
		const char prefix[] = "\\\\.\\";
		const char *p = portname;
		char pn[0x18];
		if(memcmp(prefix, portname, sizeof(prefix) - 1) != 0){
			p = pn;
			snprintf(pn, sizeof(pn)-1, "%s%s", prefix, portname);
		}
		t->port = CreateFile(p, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
	}
	if(t->port == INVALID_HANDLE_VALUE){
		serialport_destroy(t);
		return NULL;
	}

	{
		DCB d;
		GetCommState(t->port, &d);
		d.BaudRate = baud;
		switch(parity){
		case SERIAL_PARITY_NONE:
			d.Parity = NOPARITY;
			d.fParity = 0;
			break;
		case SERIAL_PARITY_EVEN:
			d.Parity = EVENPARITY;
			d.fParity= 1;
			break;
		case SERIAL_PARITY_ODD:
			d.Parity = ODDPARITY;
			d.fParity = 1;
			break;
		}
		d.StopBits = ONESTOPBIT;
		d.ByteSize = byteperbit;
		
		if(SetCommState(t->port, &d) == FALSE){
			serialport_destroy(t);
			return NULL;
		}
	}

	{
		COMMTIMEOUTS timeout = {
			MAXDWORD, MAXDWORD, 0,
			0, 0
		};
		if(SetCommTimeouts(t->port, &timeout) == FALSE){
			serialport_destroy(t);
			return NULL;
		}
	}
	if(receiver_use_thread){
		InitializeCriticalSection(&t->receiver.section);
		t->receiver.w = t->receiver.data;
		t->receiver.r = t->receiver.data;
		t->receiver.fifo_ready = CreateEvent(NULL, TRUE, FALSE, NULL);
		t->receiver.io_error = CreateEvent(NULL, TRUE, FALSE, NULL);
		t->receiver.thread = (HANDLE) _beginthreadex(
			NULL, 0, thead_readfile, t, 0, NULL
		);
		assert(t->receiver.thread);
	}
	return t;
}

void serialport_destroy(struct serialport *const t)
{
	if(t->receiver.thread){
		CloseHandle(t->receiver.thread);
		CloseHandle(t->receiver.fifo_ready);
		CloseHandle(t->receiver.io_error);
		CloseHandle(t->port);
		DeleteCriticalSection(&t->receiver.section);
	}
	Free(t);
}

void serialport_callback_set(struct serialport *t, void *parent, const struct serialport_callbacks *a)
{
	t->parent.klass = parent;
	t->parent.callbacks = a;
}
void serialport_rx_purge(struct serialport *t)
{
	BOOL r = PurgeComm(t->port, PURGE_RXCLEAR);
	myassert(t->m, r == TRUE);
}

struct rwex{
	OVERLAPPED ov; //cast のため先頭に置くこと
	volatile DWORD length;
	void *parent;
	uint8_t *d;
};
static void overlapped_callback_error(struct serialport *t, DWORD error)
{
	char str[0x200];
	FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		error,
		MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
		str,
		sizeof(str),
		NULL
	);
	t->parent.callbacks->error(t->parent.klass, error, str);
}
static void send_callback(DWORD error, DWORD length, OVERLAPPED *ov)
{
	struct rwex *const s = (struct rwex *) ov;
	struct serialport *const t = s->parent;
	if(error != 0){
		overlapped_callback_error(t, error);
		return;
	}
	s->length = length;
	t->parent.callbacks->progressbar_add(t->parent.klass, length);
}
uint32_t serialport_send(struct serialport *t, const uint8_t *buf, uint32_t length)
{
	uint32_t sent = 0;
	struct rwex s;
	memset(&s, 0, sizeof(s));
	s.parent = t;
	while(length){
		s.length = 0;
		BOOL r = WriteFileEx(t->port, buf, length, &s.ov, send_callback);
		if(r != TRUE){
			return sent;
		}
		SleepEx(INFINITE, TRUE);
		myassert(t->m, s.length);
		buf += s.length;
		sent += s.length;
		length -= s.length;
	}
	return sent;
}

void serialport_receive_timeout_set(struct serialport *const t, int ms)
{
	t->receive_timeout_ms = ms;
}

static int fifo_is_empty(struct serialport *t)
{
	EnterCriticalSection(&t->receiver.section);
	volatile int e = t->receiver.w == t->receiver.r;
	if(e){
		ResetEvent(t->receiver.fifo_ready);
	}
	LeaveCriticalSection(&t->receiver.section);
	return e;
}
static uint32_t receive_thread(struct serialport *t, uint32_t length)
{
	uint32_t red = 0;
	while(length > 0){
		if(fifo_is_empty(t)){
			HANDLE eventlist[] = {
				t->receiver.fifo_ready, 
				t->receiver.io_error
			};
			switch(WaitForMultipleObjects(
				sizeof(eventlist)/sizeof(eventlist[0]),
				eventlist, 
				FALSE,
				t->receive_timeout_ms
			)){
			case WAIT_OBJECT_0 + 0: break;
			case WAIT_OBJECT_0 + 1:
				overlapped_callback_error(t, t->error_code);
				return red;
			case WAIT_TIMEOUT:
				t->parent.callbacks->error(t->parent.klass, 0, "timeout");
				return red;
			default: 
				assert(0);
				return red;
			}
			if(fifo_is_empty(t)){
				return red;
			}
		}
		const int l = min(length, t->receiver.r->length);
		length -= l;
		t->parent.callbacks->progressbar_add(t->parent.klass, l);
		t->parent.callbacks->receive(t->parent.klass, t->receiver.r->data, l);
		red += l;
		t->receiver.r->data += l;
		t->receiver.r->length -= l;
		if(t->receiver.r->length == 0){
			t->receiver.r += 1;
			if(t->receiver.r >= t->receiver.data + RECEIVE_DATA_NUM){
				t->receiver.r = t->receiver.data;
			}
		}
	}
	return red;
}
static uint32_t receive_readfile(struct serialport *t, uint32_t length)
{
	uint32_t red = 0;
	struct rwex s;
	memset(&s, 0, sizeof(s));
	s.parent = t;
	while(length){
		s.length = 0;
		BOOL r = ReadFileEx(t->port, t->receiver.buffer, length < 0x400 ? length : 0x400, &s.ov, send_callback);
		myassert(t->m, r == TRUE);
		if(SleepEx(t->receive_timeout_ms, TRUE) == 0){
			return red;
		}
		if(s.length == 0){
			return red;
		}
		t->parent.callbacks->receive(t->parent.klass, t->receiver.buffer, s.length);
		red += s.length;
		length -= s.length;
	}
	return red;
}
uint32_t serialport_receive(struct serialport *t, uint32_t length)
{
	if(t->receiver.thread){
		return receive_thread(t, length);
	}
	return receive_readfile(t, length);
}
static void receive_callback(DWORD error, DWORD redcount, OVERLAPPED *ov);
static uint32_t thead_readfile(void *tt)
{
	struct serialport *t = tt;
	struct rwex s;
	memset(&s, 0, sizeof(s));
	s.parent = t;
	s.d = t->receiver.buffer;
	while(1){
		DWORD length = t->receiver.buffer + sizeof(t->receiver.buffer) - s.d;
		if(ReadFileEx(t->port, s.d, length, &s.ov, receive_callback) == FALSE){
			if(GetLastError() != ERROR_IO_PENDING){
				break;
			}
		}
		SleepEx(INFINITE, TRUE);
	}
	return 0;
}
static void receive_callback(DWORD error, DWORD redcount, OVERLAPPED *ov)
{
	struct rwex *const s = (struct rwex *) ov;
	struct serialport *const t = s->parent;
	if(error != 0){
		t->error_code = error;
		SetEvent(t->receiver.io_error);
		_endthreadex(0);
		return;
	}
	if(redcount == 0){
		return;
	}
	t->receiver.w->data = s->d;
	t->receiver.w->length = redcount;

	//---- update fifo pointers ----
	EnterCriticalSection(&t->receiver.section);
	t->receiver.w += 1;
	if(t->receiver.w >= t->receiver.data + RECEIVE_DATA_NUM){
		t->receiver.w = t->receiver.data;
	}
	assert(t->receiver.w != t->receiver.r);
	SetEvent(t->receiver.fifo_ready);
	LeaveCriticalSection(&t->receiver.section);
	//---- done ----
	
	s->d += redcount;
	if(s->d >= t->receiver.buffer + sizeof(t->receiver.buffer) - 0x800){
		s->d = t->receiver.buffer;
	}
}

/*
Msys64 で GUID_DEVINTERFACE_COMPORT がリンクできない問題
ntddser.h に該当変数の記載はあるがマクロで実際には定義されていないらしい. 対処方法は不明.
別件として PHYSICAL_ADDRESS の定義で止まってしまう. その変数は使わないのでダミーの型をいれればコンパイルはとりあえず通る.
*/
/*//kludge
#include <stdint.h>
typedef uintptr_t PHYSICAL_ADDRESS;
#include <ntddser.h>*/
#if 1
/*static*/ const GUID GUID_DEVINTERFACE_COMPORT = { 
	0x86e0d1e0, 0x8089, 0x11d0, 
	{0x9c, 0xe4, 0x08, 0x00, 0x3e, 0x30, 0x1f, 0x73}
};
#endif

void usbdevice_find(void *klass, const struct serialport_callbacks *a, int vendor_id, int product_id)
{
	HDEVINFO h = SetupDiGetClassDevs(&GUID_DEVINTERFACE_COMPORT, NULL, NULL, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
	if(h == NULL){
		return;
	}
	const char hardware_id_format[] = "USB\\VID_%04d&PID_%04d";
	const size_t hardware_id_size = snprintf(
		NULL, 0, hardware_id_format,
		vendor_id, product_id
	) + 1;
	char hardware_id[hardware_id_size];
	snprintf(
		hardware_id, hardware_id_size, hardware_id_format,
		vendor_id, product_id
	);
	SP_DEVINFO_DATA d;
	d.cbSize = sizeof(d);
	int device_index = 0;
	while(SetupDiEnumDeviceInfo(h, device_index, &d)){
		char portname[0x10];
		HKEY key = SetupDiOpenDevRegKey(h, &d, DICS_FLAG_GLOBAL, 0, DIREG_DEV, KEY_QUERY_VALUE);
		assert(key);
		{
			DWORD type = 0;
			DWORD size = sizeof(portname);
			RegQueryValueEx(key, "PortName", NULL, &type, (LPBYTE) portname, &size);
		}
		const DWORD spdpr = SPDRP_HARDWAREID;
		DWORD data_type, size = 0;
		BOOL r = SetupDiGetDeviceRegistryProperty(h, &d, spdpr, &data_type, NULL, 0, &size);
		assert(r == FALSE);
		assert(GetLastError() == ERROR_INSUFFICIENT_BUFFER);

		BYTE buf[size]; //C99 VLA
		r = SetupDiGetDeviceRegistryProperty(h, &d, spdpr, &data_type, buf, size, &size);
		assert(r == TRUE);
		assert(data_type == REG_MULTI_SZ);
		if(strncmp((const char *) buf, hardware_id, hardware_id_size - 1) == 0){
			a->device_find(klass, portname);
		}

		device_index += 1;
	}
	SetupDiDestroyDeviceInfoList(h);
}
