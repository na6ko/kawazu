TARGET = ../bin/hamo_debug.exe
OBJDIR = ../bin/obj_debug
include base.mk
CFLAGS += -O0 -g -D_HAMO_STATICLINK_ -D_SIM_STATICLINK_ -DMEMORY_DEBUG
CFLAGS += -I$(MRUBY_DIR)/include -I../include
OBJ = $(addprefix $(OBJDIR)/, main.o hamo.o klass_manager.o serialport_windows.o progressbar.o cui_progressbar.o textencoding_windows.o fileutil.o message.o textarea.o cui_message.o)
OBJ += $(addprefix $(OBJDIR)/mruby_, loader.o serialport.o array_converter.o)

.PHONY: all clean

all: klass_manager.c $(OBJDIR)
	make -C $(SBP_DIR)/busniki/simulator
	make -f hamo_debug.mak $(TARGET)
clean:
	rm -rf $(TARGET) $(OBJDIR)
$(TARGET): $(OBJ) $(SBP_LIB)
	$(CC) -o $@ $^ $(MRUBY_LIB) -lws2_32 -lz -lsetupapi
include objdir.mk
