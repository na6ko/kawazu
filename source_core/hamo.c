#include "myassert.h"
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <hamo.h>
struct mrb_state;
#include "mruby_loader.h"
#include "textarea_local.h"
#include "message_local.h"
#include "textencoding.h"

static const char *const USAGE[] ={
"hamo (options...) [functionname] (function arguments...)",
"options:",
"  -c ; load \"cui.rb\" and call method \"cui_for_user\"",
"  -f [filename] ; load script filename",
"  -s [software title] (software title) ...; (CUI only) search title to supported driver name",
NULL
};
int32_t hamo_main(struct mrb_state *m, int32_t c, char **v)
{
	int r = 0;
	const char *scriptfilename = NULL;
	myassert(m, c);
	log_printf(m, v[0]);
	for(int i = 1; i < c; i++){
		if(strchr(v[i], ' ')){
			log_printf(m, " \"%s\"", v[i]);
		}else{
			log_printf(m, " %s", v[i]);
		}
	}
	log_puts(m, "");

	optind = 1; //for GUI
	do{
		r = getopt(c, v, "cf:");
		switch(r){
		case 'c':{
			static const char filename[] = "cui.rb";
			c -= optind;
			v += optind;
			int l = program_dir_concat(filename, '/', NULL, 0);
			char program_dir[l]; //C99 VLA
			program_dir_concat(filename, '/', program_dir, l);
			scriptfilename = program_dir;
			r = script_exec(m, scriptfilename, "cui_for_user", c, v);
			}return r;
		case 'f':
			scriptfilename = optarg;
			break;
		case -1:
			break;
		default:
			r = 1;
			c = -1;
			message_printf(m, "unknown option '%c'\n", r);
			break;
		}
	}while(r != -1);
	if(scriptfilename == NULL){
		message_puts(m, "missing scriptfilaname (?)");
		return -1;
	}
	if((c - optind) < 1){
		const char *const *u = USAGE;
		while(*u){
			message_puts(m, *u++);
		}
		return -1;
	}
	v += optind;
	c -= optind;

	const char *funcname = *v;
	v += 1;
	c -= 1;
	if(0 && c == 0){
		message_puts(m, "no argument");
		r = -1;
	}else{
		myassert(m, v);
		r = script_exec(m, scriptfilename, funcname, c, v);
	}
	return r;
}
