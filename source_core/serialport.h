#pragma once

struct serialport;
enum serialport_parity{
	SERIAL_PARITY_NONE, SERIAL_PARITY_EVEN, SERIAL_PARITY_ODD
};
struct serialport *serialport_create(struct mrb_state *m, const char *portname, uint32_t baud, enum serialport_parity parity, int byteperbit);
void serialport_destroy(struct serialport *t);
struct serialport_callbacks{
	void (*error)(void *t, uint32_t errorcode, const char *str);
	void (*device_find)(void *tt, const char *str);
	void (*receive)(void *tt, const uint8_t *data, int length);
	void (*progressbar_add)(void *tt, int length);
};
void serialport_callback_set(struct serialport *t, void *parent, const struct serialport_callbacks *a);
void serialport_rx_purge(struct serialport *t);
void serialport_receive_timeout_set(struct serialport *const t, int ms);
uint32_t serialport_send(struct serialport *t, const uint8_t *buf, uint32_t length);
uint32_t serialport_receive(struct serialport *t, uint32_t length);
void usbdevice_find(void *klass, const struct serialport_callbacks *a, int vendor_id, int product_id);
