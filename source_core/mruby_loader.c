#include <zlib.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <mruby.h>
#include <mruby/array.h>
#include <mruby/string.h>
#include <mruby/compile.h>
#include <mruby/error.h>
#include <hamo.h>
#include <mruby_driver.h>
#include <usb_driver.h>
#include "myassert.h"
#include "klass_manager_local.h"
#include "mruby_loader.h"
#include "mruby_serialport.h"
#include "textencoding.h"
#include "progressbar_local.h"
#include "textarea_local.h"
#include "fileutil.h"

//単純な構造の場合は base = m->kernel_module
static void script_method_define(mrb_state *m, const struct script_function_list *l, struct RClass *base)
{
	while(l->name != NULL){
		mrb_define_method(m, base, l->name, l->func, l->argc);
		l++;
	}
}
void hamo_module_define(struct mrb_state *m, struct RClass *mm, const char *modulename, const struct script_function_list *l)
{
	struct RClass *mmm = mrb_define_module_under(m, mm, modulename);
	while(l->name != NULL){
		mrb_define_class_method(m, mmm, l->name, l->func, l->argc);
		l++;
	}
}
void hamo_class_define(struct mrb_state *m, struct RClass *mm, const char *classname, const struct script_function_list *l)
{
	struct RClass *mmc = mrb_define_class_under(m, mm, classname, m->object_class);
	script_method_define(m, l, mmc);
}
static int script_load(mrb_state *m, const char *filename)
{
	FILE *const f = FOPEN(filename, "r");
	if(f == NULL){
		message_printf(m, "script file: %s is not found\n", filename);
		return -1;
	}
	mrb_value rbfile;
	const int arena = mrb_gc_arena_save(m);
	{
		mrbc_context *c = mrbc_context_new(m);
		mrbc_filename(m, c, filename);
		rbfile = mrb_load_file_cxt(m, f, c);
		mrbc_context_free(m, c);
	}
	fclose(f);
	if(m->exc != 0 && (mrb_nil_p(rbfile) || mrb_undef_p(rbfile))){
		messege_backtrace_print(m);
		return -2;
	}
	mrb_gc_arena_restore(m, arena);
	return 0;
}


static mrb_value m_usleep(mrb_state *m, mrb_value self)
{
	mrb_int d;
	mrb_get_args(m, "i", &d);
	usleep(d);
	return mrb_nil_value();
}

static void crc32_calc_callback(void *parent, const uint8_t *data, mrb_int length)
{
	mrb_int *crc = parent;
	*crc = crc32(*crc, data, length);
}
static mrb_value m_crc32_calc(mrb_state *m, mrb_value self)
{
	mrb_int crc = 0;
	struct script_array t;
	t.callback = crc32_calc_callback;
	t.parent = &crc;
	mrb_get_args(m, "A|i", &t.mrb_array, &crc);
	if(crc == 0){
		crc = crc32(0L, Z_NULL, 0);
	}
	mruby_array_into_uint8_t(m, &t);
	return mrb_fixnum_value(crc);
}

static mrb_value program_path_get(mrb_state *m)
{
	int l = program_dir_get(NULL, '/', 0);
	char str[l]; //C99 VLA
	program_dir_get(str, '/', l);
	return mrb_str_new_cstr(m, str);
}
struct mrb_state *hamo_mrb_open(void)
{
	mrb_state *m = mrb_open();
	klass_manager_init(m);
	{ //define C style methods into module Hamo
		struct RClass *mm = mrb_define_module(m, "Hamo");
		mrb_value program_path =
#if defined(_WIN32) || defined(_WIN64)
			program_path_get(m);
#else
			mrb_str_new_cstr(m, "~/.kawazu");
#endif
		mrb_define_const(m, mm, "PROGRAM_DIR", program_path);
		mrb_define_const(m, mm, "USER_DIR", program_path);
		mrb_define_class_method(m, mm, "crc32_calc", m_crc32_calc, MRB_ARGS_REQ(1));
		mrb_define_class_method(m, mm, "usleep", m_usleep, MRB_ARGS_REQ(1));
		mruby_progressbar_method_define(m, mm);
		sbp_textarea_method_define(m, mm);
		sbp_message_method_define(m, mm);
		mruby_serialport_method_define(m, mm);
		fileutil_method_define(m, mm);
		mrb_value simulator = mrb_true_value();
#ifdef _SIM_STATICLINK_
		simulator_method_define(m, mm);
#endif
#ifdef _HAMO_BUILD_
		if(klass_manager_simulator_load(m, mm, "mcu_simulator.dll") == 0){
			simulator = mrb_false_value();
			mrb_define_class_under(m, mm, "SimulatorBase", m->object_class);
		}
#endif
		mrb_define_const(m, mm, "SIMULATOR_LOADED", simulator);
	}
	return m;
}
int script_exec(struct mrb_state *m, const char *filename, const char *funcname, int argc, char **c_argv)
{
	int r = script_load(m, filename);
	if(r != 0){
		message_errorlog_save(m, "loading error");
		return r;
	}
	
	mrb_value m_argv = mrb_ary_new(m);
	for(int i = 0; i < argc; i++){
		mrb_ary_push(m, m_argv, mrb_str_new_cstr(m, c_argv[i]));
	}
	mrb_funcall(m, mrb_top_self(m), funcname, 1, m_argv);

	r = 0;
	if(m->exc != 0){
		messege_backtrace_print(m);
		message_errorlog_save(m, "fatal");
		r = -2;
	}
	
	return r;
}
void hamo_mrb_close(struct mrb_state *m)
{
	struct RClass *mm = mrb_module_get(m, "Hamo");
	mm = mrb_module_get_under(m, mm, "Progressbar");
	mrb_funcall(m, mrb_obj_value(mm), "done", 0, NULL);
	klass_manager_close(m);
	mrb_close(m);
}
int32_t hamo_title_to_driver_load(struct mrb_state *m, void *klass, hamo_callback_item_add callback)
{
	title_to_driver_new(m, klass, callback);

	struct RClass *mm = mrb_module_get(m, "Hamo");
	title_to_driver_textarea_method_define(m, mm);

	const char file[] = "title_to_driver.rb";
	int l = program_dir_concat(file, '/', NULL, 0);
	char str[l];
	program_dir_concat(file, '/', str, l);
	
	if(script_load(m, str) != 0){
		return -1;
	}
	return 0;
}
int32_t hamo_title_to_driver_call(struct mrb_state *m, const char *word)
{
	mrb_funcall(m, mrb_top_self(m), "search", 1, mrb_str_new_cstr(m, word));
	if(m->exc != 0){
		messege_backtrace_print(m);
		return -2;
	}
	return 0;
}
void hamo_title_to_driver_clear(struct mrb_state *m)
{
	title_to_driver_delete(m);
}

