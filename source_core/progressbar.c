#include "myassert.h"
#include <stdint.h>
#include <mruby.h>
#include <hamo.h>
#include "klass_manager_local.h"
#include "progressbar_local.h"

struct progressbar{
	void *klass;
	const struct progressbar_callback *m;
	int index;
};
#define DECLARE_DEVICE(NAME, MM) struct progressbar *const NAME = klass_progressbar_ref(MM)

void progressbar_init(struct mrb_state *m, void *klass, const struct progressbar_callback *callback)
{
	static struct progressbar d;
	klass_progressbar_register(m, &d);
	d.index = -1;
	d.klass = klass;
	d.m = callback;
}
void progressbar_add(struct mrb_state *m, int length)
{
	DECLARE_DEVICE(d, m);
	if(d->index < 0 || d->index >= 2){
		return;
	}
	d->m->add(d->klass, d->index, length);
}

#include <mruby/string.h>
#include "mruby_driver.h"

static mrb_value m_progressbar_range_set(mrb_state *m, mrb_value self)
{
	mrb_int index, length;
	mrb_value name;
	mrb_get_args(m, "iSi", &index, &name, &length);
	DECLARE_DEVICE(d, m);
	myassert(m, index == 0 || index == 1);
	d->m->range_set(d->klass, index, RSTRING_PTR(name), length);
	return mrb_nil_value();
}
static mrb_value m_progressbar_draw_index_set(mrb_state *m, mrb_value self)
{
	mrb_int index;
	mrb_get_args(m, "i", &index);
	DECLARE_DEVICE(d, m);
	d->index = index;
	return mrb_nil_value();
}
static mrb_value m_progressbar_ready(mrb_state *m, mrb_value self)
{
	DECLARE_DEVICE(d, m);
	if(d->m->ready){
		d->m->ready(d->klass);
	}
	return mrb_nil_value();
}
static mrb_value m_progressbar_done(mrb_state *m, mrb_value self)
{
	DECLARE_DEVICE(d, m);
	d->index = -1;
	if(d->m->done){
		d->m->done(d->klass);
	}
	return mrb_nil_value();
}
//this is used for backup RAM-write and Simulator ROM-Read
static mrb_value m_progressbar_add(mrb_state *m, mrb_value self)
{
	mrb_int length;
	mrb_get_args(m, "i", &length);
	progressbar_add(m, length);
	return mrb_nil_value();
}
static mrb_value m_progressbar_label_update(mrb_state *m, mrb_value self)
{
	mrb_int index;
	mrb_value name;
	mrb_get_args(m, "iS", &index, &name);
	DECLARE_DEVICE(d, m);
	myassert(m, index == 0 || index == 1);
	d->m->label_update(d->klass, index, RSTRING_PTR(name));
	return mrb_nil_value();
}
static const struct script_function_list list[] = {
#define M(NAME, ARGC) {#NAME, m_progressbar_##NAME, MRB_ARGS_REQ(ARGC)}
	M(range_set, 3),
	M(draw_index_set, 1),
	M(add, 2),
	M(ready, 0),
	M(done, 0),
	M(label_update, 2),
	{NULL, NULL, 0}
#undef M
};
void mruby_progressbar_method_define(mrb_state *m, struct RClass *mm)
{
	hamo_module_define(m, mm, "Progressbar", list);
}
