#include <stdint.h>
#include <mruby.h>
#include <mruby/array.h>
#include <hamo.h>
#include <mruby_driver.h>

#define ARRAY_SIZE (0x2000)
static mrb_value global_buffer[ARRAY_SIZE];
static inline mrb_int length_min(mrb_int a, mrb_int b)
{
	return a < b ? a : b;
}
int mruby_array_into_uint8_t(mrb_state *m, struct script_array *t)
{
	t->size = ARY_LEN(RARRAY(t->mrb_array));
	if(t->size == 0){
		return 1;
	}
	mrb_int length = t->size;
	int i = 0;
	while(length){
		uint8_t *dest = (uint8_t *) global_buffer;
		const mrb_int l = length_min(sizeof(global_buffer), length);
		mrb_int ll = l;
		int r = 0;
		while(ll >= 4){
			mrb_int v = mrb_fixnum(mrb_ary_ref(m, t->mrb_array, i++));
			r = v < 0 || v >= 0x100;
			*dest++ = v;
			
			v = mrb_fixnum(mrb_ary_ref(m, t->mrb_array, i++));
			r |= v < 0 || v >= 0x100;
			*dest++ = v;

			v = mrb_fixnum(mrb_ary_ref(m, t->mrb_array, i++));
			r |= v < 0 || v >= 0x100;
			*dest++ = v;

			v = mrb_fixnum(mrb_ary_ref(m, t->mrb_array, i++));
			r |= v < 0 || v >= 0x100;
			*dest++ = v;

			ll -= 4;
		}
		if(r){
			return 0;
		}
		while(ll > 0){
			mrb_int v = mrb_fixnum(mrb_ary_ref(m, t->mrb_array, i++));
			if(v < 0 || v >= 0x100){
				return 0;
			}
			*dest++ = v;
			ll -= 1;
		}
		t->callback(t->parent, (uint8_t *) global_buffer, l);
		length -= l;
	}
	return 1;
}

void uint8_t_into_mruby_array(mrb_state *m, mrb_value mrb_array, const uint8_t *src, int length)
{
	//printf("sizeof(mrb_value):%d\n", sizeof(mrb_value));
	while(length){
		mrb_value *dest = global_buffer;
		const int l = length_min(length, ARRAY_SIZE);
		int ll = l;
		while(ll >= 4){
			*dest = mrb_fixnum_value(*src);
			dest++; src++;
			*dest = mrb_fixnum_value(*src);
			dest++; src++;
			*dest = mrb_fixnum_value(*src);
			dest++; src++;
			*dest = mrb_fixnum_value(*src);
			dest++; src++;
			ll -= 4;
		}
		while(ll > 0){
			*dest = mrb_fixnum_value(*src);
			dest++; src++;
			ll -= 1;
		}
		mrb_ary_concat(m, mrb_array, mrb_ary_new_from_values(m, l, global_buffer));
		length -= l;
	}
}
