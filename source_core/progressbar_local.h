#pragma once
struct progressbar;
struct progressbar_callback;
struct RClass;

//called for mruby methods
struct mrb_state;
void mruby_progressbar_method_define(struct mrb_state *m, struct RClass *mm);

//called by mruby_serialport.c
void progressbar_add(struct mrb_state *m, int length);
