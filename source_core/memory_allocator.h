#pragma once
#include <stdlib.h>

#ifdef MEMORY_DEBUG
#include <stdint.h>
#include <stdio.h>
extern FILE *debug_memory_log;
#define DEBUG_MEMORY_LOG(PREFIX, __R__) if(debug_memory_log) fprintf(debug_memory_log, PREFIX " %x %s:%d %s()\n", (uintptr_t) __R__, __FILE__, __LINE__, __FUNCTION__)
#define Malloc(DEST, _L_)  {\
	void *__r__ = malloc(_L_); \
	DEBUG_MEMORY_LOG("m", __r__); DEST = __r__; \
}
#define DECLARE_ARRAY_MALLOC(_TYPE_, _NAME_, _SIZE_) _TYPE_ *_NAME_; Malloc(_NAME_, sizeof(_TYPE_) * (_SIZE_))
#define Free(DEST) DEBUG_MEMORY_LOG("f", DEST); free(DEST)
#else
#define DEBUG_MEMORY_LOG(PREFIX, __R__)
#define Malloc(DEST, _L_) DEST = malloc(_L_)
#define DECLARE_ARRAY_MALLOC(_TYPE_, _NAME_, _SIZE_) _TYPE_ *const _NAME_ = malloc(sizeof(_TYPE_) * (_SIZE_))
#define Free(DEST) free(DEST)
#endif
#define DECLARE_STRUCT_MALLOC(_TYPE_, _NAME_) DECLARE_ARRAY_MALLOC(_TYPE_, _NAME_, 1)
