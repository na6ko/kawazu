KLASSES = <<EOS
mruby_serialport_s
progressbar
message
textarea list_driver
textarea list_action
textarea list_flashdevice
textarea title_to_driver
textarea list_config
simulator_usb_driver
simulator_cartridge_bus
EOS

M = "struct mrb_state *m"
HEADER_EXTERNAL = "klass_manager.h"
HEADER_LOCAL = "klass_manager_local.h"
SOURCE = "klass_manager.c"
NOTE = <<EOS
/* 
THIS FILE IS AUTO-GENERATED
by klass_manager.rb
*/
EOS

HEADER_START = <<EOS
#{NOTE}
#pragma once
struct mrb_state;
EOS

def header_generate(f, klasses)
	ar = []
	klasses.each{|k|
		if ar.include?(k[:type]) == false
			f.puts "struct #{k[:type]};"
		end
		ar << k[:type]
		attr = ""
		if k[:external]
			attr = "DLLFUNC"
		end
		str = "#{attr} void klass_#{k[:name]}_register(#{M}, struct #{k[:type]} *t);"
		f.puts str.strip
		str = "#{attr} struct #{k[:type]} *klass_#{k[:name]}_ref(#{M});"
		f.puts str.strip
		f.puts ""
	}
end

begin
	klasses =[]
	KLASSES.split("\n").each{|t|
		s = t.split()
		h = {:type => s[0], :name => s[0]}
		if s.size >= 2
			h[:name] = s[1]
		end
		h[:external] = h[:name] =~ /\Asimulator/
		klasses << h
	}
	f = File.open('../include/' + HEADER_EXTERNAL, 'w')
	f.puts HEADER_START
	header_generate(f, klasses.find_all{|t| t[:external]})
	f.close

	f = File.open(HEADER_LOCAL, 'w')
	f.puts <<EOS
#{HEADER_START}
void klass_manager_init(#{M});
void klass_manager_close(#{M});
struct RClass;
int klass_manager_simulator_load(#{M}, struct RClass *mm, const char *filename);
EOS
	header_generate(f, klasses.find_all{|t| !t[:external]})
	f.close
	
	f = File.open(SOURCE, 'w')
	str = <<EOS 
#{NOTE}
#if defined(_HAMO_BUILD_) && (defined(_WIN32) || defined(_WIN64))
#define SIMULATOR_DLL_LOAD
#endif
#include <string.h>
#include <mruby.h>
#include <hamo.h>
#ifdef SIMULATOR_DLL_LOAD
#include <windows.h>
#include "textencoding.h"
#endif
#include "memory_allocator.h"
#include <#{HEADER_EXTERNAL}>
#include "#{HEADER_LOCAL}"
struct manager{
EOS
	klasses.each{|k|
		str << "\tstruct #{k[:type]} *#{k[:name]};\n"
	}
	str << <<EOS
#ifdef SIMULATOR_DLL_LOAD
		HINSTANCE dll;
#endif
EOS
	str << "};"
	f.puts str
	str = <<EOS
void klass_manager_init(#{M})
{
	static struct manager t;
	m->ud = &t;
}
static inline struct manager *extract(#{M})
{
	return m->ud;
}
void klass_manager_close(#{M})
{
	struct manager *t = extract(m);
#ifdef SIMULATOR_DLL_LOAD
	if(t->dll){
		DEBUG_MEMORY_LOG("a", t->dll);
		FreeLibrary(t->dll);
	}
#endif
	memset(t, 0, sizeof(*t));
	m->ud = NULL;
}
#ifdef SIMULATOR_DLL_LOAD
int klass_manager_simulator_load(#{M}, struct RClass *mm, const char *filename)
{
	struct manager *t = extract(m);
	t->dll = my_loadlibrary(filename);
	DEBUG_MEMORY_LOG("A", t->dll);
	if(t->dll == NULL){
		return 0;
	}
	void (*f)(struct mrb_state *m, struct RClass *mm);
	f = (void (*)(struct mrb_state *m, struct RClass *mm))
		GetProcAddress(t->dll, "simulator_method_define");
	(*f)(m, mm);
	return 1;
}
#endif
EOS
	f.puts str
	klasses.each{|k|
		str = <<EOS
void klass_#{k[:name]}_register(#{M}, struct #{k[:type]} *t)
{
	extract(m)->#{k[:name]} = t;
}
struct #{k[:type]} *klass_#{k[:name]}_ref(#{M})
{
	return extract(m)->#{k[:name]};
}
EOS
		f.puts str
	}
	f.close
end

