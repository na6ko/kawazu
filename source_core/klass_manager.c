/* 
THIS FILE IS AUTO-GENERATED
by klass_manager.rb
*/

#if defined(_HAMO_BUILD_) && (defined(_WIN32) || defined(_WIN64))
#define SIMULATOR_DLL_LOAD
#endif
#include <string.h>
#include <mruby.h>
#include <hamo.h>
#ifdef SIMULATOR_DLL_LOAD
#include <windows.h>
#include "textencoding.h"
#endif
#include "memory_allocator.h"
#include <klass_manager.h>
#include "klass_manager_local.h"
struct manager{
	struct mruby_serialport_s *mruby_serialport_s;
	struct progressbar *progressbar;
	struct message *message;
	struct textarea *list_driver;
	struct textarea *list_action;
	struct textarea *list_flashdevice;
	struct textarea *title_to_driver;
	struct textarea *list_config;
	struct simulator_usb_driver *simulator_usb_driver;
	struct simulator_cartridge_bus *simulator_cartridge_bus;
#ifdef SIMULATOR_DLL_LOAD
		HINSTANCE dll;
#endif
};
void klass_manager_init(struct mrb_state *m)
{
	static struct manager t;
	m->ud = &t;
}
static inline struct manager *extract(struct mrb_state *m)
{
	return m->ud;
}
void klass_manager_close(struct mrb_state *m)
{
	struct manager *t = extract(m);
#ifdef SIMULATOR_DLL_LOAD
	if(t->dll){
		DEBUG_MEMORY_LOG("a", t->dll);
		FreeLibrary(t->dll);
	}
#endif
	memset(t, 0, sizeof(*t));
	m->ud = NULL;
}
#ifdef SIMULATOR_DLL_LOAD
int klass_manager_simulator_load(struct mrb_state *m, struct RClass *mm, const char *filename)
{
	struct manager *t = extract(m);
	t->dll = my_loadlibrary(filename);
	DEBUG_MEMORY_LOG("A", t->dll);
	if(t->dll == NULL){
		return 0;
	}
	void (*f)(struct mrb_state *m, struct RClass *mm);
	f = (void (*)(struct mrb_state *m, struct RClass *mm))
		GetProcAddress(t->dll, "simulator_method_define");
	(*f)(m, mm);
	return 1;
}
#endif
void klass_mruby_serialport_s_register(struct mrb_state *m, struct mruby_serialport_s *t)
{
	extract(m)->mruby_serialport_s = t;
}
struct mruby_serialport_s *klass_mruby_serialport_s_ref(struct mrb_state *m)
{
	return extract(m)->mruby_serialport_s;
}
void klass_progressbar_register(struct mrb_state *m, struct progressbar *t)
{
	extract(m)->progressbar = t;
}
struct progressbar *klass_progressbar_ref(struct mrb_state *m)
{
	return extract(m)->progressbar;
}
void klass_message_register(struct mrb_state *m, struct message *t)
{
	extract(m)->message = t;
}
struct message *klass_message_ref(struct mrb_state *m)
{
	return extract(m)->message;
}
void klass_list_driver_register(struct mrb_state *m, struct textarea *t)
{
	extract(m)->list_driver = t;
}
struct textarea *klass_list_driver_ref(struct mrb_state *m)
{
	return extract(m)->list_driver;
}
void klass_list_action_register(struct mrb_state *m, struct textarea *t)
{
	extract(m)->list_action = t;
}
struct textarea *klass_list_action_ref(struct mrb_state *m)
{
	return extract(m)->list_action;
}
void klass_list_flashdevice_register(struct mrb_state *m, struct textarea *t)
{
	extract(m)->list_flashdevice = t;
}
struct textarea *klass_list_flashdevice_ref(struct mrb_state *m)
{
	return extract(m)->list_flashdevice;
}
void klass_title_to_driver_register(struct mrb_state *m, struct textarea *t)
{
	extract(m)->title_to_driver = t;
}
struct textarea *klass_title_to_driver_ref(struct mrb_state *m)
{
	return extract(m)->title_to_driver;
}
void klass_list_config_register(struct mrb_state *m, struct textarea *t)
{
	extract(m)->list_config = t;
}
struct textarea *klass_list_config_ref(struct mrb_state *m)
{
	return extract(m)->list_config;
}
void klass_simulator_usb_driver_register(struct mrb_state *m, struct simulator_usb_driver *t)
{
	extract(m)->simulator_usb_driver = t;
}
struct simulator_usb_driver *klass_simulator_usb_driver_ref(struct mrb_state *m)
{
	return extract(m)->simulator_usb_driver;
}
void klass_simulator_cartridge_bus_register(struct mrb_state *m, struct simulator_cartridge_bus *t)
{
	extract(m)->simulator_cartridge_bus = t;
}
struct simulator_cartridge_bus *klass_simulator_cartridge_bus_ref(struct mrb_state *m)
{
	return extract(m)->simulator_cartridge_bus;
}
