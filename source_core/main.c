#include <stdint.h>
#include <stdio.h>
#include <string.h>
#if defined(_WIN32) || defined(_WIN64)
#include <assert.h>
#include "memory_allocator.h"
#endif
#include <hamo.h>
struct mrb_state;
#include "textencoding.h"
#include "cui_progressbar.h"
#include "cui_message.h"

#ifdef MEMORY_DEBUG
FILE *debug_memory_log;
#endif

#if defined(_WIN32) || defined(_WIN64)
#define LOGPATH
static int utf8_main(const int c, char **const v)
#else
#define LOGPATH "~/.kawazu/"
//TBD: mkdir -p ~/.kawazu
int main(const int c, char **const v)
#endif
{
#ifdef MEMORY_DEBUG
#if defined(_WIN32) || defined(_WIN64)
	{
		const char filename[] = "memory_debug.log";
		int l = program_dir_concat(filename, '/', NULL, 0);
		char fullpath[l];
		program_dir_concat(filename, '/', fullpath, l);
		debug_memory_log = FOPEN(fullpath, "w");
	}
#else
	debug_memory_log = FOPEN(LOGPATH "memory_debug.log", "w");
#endif
#endif
	struct mrb_state *const m = hamo_mrb_open();
	cui_progressbar_init(m);

	{
		const char filename[] = LOGPATH
#ifdef _HAMO_LOAD_
			"kawazu_cui.log";
#else
			"hamo.log";
#endif
#if defined(_WIN32) || defined(_WIN64)
		int l = program_dir_concat(filename, '/', NULL, 0);
		char fullpath[l];
		program_dir_concat(filename, '/', fullpath, l);
#else
		const char *fullpath = filename;
#endif

		cui_textarea_new(m, fullpath, "w");
	}
	int r, search = 1;
	if(c >= 3){
		search = strncmp(v[1], "-s", 3);
	}
	if(search == 0){
		r = cui_title_to_driver(m, c - 2, v + 2);
	}else{
		r = hamo_main(m, c, v);
	}
	cui_textarea_delete(m);
	
	hamo_mrb_close(m);
#ifdef MEMORY_DEBUG
	fclose(debug_memory_log);
#endif
	return r;
}

#if defined(_WIN32) || defined(_WIN64)
int main(void)
{
	return my_wmain(utf8_main);
}
#endif
