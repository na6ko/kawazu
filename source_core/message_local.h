#pragma once
struct mrb_state;
struct RClass;
void messege_backtrace_print(struct mrb_state *m);
void message_printf(struct mrb_state *m, const char *format, ...);
void message_puts(struct mrb_state *m, const char *str);
void log_printf(struct mrb_state *m, const char *format, ...);
void log_puts(struct mrb_state *m, const char *str);
void myassert_printf(struct mrb_state *m, const char *format, ...);
void message_errorlog_save(struct mrb_state *m, const char *note);
void sbp_message_method_define(struct mrb_state *m, struct RClass *mm);
#define DEBUGOUT(_M_) message_printf(_M_, "%s:%d %s()\n", __FILE__, __LINE__, __FUNCTION__)
