#include <assert.h>
#include <stdio.h>
#include <hamo.h>
#include <textarea.h>
#include "message_local.h"
#include "textarea_local.h"

static void title_add(void *t, int32_t c, const struct text_item *items);

int cui_title_to_driver(struct mrb_state *m, int c, char **v)
{
	int r = hamo_title_to_driver_load(m, NULL, title_add);
	if(r != 0){
		return r;
	}
	while(c){
		r = hamo_title_to_driver_call(m, *v);
		c -= 1;
		v += 1;
	}
	hamo_title_to_driver_clear(m);
	return r;
}
static void print(void *t, const char *str)
{
	printf("%s", str);
	fflush(stdout);
}
static void add(void *t, int32_t c, const struct text_item *items, const char *from)
{
	if(0) puts(from);
	for(int i = 0; i < c; i++){
		printf("\"%s\" => \"%s\", ", items->key, items->value);
		items += 1;
	}
	puts("");
	fflush(stdout);
}
static void title_add(void *t, int32_t c, const struct text_item *items){
	add(t, c, items, __FUNCTION__);
}
static void driver_add(void *t, int32_t c, const struct text_item *items)
{
	add(t, c, items, __FUNCTION__);
}
static void action_add(void *t, int32_t c, const struct text_item *items)
{
	add(t, c, items, __FUNCTION__);
}
static void flashdevice_add(void *t, int32_t c, const struct text_item *items)
{
	add(t, c, items, __FUNCTION__);
}
static void config_send(void *t, int32_t c, const struct text_item *items)
{
	add(t, c, items, __FUNCTION__);
}
//GUI の API 確認用のみに使うのでユーザー向けに作ってない
static void *logsender_open(void *tt)
{
	FILE *f = fopen("e:/hoge.txt", "w");
	return f;
}
static void logsender_puts(void *tt, const char *str)
{
	FILE *f = tt;
	fputs(str, f);
}
static void logsender_close(void *tt)
{
	FILE *f = tt;
	fclose(f);
}

void cui_textarea_new(struct mrb_state *m, const char *filename, const char *mode)
{
	const struct errorlog_callback *e = NULL;
	if(0){
		static const struct errorlog_callback errorlog_callback = {
			.open = logsender_open,
			.puts = logsender_puts,
			.close = logsender_close
		};
		e = &errorlog_callback;
	}
	message_new(m, NULL, print, filename, mode, e);
	list_driver_init(m, NULL, driver_add);
	list_action_init(m, NULL, action_add);
	list_flashdevice_init(m, NULL, flashdevice_add);
	list_config_init(m, NULL, config_send);
}
void cui_textarea_delete(struct mrb_state *m)
{
	message_delete(m);
	list_driver_clear(m);
	list_action_clear(m);
	list_flashdevice_clear(m);
	list_config_clear(m);
}

