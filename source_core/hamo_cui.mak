TARGET = ../bin/kawazu_cui.exe
OBJDIR = ../bin/obj_hamo_cui
include base.mk
CFLAGS += -O2  -D_HAMO_LOAD_
CFLAGS += -I../include
OBJ = $(addprefix $(OBJDIR)/, main.o cui_message.o cui_progressbar.o)


.PHONY: all clean
all: $(OBJDIR) $(TARGET)
clean:
	rm -rf $(TARGET) $(OBJDIR)
$(TARGET): $(OBJ) ../bin/hamo.dll
	$(CC) -o $@ $^
	strip $@
include objdir.mk
