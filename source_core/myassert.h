#include <assert.h>
#include <hamo.h>
#include "message_local.h"
#define myassert(_M_, _CONDITION_) if(!(_CONDITION_)){ \
myassert_printf(_M_, "assertion at %s:%d %s()\n", __FILE__, __LINE__, __FUNCTION__);\
} assert(_CONDITION_)
