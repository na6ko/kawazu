#include <assert.h> //myasert は禁止. mruby が動いてないときでも広範囲に利用される.
#include <stdio.h>
#include <string.h>
#include <windows.h>
#include <io.h>
#include <hamo.h>
#include "textencoding.h"

static int widechar_length_get(UINT codepage, const char *src)
{
	const int wl = MultiByteToWideChar(codepage, 0, src, -1, NULL, 0);
	assert(wl);
	return wl;
}

static void widechar_str_set(UINT codepage, const char *src, wchar_t *dest, int dest_length)
{
	assert(dest_length == widechar_length_get(codepage, src));
	const int wwl = MultiByteToWideChar(codepage, 0, src, -1, dest, dest_length);
	assert(wwl == dest_length);
}
static int multibyte_length_get(UINT codepage, const wchar_t *wchar_str, int wl)
{
	return WideCharToMultiByte(codepage, 0, wchar_str, wl, NULL, 0, NULL, 0);
}

static int wchar_to_utf8_length_get(const wchar_t *src)
{
	return multibyte_length_get(CP_UTF8, src, wcslen(src) + 1);
}
static void wchar_to_utf8_set(const wchar_t *src, char *dest, int dest_length)
{
	const int eel = WideCharToMultiByte(CP_UTF8, 0, src, wcslen(src) + 1, dest, dest_length, NULL, 0); 
	assert(dest_length == eel);
}

//it uses C99 Variable Length Array
#define DECLARE_WCHAR_STRING(_CODEPAGE_, _TEMP_, _NAME_, _SRC_) \
  _TEMP_ = widechar_length_get(_CODEPAGE_, _SRC_);\
  wchar_t _NAME_[_TEMP_];\
  widechar_str_set(_CODEPAGE_, _SRC_, _NAME_, _TEMP_)

#if 0
static int defaultcodepage_to_utf8_length_get(const char *src)
{
	int wl;
	DECLARE_WCHAR_STRING(CP_ACP, wl, wchar_str, src);
	return multibyte_length_get(CP_UTF8, wchar_str, wl);
}
static void defaultcodepage_to_utf8_set(const char *src, char *dest, int dest_length)
{
	//ACP -> wide character
	int wl;
	DECLARE_WCHAR_STRING(CP_ACP, wl, wchar_str, src);
	
	//wide character -> UTF-8
	const int eel = WideCharToMultiByte(CP_UTF8, 0, wchar_str, wl, dest, dest_length, NULL, 0); 
	assert(dest_length == eel);
}
#endif

FILE *my_fopen(const char *src, const char *mode)
{
	int length;
	DECLARE_WCHAR_STRING(CP_UTF8, length, wchar_str, src);
	DECLARE_WCHAR_STRING(CP_UTF8, length, wchar_mode, mode);
	return _wfopen(wchar_str, wchar_mode);
}

int my_io_open(const char *src, int pflag, int pmode)
{
	int length;
	DECLARE_WCHAR_STRING(CP_UTF8, length, wchar_str, src);
	return _wopen(wchar_str, pflag, pmode);
}
void my_mkdir(const char *src)
{
	int length;
	DECLARE_WCHAR_STRING(CP_UTF8, length, wchar_str, src);
	BOOL r = CreateDirectoryW(wchar_str, NULL);
	assert(r != 0);
}
void my_unlink(const char *src)
{
	int length;
	DECLARE_WCHAR_STRING(CP_UTF8, length, wchar_str, src);
	BOOL r = DeleteFileW(wchar_str);
	assert(r != 0);
}
int my_realpath(const char *src, char *dest, int dest_length)
{
	int length;
	DECLARE_WCHAR_STRING(CP_UTF8, length, wchar_str, src);
	length = GetFullPathNameW(wchar_str, 0, NULL, NULL);
	if(length == 0){
		return 0;
	}
	wchar_t fullpath[length];
	GetFullPathNameW(wchar_str, length, fullpath, NULL);
	if(dest_length == 0){
		return wchar_to_utf8_length_get(fullpath);
	}
	
	wchar_to_utf8_set(fullpath, dest, dest_length);
	return dest_length;
}

void file_find(void *klass, void (*callback)(void *k, const char *result), const char *word)
{
	WIN32_FIND_DATAW t;
	HANDLE h;
	{
		int length;
		DECLARE_WCHAR_STRING(CP_UTF8, length, wchar_str, word);
		h = FindFirstFileW(wchar_str, &t);
	}
	if(h == INVALID_HANDLE_VALUE){
		return;
	}
	do{
		DWORD attr = GetFileAttributesW(t.cFileName);
		if((attr & FILE_ATTRIBUTE_NORMAL) == 0){
			continue;
		}
		int length = multibyte_length_get(
			CP_UTF8, t.cFileName, 
			sizeof(t.cFileName) / sizeof(t.cFileName[0])
		);
		char result[length]; //C99 VLA
		WideCharToMultiByte(CP_UTF8, 0, t.cFileName, MAX_PATH, result, length, NULL, 0); 
		(*callback)(klass, result);
	}while(FindNextFileW(h, &t));
	assert(GetLastError() == ERROR_NO_MORE_FILES);
	FindClose(h);
}
#if defined(_HAMO_BUILD_)
struct HINSTANCE__ *my_loadlibrary(const char *file)
{
	int l = program_dir_concat(file, '\\', NULL, 0);
	char fullpath[l];
	program_dir_concat(file, '\\', fullpath, l);
	l = widechar_length_get(CP_UTF8, fullpath);
	wchar_t wc[l];
	widechar_str_set(CP_UTF8, fullpath, wc, l);
	return LoadLibraryW(wc);
}
#endif

int my_wmain(int (*callback)(int c, char **v))
{
	int c;
	wchar_t **const wv = CommandLineToArgvW(GetCommandLineW(), &c);
	assert(wv);
	int length = 0, l[c];
	for(int i = 0; i < c; i++){
		l[i] = wchar_to_utf8_length_get(wv[i]);
		length += l[i];
	}
	assert(length < 0x8000);
	char *v[c], utf8_buffer[length]; //C99 VLA
	{
		char *dest = utf8_buffer;
		for(int i = 0; i < c; i++){
			wchar_to_utf8_set(wv[i], dest, l[i]);
			v[i] = dest;
			dest += l[i];
		}
	}
	LocalFree(wv);
	return (*callback)(c, v);
}

static int 
s_program_dir_concat_length_get(const char dir, const char *suffix)
{
	return program_dir_get(NULL, dir, 0) + 1 + strnlen(suffix, MAX_PATH);
}

int program_dir_concat(const char *suffix, const char dir, char *dest, int dest_length)
{
	if(dest_length == 0){
		return s_program_dir_concat_length_get(dir, suffix);
	}
	int l = dest_length - strnlen(suffix, MAX_PATH) - 1;
	char temp[l]; //C99 VLA
	program_dir_get(temp, dir, l);
	return snprintf(dest, dest_length, "%s%c%s", temp, dir, suffix) + 1;
}
struct program_dir{
	char utf8_str[MAX_PATH*2 + MAX_PATH/2];
	int wchar_length, utf8_length;
};

static int 
program_dir_length_get(struct program_dir *t)
{
	if(t->wchar_length){
		assert(t->utf8_length);
		return t->utf8_length;
	}
	wchar_t wchar_str[MAX_PATH];
	GetModuleFileNameW(NULL, wchar_str, MAX_PATH);
	wchar_t *s = wchar_str;
	wchar_t *lastpath = NULL;
	//"c:\some\where\path\hoge.exe" -> "c:\some\where\path"
	while(*s != L'\0'){
		if(*s == L'\\'){
			lastpath = s;
		}
		s++;
	}
	assert(lastpath);
	*lastpath = L'\0'; //replace last '\\' -> '\0'
	t->wchar_length = wcslen(wchar_str) + 1;
	t->utf8_length = multibyte_length_get(CP_UTF8, wchar_str, t->wchar_length);
/*
MAX_PATH が wchar_t の配列の数という定義という前提. (MSDN の説明は wchar_t なのか ACP かよくわからない)
UTF-8 で非 ASCII が多ければ char の配列の数は MAX_PATH より増える. よって暫定的に MAX_PATH * 2.5 を設定している.
UTF-8 で非 ASCII が 2 bytes または 3 bytes になることも想定できるため、この assert が頻繁にでる場合は配列の量を動的確保にしたほうがいいかもしれない.
注) 動的確保の場合、開放のタイミングが現在の構造ではしづらい.
*/
	assert(t->utf8_length);
	assert(t->utf8_length <= sizeof(t->utf8_str));
	wchar_to_utf8_set(wchar_str, t->utf8_str, t->utf8_length);
	return t->utf8_length;
}

int program_dir_get(char *dest, const char dir, const int length)
{
	static struct program_dir program_dir;

	if(length == 0){
		return program_dir_length_get(&program_dir);
	}
	assert(program_dir.wchar_length);
	memcpy(dest, program_dir.utf8_str, program_dir.utf8_length);
	if(dir == '\\'){
		return program_dir.utf8_length;
	}
	char *s = dest;
	while(*s != '\0'){
		if(*s == '\\'){
			*s = dir;
		}
		s++;
	}
	return program_dir.utf8_length;
}

