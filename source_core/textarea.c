#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <hamo.h>
#include "klass_manager_local.h"
#include "textarea_local.h"
#include "textencoding.h"

struct textarea{
	void *klass;
	hamo_callback_item_add item_add;
};
static void textarea_init(struct textarea *t, void *klass, hamo_callback_item_add callback)
{
	t->klass = klass;
	t->item_add = callback;
}
static void textarea_clear(struct textarea *d)
{
	memset(d, 0, sizeof(d[0]));
}
/*
list.driver
*/
void list_driver_init(struct mrb_state *m, void *klass, hamo_callback_item_add callback)
{
	static struct textarea t;
	klass_list_driver_register(m, &t);
	textarea_init(&t, klass, callback);
}

void list_driver_clear(struct mrb_state *m)
{
	textarea_clear(klass_list_driver_ref(m));
}
/*
list.action
*/
void list_action_init(struct mrb_state *m, void *klass, hamo_callback_item_add callback)
{
	static struct textarea t;
	klass_list_action_register(m, &t);
	textarea_init(&t, klass, callback);
}

void list_action_clear(struct mrb_state *m)
{
	textarea_clear(klass_list_action_ref(m));
}
/*
list.flashdevice
*/
void list_flashdevice_init(struct mrb_state *m, void *klass, hamo_callback_item_add callback)
{
	static struct textarea t;
	klass_list_flashdevice_register(m, &t);
	textarea_init(&t, klass, callback);
}

void list_flashdevice_clear(struct mrb_state *m)
{
	textarea_clear(klass_list_flashdevice_ref(m));
}
/*
list.config
*/
void list_config_init(struct mrb_state *m, void *klass, hamo_callback_item_add config_send)
{
	static struct textarea t;
	klass_list_config_register(m, &t);
	textarea_init(&t, klass, config_send);
}
void list_config_clear(struct mrb_state *m)
{
	textarea_clear(klass_list_config_ref(m));
}
/*
title_to_driver
*/
void title_to_driver_new(struct mrb_state *m, void *klass, hamo_callback_item_add callback)
{
	static struct textarea t;
	klass_title_to_driver_register(m, &t);
	textarea_init(&t, klass, callback);
}
void title_to_driver_delete(struct mrb_state *m)
{
	textarea_clear(klass_title_to_driver_ref(m));
}
/*
dbinfo.action
*/

#include <mruby.h>
#include <mruby/array.h>
#include <mruby/hash.h>
#include <mruby/string.h>
#include "mruby_driver.h"

/* lists */
static mrb_value list_item_add(struct mrb_state *m, mrb_value self, struct textarea *d)
{
	mrb_value h;
	mrb_get_args(m, "H", &h);
	const mrb_int size = mrb_hash_size(m, h);
	struct text_item items[size]; //C99 Variable Length Array
	mrb_value keys = mrb_hash_keys(m, h);
	for(int i = 0; i < size; i++){
		mrb_value s = mrb_ary_ref(m, keys, i);
		items[i].key = RSTRING_PTR(s);
		items[i].value = RSTRING_PTR(mrb_hash_get(m, h, s));
	}
	d->item_add(d->klass, size, items);
	return mrb_nil_value();
}
static mrb_value m_driverlist_item_add(struct mrb_state *m, mrb_value self)
{
	return list_item_add(m, self, klass_list_driver_ref(m));
}
static mrb_value m_actionlist_item_add(struct mrb_state *m, mrb_value self)
{
	return list_item_add(m, self, klass_list_action_ref(m));
}
static mrb_value m_flashdevicelist_item_select(struct mrb_state *m, mrb_value self)
{
	return list_item_add(m, self, klass_list_flashdevice_ref(m));
}

static mrb_value m_config_send(struct mrb_state *m, mrb_value self)
{
	return list_item_add(m, self, klass_list_config_ref(m));
}

static const struct script_function_list sbp_list[] = {
#define M(NAME, ARGC) {#NAME, m_##NAME, MRB_ARGS_REQ(ARGC)}
	M(driverlist_item_add, 1),
	M(actionlist_item_add, 1),
	M(flashdevicelist_item_select, 1),
	M(config_send, 1),
	{NULL, NULL, 0}
#undef M
};
void sbp_textarea_method_define(struct mrb_state *m, struct RClass *mm)
{
	hamo_module_define(m, mm, "Textarea", sbp_list);
}

static mrb_value m_title_to_driver_item_add(struct mrb_state *m, mrb_value self)
{
	struct textarea *const d = klass_title_to_driver_ref(m);
	mrb_value h;
	mrb_get_args(m, "H", &h);
	const mrb_int size = mrb_hash_size(m, h);
	struct text_item items[size]; //C99 VLA
	mrb_value keys = mrb_hash_keys(m, h);
	for(int i = 0; i < size; i++){
		static const char driver[] = "driver";
		mrb_value s = mrb_ary_ref(m, keys, i);
		items[i].key = RSTRING_PTR(s);
		if(items[i].key[0] == 'd'){
			items[i].key = driver;
		}
		items[i].value = RSTRING_PTR(mrb_hash_get(m, h, s));
	}
	d->item_add(d->klass, size, items);
	return mrb_nil_value();
}

static const struct script_function_list ttd_list[] = {
#define M(NAME, ARGC) {#NAME, m_##NAME, MRB_ARGS_REQ(ARGC)}
	M(title_to_driver_item_add, 1),
	{NULL, NULL, 0}
#undef M
};
void title_to_driver_textarea_method_define(struct mrb_state *m, struct RClass *mm)
{
	hamo_module_define(m, mm, "Textarea", ttd_list);
}
