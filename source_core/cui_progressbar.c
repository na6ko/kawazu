#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <hamo.h>
#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#endif
#include "progressbar_local.h"

#define LABEL_LENGTH (20)
struct cui_progressbar{
	int enable;
	struct bar{
		char name[LABEL_LENGTH];
		int length, offset;
	}bar[2];
#if defined(_WIN32) || defined(_WIN64)
	HANDLE std;
	DWORD console_mode;
#endif
};
static struct bar *bar_get(struct cui_progressbar *t, int index)
{
	assert(index < 2);
	struct bar *b = t->bar + index;
	return b;
}
static void cui_range_set(void *tt, int32_t index, const char *name, int32_t length)
{
	struct bar *b = bar_get(tt, index);
	strncpy(b->name, name, LABEL_LENGTH);
	b->name[LABEL_LENGTH - 1] = '\0';
	b->length = length;
	b->offset = 0;
}
static int percent(struct bar *b)
{
	int r = 100 * b->offset;
	assert(b->length);
	return r / b->length;
}
static void draw(struct cui_progressbar *t)
{
	if(t->enable == 0){
		return;
	}
	if(t->bar[1].length == 0){
		printf("\r%s %3d%%", 
			t->bar[0].name, 
			percent(&t->bar[0])
		);
		if(t->bar[1].name[0] != '\0'){
			printf(";%s", t->bar[1].name);
		}
	}else{
		printf("\r%s %3d%%;%s %3d%%",
			t->bar[0].name, 
			percent(&t->bar[0]), 
			t->bar[1].name,
			percent(&t->bar[1])
		);
	}
	fflush(stdout);
}
static void cui_add(void *tt, int32_t index, int32_t length)
{
	struct cui_progressbar *t = tt;
	if(t->bar[0].length == 0 && t->bar[1].length == 0){
		return;
	}
	struct bar *b = bar_get(t, index);
	b->offset += length;
	assert(b->offset <= b->length);
	draw(t);
}
static void cui_ready(void *tt)
{
	struct cui_progressbar *t = tt;
	t->enable = 1;
#if defined(_WIN32) || defined(_WIN64)
	if(t->std){
		BOOL r = SetConsoleMode(t->std, t->console_mode | ENABLE_VIRTUAL_TERMINAL_PROCESSING);
		assert(r);
	}
#endif
	printf("\e[?25l");
	draw(t);
}
static void cui_done(void *tt)
{
	struct cui_progressbar *t = tt;
	if(t->enable == 0){
		return;
	}
	t->enable = 0;
	int r = 1 + strlen(t->bar[0].name);
	r += 1 + 3 + 1;
	r += strlen(t->bar[1].name);
	if(t->bar[1].length != 0){
		r += 1 + 3 + 1;
	}
	char str[r + 1 + 1];
	memset(str, ' ', r + 1 + 1);
	str[0] = '\r';
	str[r] = '\r';
	str[r + 1] = '\0';
	printf(str);
	fflush(stdout);
	printf("\e[?25h");
	fflush(stdout);
	memset(t->bar, 0, sizeof(t->bar));
#if defined(_WIN32) || defined(_WIN64)
	if(t->std){
		SetConsoleMode(t->std, t->console_mode);
	}
#endif
}
static void label_update(void *tt, int32_t index, const char *name)
{
	struct bar *b = bar_get(tt, index);
	strncpy(b->name, name, sizeof(b->name) - 1);
}
static const struct progressbar_callback cui_progress_callback = {
	.range_set = cui_range_set, .add = cui_add, 
	.ready = cui_ready, .done = cui_done,
	.label_update = label_update
};

void cui_progressbar_init(struct mrb_state *m)
{
	static struct cui_progressbar tt;
	memset(&tt, 0, sizeof(tt));
	struct cui_progressbar *const t = &tt;
	progressbar_init(m, t, &cui_progress_callback);
#if defined(_WIN32) || defined(_WIN64)
	t->std = GetStdHandle(STD_OUTPUT_HANDLE);
	BOOL r = GetConsoleMode(t->std, &t->console_mode); //bash returns false, cmd.exe returns true
	if(r == FALSE){
		t->std = NULL;
	}
#endif
}
