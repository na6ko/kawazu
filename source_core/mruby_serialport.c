#include "myassert.h"
#include <mruby.h>
#include <mruby/string.h>
#include <mruby/array.h>
#include <mruby/error.h>
#include <hamo.h>
#include "klass_manager_local.h"
#include "textarea_local.h"
#include "progressbar_local.h"
#include "serialport.h"
#include "mruby_loader.h"
#include "mruby_driver.h"

struct mruby_serialport_s{
	mrb_state *m;
	mrb_value self, callback_value;
	struct serialport *serialport;
};
#define DECLARE_DEVICE(NAME) struct mruby_serialport_s *const NAME = klass_mruby_serialport_s_ref(m)

static void api_error(void *s, uint32_t errorcode, const char *str)
{
	struct mruby_serialport_s *const t = s;
	message_printf(t->m, "%s code: %u, %s \n", __FUNCTION__, errorcode, str);
	mrb_f_raise(t->m, t->self);
}
static const struct serialport_callbacks callbacks;

static mrb_value m_serialport_open(mrb_state *m, mrb_value self)
{
	static struct mruby_serialport_s klass;
	struct mruby_serialport_s *const d = &klass;
	mrb_value portname;
	mrb_get_args(m, "S", &portname);
	d->serialport = serialport_create(m, RSTRING_PTR(portname), 3000000, SERIAL_PARITY_NONE, 8);
	if(d->serialport == NULL){
		return mrb_false_value();
	}
	klass_mruby_serialport_s_register(m, d);
	serialport_callback_set(d->serialport, d, &callbacks);
	return mrb_true_value();
}
static mrb_value m_serialport_close(mrb_state *m, mrb_value self)
{
	DECLARE_DEVICE(d);
	if(d->serialport == NULL){
		return mrb_nil_value();
	}
	klass_mruby_serialport_s_register(m, NULL);
	serialport_destroy(d->serialport);
	d->serialport = NULL;
	return mrb_nil_value();
}
static void send_array_convert_callback(void *parent, const uint8_t *data, mrb_int length)
{
	serialport_send(parent, data, length);
}
static mrb_value m_serialport_send(mrb_state *m, mrb_value self)
{
	DECLARE_DEVICE(d);
	myassert(m, d->serialport);
	d->m = m;
	struct script_array s = {.parent = d->serialport, .callback = send_array_convert_callback};
	mrb_get_args(m, "A", &s.mrb_array);
	if(mruby_array_into_uint8_t(m, &s) == 0){
		message_printf(m, "%s: range_error\n", __FUNCTION__);
		mrb_f_raise(m, self);
	}
	return mrb_nil_value();
}
static void progressbar_add_callback(void *tt, int length)
{
	struct mruby_serialport_s *t = tt;
	assert(t->m);
	progressbar_add(t->m, length);
}
static void receive_callback(void *tt, const uint8_t *src, int length)
{
	struct mruby_serialport_s *t = tt;
	uint8_t_into_mruby_array(t->m, t->callback_value, src, length);
}
static mrb_value m_serialport_receive(mrb_state *m, mrb_value self)
{
	DECLARE_DEVICE(d);
	myassert(m, d->serialport);
	mrb_int length;
	mrb_get_args(m, "i", &length);
	d->m = m;
	d->self = self;
	d->callback_value = mrb_ary_new(m);
	length = serialport_receive(d->serialport, length);
	return d->callback_value;
}
static mrb_value m_serialport_rx_purge(mrb_state *m, mrb_value self)
{
	DECLARE_DEVICE(d);
	myassert(m, d->serialport);
	serialport_rx_purge(d->serialport);
	return mrb_nil_value();
}
static mrb_value m_serialport_receive_timeout_set(mrb_state *m, mrb_value self)
{
	DECLARE_DEVICE(d);
	myassert(m, d->serialport);
	mrb_int ms;
	mrb_get_args(m, "i", &ms);
	serialport_receive_timeout_set(d->serialport, ms);
	return mrb_nil_value();
}

static void usbdevice_find_callback(void *tt, const char *str)
{
	struct mruby_serialport_s *t = tt;
	mrb_ary_push(t->m, t->callback_value, mrb_str_new_cstr(t->m, str));
}

static mrb_value m_usbdevice_find(mrb_state *m, mrb_value self)
{
	struct mruby_serialport_s d;
	mrb_int vendor_id, product_id;
	d.m = m;
	d.self = self;
	d.callback_value = mrb_ary_new(m);
	mrb_get_args(m, "ii", &vendor_id, &product_id);
	usbdevice_find(&d, &callbacks, vendor_id, product_id);
	return d.callback_value;
}
static const struct script_function_list list[] = {
	{"usbdevice_find", m_usbdevice_find, MRB_ARGS_REQ(3)},
#define M(NAME, ARGC) {#NAME, m_serialport_##NAME, MRB_ARGS_REQ(ARGC)}
	M(open, 1), M(close, 0),
	M(send, 1), 
	M(receive, 1),
	M(rx_purge, 0),
	M(receive_timeout_set, 1),
#undef M
	{NULL, NULL, 0}
};

void mruby_serialport_method_define(mrb_state *m, struct RClass *mm)
{
	hamo_class_define(m, mm, "Usbcdc", list);
}

static const struct serialport_callbacks callbacks = {
	api_error, usbdevice_find_callback, receive_callback,
	progressbar_add_callback
};
