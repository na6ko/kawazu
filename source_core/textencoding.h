#pragma once
#if defined(_WIN32) || defined(_WIN64)
/*
Windows でのファイル名をすべて wchar_t で処理するための関数群.

[my_wmain()]
引数の文字列は UTF-8 になり (*callback)() で本来の main() を実行する.
(*callback)() での処理はすべてのファイル名は文字コードを UTF-8 を前提とする. textencoding_windows.c で wchar_t にして Windows API に渡して処理する.

[文字列領域の確保について]
可変長のため、処理前に dest = NULL, length = 0 で処理後の必要なデータ量を取得後、文字列領域を確保し、目的の文字列を取得する. 文字列の確保方法は free() 呼び出し忘れを防ぐため、 C99 の Variable Length Array の利用を推奨.

この手順で文字列を処理する global 関数は下記のとおりである.
my_realpath()
program_dir_get()
program_dir_concat()
*/
FILE *my_fopen(const char *utf8, const char *mode);
int my_io_open(const char *utf8, int pflag, int pmode);
void my_mkdir(const char *utf8);
void my_unlink(const char *utf8);
int my_realpath(const char *utf8, char *dest, int dest_length);
struct HINSTANCE__;
struct HINSTANCE__ *my_loadlibrary(const char *file);
void file_find(void *klass, void (*callback)(void *k, const char *result), const char *utf8_word);
int program_dir_get(char *dest, const char dir, const int length);

#define FOPEN(_filename_, _mode_) my_fopen(_filename_, _mode_)
#else
#define FOPEN(_filename_, _mode_) fopen(_filename_, _mode_)
#endif
