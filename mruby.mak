.PHONY: diff patch lib rebuild_lib
ROOT = mruby-3.1.0
OBJ_TEXTENCODING = bin/obj_textencoding/textencoding.o
ARCHIVE_MRUBY = $(ROOT)/build/host/lib/libmruby.a

lib: $(ROOT)/README.md $(ROOT)/build/host/lib/libmruby.a $(ROOT)/README.md bin/mruby.dll
diff: mruby.patch
patch: $(ROOT)/README.md
rebuild_lib:
	(cd $(ROOT); rake -v MRUBY_CONFIG=hamo)
	make -f mruby.mak lib
mruby.patch:
	diff -u -r $(ROOT)_org $(ROOT) > $@
$(ARCHIVE_MRUBY): $(ROOT)/build_config/hamo.rb
	(cd $(ROOT); rake -v MRUBY_CONFIG=hamo)
$(ROOT)/README.md:
	wget -q https://github.com/mruby/mruby/archive/3.1.0.tar.gz
	mv 3.1.0.tar.gz $(ROOT).tar.gz
	tar xf $(ROOT).tar.gz
	patch -u -p0 < mruby.patch
$(OBJ_TEXTENCODING): source_core/textencoding_windows.c
	mkdir -p bin/obj_textencoding
	gcc -o $@ -Werror -Wall -Iinclude -Isource_core -DDLLFUNC="" -O2 -c $<
bin/mruby.dll: $(ARCHIVE_MRUBY) $(OBJ_TEXTENCODING)
	$(CC) -static -shared -o $@ -Wl,--whole-archive $(ARCHIVE_MRUBY) -Wl,--no-whole-archive $(OBJ_TEXTENCODING) -lws2_32
	strip $@
