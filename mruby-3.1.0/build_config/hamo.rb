MRuby::Build.new do |conf|
	conf.cc.defines = %w(MRB_BUILD_AS_DLL MRB_CORE MRB_UTF8_STRING)
	if ENV['OS'] != 'Windows_NT' then
		conf.cc.flags << %w|-fPIC| # needed for using bundled gems
	end
	conf.cc.include_paths << "../source_core"
	# load specific toolchain settings
	conf.toolchain 

	# include the GEM box
	conf.gembox "stdlib"
	conf.gembox "stdlib-ext"
	conf.gembox "stdlib-io"
	#conf.gembox "math"
	conf.gem :core => "mruby-math"
	conf.gembox "metaprog"

	conf.gem :github => "mattn/mruby-require"
	#conf.gem "#{root}/mrbgems/mruby-bin-mruby"
	#conf.gem "#{root}/mrbgems/mruby-bin-mirb" 

	# Add configuration
	conf.enable_bintest
	conf.enable_test
end
