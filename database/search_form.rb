require "sqlite3"
require "./common.rb"

SLOT_TO_DRIVER = {
	"axrom" => "DriverAMROM",
	"bandai_pt554" => "DriverCNROM",
	"bnrom" => "DriverBNROM",
	"cnrom" => "DriverCNROM",
	"datach_rom" => "DriverLZ93D50Standrd",
	"discrete_74x139" => "Driver74161JF05",
	"discrete_74x161" => "Driver74161WARA",
	"fcg" => "DriverFCG2",
	"fjump2" => "DriverLZ93D50FJUMP2",
	"fxrom" => "DriverPxROM",
	"g101" => "DriverG101",
	"gxrom" => "DriverGNROM",
	"h3001" => "DriverH3001",
	"holydivr" => "DriverJF16IF12",
	"hvc_basic" => "DriverCNROM",
	"jf11" => "Driver74161JF11",
	"jf16" => "Driver74161JF16IF12",
	"jf17" => "Driver74161JF17",
	"jf19" => "Driver74161JF19",
	"jf23" => "DriverSS88006",
	"jf24" => "DriverSS88006",
	"jf29" => "DriverSS88006",
	"jf33" => "DriverSS88006",
	"lz93d50" => "DriverLZ93D50Standrd",
	"lz93d50_ep1" => "DriverLZ93D50Standrd",
	"lz93d50_ep2" => "DriverLZ93D50Standrd",
	"namcot_163" => "DriverNAMCO163",
	"namcot_175" => "DriverNAMCO163",
	"namcot_340" => "DriverNAMCO163",
	"namcot_3425" => "DriverDRROMNTCONROL",
	"namcot_3446" => "DriverDRROMDDS1",
	"nrom" => "DriverCNROM",
	"oekakids" => "Driver74161OEKAKIDS",
	"pxrom" => "DriverPxROM",
	"ss88006" => "DriverSS88006",
	"sunsoft1" => "DriverSUNSOFT1",
	"sunsoft2" => "DriverSUNSOFT2",
	"sunsoft3" => "DriverSUNSOFT3",
	"sunsoft4" => "DriverSUNSOFT4",
	"sunsoft_dcs" => "DriverSUNSOFT4",
	"sunsoft5a" => "DriverSUNSOFT5",
	"sunsoft5b" => "DriverSUNSOFT5",
	"sunsoft_fme7" => "DriverSUNSOFT5",
	"sorom" => "DriverSOROM",
	"szrom" => "DriverSZROM",
	"tam_s1" => "DriverTAMS1",
	"tc190fmc" => "DriverTC0190",
	"txrom" => "DriverTKROM",
	"txsrom" => "DriverTKROM",
	"un1rom" => "DriverUN1ROM",
	"unrom_cc" => "DriverUNROM7408",
	"uxrom" => "DriverUNROM",
	"vrc1" => "DriverVRC1",
	"vrc2" => "DriverVRC2",
	"vrc3" => "DriverVRC3",
	"vrc4" => "DriverVRC4",
	"vrc6" => "DriverVRC6",
	"vrc7" => "DriverVRC7",
	"x1_005" => "DriverX1_005",
	"x1_017" => "DriverX1_017"
}
=begin
#slot だけでは特定不可
exrom
namcot3433
sxrom

#対応予定, 未対応
hkrom karastudio tc0190p tc350fmr
=end

begin
	filename = ARGV.shift
	test = !filename

	db = MYDB.new
	select = 'media'
	join = db.idstring_join(select, "OUTER LEFT", "feature_slot")
	join << db.idstring_join(select, "OUTER LEFT", "feature_pcb")
	join << db.idstring_join(select, "INNER", "title")
	join << db.idstring_join(select, "OUTER LEFT", "revision")
	if test
		join << db.idstring_join(select, "OUTER LEFT", "publisher")
	end
	join << db.idstring_join(select, "OUTER LEFT", "info_alt_title")
	select = select.strip.gsub(" ", ",")
	sql = <<EOS
SELECT DISTINCT #{select}
FROM software_horizontal
#{join}
WHERE info_serial is not null AND idstring_revision.name not like 'China%'
EOS
	if test
		sql << "ORDER BY media, idstring_feature_slot.name, idstring_feature_pcb.name"
		puts sql
		exit
	end
	list_title = {}
	list_driver = {}
	db.execute(sql + ';'){|r|
		disk = db.id_media("disk card") == r.shift
		slot = r.shift
		pcb = r.shift
		title = r.shift
		revision = r.shift
		alttitle = r.shift
		driver_name = nil
		if disk
			driver_name = "DriverDiskSystem"
		elsif SLOT_TO_DRIVER.key? slot
			driver_name = SLOT_TO_DRIVER[slot]
		elsif slot == 'sxrom' && pcb
			driver_name = "DriverSKROM"
			%w(SUROM SXROM).each{|t|
				if pcb.include? t
					driver_name = "Driver#{t}"
					break
				end
			}
		elsif slot == 'exrom'
			driver_name = "DriverEKROM"
			if pcb.include? "EWROM"
				driver_name = "DriverEWROM"
			end
		elsif slot == 'namcot_3433'
			driver_name = "DriverDRROM"
			if pcb.include?('3433') or pcb.include?('3453')
				driver_name = 'DriverDRROMCHRROMA16'
			end
		end
		if !driver_name
			next
		end
		list_driver[driver_name] = nil
		#title.upcase!
		if list_title.key?(title) == false
			list_title[title] = []
		end
		list_title[title] << driver_name
		if !alttitle
			next
		end
		#alttitle.upcase!
		if list_title.key?(alttitle) == false
			list_title[alttitle] = []
		end
		list_title[alttitle] << driver_name
	}
	f = File.open(filename, 'w')
	f.puts '=begin'
	list_title.each{|k,v|
		v.uniq!
	}
	list_title.find_all{|k,v|
		(v.size >= 3 && v.include?("DriverDiskSystem")) ||
		(v.size >= 2 && !v.include?("DriverDiskSystem"))
	}.sort.each{|k,v|
		f.puts k
		f.puts "\t#{v}"
	}
	f.puts '=end'
	#normalize driver name
	i = 0
	list_driver.each{|k,v|
		list_driver[k] = i
		i += 1
	}
	list_title.each{|k,v|
		v.uniq!
		v.map!{|t|
			list_driver[t]
		}
	}
	f.puts 'module SearchForm'
	f.puts 'LIST_DRIVER = '
	f.puts list_driver.invert.to_s
	f.puts 'LIST_TITLE = '
	f.puts list_title.to_s
	f.puts "end"
end
