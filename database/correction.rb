require "sqlite3"
require "./common.rb"

def software_data_correction(db, column, s, d)
	s = s.gsub('"', '\"')
	d = d.gsub('"', '\"')
sql = <<EOS
UPDATE software_data
SET data = (
	SELECT new.id
	FROM idstring_#{column} as new
	WHERE name = "#{d}"
)
WHERE property = (
	SELECT software_property.id
	FROM software_property
	WHERE name = "#{column}"
) AND data = (
	SELECT old.id
	FROM idstring_#{column} as old
	WHERE name = "#{s}"
);
EOS
	db.execute(sql)
end

def string_correction(db, column, list)
	list.each{|t|
		s = t[0].gsub('"', '\"')
		d = t[1].gsub('"', '\"')
sql = <<EOS
SELECT count(name)
FROM idstring_#{column}
WHERE name = "#{d}" OR name = "#{s}";
EOS
		r = db.execute(sql)[0][0]
		case r
		when 0
			puts "#{r}: #{t[1]}"
			next
		when 1
			#no nothing
		when 2
			puts "#{r}: #{t[1]}"
			software_data_correction(db, column, t[0], t[1])
			next
		else
			xxx
		end
			
sql = <<EOS
UPDATE idstring_#{column}
SET name = "#{d}"
WHERE name = "#{s}";
EOS
		db.execute(sql)
	}
end

def str_find(db, table, name)
sql = <<EOS
SELECT id
FROM #{table}
WHERE name = '#{name}';
EOS
	id = db.execute(sql)[0][0]
end
def alt_title_add(db, title, alt)
sql = <<EOS
INSERT INTO idstring_info_alt_title
(name) VALUES('#{alt}')
ON CONFLICT (name) DO NOTHING;
EOS
	db.execute(sql);
sql = <<EOS
SELECT software_horizontal.id
FROM software_horizontal
INNER JOIN idstring_title
ON software_horizontal.title = idstring_title.id
WHERE idstring_title.name = '#{title}';
EOS
	id = db.execute(sql)[0][0]
	alt = str_find(db, 'idstring_info_alt_title', alt)
sql = <<EOS
INSERT INTO software_data
(id, property, data)
VALUES (#{id}, #{db.property("info_alt_title")}, #{alt});
EOS
	db.execute(sql)
	db.pivot
end
=begin
Mother -> マザー
Wild Gunman -> ワイルドガンマン (revison = World に日本語表記あり)
SWAT - Special Weapons and Tactics -> ???
SELECT software_horizontal.id, name, info_alt_title
FROM software_horizontal
INNER JOIN idstring_title
ON idstring_title.id = title
WHERE name = 'Mother' OR name = 'Wild Gunman';
=end
=begin
T: 純粋な間違い
P: typo または日本語の知識がないまま入力している
N: 國と国の意味は同じだが、光栄は三國志で、ナムコは三国志になっている. これに関係する4作品は中身が異なるため、区別が必要.
F: 間違った情報が付加されている
S: 間違いではないが、続編または別メディアの同内容ソフトと表記方法の統一が目的
J: Jr, JR., JR / Vs が混在しているのでパッケージの表記の JR. / VS. で統一
A: 大文字の ABC まで過度にカタカナで入力していて日本語として不自然. Blank は例外的だが原作の表記に準拠.
B: A とは逆に英語で書いてあるもののパッケージでは日本語で書いてある
K: 読みがなが入っている
U: unicode chara 0xfe0e を削除
=end

def cartridge_correction(db)
list = [
	%w(B‐ワイング B-ウイング), #P
	["三國志 中原の覇者", "三国志 中原の覇者"], #N
	["三國志II 覇王の大陸", "三国志II 覇王の大陸"], #N
	["チイーネーソ ミュータント ニンジャ タートルス", "ティーンエージ ミュータント ニンジャ タートルズ"], #P
	["チイーネーソ ミュータント ニンジャ タートルス ザマンハッタンプロジェクト", "ティーンエージ ミュータント ニンジャ タートルズ ザ マンハッタンプロジェクト"], #P
	["さんまの名探偵 改造品", "さんまの名探偵"], #F
	["グナム危機一髪危機一発 エンパイアシティ1931", "マグナム危機一髪危機一発 エンパイアシティ1931"], #T
	["アフターバーナー ～ After Burner (Box)", "アフターバーナー"], #F
	["タッチダウンフェーバー ～ American Football - Touchdown Fever (Box)", "タッチダウンフェーバー"], #F
	["水戸黄門II 世界漫遊記 ～ Mito Koumon II - Sekai Manyuu Ki (Box)", "水戸黄門II 世界漫遊記"], #F
	["美味しんぼ 究極のメニュー三本勝負 ～ Oishinbo - Kyuukyoku no Menu Sanbon Shoubu (Box)", "美味しんぼ 究極のメニュー三本勝負"], #F
	["パチ夫くん5 Jrの挑戦 ～ Pachio-kun 5 - Jr. no Chousen (Box)", "パチ夫くん5 Jrの挑戦"], #F
	["ウルティマ ～恐怖のエクソダス～ ～ Ultima - Kyoufu no Exodus (Box)", "ウルティマ ～恐怖のエクソダス～"], #F
	["ヴィナス戦記 ～ Venus Senki (Box)", "ヴィナス戦記"], #F
	["ミリピード 巨大昆虫の逆襲 ～ Millipede - Kyodai Konchuu no Gyakushuu (Box)", "ミリピード 巨大昆虫の逆襲"], #F
	["妖精物語 ロッド・ランド ～ Yousei Monogatari Rod Land (Box)", "妖精物語 ロッド・ランド"], #F
	["プロ野球 ファミリースタジアム'87", "プロ野球ファミリースタジアム'87"], #S
	%w(ドンキーコングJr ドンキーコングJR.), #J
	["ドンキーコングJR / JRの算数レッスン", "ドンキーコングJR./ JR.の算数レッスン"], #J
	["ドンキーコングJrの算数遊び", "ドンキーコングJR.の算数遊び"], #J
	["落っことしパズルとんじゃん!? ～ Okkotoshi Puzzle Tonjan!? (Box)", "落っことしパズルとんじゃん!?"], #F
	["エー・エス・オー", "ASO"], #A
	["Bloody Warriors - Shan-Go Troop Strikes Back!", "ブラッディ ウォリアーズ - シャンゴーの逆襲"], #B, title は Bloody Warriors - Shan-Go no Gyakushuu
	["コントラ 魂斗羅", "魂斗羅"], #K
	["ロックマン3 Dr.ワイリーの最後!?", "ロックマン3 Dr.ワイリーの最期!?"], #T
	["スーパーモグラたたき!!︎ぽっくんモグラー", "スーパーモグラたたき!! ぽっくんモグラー"], #U
	["全米!!︎プロバスケット", "全米!! プロバスケット"] #U
]
	
	string_correction(db, "info_alt_title", list)
list = [
	[
		"Dragon Quest III - Soshite Densetsu e...",
		"Dragon Quest III - Soshite Densetsu he..."
	],[
		"Hokuto no Ken 4 - Shichisei Haken Den - Hokuto Shinken no Kanata e",
		"Hokuto no Ken 4 - Shichisei Haken Den - Hokuto Shinken no Kanata he"
	]
]
	string_correction(db, "title", list)
	alt_title_add(db, "Mother", "マザー")
end

def zenpenkouhen(db)
	sql = <<EOS
SELECT name
FROM idstring_info_alt_title
WHERE name like "% (前編 & 後編)";
EOS
	list = []
	db.execute(sql){|r|
		list << [r[0], r[0].sub(" (前編 & 後編)", "")]
	}
	string_correction(db, "info_alt_title", list)
end

def revision_correction(db)
#datach と ntbrom が Revision に Jpn を使っているので Japan に変更
	sql = <<EOS
UPDATE software_data
SET data = #{db.id_revision("Japan")}
WHERE property = #{db.property("revision")} AND data = #{db.id_revision("Jpn")};
EOS
	db.execute sql
	
#修正対象: serial がある disk
	where = "media = #{db.id_media("disk card")} AND info_serial is not null"
#revision がないものは Japan にする
	sql = <<EOS
INSERT INTO software_data (id, property, data)
SELECT software_horizontal.id, #{db.property("revision")}, #{db.id_revision("Japan")}
FROM software_horizontal
WHERE #{where} AND revision is null;
EOS
	db.execute sql
	
#revision があるものは Japan, を前につける
	select = "revision"
	join = db.idstring_join(select, "INNER", "revision")
	select = select.strip.gsub(" ", ",")
	sql = <<EOS
SELECT DISTINCT #{select}
FROM software_horizontal
#{join}
WHERE #{where} AND idstring_revision.name not like "Japan%";
EOS
	db.execute(sql){|r|
		newrevision = "Japan, #{r[1]}"
		revision = db.id_revision(newrevision)
		if revision == nil
#revision に Japan, をつける. それがないものは文字列の修正
			sql = <<EOS
UPDATE idstring_revision
SET name = "#{newrevision}"
WHERE name = "#{r[1]}";
EOS
			puts sql
			db.execute(sql)
			next
		end
#revision があるものは, software_data.data の変更
		sql = <<EOS
UPDATE software_data
SET data = #{revision}
WHERE property = #{db.property("revision")} AND data = #{r[0]} AND id IN(
	SELECT id
	FROM software_horizontal
	WHERE #{where}
);
EOS
		db.execute(sql)
	}
end

def disk_correction(db)
list = [
	["ゴルフ ジャパン コース", "ゴルフ JAPAN コース"], #A
	["ゴルフ ユーエス コース", "ゴルフ US コース"], #A
	["ファミコン グランプリII スリーディー ホット ラリー", "ファミコングランプリII 3D ホットラリー"], #A
	["VSエキサイトバイク", "VS.エキサイトバイク"], #J
	["イタニックミステリー ―蒼の戦慄―", "タイタニックミステリー ―蒼の戦慄―"], #T
	["セクション ゼット", "セクションZ"], #A
	["バイオミラクルぼくってウパ", "バイオミラクル ぼくってウパ"], #S
	["ドンキーコングJr.", "ドンキーコングJR."], #J
	["仮面ライダーブラック 対決シャドームーン", "仮面ライダーBlack 対決シャドームーン"] #A
]
	string_correction(db, "info_alt_title", list)
list = [
	["Famicom Mukashi Banashi - Yuu Yuu Ki - kou Hen", "Famicom Mukashi Banashi - Yuu Yuu Ki"], #F
	["Burger Time", "BurgerTime"], #S
	["Famimaga Disk Vol. 6 - Janken Disk Shiro", "Famimaga Disk Vol. 6 - Janken Disk Jou"] #P
]
	string_correction(db, "title", list)
list = [
	["Hudson", "Hudson Soft"],
	["Namco", "Namcot"],
	["J. Y. Company", "J.Y. Company"],
	["Pack-In Video", "Pack-In-Video"],
	["VAP", "Vap"],
	["TXC Corp", "TXC Corp."],
	["CBS Sony Group", "CBS / Sony Group"],
	["Gakushu Kenkyusha", "Gakken"],
	["Imagineering", "Imagineer"],
	["Kakenhi", "Kaken"],
	["Sigma Shougi", "Sigma Shouji"],
	["Technos", "Technos Japan"]
]
	string_correction(db, "publisher", list)
	zenpenkouhen(db)
end

def pcb_correction(db)
	[
		["Dragon Quest IV%", "HVC-SUROM"],
		["The Best Play Pro Yakyuu Special", "HVC-SXROM"]
	].each{|t|
		title = t.shift
		pcb = t.shift
sql = <<EOS
SELECT software_horizontal.id
FROM idstring_title
INNER JOIN software_horizontal
ON idstring_title.id = software_horizontal.title
WHERE idstring_title.name like '#{title}';
EOS
		id = ""
		db.execute(sql){|r|
			id << "id=#{r[0]} "
		}
		id.strip!
		id.gsub!(" ", " OR ")
		pcb = db.id_feature_pcb(pcb)
sql = <<EOS
UPDATE software_data
SET data = #{pcb}
WHERE (#{id}) AND 
property = #{db.property("feature_pcb")} AND data <> #{pcb};
EOS
		#puts sql
		db.execute(sql)
	}
end

begin
	db = MYDB.new
	db.transaction
	db.pivot
	case ARGV[0]
	when "cartridge"
		cartridge_correction(db)
	when "disk"
		disk_correction(db)
	else
		cartridge_correction(db)
		disk_correction(db)
		pcb_correction(db)
		revision_correction(db)
	end
	db.commit
end
