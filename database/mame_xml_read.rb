require "unf"

def name_value_scan(h, tag, t)
	t.scan(/<#{tag}\s+name="([^"]+)"\s+value="([^"]+)" ?\/>/).each{|r|
		key = "#{tag}_#{r[0]}"
		if h.key?(key) == false
			h[key] = []
		end
		h[key] << r[1]
	}
end

def feature_concat(feature, key_regexp, key_new)
	key_new = "feature_" + key_new
	feature[key_new] = []
	feature.each{|key, value|
		if key !~ key_regexp
			next
		end
		feature[:p][:name] << key
		feature[:p][:alias][key] = key_new
		feature[key_new].concat(value)
		feature.delete(key)
	}
end
def single_scan(t, h, key)
	r = t.scan(/<#{key}>([^>]+)<\/#{key}>/)
	if r[0] != nil
		h[key] << r[0][0]
	end
end

def romandigit_zenkaku(str)
	UNF::Normalizer.normalize(str, :nfkc).gsub("~", "～")
end

def he_wo(str)
	str.strip.gsub(" e ", " he ").gsub(" o ", " wo ")
end

def normarilize_table_generate(f, xml)
	single = {}
	description = []
	info = {}
	feature = {}
	cartridge_property = {
		:name => %w(title revision year publisher rominfo raminfo),
		:alias => {}
	}
	%w(description year publisher).each{|key|
		single[key] = []
	}
	xml.each{|t|
		single.each{|key, value|
			single_scan(t, single, key)
		}
		name_value_scan(info, "info", t)
		name_value_scan(feature, "feature", t)
	}
	feature[:p] = cartridge_property
	feature_concat(feature, /(batt\??|battery)/, "Battery")
	feature_concat(feature, /outer\-(chr|prg)\-size/, "outer_size")
	feature_concat(feature, /ic\d+/, "ic")
	feature_concat(feature, /u\d+/, "u")
	feature_concat(feature, /mmc\d_type/, "mmc_type")
	feature_concat(feature, /(x1|vrc\d|chr)\-pin\d+/, "pin_assign")
	feature.delete(:p)
	
	[info, feature].each{|t| t.each{|key, value|
		cartridge_property[:name] << key
		value.uniq!
	}}
	f.puts "CARTRIDGE_PROPERTY ="
	f.puts cartridge_property.to_s
	revision = []
	name = []
	single["description"].uniq.each{|t|
		#p t
		r = t.scan(/\((.+)\)\z/)
		if r[0] != nil
			revision << r[0].last
		end
		name << he_wo(t.split("(")[0])
	}
	f.puts "REVISION = "
	f.puts revision.uniq.to_s
	f.puts "NAME = "
	f.puts name.uniq.to_s
	%w(year publisher).each{|key|
		f.puts key.upcase + " = "
		f.puts single[key].uniq.sort.to_s
	}
	info["info_alt_title"].map!{|t|
		romandigit_zenkaku(t)
	}
	f.puts "INFO = "
	f.puts info.to_s
	f.puts "FEATURE = "
	f.puts feature.to_s
end
def romhash_table_generate(f, xml)
	region = []
	rom = []
	status = []
	xml.each{|u|
		r = u.scan(/<description>([^<]+)<\/description>/)
		if r.size == 0
			next
		end
		description = r[0][0].split("(")[0].strip
		u.split("</dataarea>").each{|t|
			area = t.scan(/<dataarea name=\"([^\"]+)\" size=\"([^\"]+)\">/)
			if area.size == 0
				next
			end
			region << area[0][0]
			if t.include?("<rom ") == false
				h = {"description" => description, "dataarea" => area[0][0]}
				h["size"] = area[0][1]
				rom << h
				next
			end
			t.scan(/<rom ([^>]+)>/).each{|rominfo|
				h = {"description" => description, "dataarea" => area[0][0]}
				rominfo[0].scan(/([^=\s]+)=\"([^\"]+)\"/).each{|element|
					h[element[0]] = element[1]
				}
				if h.key? "status"
					status << h["status"]
				end
				rom << h
			}
		}
	}
	f.puts "DATAAREA = "
	f.puts region.uniq.to_s
	f.puts "STATUS = "
	f.puts status.uniq.to_s
	f.puts "ROM = "
	f.puts rom.to_s
end
def noattr_scan(t, tag)
	t.scan(/<#{tag}>([^<]+)<\/#{tag}>/)
end

def software_table_generate(f, xml)
	software = []
	#xml[0, 4].each{|t|
	xml.each{|t|
		h = {}
		#description -> title, revison
		r = noattr_scan(t, "description")
		if r.size == 0
			next
		end
		description = r[0][0]
		revision = description.scan(/\((.+)\)\z/)
		if revision[0] != nil
			h["revision"] = revision[0].last
		end
		h["title"] = he_wo(description.split("(")[0])
		
		#year
		r = noattr_scan(t, "year")
		if r.size != 0
			h["year"] = r[0][0]
		end
		#publisher
		r = noattr_scan(t, "publisher")
		if r.size != 0
			h["publisher"] = r[0][0]
		end
		
		#feature
		h["feature"] = t.scan(/<feature\s+name="([^"]+)"\s+value="([^"]+)" ?\/>/)
		h["info"] = t.scan(/<info\s+name="([^"]+)"\s+value="([^"]+)" ?\/>/)
		h["info"].map!{|s|
			if s[0] == "alt_title"
				s[1] = romandigit_zenkaku(s[1])
			end
			s
		}
		#dataarea
		h["dataarea"] = []
		t.split("</dataarea>").each{|s|
			hh = {}
			s.scan(/<dataarea ([^>]+)>/).each{|dataarea|
				dataarea[0].scan(/([^=\s]+)=\"([^\"]+)\"/).each{|u|
					hh[u[0]] = u[1]
				}
				break
			}
			hh["rom"] = []
			s.scan(/<rom ([^>]+)>/).each{|rominfo|
				hhh = {}
				rominfo[0].scan(/([^=\s]+)=\"([^\"]+)\"/).each{|u|
					hhh[u[0]] = u[1]
				}
				hh["rom"] << hhh
			}
			if hh["rom"].size == 0
				hh.delete "rom"
			end
			if hh.size == 0
				next
			end
			h["dataarea"] << hh
		}
		software << h
	}
	f.puts "SOFTWARE = "
	f.puts software.to_s
end
begin
	path = '.'
	if ARGV[0] == '-o'
		ARGV.shift
		path = ARGV.shift
	end
	xml_filename = ARGV[0]
	xml = []
	ARGV.each{|t|
		f = File.open(t)
		xml.concat(f.read.split("</software>"))
		f.close
	}
	f = File.open("#{path}/#{xml_filename}.normalized_data.rb", "w")
	normarilize_table_generate(f, xml)
	romhash_table_generate(f, xml)
	f.close
	f = File.open("#{path}/#{xml_filename}.software_data.rb", "w")
	software_table_generate(f, xml)
	f.close
end
