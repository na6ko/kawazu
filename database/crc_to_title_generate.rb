require "sqlite3"
require "./common.rb"
#PCB名称 software_data.property = feature_pcb から引ける
#
def battery_set(db)
	sql = <<EOS
SELECT DISTINCT cart_id
FROM view_ram
INNER JOIN raminfo
ON memory_id = raminfo.id
WHERE 
(raminfo.dataarea = #{db.id_dataarea("bwram")} OR 
raminfo.dataarea = #{db.id_dataarea("mapper_bram")});
EOS
	bram = []
	db.execute(sql){|r|
		bram << r[0]
	}
	bram
end

def adpcm_set(db)
	sql = <<EOS
SELECT DISTINCT cart_id
FROM view_rom
INNER JOIN rominfo
ON memory_id = rominfo.id
WHERE 
rominfo.dataarea = #{db.id_dataarea("adpcm")};
EOS
	adpcm = []
	db.execute(sql){|r|
		adpcm << r[0]
	}
	adpcm
end

def rom_out(db, f)
	sql = <<EOS
SELECT id, crc
FROM rominfo
WHERE status = 0
ORDER BY crc;
EOS
	crc = {}
	db.execute(sql){|r|
		crc[r[1]] = r[0]
	}
	
	sql = <<EOS
SELECT cart_id, rominfo.id, rominfo.dataarea, rominfo.offset, rominfo.size
FROM view_rom
INNER JOIN rominfo
ON rominfo.id = memory_id
WHERE rominfo.dataarea = #{db.id_dataarea("prg")} OR rominfo.dataarea = #{db.id_dataarea("chr")}
ORDER BY cart_id, rominfo.dataarea, rominfo.offset;
EOS
	rominfo_multiid_to_cartridge_id = {}
	rominfo = {}
	key = []
	cartridge_id = nil
	db.execute(sql){|r|
		if cartridge_id != r[0] && cartridge_id != nil
			rominfo_multiid_to_cartridge_id[key] = cartridge_id
			key = []
		end
		key << r[1]
		rominfo[r[1]] = r[2,3]
		cartridge_id = r[0]
	}
	if key.size != 0
		rominfo_multiid_to_cartridge_id[key] = cartridge_id
	end
	battery = battery_set(db)
	
	sql = <<EOS
SELECT t1.cart_id, t1.memory_id
FROM (
	SELECT cart_id, count(memory_id) as count
	FROM view_rom
	GROUP BY cart_id
) AS t0
INNER JOIN view_rom as t1
ON t0.cart_id = t1.cart_id
INNER JOIN rominfo
ON t1.memory_id = rominfo.id
WHERE t0.count >= 2;
EOS
	rominfo_singleid_to_cartridge_id = {}
	db.execute(sql){|r|
		rominfo_singleid_to_cartridge_id[r[1]] = r[0]
	}

	f.puts "module Nesfile"
	f.puts "BATTERY = "
	f.puts battery.to_s
	f.puts "ADPCM = "
	f.puts adpcm_set(db).to_s
	f.puts "CRC_TO_ROMINFO_ID = "
	f.puts crc.to_s
	f.puts "ROMINFO_ID = "
	f.puts rominfo.to_s
	f.puts "ROMINFO_MULTIID_TO_CARTRIDGE_ID = "
	f.puts rominfo_multiid_to_cartridge_id.to_s
	f.puts "ROMINFO_SINGLEID_TO_CARTRIDGE_ID = "
	f.puts rominfo_singleid_to_cartridge_id.to_s
	f.puts "end"
end
def string_out(db, f)
	strings = {}
	columns = %w(title revision publisher feature_pcb)
	%w(serial alt_title).each{|t|
		columns << ("info_" + t)
	}
	sql = "DROP VIEW IF EXISTS temp;"
	db.execute(sql)

	sql = <<EOS
CREATE VIEW temp AS
SELECT DISTINCT #{db.array_to_columns(columns)}, media, cart_id
FROM software_horizontal
OUTER LEFT JOIN view_rom
ON cart_id = software_horizontal.id
WHERE info_serial is not null;
EOS
	db.execute(sql)
	columns.each{|column|
		strings[column] = {}
		sql = <<EOS
SELECT DISTINCT #{column}, idstring_#{column}.name
FROM temp
OUTER LEFT JOIN idstring_#{column}
ON #{column} = idstring_#{column}.id
WHERE media = #{db.id_media("disk card")} OR (
	media = #{db.id_media("ROM cartridge")} AND cart_id is not null
);
EOS
		db.execute(sql){|r|
			if r[1] == nil
				next
			end
			strings[column][r[0]] = r[1]
		}
	}
	sql = "DROP VIEW IF EXISTS temp;"
	db.execute(sql)
	
	f.puts "module IdString"
	strings.each{|key, value|
		f.puts key.upcase + '='
		f.puts value.to_s
	}
	f.puts "end"
end

def ssq_execute(db, sql)
	r = db.execute2(sql)
	row = r.shift
	ha = []
	r.each{|t|
		h = {}
		t.each{|s|
			key = row.shift
			if s != nil
				h[key.intern] = s
			end
			row << key
		}
		ha << h
	}
	ha
end
	
def cartridge_out(db, f)
	sql = <<EOS
SELECT software_horizontal.id, title, revision, publisher, info_serial, info_alt_title, feature_pcb
FROM software_horizontal 
INNER JOIN view_rom
ON cart_id = software_horizontal.id
ORDER BY id;
EOS
	h = {}
	ssq_execute(db, sql).each{|t|
		p t
		h[t[:id]] = t
		t.delete :id
	}
	f.puts "module Nesfile"
	f.puts "CARTRIDGE = "
	f.puts h.to_s
	f.puts "end"
end

def ram_out(db, f)
	h = {}
	%w(vram wram bwram).each{|dataarea|
		sql = <<EOS
SELECT cart_id, raminfo.size
FROM view_ram
INNER JOIN raminfo
ON memory_id = raminfo.id
WHERE dataarea = #{db.id_dataarea(dataarea)};
EOS
		h[dataarea] = {}
		db.execute(sql){|r|
			h[dataarea][r[0]] = r[1]
		}
	}
	f.puts "module Nesfile"
	f.puts "RAMSIZE = "
	f.puts h.to_s
	f.puts "end"
end

def disk_out(db, f)
	where = "media = #{db.id_media("disk card")} AND info_serial is not null"
	sql = <<EOS
SELECT DISTINCT
title, info_serial, info_alt_title, name, disk_side_qty, publisher
FROM software_horizontal
INNER JOIN idstring_info_serial
ON idstring_info_serial.id = info_serial
WHERE #{where}
EOS
	h = {}
	ssq_execute(db, sql).each{|t|
		t[:name].split(", ").each{|serial|
			h[serial[4,3]] = t
		}
		t.delete :name
	}
=begin
	["title", "info_alt_title"].each{|key|
		sql = <<EOS
SELECT DISTINCT #{key}, name
FROM software_horizontal
INNER JOIN idstring_#{key}
ON idstring_#{key}.id = #{key}
WHERE #{where}
EOS
		hh[key] = {}
		ssq_execute(db, sql).each{|t|
			hh[key][t[key]] = t["name"]
		}
	}
=end
	f.puts "module FDSfile"
	f.puts "SOFTWARE = "
	f.puts h.to_s
	f.puts "end"
end
begin
	db = MYDB.new
	db.pivot
	f = File.open(ARGV.shift, 'w')
	rom_out(db, f)
	string_out(db, f)
	cartridge_out(db, f)
	ram_out(db, f)
	disk_out(db, f)
	f.close
	db.close
end
