require "./data_to_sqlite.rb"
require "./obj/nes.xml.normalized_data.rb"
require "./obj/nes.xml.software_data.rb"
require "./common.rb"

begin
	db = MYDB.new
	db.transaction
	normalize_set(db)
	software_set(db, true, "ROM cartridge", SOFTWARE)
	db.commit
end
