require "sqlite3"
class MYDB < SQLite3::Database
	def initialize
		super("nes_famicom.db")
		@property = {}
		@idstring = {}
		sql = <<EOS
SELECT name
FROM sqlite_master
WHERE type = 'table';
EOS
		table = []
		self.execute(sql).each{|t|
			table << t[0]
		}
		if table.include?("software_property")
			idstring_query_hash_set(
				@property,
				"SELECT alias, name FROM software_property;"
			)
			%w(dataarea media status).each{|k|
				idstring_set(k)
			}
		end
	end
	def idstring_query_hash_set(h, sql)
		self.execute(sql){|r|
			h[r[1]] = r[0]
		}
	end
	def idstring_set(key)
		if @idstring.key?(key)
			return
		end
		@idstring[key] = {}
		idstring_query_hash_set(
			@idstring[key],
			"SELECT id, name FROM idstring_#{key};"
		)
=begin
		key = %w(dataarea media title status publisher revision)
		%w(mirroring pcb slot).each{|t|
			key << "feature_#{t}"
		}
		%w(alt_title serial).each{|t|
			key << "info_#{t}"
		}
		key.each{|k|
			@idstring[k] = {}
			idstring_query_hash_set(
				@idstring[k],
				"SELECT id, name FROM idstring_#{k};"
			)
		}
=end
	end
	def property(key)
		@property[key]
	end
	def id_dataarea(key)
		idstring_get("dataarea", key)
	end
	def idstring_get(property, key)
		idstring_set(property)
		@idstring[property][key]
	end
	def id_feature_pcb(key)
		idstring_get("feature_pcb", key)
	end
	def id_media(key)
		idstring_get("media", key)
	end
	def id_revision(key)
		idstring_get("revision", key)
	end
	def id_status(key)
		idstring_get("status", key)
	end
	def idstring_join(select, inout, column_name)
		table = "idstring_" + column_name
		if inout == "OUTER"
			inout << " LEFT"
		end
		select << " #{table}.name"
		<<EOS
#{inout} JOIN #{table}
ON software_horizontal.#{column_name} = #{table}.id
EOS
	end
	def array_to_columns(array)
		select = ''
		array.each{|t|
			select << t << " "
		}
		select.strip!
		select.gsub!(" ", ", ")
		select
	end
	
	def pivot
		sql = "DROP VIEW IF EXISTS software_horizontal;"
		self.execute(sql)
		sql = <<EOS
CREATE VIEW software_horizontal as
SELECT id,
EOS
		%w(title media revision year publisher disk_side_qty).each{|t|
			s = @property[t]
			sql << "MAX(CASE WHEN property = #{s} THEN data END) AS #{t},"
		}
		[
			{:prefix => "info_", :name => %w(serial release alt_title)},
			{:prefix => "feature_", :name => %w(pcb slot)}
		].each{|h| h[:name].each{|t|
			t = h[:prefix] + t
			s = @property[t]
			sql << "MAX(CASE WHEN property = #{s} THEN data END) AS #{t},"
		}}
		sql.chop!
		sql << ' '
		sql << <<EOS
FROM software_data
GROUP BY id;
EOS
		self.execute(sql)

		self.execute "DROP VIEW IF EXISTS view_rom;"
		sql = <<EOS
CREATE VIEW view_rom as
SELECT base.id as cart_id, data as memory_id
FROM software_data as base
INNER JOIN rominfo
ON base.data = rominfo.id
WHERE base.property = #{@property["rominfo"]} AND rominfo.status <> #{@idstring["status"]["baddump"]};
EOS
		self.execute(sql)

		self.execute "DROP VIEW IF EXISTS view_ram;"
		sql = <<EOS
CREATE VIEW view_ram as
SELECT cart_id, data as memory_id
FROM software_data as base
INNER JOIN view_rom
ON base.id = view_rom.cart_id
WHERE base.property = #{@property["raminfo"]};
EOS
		self.execute(sql)
	end
end
