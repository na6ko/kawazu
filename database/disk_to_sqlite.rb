require "./data_to_sqlite.rb"
require "./obj/famicom_flop.xml.normalized_data.rb"
require "./obj/famicom_flop.xml.software_data.rb"
require "./common.rb"

begin
	db = MYDB.new
	db.transaction
	[
		[NAME, 'title'],
		[REVISION, 'revision'],
		[YEAR, 'year'],
		[PUBLISHER, 'publisher']
	].each{|t|
		name_insert(db, t[0], 'idstring_' + t[1])
	}
	INFO.each{|key,data|
		name_insert(db, data, 'idstring_' + key)
	}
	software_set(db, false, "disk card", SOFTWARE)
	db.commit
end
