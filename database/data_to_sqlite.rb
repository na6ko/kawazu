require "sqlite3"

def escape(t)
	t.gsub!("&qout;", '"')
	t.gsub!("&amp;", '&')
	t.gsub!("&lt;", '<')
	t.gsub!("&gt;", '>')
	if t =~ /&[^;]+;/
		xxx
	end
	#t.gsub!("'", "\\\\'")
	t.gsub!('"', '\'')
end
def create_table(db, name, columns)
	sql = "DROP TABLE IF EXISTS #{name};"
	db.execute sql
	sql = "CREATE TABLE #{name}(#{columns}, lastupdate integer not null DEFAULT(unixepoch('now', 'localtime')));"
	db.execute sql
	sql = <<EOS
CREATE TRIGGER #{name}_timestamp_update 
AFTER UPDATE ON #{name}
BEGIN
    UPDATE #{name} 
    SET lastupdate = unixepoch('now', 'localtime') 
    WHERE rowid = NEW.rowid;
END;
EOS
	db.execute sql
end
def name_insert(db, strarray, name)
	sql = "INSERT INTO #{name} (name) VALUES"
	strarray.each{|t|
		escape(t)
		sql << "(\"#{t}\"),"
	}
	sql.chop!
	sql << "ON CONFLICT (name) DO NOTHING;"
	db.execute sql
end
def str_to_id(db, strarray, name)
	name = 'idstring_' + name
	columns = <<EOS
		id integer primary key autoincrement,
		name text unique
EOS
	create_table(db, name, columns)
	name_insert(db, strarray, name)
end
def idstring_set(db)
	str_to_id(db, PUBLISHER, "publisher")
	str_to_id(db, REVISION, "revision")
	str_to_id(db, YEAR, "year")
	str_to_id(db, NAME, "title")
	str_to_id(db, DATAAREA, "dataarea")
	str_to_id(db, STATUS, "status")
	str_to_id(db, ["ROM cartridge", "disk card", "appended ROM cartridge"], "media")
end

def xxstring_select(db, tablename)
	sql = "SELECT id, name FROM #{tablename};"
	h = {}
	db.execute(sql){|q|
		h[q[1]] = q[0]
	}
	h
end

def rom_generate(db, hh)
	name = 'rominfo'
	columns = <<EOS
		id integer primary key autoincrement,
		dataarea integer not null,
		status integer not null,
		size integer,
		offset integer,
		name integer,
		crc unsigned integer not null,
		sha1 character(41) not null unique
EOS
	create_table(db, name, columns)
	sql = "INSERT INTO #{name} ("
	sql << "dataarea, status, size, offset, name, crc, sha1"
	sql << ") VALUES "
	ROM.each{|t|
		if t.key?("crc") == false && t["status"] != "nodump"
			next
		end
		offset = 0
		if t.key?("offset")
			offset = t["offset"].to_i(0)
		end
		#ti = t["description"]
		#escape(ti)
		status = 0
		if t.key? "status"
			status = db.id_status(t["status"])
		end
		if t["status"] == "nodump"
			sql <<  sprintf(
				"(%d, %d, null, null, null, 0, \"da39a3ee5e6b4b0d3255bfef95601890afd80709%d\"),",
				db.id_dataarea(t["dataarea"]), status,
				status
			)
		else
			name_id = db.idstring_get("rominfo_name", t["name"])
			if name_id == nil
				name_id = "null"
			else
				name_id = name_id.to_s
			end
			sql << sprintf(
				"(%d, %d, %d, %d, %s, %d, \"%s%d\"),",
				db.id_dataarea(t["dataarea"]), status,
				t["size"].to_i(0), offset, 
				name_id,
				t["crc"].to_i(0x10), t["sha1"].downcase, status
			)
		end
	}
	sql.chop!
	sql << "ON CONFLICT (sha1) DO NOTHING;"
	db.execute sql
end
=begin
SELECT idstring_dataarea.name, count(dataarea) 
FROM raminfo 
INNER JOIN idstring_dataarea
ON idstring_dataarea.id = raminfo.dataarea
group by dataarea;

SELECT rominfo.id, idstring_title.name, idstring_dataarea.name, raminfo.size, raminfo.offset 
FROM raminfo
INNER JOIN idstring_dataarea
ON idstring_dataarea.id = raminfo.dataarea
INNER JOIN rominfo
ON rominfo.id = raminfo.rominfo_id
INNER JOIN idstring_title
ON rominfo.title = idstring_title.id
where raminfo.dataarea <> 2 OR raminfo.size <> 8192;
=end
def to_i(t, name)
	if t.key? name
		t[name] = t[name].to_i(0)
	end
end
def ram_normalize
	ram = []
	ROM.each{|t|
		if t["dataarea"].include?("ram") == false
			next
		end
		t.delete "description"
		t.delete "prg_sha1"
		ram << t
	}

	ram.map!{|t|
		to_i(t, "size")
		to_i(t, "offset")
		to_i(t, "value")
		t
	}
	ram.uniq
end
def ram_generate(db, hh)
	tablename = 'raminfo'
	c = <<EOS
id integer primary key autoincrement,
dataarea integer not null,
offset integer default 0,
size integer not null,
value integer
EOS
	create_table(db, tablename, c)
	ram_normalize.each{|t|
		c = "dataarea,"
		d = "idstring_dataarea.id,"
		%w(offset size value).each{|s|
			if t.key?(s)
				c << s << ','
				d << t[s].to_s << ','
			end
		}
		c.chop!
		d.chop!
		sql = "INSERT INTO #{tablename} (#{c}) "
		str = t["dataarea"]
		sql << "SELECT #{d} FROM idstring_dataarea WHERE name = \"#{str}\""
		db.execute sql + ';'
	}
end

def name_value_set(db)
	INFO.each{|key, value|
		str_to_id(db, value, key)
	}
	FEATURE.each{|key, value|
		str_to_id(db, value, key.gsub(/[\-\?]/, '_'))
	}
end
def software_property_set(db)
	name = 'software_property'
	columns = <<EOS
		id integer primary key autoincrement,
		name text unique,
		alias integer
EOS
	create_table(db, name, columns)
	CARTRIDGE_PROPERTY[:name].unshift("media")
	CARTRIDGE_PROPERTY[:name].push("disk_side_qty")
	
	name_insert(db, CARTRIDGE_PROPERTY[:name], name)
	h = xxstring_select(db, name)
	alias_h = {}
	CARTRIDGE_PROPERTY[:name].each{|str|
		id = h[str]
		alias_h[id] = id
		if CARTRIDGE_PROPERTY[:alias].key? str
			alias_h[id] = h[CARTRIDGE_PROPERTY[:alias][str]]
		end
	}
	h.each{|key, id|
		alias_v = alias_h[id]
#		if alias_v == id
#			next
#		end
		sql = "UPDATE #{name} SET alias = #{alias_v} WHERE id = #{id};"
		db.execute sql
	}
end

=begin
SELECT dataarea, offset, size, idstring_title.name
FROM rominfo
INNER JOIN idstring_title
ON idstring_title.id = rominfo.title
WHERE offset <> 0;
=end
def normalize_set(db)
	software_property_set(db)
	name_value_set(db)
	idstring_set(db)
	ar = []
	ROM.each{|t|
		if t.key?("name") == false
			next
		end
		ar << t["name"]
	}
	str_to_id(db, ar.uniq, "rominfo_name");
	
	rom_generate(db, ROM)
	ram_generate(db, ROM)
end

def software_data_insert(db, table_name, software_id, property, key, data)
	subtable_name = "idstring_" + property[key][:name].gsub('-', '_')
	subtable_property = property[key][:id]
	escape(data)
	data.gsub!('"', '\"')
	sql = <<EOS
INSERT INTO #{table_name} (id, property, data)
SELECT #{software_id}, #{subtable_property}, #{subtable_name}.id
FROM #{subtable_name}
WHERE name = \"#{data}\";
EOS
	db.execute(sql)
end

def software_set(db, create, media, software)
	table_name = 'software_data'
	if create
		db.execute "DROP TABLE IF EXISTS #{table_name};"
		columns = <<EOS
			id integer,
			property integer,
			data integer
EOS
		create_table(db, table_name, columns)
	end
	sql = <<EOS
SELECT a.alias, a.name, b.name
FROM software_property as a
INNER JOIN software_property as b
ON a.alias = b.id;
EOS
	property = {}
	db.execute(sql){|q|
		property[q[1]] = {:id => q[0], :name => q[2]}
	}
	sql = "SELECT max(id) FROM #{table_name};"
	software_id = db.execute(sql)[0][0]
	if software_id == nil
		software_id = 0
	end
	software_id += 1
	software.each{|t|
=begin
サブクエリなし     あり
real    0m1.358s / 0m1.879s
user    0m0.000s / 0m0.015s
sys     0m0.015s / 0m0.015s
=end
		software_data_insert(db, table_name, software_id, property, "media", media)
		t.each{|key, value|
			case key
			when "info", "feature"
				value.each{|t|
					software_data_insert(db, table_name, software_id, property, "#{key}_#{t[0]}", t[1])
				}
			when "dataarea"
				next
			else
				software_data_insert(db, table_name, software_id, property, key, value)
			end
		}
		
		property_rominfo = property["rominfo"][:id]
		property_raminfo = property["raminfo"][:id]
		property_diskside = property["disk_side_qty"][:id]
		t.each{|key, value|
			if key != "dataarea"
				next
			end
			value.each{|s|
				if s["name"] == "prg" or s["name"] == "chr"
					s["rom"].each{|rom|
						sha1 = rom["sha1"]
						if sha1 == nil
							next
						end
						if rom.key?("status") && rom["status"] == "baddump"
							sha1 << db.id_status("baddump").to_s
						else
							sha1 << '0'
						end
						sql = <<EOS
INSERT INTO #{table_name} (id, property, data)
SELECT #{software_id}, #{property_rominfo}, rominfo.id
FROM rominfo
WHERE sha1=\"#{sha1}\";
EOS
#	puts sql
						db.execute(sql)
					}
				elsif s["name"] == 'adpcm'
					sql = <<EOS
INSERT INTO #{table_name} (id, property, data)
SELECT #{software_id}, #{property_rominfo}, rominfo.id
FROM rominfo
WHERE crc=0;
EOS
					db.execute(sql)
				elsif s["name"].include? "ram"
					dataarea = s["name"]
					sql = <<EOS
INSERT INTO #{table_name} (id, property, data)
SELECT #{software_id}, #{property_raminfo}, raminfo.id
FROM raminfo
INNER JOIN idstring_dataarea
ON idstring_dataarea.id = raminfo.dataarea
WHERE idstring_dataarea.name = \"#{dataarea}\"
EOS
					if s.key?("rom") == false
						c = "size"
						sql << " AND #{c} = #{s[c]}"
					else
						s["rom"].each{|rom|
							%w(offset size value).each{|c|
								if rom.key? c
									sql << " AND #{c} = #{rom[c]}"
								end
							}
						}
					end
#	puts sql
					db.execute(sql + ';')
				elsif s["name"] == "flop"
					image_size = s["size"].to_i
					if image_size % 65500 != 0
						xx
					end
					sidenum = image_size / 65500
					sql = <<EOS
INSERT INTO #{table_name} (id, property, data)
VALUES(#{software_id}, #{property_diskside}, #{sidenum});
EOS
					db.execute(sql)
				end
			}
		
		}
		software_id += 1
	}
end
