module FlashMemoryParameters

module ParameterFlash
	def memory_capacity
		0x80
	end
end
class DriverFlash
	include ParameterFlash
	def byte_identify(d)
		d[0] == manufacturer_id && d[1] == device_id
	end
	def equable_sector(size)
		@erase_sector = []
		0.step(memory_capacity - 1, size){|i|
			@erase_sector << (i...(i+size))
		}
	end
	def erase_set_supported?(v)
		@erase_request = v
		@erase_supported.include?(v)
	end
	def erase_branch(offset)
		case @erase_request
		when :erase_type_none
			#nothing todo
		when :erase_type_entire
			if @erase_sector.size != 0
				@erase_sector.clear
				return :erase_type_entire
			end
		when :erase_type_sector
			r = @erase_sector.reject!{|t|
				t.include? offset
			}
			if r != nil
				return :erase_type_sector 
			end
		else
			xxx
		end
		return :erase_type_none
	end
end
module ParameterAM29F040B
	def name
		"AM29F040B"
	end
	def memory_capacity
		0x80000
	end
	def command_address_mask
		0x007ff
	end
	def autoselect_address_mask
		0xff #2桁なので A7:0 (?)
	end
	def manufacturer_id
		0x01
	end
	def device_id
		0xa4
	end
	def skip_data_0xff?
		true
	end
	def program_page_size
		1
	end
end
class DriverAM29F040B < DriverFlash
	include ParameterAM29F040B
	def initialize
		@erase_supported = [:erase_type_entire, :erase_type_sector]
	end
	def sector_assign
		equable_sector(0x10000)
	end
end
module ParameterSST39SF040
	def name
		"SST39SF040"
	end
	def command_address_mask
		0x07fff
	end
	def autoselect_address_mask
		0xffff #range 詳細不明 A15:0??
	end
	def manufacturer_id
		0xbf
	end
	def device_id
		0xb7
	end
	def sector_assign
		#sector address range A11:0
		#todo:sector size が banks size より小さい場合の対応
		equable_sector(0x1000)
	end
end
class DriverSST39SF040 < DriverAM29F040B
	include ParameterSST39SF040
end
module ParameterMX29F040C
	def name
		"MX29F040C"
	end
	#command address mask 3桁ではあるが詳細の明記なし.
	def manufacturer_id
		0xc2
	end
	def device_id
		0xa4
	end
end
class DriverMX29F040C < DriverAM29F040B
	include ParameterMX29F040C
end

#---- byte write, boot block sector ----
module W49F002
	NAME = "W49F002"
	ERASE_SUPPORTED = [:erase_type_entire, :erase_type_sector]
	def manufacturer_id
		0xda
	end
	def memory_capacity
		0x40000
	end
	def command_address_mask
		0x07fff
	end
	def autoselect_address_mask
		0x07fff
	end
	def program_page_size
		1
	end
end
class DriverW49F002U < DriverAM29F040B
	include W49F002
	def name
		NAME + 'U'
	end
	def device_id
		0x0b
	end
	def sector_assign
		@erase_sector = [
			0x00000..0x1ffff,
			0x20000..0x37fff,
			0x38000..0x39fff,
			0x3a000..0x3bfff,
			0x3c000..0x3ffff
		]
	end
end

module EN29F002
	NAME = "EN29F002"
	ERASE_SUPPORTED = [:erase_type_entire, :erase_type_sector]
	def manufacturer_id
		0x7f
	end
	def memory_capacity
		0x40000
	end
	def command_address_mask
		0x007ff #3桁,A11は未確認
	end
	def autoselect_address_mask
		0x007ff #同上
	end
	def program_page_size
		1
	end
	def sector_assign
		@erase_sector = [
			[0x00000, 0x0ffff],
			[0x10000, 0x1ffff],
			[0x20000, 0x2ffff],
			[0x30000, 0x37fff],
			[0x38000, 0x39fff],
			[0x3a000, 0x3bfff],
			[0x3c000, 0x3ffff]
		].map{|t|
			r = t.map{|s|
				s ^ sector_address_mask
			}.sort
			(r[0]..r[1])
		}
	end
end

class DriverEN29F002T < DriverAM29F040B
	include EN29F002
	def name
		NAME + 'T'
	end
	def device_id
		0x92
	end
	def sector_address_mask
		0
	end
end

class DriverEN29F002B < DriverAM29F040B
	include EN29F002
	def name
		NAME + 'B'
	end
	def device_id
		0x97
	end
	def sector_address_mask
		memory_capacity - 1
	end
end

#---- page write devices ----
module ParameterW29C040
	def name
		"W29C040"
	end
	def command_address_mask
		0x07fff
	end
	def autoselect_address_mask
		0x7ffff #A18:1
	end
	def manufacturer_id
		0xda
	end
	def device_id
		0x46
	end
	def memory_capacity
		0x80000
	end
	def skip_data_0xff?
		false
	end
	def program_page_size
		0x100
	end
end
class DriverW29C040 < DriverFlash
	include ParameterW29C040
	def initialize
		@erase_supported = [:erase_type_entire, :erase_type_none]
	end
	def sector_assign
		#no sector
		@erase_sector = [0...memory_capacity]
	end
end
class DriverNil
	def initialize(t)
		@name = "unknown id:"
		t.each{|s|
			@name += " 0x%02X" % s
		}
	end
	def command_address_mask
		0
	end
	def name
		@name
	end
end

DEVICES = [
	DriverAM29F040B, DriverMX29F040C, DriverSST39SF040, 
	DriverW49F002U, DriverEN29F002T, DriverEN29F002B,
	DriverW29C040, 
]

def self.driver_get(name)
	DEVICES.each{|t|
		t = t.new
		if t.name == name
			return t
		end
	}
	nil
end

def self.byte_identify(d)
	DEVICES.each{|t|
		t = t.new
		if t.byte_identify(d)
			return t
		end
	}
	DriverNil.new(d)
end

def self.devices_name_get
	str = "auto"
	DEVICES.map{|t|
		t.new.name
	}.sort.each{|t|
		str << ',' << t
	}
	str
end
end
