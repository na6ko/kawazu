require Hamo::PROGRAM_DIR + '/searchform.mrb'
require Hamo::PROGRAM_DIR + '/message.rb'

def search(word)
	typedword = word.upcase.strip
	if typedword == ''
		return
	end
	s = SearchForm::LIST_TITLE.find_all{|t|
		t[0].upcase.include? typedword
	}
	s.each{|t|
		h = {"title" => t[0]}
		i = 0 
		t[1].each{|d|
			h["driver#{i}"] = SearchForm::LIST_DRIVER[d]
			i += 1
		}
		Hamo::Textarea::title_to_driver_item_add(h)
	}
end

