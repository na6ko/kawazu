module Message
extend self
	def puts(str, logonly = false)
		Hamo::Textarea::message_print(str + "\n", logonly)
	end
	def print(str, logonly = false)
		Hamo::Textarea::message_print(str, logonly)
	end
	def printf(format, *arg)
		Hamo::Textarea::message_print(sprintf(format, *arg), false)
	end
end
