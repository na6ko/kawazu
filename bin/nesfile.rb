require Hamo::PROGRAM_DIR + '/middlelibrary.rb'
require Hamo::PROGRAM_DIR + '/message.rb'
require Hamo::PROGRAM_DIR + '/romdb.mrb' #.rb だと起動前のコンパイルに270msも取られてしまうので mrbc でコンパイルしておく.
module Nesfile
	include IdString
extend self
	VRAM_MIRROR_DYNAMIC = 0
	VRAM_MIRROR_H = 1
	VRAM_MIRROR_V = 2
	def memory_mirror_detection(memory, min)
		length = memory[:data].size
		while length > min
			l = length / 2
			if memory[:data][0, l] != memory[:data][l, l]
				break
			end
			length /= 2
		end
		memory[:length] = length
	end

	def header_new
		prg = {
			:data => [], :name => "Program ROM", 
		}
		chr = {
			:data => [], :name => "Character RAM", 
		}
		wram = {
			:data => [], :name => "Cartridge Work RAM",
		}
		{
			:cpu_program => prg, :ppu_chara => chr, :cpu_wram => wram,
			:mappernum => 0, :vram_mirror => VRAM_MIRROR_DYNAMIC
		}
	end
	def header_load(header)
		if header[0, 4] != [0x4e, 0x45, 0x53, 0x1a]
			return nil
		end
		h = {}
		h[:cpu_program] = {
			:pagesize => 0x4000, :name => "Program ROM", 
			:length => header[4] * 0x4000
		}
		h[:ppu_chara] = {
			:pagesize => 0x1000, :name => "Character ROM",
			:length => header[5] * 0x2000
		}
		if h[:ppu_chara][:length] == 0
			h[:ppu_chara][:name] = "Character RAM"
			h[:ppu_chara].delete :length
		end
		h[:cpu_wram] = {:battery => (header[6] & (1 << 1)) != 0}
		h[:vram_mirror] = (header[6] & 1) == 0 ? VRAM_MIRROR_H : VRAM_MIRROR_V
		h[:mappernum] = header[6] >> 4
		h[:mappernum] |= header[7] & 0xf0
		#TBD: mapper number による pagesize の設定
		h
	end

	def log2(i)
		v = 1
		15.times{|c|
			if v == i
				return c
			end
			v <<= 1
		}
		nil
	end
	def memory_info_get(memory, print_do)
		entire_crc = Hamo::crc32_calc(memory[:data][0, memory[:length]])
		str = sprintf("%s: 0x%X bytes, CRC32 %08X", memory[:name], memory[:length], entire_crc)
		memory[:crc] = entire_crc
		memory[:id] = CRC_TO_ROMINFO_ID[entire_crc]
		r = false
		if memory[:id] != nil
			str << ", db: found"
			r = true
		end
		if print_do
			Message.puts str
		end
		r
	end
	def crc_print(memory, dbreturn = true)
		if memory[:length] == nil or memory[:length] == 0 or memory[:data] == nil
			Message.puts memory[:name]
			return
		end
		db_found = memory_info_get(memory, true)
		crc = []
		crc_00 = 0
		crc_ff = Hamo::crc32_calc(Array.new(memory[:pagesize], 0xff))
		display_count = 0
		0.step(memory[:length] - 1, memory[:pagesize]){|i|
			src = memory[:data][i, memory[:pagesize]]
			c = Hamo::crc32_calc(src)
			f = [crc_00, crc_ff].include?(c)
			if f == false
				display_count += 1
			end
			crc << {:crc => c, :filled => f}
		}

		if display_count >= 4 && display_count <= 16
			width = 4
		else
			width = [display_count, 8].min
		end
		if width == 0
			Message.puts 'ROM data is filled by 0 or 0xff'
			return
		end
		height = display_count / width
		if display_count % width != 0
			height += 1
		end
		str = Array.new(height, "")
		x = 0; y = 0; i = 0
		filled = []; enabled = []
		fill_prev = false
		crc.each{|t|
			if t[:filled] == false
				str[y] += sprintf("%02X:%04X ", i, t[:crc] >> 16)
				y += 1
				if enabled.size == 0 || fill_prev == true
					enabled << [i]
				elsif
					enabled.last << i
				end
			elsif fill_prev == false
				filled << [i]
			else
				filled.last << i
			end
			if y >= height
				y = 0
				x += 1
			end
			fill_prev = t[:filled]
			i += 1
		}
		if db_found && dbreturn
			return
		end
		if filled.size == 0
			str.each{|t|
				Message.puts t.strip
			}
			return
		end
		if enabled.size == 1 && log2(enabled[0].size) != nil
			m = {
				:name => memory[:name] + " (nofill)",
				:data => memory[:data][
					enabled[0][0] * memory[:pagesize],
					enabled[0].size * memory[:pagesize]
				]
			}
			m[:length] = m[:data].size
			memory_info_get(m, true)
			memory[:id] = m[:id]
			return
		end
		str.each{|t|
			Message.puts t.strip
		}
		str = 'filled: '
		filled.each{|t|
			str << ("0x%02x" % t[0])
			if t.size != 1
				str << ("-0x%02x" % t.last)
			end
			str << ' '
		}
		Message.puts str.strip
	end
	def rom_get(h)
		h[:data][0, h[:length]]
	end
	def bettery_detect_by_database(h)
		if h[:ppu_chara][:data].size == 0
			h[:id] = ROMINFO_MULTIID_TO_CARTRIDGE_ID[[h[:cpu_program][:id]]]
			return BATTERY.include?(h[:id])
		end
		[:cpu_program, :ppu_chara].each{|t|
			if ROMINFO_SINGLEID_TO_CARTRIDGE_ID.key?(h[t][:id])
				h[:id] = ROMINFO_SINGLEID_TO_CARTRIDGE_ID[h[t][:id]]
				return BATTERY.include?(h[:id])
			end
		}
		false
	end
	def save(h, arg)
		memory_mirror_detection(h[:cpu_program], 0x4000)
		crc_print(h[:cpu_program])
		chrrom = h[:ppu_chara][:data].size != 0
		if chrrom
			memory_mirror_detection(h[:ppu_chara], 0x2000)
		end
		crc_print(h[:ppu_chara])
		case h[:vram_mirror]
		when VRAM_MIRROR_H
			Message.puts("VRAM connection: Nametable, H mode")
		when VRAM_MIRROR_V
			Message.puts("VRAM connection: Nametable, V mode")
		end
		
		header = [
			0x4e, 0x45, 0x53, 0x1a, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0
		]
		header[4] = h[:cpu_program][:length] / 0x4000
		if chrrom
			header[5] = h[:ppu_chara][:length] / 0x2000
		end
		header[6] = (h[:mappernum] & 0xf) << 4
		if h[:vram_mirror] == VRAM_MIRROR_V
			header[6] |= 1
		end
		battery_detected = bettery_detect_by_database(h)
		if battery_detected
			Message.puts("Battery flag is added by database query")
			header[6] |= 1 << 1
		end
		header[7] = h[:mappernum] & 0xf0
		if h.key?(:submappernum)
			header[7] |= 0b1000
			header[8] = h[:submappernum] << 4
			Message::puts "NES 2.0 is assigned, please check each memory capacities."
			entry = [
				{:key => "vram", :name => "CHR-RAM", :dest_index => 11, :dest_shift => 0},
				{:key => "wram", :name => "PRG-RAM", :dest_index => 10, :dest_shift => 0},
				{:key => "bwram", :name => "PRG-NVRAM", :dest_index => 10, :dest_shift => 4}
			].each{|t|
				size = RAMSIZE[t[:key]][h[:id]]
				if h.key?(t[:key])
					size = h[t[:key]]
				end
				if size == nil
					size = 0
					if h[:ppu_chara][:length] != 0 && t[:key] == "vram"
						size = 0x2000 #UxROM + non-busconflict 対策
					end
				end
				if size != 0
					header[t[:dest_index]] |= log2(size / 64) << t[:dest_shift]
				end
				Message::printf "%s: 0x%x bytes\n", t[:name], size
			}
		end
		db = title_detect(h)
		if db.key?("dump_log")
			Message::puts db["dump_log"]
		end
		s = Saver.new(arg, db, h, header, chrrom)
		s.save
		db[:battery_detected] = battery_detected
		db[:voice] = ADPCM.include?(h[:id])
		db
	end
	def identify(h)
		memory_mirror_detection(h[:cpu_program], 0x4000)
		memory_info_get(h[:cpu_program], false)
		chrrom = h[:ppu_chara][:data].size != 0
		if chrrom
			memory_mirror_detection(h[:ppu_chara], 0x2000)
			memory_info_get(h[:ppu_chara], false)
		end
		title_detect(h)
	end
	class Saver < FileSaver
		def initialize(arg, db, c, header, chrrom)
			@suffix = "nes"
			@arg = arg
			@db = db
			@cartridge = c
			@header = header
			@chrrom = chrrom
		end
		def save_file(f)
			f.write(@header.pack('C*'))
			f.write(Nesfile::rom_get(@cartridge[:cpu_program]).pack('C*'))
			if @chrrom
				f.write(Nesfile::rom_get(@cartridge[:ppu_chara]).pack('C*'))
			end
		end
	end
	
	def Load(filename)
		f = File.open(filename, 'rb')
		h = header_load(f.read(0x10).unpack('C*'))
		if h == nil
			f.close
			return nil
		end
		h[:cpu_program][:data] = f.read(h[:cpu_program][:length]).unpack('C*')
		h[:ppu_chara][:data] = f.read(h[:ppu_chara][:length]).unpack('C*')
		f.close
		h
	end
	def title_detect(h)
		romid = [h[:cpu_program][:id]]
		if h[:ppu_chara][:length] != nil #&& h[:ppu_chara][:length] != 0
			romid << h[:ppu_chara][:id]
		end
		cartridge_id = ROMINFO_MULTIID_TO_CARTRIDGE_ID[romid]
		rom_num = romid.size
		rom_found = romid.size
		if cartridge_id == nil
			if romid.size == 1
				return {}
			end
			#片方の ROM は db 上にあるので再調査を試みる
			romid.each{|t|
				if ROMINFO_SINGLEID_TO_CARTRIDGE_ID.key? t
					cartridge_id = ROMINFO_SINGLEID_TO_CARTRIDGE_ID[t]
					break
				end
			}
			if cartridge_id == nil
				return {}
			end
			romset = ROMINFO_MULTIID_TO_CARTRIDGE_ID.find{|key,value|
				cartridge_id == value
			}
			if romset == nil
				return {}
			end
			rom_num = romset[0].size
			rom_found = 0
			filesize = {:cpu_program => 0, :ppu_chara => 0}
			romset[0].each{|romid|
				t = ROMINFO_ID[romid]
				d = nil
				case t[0]
				when 1
					d = h[:cpu_program][:data]
					filesize[:cpu_program] += t[2]
				when 3
					d = h[:ppu_chara][:data]
					filesize[:ppu_chara] += t[2]
				end
				if CRC_TO_ROMINFO_ID.key?(Hamo::crc32_calc(d[t[1], t[2]]))
					rom_found += 1
				end
			}
			filesize.each{|key, value|
				if h[key][:data] == nil
					next
				end
				if h[key][:length] != value
					return {}
				end
			}
		end
		t = CARTRIDGE[cartridge_id]
		title = INFO_ALT_TITLE[t[:info_alt_title]]
		if title == nil
			title = TITLE[t[:title]]
		end
		serial = INFO_SERIAL[t[:info_serial]]
		str = "#{serial} #{title}"
		if rom_num != rom_found
			str << sprintf(" **db found %d/%d**", rom_found, rom_num)
		end
		if rom_num != rom_found
			return {}
		end
		{
			"title" => TITLE[t[:title]], 
			"info_serial" => serial, 
			"info_alt_title" => INFO_ALT_TITLE[t[:info_alt_title]], 
			"revision" => REVISION[t[:revision]],
			"feature_pcb" => FEATURE_PCB[t[:feature_pcb]],
			"dump_log" => str
		}
	end
end
def nescrc(arg)
	dbreturn = true
	arg.each{|filename|
		if filename == '-x'
			dbreturn = false
			next
		end
		Message::puts filename
		h = Nesfile.Load(filename)
		Nesfile.crc_print(h[:cpu_program], dbreturn)
		Nesfile.crc_print(h[:ppu_chara], dbreturn)
		Nesfile.title_detect(h)
	}
end
def database_battery_used(arg)
	Nesfile::BATTERY.each{|id|
		Message::puts Nesfile::TITLE[id]
	}
end
