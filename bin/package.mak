PUBLIC_ZIP = ../public/kawazu_`date +%Y%m%d`.zip
GUI_PACKAGE = ../public/Kawazu_GUI_20241022.zip
FIRMWARE = d:/work/hvc/sam_bus_programmer/firmware/busniki/archive/sbp_firmware_202405080405.bin
#(cd /e; git clone -b testers_firmware_20231208 https://gitlab.com/na6ko/kawazu;)
TEMP_DIR = /e
zip:
	rm -rf $(TEMP_DIR)/kawazu
	(cd $(TEMP_DIR); git clone https://gitlab.com/na6ko/kawazu)
	(cd $(TEMP_DIR)/kawazu && sh /d/work/git_file_timestamp_recover.sh)
	cp -a kawazu_cui.exe $(TEMP_DIR)/kawazu/bin
	7za x -o$(TEMP_DIR)/kawazu/bin $(GUI_PACKAGE)
	rm -rf $(PUBLIC_ZIP)
	7za a -o../public/ -w$(TEMP_DIR)/kawazu/ $(PUBLIC_ZIP) $(TEMP_DIR)/kawazu/bin
	7za a $(PUBLIC_ZIP) $(FIRMWARE)
	7za d $(PUBLIC_ZIP) $(addprefix bin/, Makefile .gitignore debug.rb sbp_test.rb)
	7za l $(PUBLIC_ZIP)
	rm -rf $(TEMP_DIR)/j
	7za x -o$(TEMP_DIR)/j $(PUBLIC_ZIP)
