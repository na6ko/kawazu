module Programmer
	FLASH_STATUS_FIFO_READY = (0 << 0)
	FLASH_STATUS_FIFO_BUSY = (1 << 0)
	FLASH_STATUS_PROGRAMMING_READY = (0 << 1)
	FLASH_STATUS_PROGRAMMING_BUSY = (1 << 1)
	FLASH_STATUS_ERASING = (1 << 2)
	FLASH_STATUS_FIFO_EMPTY_SUSPENDED = (1 << 3)
	FLASH_STATUS_ERASE_ERROR = (1 << 4)

	FLASH_STATUS_DONE_MASK = FLASH_STATUS_PROGRAMMING_BUSY | FLASH_STATUS_ERASING | FLASH_STATUS_FIFO_EMPTY_SUSPENDED
	class Fifo
		def initialize(commander, region, src, progressbar_index)
			@commander = commander
			@region = region
			@src = src
			@sent = nil
			@progressbar_index = progressbar_index
		end
		def command_flash_fifo_clear
			@commander.fifo_set(0, @region, [])
		end

		def command_flash_fifo_new
			@sent = @commander.fifo_set(1, @region, @src)
		end
		def progressbar_update
			if $dma_puts
				return
			end
			Hamo::Progressbar::draw_index_set(@progressbar_index)
			Hamo::Progressbar::add(@sent)
			Hamo::Progressbar::draw_index_set(-1)
		end
		def command_flash_fifo_append
			progressbar_update
			@sent = @commander.fifo_set(2, @region, @src)
			#statuses.concat(Hamo::device.receive(h[:mcu_to_pc]))
		end
		def empty?
			@src.size == 0
		end
	end

	class CommandAddress
		def command_processor_address_get(t)
			r = t[:processor][0].begin 
			r |= t[:absolute] & (t[:processor][0].size - 1)
			r
		end
		def initialize(param, region_read, region_write)
			@param = param
			@region_write = region_write
			@region_read = region_read
			@processor_address = command_processor_address_get(param)
		end
		def xpu
			@processor_address
		end
		def bank_size
			@param[:combined_bank].size
		end
#		def absolute
#			@param[:absolute]
#		end
		def bank_switch(driver, c)
			if @param[:switch] && @param[:uniq_bank]
				driver.bank_switch(c, @region_write, 
					@param[:combined_bank].begin,
					@param[:absolute]
				)
			end
		end
	end
	class CommandDestination < CommandAddress
		def command_processor_address_get(t)
			t[:processor][0].begin
		end
		def programming_length_set(l, f)
			@programming_length = l
			@filled_page = f
		end
		def programming_length_get
			@programming_length
		end
		def abs_offset_clear
			@offset = 0
		end
		def abs_offset_add
			@offset += bank_size
		end
		def abs_offset_lessthan?(size)
			@offset < size
		end
		def abs_offset_get
			@offset
		end
		def abs_offset_skip_filled_page
			r = false
			while @filled_page.include?(@offset)
				abs_offset_add
				r = true
			end
			r
		end
		def log2_fixnum(val)
			if val == 0
				xxx
			end
			pow = 0
			while val != 1
				val >>= 1
				pow += 1
			end
			pow
		end
		def program_content_make_read(c, flag, memory_driver)
			address = {:type => Command::TYPE_FIXED, :value => [xpu]}
			c.access_set(2, @region_read, Command::RW_READ_WITH_DATA, address, nil, Command::FLASH_COMMAND_POLLING)
			f = (log2_fixnum(bank_size) - 8) << 4
			if memory_driver.program_page_size != 1
				f |= (log2_fixnum(memory_driver.program_page_size) - 4) << 1
			end
			if memory_driver.skip_data_0xff?
				f |= 1
			end
			f
		end
		def bank_switch(driver, c)
			i = abs_offset_get
			@param[:processor].each{|t|
				driver.bank_switch(c, @region_write, t.begin, i)
				i += t.size
			}
		end
		def compare(rom, progressbar_index)
			programmed_data = Memory.read(@region_read, xpu, bank_size, progressbar_index)
			str = ''
			if programmed_data != rom[@offset, bank_size]
				str = sprintf("$%05x %04X %04X", @offset, Hamo::crc32_calc(rom[@offset, bank_size]), Hamo::crc32_calc(programmed_data))
			end
			str
		end
	end
	class ProgrammerBase
		def initialize(driver, param)
			@cartridge_driver = driver
			@c_5555 = CommandAddress.new(param[:c_5555], @region_read, @region_write)
			@c_2aaa = CommandAddress.new(param[:c_2aaa], @region_read, @region_write)
			@dest = CommandDestination.new(param[:c_dest], @region_read, @region_write)
			@dest.abs_offset_clear
		end
		def progressbar_index_set(i)
			@progressbar_index = i
		end
		def flash_parameter_set(memory_driver, romdata)
			@memory_driver = memory_driver
			@fifo = nil
			@programming_first_time = true
			@programming_done = romdata.size == 0
			@rom = romdata
			#if supported_device?(param[memory].command_address_mask)
		end
		def command_driver_set(d)
			@command_driver = d
		end
		def status_get_xx
			@command_driver.status_get(status_index)
		end
		def done?
			@programming_done
		end
		def idle?
			(status_get_xx & FLASH_STATUS_DONE_MASK) == 0
		end
		def fifo_data_ready?
			(status_get_xx & FLASH_STATUS_FIFO_BUSY) == 0
		end
		def suspended?
			(status_get_xx & FLASH_STATUS_FIFO_EMPTY_SUSPENDED) != 0
		end
		def erase_error?
			(status_get_xx & FLASH_STATUS_ERASE_ERROR) != 0
		end
		def fifo_empty?
			if @fifo == nil
				return true
			end
			@fifo.empty?
		end
		def sleepable?
			if done?
				return true
			end
			if fifo_empty? && idle?
				@programming_done = true
				#p "done!!"
				return true
			end
			if @fifo.empty? && fifo_data_ready?
				return true
			end
			s = status_get_xx
			if fifo_data_ready?
				return false
			end
			if (s & (FLASH_STATUS_PROGRAMMING_BUSY | FLASH_STATUS_ERASING)) == 0
				return false
			end
			if (s & FLASH_STATUS_FIFO_EMPTY_SUSPENDED) != 0
				return false
			end
			return true
		end
		def command_address_map(ar)
			address = {:type => Command::TYPE_RANDOM}
			address[:value] = ar.map{|t|
				t.xpu
			}
			address
		end
		def flash_id_get
			@programming_first_time = true
			@dest.abs_offset_clear
			c = Command::Content.new
			content_memorybank_update(c)
			Memory::read(@region_read, @dest.xpu, 2, -1, c)
			#autoselect
			address = command_address_map([@c_5555, @c_2aaa, @c_5555])
			data = {:type => Command::TYPE_RANDOM, :value => [
				0xaa, 0x55, 0x90
			]}
			c.access_set(address[:value].size, @region_write, Command::RW_WRITE_WITH_DATA, address, data)
			#note: auto select 導入後に 10 us の待ちが必要と W29C040 と W49F002U の datasheet に記載あり. 手持ちの W29C040 では待ちがなくても動く. W29C040 と W49F002U timechart に明確な記載はない. SST39SF040 には記載があるが 10 ns
			#TBD: 4 spi/memory cycle で 2.5 us - 3.0 us かかるので 4 memory cycle の dummy 挿入
			#id
			Memory::read(@region_read, @dest.xpu, 2, -1, c)
			#reset
			data[:value][-1] = 0xf0
			#Memory::write(@region_write, @dest.xpu, 0xf0, c)
			c.access_set(address[:value].size, @region_write, Command::RW_WRITE_WITH_DATA, address, data)
			#note: reset 後も 10 us の待ちが必要だが、一連の操作はここで終わる場合は待ちは不要.
			r = c.send_receive(Command::NUMBER_BUS_ACCESS_START, [0, 0])
			if r[0, 2] == r[2, 2]
				Message::puts("warning: autoselect is not worked in #{region_name}")
			end
			FlashMemoryParameters::byte_identify(r[2, 2])
		end
		def progressbar_range_set
			if $dma_puts
				return
			end
			Hamo::Progressbar::range_set(@progressbar_index, region_name, @dest.programming_length_get)
		end
		def command_fifo_new(d)
			if @rom.size == 0
				@programming_done = true
				return
			end
			src = @rom
			filled_page_data = Array.new(@dest.bank_size, 0xff)
			filled_page = []
			dest = []
			offset = 0
			while offset < src.size
				if src[offset, @dest.bank_size] == filled_page_data
					filled_page << offset
				else
					dest.concat(src[offset, @dest.bank_size])
				end
				offset += @dest.bank_size
			end
			@dest.programming_length_set(dest.size, filled_page)
			progressbar_range_set
			if dest.size == 0
				Message::puts "ROM image is filled by data 0xff"
				Fiber.raise
			end
			@fifo = Fifo.new(d, @region_write == Command::REGION_CPU_FLASH ? 0 : 1, dest, @progressbar_index)
			@fifo.command_flash_fifo_new
		end
		def content_flash_erase_address(chip_erase)
			command_address_map([
				@c_5555, @c_2aaa, @c_5555, @c_5555, @c_2aaa, 
				chip_erase ? @c_5555 : @dest
			])
		end
		def content_flash_erase(c)
			e = @memory_driver.erase_branch(@dest.abs_offset_get)
			if e == :erase_type_none
				return
			end
			chip_erase = e == :erase_type_entire
			address = content_flash_erase_address(chip_erase)
			data = {:type => Command::TYPE_RANDOM, :value => [
				0xaa, 0x55, 0x80, 0xaa, 0x55, 
				chip_erase ? 0x10 : 0x30
			]}
			c.access_set(address[:value].size, @region_write, Command::RW_WRITE_WITH_DATA, address, data, Command::FLASH_COMMAND_ERASE)
		end
		def program_content_make_page(c)
			address = {}
			address[:type] = Command::TYPE_SEQUENTIAL
			address[:value] = [@dest.xpu]
			data = {}
			data[:type] = Command::TYPE_FIFO_PAGE
			data[:value] = [0xde]
			c.access_set(@memory_driver.program_page_size, @region_write, Command::RW_WRITE_WITH_DATA, address, data, Command::FLASH_COMMAND_PROGRAMMING)
		end
		def program_content_make_write_address
			command_address_map([
				@c_5555, @c_2aaa, @c_5555, @dest
			])
		end
		def program_content_make_write(c)
			address = program_content_make_write_address
			data = {:type => Command::TYPE_RANDOM, :value => [0xaa, 0x55, 0xa0, 0xdd]}
			if @memory_driver.program_page_size != 1
				address[:value].pop
				data[:value].pop
			end
			c.access_set(address[:value].size, @region_write, Command::RW_WRITE_WITH_DATA, address, data, Command::FLASH_COMMAND_PROGRAMMING)
			if @memory_driver.program_page_size == 1
				return
			end
			program_content_make_page(c)
		end
		def program_page_size
			@memory_driver.program_page_size
		end
		def program_content_make_read(c, flag)
			flag[status_index] = @dest.program_content_make_read(c, flag, @memory_driver)
		end
		def content_random_write_set(c, region, a, d, flash_command = Command::FLASH_COMMAND_NONE)
			address = {:type => Command::TYPE_RANDOM, :value => a}
			data = {:type => Command::TYPE_RANDOM, :value => d} 
			c.access_set(address[:value].size, region, Command::RW_WRITE_WITH_DATA, address, data, flash_command)
		end
		def content_memorybank_update(c)
			if @programming_first_time
				@c_2aaa.bank_switch(@cartridge_driver, c)
				@c_5555.bank_switch(@cartridge_driver, c)
			end
			@dest.bank_switch(@cartridge_driver, c)
		end
		def command_memorybank_update_program_start(d)
			c = Command::Content.new
			content_memorybank_update(c)
			content_flash_erase(c)
			@command_driver.flash_program_start(c, @region_write)
		end
		def programming_bank_switch_content_size_max
			@dest.abs_offset_clear
			@memory_driver.sector_assign
			s = []
			while @dest.abs_offset_lessthan?(@rom.size)
				c = Command::Content.new
				content_memorybank_update(c)
				content_flash_erase(c)
				s << c.close.size
				@dest.abs_offset_add
			end
			@dest.abs_offset_clear
			@memory_driver.sector_assign
			s.max
		end
		def programming_loop(d)
			if self.done?
				return
			end
			if @fifo.empty? && idle?
				@programming_done = true
				@fifo.progressbar_update
				return
			end
			if @fifo.empty?
				return
			end
			if idle?
				if fifo_data_ready?
					@fifo.command_flash_fifo_append
				end
				if @programming_first_time == false
					@dest.abs_offset_add
				end
				@dest.abs_offset_skip_filled_page
				str = self.region_name + '.abs '
				str += "$%05X" % @dest.abs_offset_get
				if $dma_puts
					Message::puts str
				end
				command_memorybank_update_program_start(d)
				@programming_first_time = false
			elsif fifo_data_ready? || suspended?
				@fifo.command_flash_fifo_append
			end
		end
		
		def compare
			if @rom.size == 0
				return
			end
			@dest.abs_offset_clear
			error = 0
			error_message = ''
			while @dest.abs_offset_lessthan?(@rom.size)
				if @dest.abs_offset_skip_filled_page
					next
				end
				c = Command::Content.new
				@dest.bank_switch(@cartridge_driver, c)
				c.send_receive(Command::NUMBER_BUS_ACCESS_START, [0, 0])
				str = @dest.compare(@rom, @progressbar_index)
				if str != ''
					error_message << str << "\n"
					error += 1
				end
				@dest.abs_offset_add
			end
			if error != 0
				Message::puts "compare error"
				Message::print error_message
				return
			end
			Message::puts "#{region_name}: compare ok"
			#Memory.compare(region_name, @rom, programmed_data)
		end
		
		def clear
			if @fifo == nil
				return
			end
			@fifo.command_flash_fifo_clear
		end
	end

	class ProgrammerCpu < ProgrammerBase
		def initialize(d, param)
			@region_read = Command::REGION_CPU_6502
			@region_write = Command::REGION_CPU_FLASH
			super(d, param)
		end
		def region_name
			"PRGROM"
		end
		def status_index
			0
		end
		def supported_device?(flash_command_address_mask)
			if @dest[:uniq_bank]
				return true
			end
			(flash_command_address_mask & (1 << 14)) == 0
		end
	end
	class ProgrammerPpu < ProgrammerBase
		def initialize(d, param)
			@region_read = Command::REGION_PPU
			@region_write = Command::REGION_PPU
			#p param; xxx
			super(d, param)
		end
		def region_name
			"CHRROM"
		end
		def status_index
			1
		end
		def supported_device?(flash_command_address_mask)
			if (flash_command_address_mask & (1 << 14)) == 0
				return true
			end
			#TBD: flash に A14 までを配置すること, 可変バンクが2つあること
			return true
		end
	end

	class ProgrammerBoth
		def initialize
			@programmer = {}
		end
		def align(num, v)
			mask = num - 1
			if (v & mask) == 0
				return v
			end
			v &= ~mask
			v += num
			v
		end
		def flash_buffer_init(d)
			d.flash_program_write(false)
			bankswitch_content_size = []
			@programmer.each{|key, t|
				if t.done?
					next
				end
				bankswitch_content_size << t.programming_bank_switch_content_size_max
			}
			buffer_size_total = d.flash_buffer_free & ~0x40
			#Message::puts "MCU buffersize total: #{buffer_size_total}"
			buffersizes = [
				bankswitch_content_size.max, d.content_size_get
			].map{|t|
				r = align(0x40, t)
				buffer_size_total -= r
				r
			}

			buffer_alignment_mask = ~(0x40 - 1)
			ar = []
			%w(PRGROM CHRROM).each{|key|
				if @programmer.key?(key)
					ar << @programmer[key].done?
				else
					ar << true
				end
			}
			buffersizes << 0 << 0
			case ar
			when [false, false]
				#pagesize の大きい領域からbuffersizeを確保する
				index = @programmer["PRGROM"].program_page_size >= @programmer["CHRROM"].program_page_size ? 2 : 3
				key = %w(x x PRGROM CHRROM)[index]
				s = buffer_size_total / 2
				s &= buffer_alignment_mask
				s &= ~(@programmer[key].program_page_size - 1)
				buffer_size_total -= s
				buffersizes[index] = s
				index ^= 1
				buffersizes[index] = (buffer_size_total & buffer_alignment_mask)
				#[0x40, 0x80, 0x240, 0x200]
			when [false, true], [true, false]
				key = ar[0] == false ? "PRGROM" : "CHRROM"
				index = key == "PRGROM" ? 2 : 3
				buffer_size_total &= buffer_alignment_mask
				buffer_size_total &= ~(@programmer[key].program_page_size - 1)
				buffersizes[index] = buffer_size_total
				#[0x40, 0x40, 0, 0x400]
			else
				xxx
			end
			#Message::puts("buffersizes: #{buffersizes.to_s}")
			d.flash_buffer_allocate(buffersizes)
		end
		REGION_VALUES_MEMORY = %w(PRGROM CHRROM)
		def address_assign(cartridge_driver, region_values)
			progressbar_index = 0
			REGION_VALUES_MEMORY.each{|key|
				t = region_values[key]
				if t[:enable] == false
					next
				end
				@programmer[key] = cartridge_driver.programming_address_assignment(t, key)
				if @programmer[key] == nil
					return false
				end
				@programmer[key].progressbar_index_set(progressbar_index)
				progressbar_index += 1
			}
			true
		end
		def flash_programming_main(cartridge_driver, region_values, arg)
			REGION_VALUES_MEMORY.each{|key|
				t = region_values[key]
				cartridge_driver.flash_data_layout(t, key, arg["flash:#{key}.layout"])
				if t[:enable] == false
					next
				end
				if key == "CHRROM"
					t[:enable] = !Memory::chararam_detection
					if t[:enable] == false
						next
					end
				end
			}
			if cartridge_driver.cartridge_initialize == false
				return
			end
			if address_assign(cartridge_driver, region_values) == false
				return
			end
			cartridge_driver.workram_control_set(false)
			cartridge_driver.irq_disable
			cartridge_driver.vram_connection_compare(region_values[:vram_mirror])

			REGION_VALUES_MEMORY.each{|key|
				t = region_values[key]
				if t[:enable] == false
					next
				end
				device_name = arg["flash:#{key}.device"]
				memory_driver = nil
				if device_name == "auto"
					memory_driver = @programmer[key].flash_id_get
					if memory_driver.command_address_mask == 0
						Message::puts("flash memory auto detection failed: #{memory_driver.name}")
						return
					end
					Message::puts("#{key} flash detected: #{memory_driver.name}")
				else
					memory_driver = FlashMemoryParameters::driver_get(device_name)
					if memory_driver == nil
						Message::puts("unknown flash memory: #{device_name}")
						return
					end
				end
				e = arg["flash:#{key}.erase"]
				v = nil
				case e
				when "chip"
					v = :erase_type_entire
				when "sector"
					v = :erase_type_sector
				when "none"
					v = :erase_type_none
				else
					Message::puts "unknown erase type: #{e}"
					Fiber.raise
				end
				@programmer[key].flash_parameter_set(memory_driver, t[:romdata])
				if memory_driver.erase_set_supported?(v) == false
					Message::puts "#{memory_driver.name} is not supported #{e} erasing"
					Fiber.raise
				end
			}
			
			d = Command::FlashDriver.new(@programmer)
			d.flash_buffer_free
			#$dma_puts = true
			flash_buffer_init(d)
			@programmer.each{|key, t|
				t.command_driver_set(d)
				if !t.done?
					t.command_fifo_new(d)
				end
			}
			if !$dma_puts
				Hamo::Progressbar::ready
			end
			if Hamo::simulator? == false
				Hamo::device.receive_timeout_set(8_000)
				#Hamo::usleep(200_000)
			end
			d.flash_program_write(true)
			
			#programming loop
			@programmer.each{|key, t|
				t.programming_loop(d)
				if t.erase_error?
					Message::puts 'erase error'
					xxx
				end
			}
			#$dma_puts = nil
			#$dma_puts = true
			i = 0
			#while @programmer[:cpu_program].done? == false || @programmer[:ppu_chara].done? == false
			while @programmer.find{|key, t| t.done? == false}
				#if @programmer[:cpu_program].sleepable? && @programmer[:ppu_chara].sleepable?
				if !@programmer.find{|key, t| t.sleepable? == false}
					i = 0
					#d.fifo_notify_wait(simulation, @programmer[:cpu_program].fifo_empty? && @programmer[:ppu_chara].fifo_empty?)
					d.fifo_notify_wait(!@programmer.find{|key, t| t.fifo_empty? == false})
				end
				@programmer.each{|key, t|
					if t.erase_error?
						Message::puts 'erase error'
						xxx
					end
					t.programming_loop(d)
				}
				i += 1
				if i >= 20
					Message::puts 'dead lock on programming loop'
					100.times{|i|
						Fiber.yield
					}
					Fiber.raise
				end
			end
			@programmer.each{|key, t|
				t.clear
			}
			if !$dma_puts
				Hamo::Progressbar::done
			end
			d.flash_buffer_free
			if !$dma_puts
				@programmer.each{|key, t|
					t.progressbar_range_set
				}
				Hamo::Progressbar::ready
				@programmer.each{|key, t|
					t.compare
				}
				Hamo::Progressbar::done
			end
		end
		def flash_id_get(cartridge_driver, region_values)
			region_values["CHRROM"][:enable] = cartridge_driver.charrom_bank_defined?
			if cartridge_driver.cartridge_initialize == false
				return
			end
			if address_assign(cartridge_driver, region_values) == false
				return
			end
			
			h = {}
			@programmer.sort{|a, b|
				b[0] <=> a[0] #PRGROM -> CHRROM に順番を確定させる
			}.each{|key, t|
				if key == "CHRROM" && region_values[key][:enable]
					if Memory::chararam_detection
						Message::puts "#{key}: RAM"
						next
					end
					Hamo::usleep(100_000)
				end
				r = t.flash_id_get
				Message::puts "#{key}: #{r.name}"
				if r.command_address_mask != 0
					h["flash:#{key}.device"] = r.name
				end
			}
			if h.size != 0
				Hamo::Textarea::flashdevicelist_item_select(h)
			end
		end
	end

	def self.memory_compare(name, src, dest)
		if src == dest
			puts name + " programming ok!"
			return
		end

		puts name + " programming failed..."
		offset = 0
		l = src.size
		while l >= 0x400
			offset = 0
			str = ''
			while offset < src.size
				diff = 0
				l.times{|i|
					if src[offset + i] != dest[offset + i]
						diff += 1
					end
				}
				if diff != 0
					str += sprintf("$%05x-$%05x:%d ", offset, offset+l-1, diff)
				end
				offset += l
			end
			if str != ''
				puts str.chomp
			end
			l /= 2
		end
		diff = 0
		str = ""
		src.size.times{|i|
			if src[i] != dest[i]
				diff += 1
				str += sprintf("$%05x:%02x_%02x ", i, src[i], dest[i])
				if str.size >= 60
					puts str
					str = ''
				end
			end
			if diff >= 0x20
				break
			end
		}
		if str != ''
			puts str
		end
		if diff >= 0x20
			puts "and more..."
		end
	end
	
	def self.gui_list_add(h, developer_mode)
		h["combo.layout"] = "disable,full"
		if developer_mode
			h["combo.layout"] << ",top,bottom"
		end
		h["combo.device"] = FlashMemoryParameters::devices_name_get
		h["combo.erase"] = "chip"
		if developer_mode
			h["combo.erase"] << ",sector,none"
		end
	end
end
