require Hamo::PROGRAM_DIR + '/message.rb'
require Hamo::PROGRAM_DIR + '/romdb.mrb'

=begin
disk image 取得処理について:
data block # は 1,2,3,4 を正常な値として、それ以外を見つけた場合はデータ末尾もしくはエラーとしてみなして処理している.
GAP 期間へレジスタを更新する RAM アダプタ特有の仕様が原因で正味以外のデータの取得は困難である. このため、非ライセンスソフトはデータが取得できない可能性が高い.

DUMP した編集前のイメージ内容: 
CPU address $4030.r, $4031.r, $4032.r のデータを連続していれている. GAP は含まれていない.
$4030.r.7 が 0 の場合は 1 byte で $4031 の中身は data $00 であるが GAP 開始の初期化直後に得られたデータで block start ではない. 本来はバグであるが block start のかわりになり処理の手間が省けたのでそのままにしている.
$4030.r.7 が 1 の場合でも block の末尾 3 byte は FDS format では破棄されている. offset -3, -2 は CRC, offset -1 は MCU 側の処理の過程で得られるもの.
$4032 のデータはいまのところ有効な情報はない.

data block #3 の解釈について: 
data block #3 offset 0x0d, 0x0e の filesize を信用して data block #4 の長さを得ている. filesize が規定より長い場合は CPU address $4025.w.6 の制御の都合で正しく情報が取れていない.
=end
class FDSSide
	include IdString
	def initialize()
		@identify = {}
	end
	def label_for_progressbar(side_number)
		label = @identify[:game_name]
		if label == nil || @identify[:database]
			"???"
		end
		side_qty = database("disk_side_qty")
		label += " #{(side_number + 1).to_s}/#{side_qty.to_s}"
		label
	end
	def identify_print
		side_qty = database("disk_side_qty")
		if side_qty == nil
			side_qty = 2
		end
		Hamo::Progressbar::range_set(0, label_for_progressbar(0), side_qty * 0xe000 * 3)
		Hamo::Progressbar::range_set(1, "", 0)
		Hamo::Progressbar::ready
		#Hamo::Progressbar::draw_index_set(0)
		#Hamo::Progressbar::add(@data.size)
	end
	def identify
		if @data[0] != 1
			@identify[:game_name] = '!error'
			Message::puts "error: can't identify diskdata"
			return
		end
		[
			[1, 14, :verification],
			[0x10, 3, :game_name],
			[0x14, 1, :game_version],
			[0x15, 1, :side_number],
			[0x16, 1, :disk_number]
		].each{|t|
			s = @data[t[0], t[1]]
			if s.size == 1
				s = s.shift
			else
				s = s.pack('C*')
			end
			@identify[t[2]] = s
		}
		@identify[:database] = FDSfile::SOFTWARE[@identify[:game_name]]
	end
	def diskimage_set(buf)
		@data = []
		@raw = buf.dup
		while buf.size != 0
			if (buf[0] & 0x80) == 0
				3.times{buf.shift}
				3.times{@data.pop}
				if buf.size < 3
					break
				end
				if (buf[0] & 0x80) == 0 || buf[1] == 0 || buf[1] > 4
					break
				end
			end
			buf.shift
			@data << buf.shift
			buf.shift
		end
		@data.concat(Array.new(65500 - @data.size, 0))
		identify
	end
	def game_name
=begin
		if @identify[:verification] != "*NINTENDO-HVC*"
			return nil
		end
=end
		return @identify[:game_name]
	end
	def disk_number
		@identify[:disk_number]
	end
	def side_number
		@identify[:side_number]
	end
	def save(f)
		f.write(@data.pack("C*"))
	end
	def save_raw(f)
		f.write([@raw.size].pack('V'))
		f.write(@raw.pack("C*"))
	end
	def database(key)
		if @identify[:database] == nil
			return nil
		end
		string = nil
		case key
		when "disk_side_qty"
			return @identify[:database][:disk_side_qty]
		when "title"
			string = TITLE
			key = :title
		when "info_serial"
			string = INFO_SERIAL
			key = :info_serial
		when "info_alt_title"
			string = INFO_ALT_TITLE
			key = :info_alt_title
		else
			xx
		end
		return string[@identify[:database][key]]
	end
	def database_get
		h = {}
		if @identify[:database] == nil
			return h
		end
		%w(disk_side_qty title info_serial info_alt_title).each{|key|
			h[key] = database(key)
		}
		h
	end
	def label_update(i)
		Hamo::Progressbar::draw_index_set(0)
		Hamo::Progressbar::add(0xe000 * 3 - @raw.size)
		Hamo::Progressbar::draw_index_set(-1)
		Hamo::Progressbar::label_update(0, label_for_progressbar(i))
	end
=begin
ヘッドの位置を知るレジスタがないため、時間を予測して待つ.
ヘッドの初期位置が定位置でない場合はかなりずれるので注意.
移動時間についても精度はとても低い.
sec
xxx 途中->内周
2.0 内周->外周 (この区間であることだけは $4032.r.1 から得られる)
6.5 外周->内周
0.5 猶予
=end
	def motor_wait
		free = 0xf000 - @raw.size / 3 #ディスクの容量も正確な値が不明かつ RAW でも大半が正味の部分であることに注意
		free = free.to_f / 0xf000
		sec = 6.5 * free
		sec += 0.5
		Hamo::usleep((sec * 10 ** 6).to_i)
	end
end

class FDSFile
	def initialize
		@side = []
	end
	def diskimage_set(buf)
		s = FDSSide.new
		s.diskimage_set(buf)
		@side << s
	end
	def gamename_to_side_qty
		r = @side[0].database("disk_side_qty")
		if r == nil
			return 2
		end
		r
	end
	def sort
		@side.sort{|a,b|
			r = a.game_name <=> b.game_name
			if r == 0
				r = a.disk_number <=> b.disk_number
			end
			if r == 0
				r = a.side_number <=> b.side_number
			end
			r
		}
	end
	class SaverFDS < FileSaver
		def initialize(arg, side)
			@suffix = "fds"
			@arg = arg
			@side = side
			@db = side[0].database_get
		end
		def save_file(f)
			header = Array.new(0x10, 0)
			header[0, 3] = "FDS".unpack("C*")
			header[3] = 0x1a
			header[4] = @side.size
			f.write(header.pack('C*'))
			@side.each{|t|
				t.save(f)
			}
		end
	end
	class SaverRAW < FileSaver
		def initialize(arg, side)
			@suffix = "raw"
			@arg = arg
			@side = side
			@db = side[0].database_get
		end
		def save_file(f)
			f.print ("RAW%d" % @side.size)
			@side.each{|t|
				t.save_raw(f)
			}
		end
	end
	def save(arg, raw = false)
		s = SaverFDS.new(arg, sort())
		s.save
		if raw == false
			return
		end
		s = SaverRAW.new(arg, sort())
		s.save
	end
	def motor_wait
		@side.last.motor_wait
	end
	def label_update
		@side.last.label_update(@side.size)
	end
end

def diskimage_raw_to_fds(arg)
	f = File.open(arg.shift, 'rb')
	header = f.read(4)
	sidenum = header[3].to_i
	if header[0,3] != "RAW" or sidenum > 4
		puts "header error"
		return
	end
	fds = FDSFile.new
	sidenum.times{
		l = f.read(4).unpack("V")[0]
		fds.diskimage_set f.read(l).unpack('C*')
	}
	f.close
	fds.save false
end
