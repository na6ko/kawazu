[
	'message.rb',
	'sbp_binary_command.rb',
	'sbp_bootloader.rb',
	'sbp_memoryaccess.rb',
	'middlelibrary.rb',
	'config.rb',
	'cartridge_driver.rb',
	'nesfile.rb',
	'flashmemory_driver.rb'
].each{|t|
	require Hamo::PROGRAM_DIR + '/' + t
}
[
	'generic_74161.rb',
	'disksystem.rb',
	'nintendo_mmc1_sxrom.rb', 'nintendo_mmcs.rb',
	'bandai.rb', 'irem.rb', 'jaleco.rb',
	'konami.rb', 'namco.rb', 'sunsoft.rb',
	'taito.rb'
].each{|t|
	require Hamo::PROGRAM_DIR + '/cartridge_drivers/' + t
}
module DriverManager
	class DriverAutoDetector
		def initialize(dummy)
		end
		def list_set
			#TBD: module 化して cartridge_driver.rb との重複内容を統合
			item = {
				"name" => self.class.name,
				"description_outline" => description_outline,
				"description_number" => description_number,
				"auto_detector" => auto_detector.to_s,
				"autodetect_attribute" => autodetect_attribute.to_s,
				"mapper_digits" => mapper_digits
			}
			Hamo::Textarea::driverlist_item_add(item)
		end
		def auto_detector
			true
		end
		def description_outline
			"hardware auto detector by nametable control registers"
		end
		def description_number
			""
		end
		def mapper_digits
			""
		end
		def autodetect_attribute
			DriverBase::AUTO_DETECTION_NOT_AVAILABLE
		end
		def new_by_mappernum(num)
			h = {0 => DriverCNROM, 1 => DriverSKROM, 2 => DriverUNROM, 3 => DriverCNROM, 4 => DriverTKROM, 72 => Driver74161JF17, 24 => DriverVRC6, 26 => DriverVRC6}
			h[num].new(true)
		end
		def find_by_nametablecontrol
			d = DriverManager::DRIVERS.map{|t|
				t.new(true)
			}
			d.delete_if{|t|
				t.auto_detector
			}
			#$debug_print = true
			d = d.sort{|a,b|
				if b.workram_have_writeprotection != a.workram_have_writeprotection
					next a.workram_have_writeprotection == false ? -1 : 1
				end
				#降順
				b.nametable_wirecount <=> a.nametable_wirecount
			}.find{|t|
				#p t.class.name
				if t.nametable_wirecount <= 2
					return nil
				end
				#p t.class.name
				if t.cartridge_initialize == false
					next
				end
				t.nametable_detection
			}
			if d == nil
				return nil
			end
			d
		end
	end
	DRIVERS = [
		DriverAutoDetector,
		DriverCNROM, DriverUNROM, DriverUNROMFlash, DriverGNROM, DriverAMROM, DriverBNROM,
		DriverSKROM, DriverSUROM, 
		DriverSXROM, DriverSZROM, DriverSOROM,
		DriverPxROM, DriverTKROM, DriverEKROM, DriverEWROM,
		DriverVRC1, DriverVRC2, DriverVRC4, DriverVRC3, DriverVRC6, DriverVRC7,
		DriverDRROM, DriverDRROMCHRROMA16, DriverDRROMNTCONROL, DriverDRROMDDS1,
		DriverNAMCO163, DriverNAMCO340,
		DriverX1_005, DriverX1_017, DriverTC0190, 
		DriverSUNSOFT1, DriverSUNSOFT2, DriverSUNSOFT3, DriverSUNSOFT4, DriverSUNSOFT5,
		DriverFCG2, DriverLZ93D50Standard, DriverLZ93D50FJUMP2,
		DriverTAMS1, DriverG101, DriverH3001,
		DriverSS88006,
		Driver74161JF05, Driver74161JF11, Driver74161JF13, 
		Driver74161JF16IF12, Driver74161JF17, Driver74161JF19, 
		Driver74161IF09,
		Driver74161WARA, Driver74161OEKAKIDS,
		DriverUN1ROM, DriverUNROM7408,
		DriverDiskSystem
	]

	class ActionBase
		def initialize(arg)
			@arg = arg
			@driver = nil
		end
		def panel
			"etc"
		end
		def driver_required?
			true
		end
		def list_generate(config)
			{
				"description" => description + device_name,
				"action" => self.class.name,
				"command" => "dummy -f #{Hamo::PROGRAM_DIR}/driver_manager.rb gui_action_start [action] #{arguments}".strip,
				"panel" => panel
			}
		end
		def list_set(config)
			Hamo::Textarea::actionlist_item_add(list_generate(config))
		end
	end
	#実質 CUI 用. 入力文字列はドライバ名からドライバ名を検索する.
	class ActionDriverList < ActionBase
		def description
			"show name and description for drivers"
		end
		def device_name
			""
		end
		def arguments
			"(find_word)"
		end
		def device_open
			true
		end
		def device_close(x)
		end
		def driver_required?
			false
		end
		def action
			#p @arg; xx
			DriverManager::DRIVERS.find_all{|t|
				t = t.new
				[t.class.name, t.description_outline].find{|s|
					s.upcase.include?(@arg["find_word"].upcase)
				}
			}.each{|t|
				str = ""
				t = t.new
				[t.class.name, t.description_outline, t.mapper_digits, t.description_number, t.autodetect_attribute.to_s].each{|s|
					str << s << "\t"
				}
				Message::puts str.strip
			}
		end
	end
	class ActionFirmwareVersionGet < ActionBase
		def description
			"get firmware version"
		end
		def driver_required?
			false
		end
		def action
			str = Command::version_get().pack('C*')
			Message.print str.gsub("\x00", "\n")
		end
	end
	class ActionDriverDetail < ActionBase
		def description
			"show driver details"
		end
		def action
			@driver.detail_print
		end
	end
	class ActionCartridge < ActionBase
		def initialize(arg)
			if arg == nil
				return
			end
			super(arg)
			@driver = DRIVERS.find{|t|
				t.name == arg["drivername"]
			}
			if @driver == nil
				Message::puts "error: driver #{arg["drivername"]} is not found."
				return
			end
			@driver = @driver.new(arg["drivername"] == 'DriverManager::DriverAutoDetector')
		end
	end
	class ActionRomDump < ActionCartridge
		def description
			"dump ROM"
		end
		def panel
			"dump"
		end
		def action
			@driver.rom_dump(@arg)
		end
	end
	class ActionRamRead < ActionCartridge
		def description
			"read backup RAM"
		end
		def action
			@driver.workram_read(@arg, {})
		end
	end
	class ActionRomRamDump < ActionRomDump
		def description
			"dump ROM and read backup RAM"
		end
		def action
			db = super()
			if db == nil
				return
			end
=begin
:disk_dump disksystem 用. bios の中身がデータベースに一致した場合に遷移する用.
:battery_detected データベースから取得した電池の有無. EEPROM の場合は電池がないので false になることに注意.
:voice ADPCM 録音用フラグ
:bwram 電池付きSRAMまたは EEPROM の容量
=end
			bwram = false
			if db.key?(:bwram)
				bwram = db[:bwram] != 0
			end
			if db[:disk_dump] || db[:battery_detected] || db[:voice] || bwram
				@driver.workram_read(@arg, db)
			end
		end
	end
	class ActionRamWrite < ActionCartridge
		def description
			"write backup RAM"
		end
		def action
			@driver.workram_write @arg
		end
	end

	class ActionProgram < ActionCartridge
		def description
			"program flash memory in game cartridge"
		end
		def arguments
			#flash:(PRG|CHR)ROM\.(layout|device|erase)
			#layout = full,disable,top,bottom ;default full
			#device = auto,AM29F040B,SST39SF040,W29C040 ;default auto
			#erase = chip,sector ;default chip
			str = "[drivername] [in:filename.nes]"
			%w(PRGROM CHRROM).each{|region|
				%w(layout device erase).each{|option|
					str += " (flash:#{region}.#{option})"
				}
			}
			str
			#"[drivername] [in:filename.nes]"
		end
		def panel
			"program.start"
		end
		def action
			if @driver.nametable_register_has_busconflict?
				Message::puts("this driver is not suported programming")
				return
			end
			if %w(PRGROM CHRROM).find{|region|
				@arg["flash:#{region}.layout"] != "disable"
			} == nil
				Message::puts ("Both layouts are disabled. Please enable any layout.")
				return
			end
			require Hamo::PROGRAM_DIR + '/flashmemory_driver.rb'
			nesfile = Nesfile.Load(@arg["in:filename.nes"])
			s = {}
			action_init(s, nesfile)
			region_values = {}
			region_values[:vram_mirror] = nesfile[:vram_mirror]
			[
				[:cpu_program, "PRGROM"],
				[:ppu_chara, "CHRROM"]
			].each{|id, region|
				region_values[region] = {
					:dest_abs_address => 0
				}
				region_values[region][:romdata] = nesfile[id][:data]
			}
			action_start(s, region_values)
		end
	end
	
	class ActionFlashIDGet < ActionCartridge
		def description
			"get flash memory ID in game cartridge"
		end
		def panel
			"program.id"
		end
		def action
			if @driver.nametable_register_has_busconflict?
				Message::puts("this driver is not suported flash devices")
				return
			end
			require Hamo::PROGRAM_DIR + '/flashmemory_driver.rb'
			s = {}
			action_init(s)
			action_start(s, {"PRGROM" => {:enable => true}, "CHRROM" => {:enable => true}})
		end
	end
	#communicate to MCU
	module DeviceSBP
		def device_name
			"@SBP"
		end
		def device_open
			if Hamo::usbcdc_open(@arg["portname"], @arg["developer_mode"]) == false
				return false
			end
			if driver_required? == false
				return true #version_get で使う
			end
			if @driver == nil
				return false
			end
			if @driver.auto_detector == false
				return true
			end
			@driver = @driver.find_by_nametablecontrol
			if @driver == nil
				Message.puts "nametable control auto detection is failed"
				self.device_close(true)
				return false
			end
			Message.puts "detected by name table control register: #{@driver.class.name}"
			return true
		end
		def device_close(binarymode_exit)
			Hamo::device.close(binarymode_exit)
		end
	end
	class ActionFirmwareVersionGetSBP < ActionFirmwareVersionGet
		include DeviceSBP
		def arguments
			"(portname)"
		end
	end
	class ActionFirmwareProgramSBP < ActionBase
		def arguments
			"[in:filename.bin] (portname)"
		end
		def description
			"program firmware into MCU"
		end
		def device_name
			"@SBP"
		end
		def driver_required?
			false
		end
		def device_open
			filename = @arg["in:filename.bin"]
			Message::puts @arg.to_s
			if File.exist?(filename) == false
				Message::printf "file %s is not found.\n", filename
				return false
			end
			f = File.open(filename, 'rb')
			binstr = f.read(0x7000)
			buf = binstr.unpack('v*')
			f.close
			
			@address = buf.shift
			buf[0, 4].each{|t|
				@address ^= t
			}
			size = buf[2] * 2
			if @address < 0x1800 || (@address + size) >= 0x8000
				Message::printf "programming address range error. 0x%x..0x%x", @address, @address + size
				return false
			end
			sum = 0
			buf.each{|t|
				sum += t
				sum &= 0xffff
			}
			if sum != 0
				Message::puts "checksum error"
			end
			@buf = binstr[2, size].unpack('C*')
			Hamo::usbbootloader_open(@arg["portname"])
		end
		def device_close(binarymode_exit)
			Hamo::device.close
		end
		def action
			Bootloader.program_main(@buf, @address)
		end
	end 
	class ActionRomDumpSBP < ActionRomDump
		include DeviceSBP
		def arguments
			"(drivername) (out:dirname.nes) (portname)"
		end
	end
	class ActionRomRamDumpSBP < ActionRomRamDump
		include DeviceSBP
		def panel
			"dump"
		end
		def arguments
			"(drivername) (out:dirname.nes) (out:dirname.sav) (portname)"
		end
	end
	class ActionRamReadSBP < ActionRamRead
		include DeviceSBP
		def panel
			"dump"
		end
		def arguments
			"(drivername) (out:dirname.sav) (portname)"
		end
	end
	class ActionRamWriteSBP < ActionRamWrite
		include DeviceSBP
		def arguments
			"[drivername] [in:filename.sav] (portname)"
		end
	end
	class ActionCartridgeProgramSBP < ActionProgram
		include DeviceSBP
		def arguments
			super + " (portname)"
		end
		def list_generate(config)
			h = super
			Programmer::gui_list_add(h, config["developer_mode"] == "true")
			h
		end
		def action_init(s, nesfile)
		end
		def action_start(s, region_values)
			@arg[:simulation] = false
			t = Programmer::ProgrammerBoth.new
			t.flash_programming_main(@driver, region_values, @arg)
		end
	end
	class ActionCartridgeFlashIDGetSBP < ActionFlashIDGet
		include DeviceSBP
		def arguments
			"(drivername) (portname)"
		end
		def action_init(s)
		end
		def action_start(s, region_values)
			@arg[:simulation] = false
			Programmer::ProgrammerBoth.new.flash_id_get(@driver, region_values)
		end
	end
	ACTIONS_GUI = [
		ActionRomRamDumpSBP, ActionRamReadSBP, ActionRomDumpSBP,
		ActionRamWriteSBP,
		ActionFirmwareVersionGetSBP, ActionFirmwareProgramSBP,
		ActionCartridgeProgramSBP, ActionCartridgeFlashIDGetSBP
	]
	
	def self.action_start(arg)
		a = arg["action"].new(arg)
		if a.device_open == false
			return
		end
		r = true
		begin
			a.action
		rescue => e
			Message.puts "trace (most recent call last):"
			ee = e.backtrace.reverse
			i = ee.size
			ee.each{|t|
				Message.puts("    [%d] #{t}" % i, !arg["developer_mode"])
				i -= 1
			}
			Message.puts e.backtrace[0]
			Message.puts e.inspect
			Hamo::Textarea::errorlog_save("exception")
			r = false
			if arg.key?("usleep")
				Hamo::usleep(arg["usleep"])
			end
		end
		a.device_close(r)
		r
	end
	def self.gui_actions_for_develper
		require(Hamo::PROGRAM_DIR + '/driver_simulator.rb')
		[
			ActionRomDumpSim, 
			ActionRamWriteSim,
			ActionFirmwareVersionGetSim
		]
	end
	def self.gui_list_set(config)
		d = DRIVERS.map{|t|
			t.new(false)
		}
		d.each{|t|
			t.list_set
		}
		ACTIONS_GUI.each{|t|
			t.new(nil).list_set(config)
		}
		Hamo::Textarea::config_send(config)
		if config["developer_mode"] != "true"
			return
		end
		gui_actions_for_develper.each{|t|
			t.new(nil).list_set(config)
		}
	end
	def self.gui_action_start(arguments)
		time_start = Time.now
		Message::puts "---- #{time_start}; action start  ----"
		actions = ACTIONS_GUI
		if arguments["developer_mode"] == "true"
			actions += gui_actions_for_develper
		end
		arguments["action"] = actions.find{|t|
			t.name == arguments["action"]
		}
		action_start(arguments)
		delta = (Time.now - time_start)
		delta_str = "%4.1f sec" % delta
		if delta >= 60.0
			delta_str = sprintf("%dm%ds", delta.to_i / 60, delta.to_i % 60)
		end
		Message::puts "---- #{Time.now}; action end in #{delta_str} ----\n"
	end
end

def gui_list_set(arg)
	g = GuiForUser.new
	DriverManager::gui_list_set(g.config_load("list_set"))
end

class GuiForUser < Config
	def start(arg_array)
		arguments = config_load(arg_array)
		arg_array.each{|t|
=begin
			if t[-1] == '=' #緊急措置
				next
			end
=end
			if t[0, 2] != '--' || !t.include?('=')
				Message::puts("bad argument format: #{t}")
				return
			end
			t.gsub!('\=', "\0x1b")
			t = t.split('=')
			key = t[0][2..-1]
			value = t[1]
			value.gsub!("\0x1b", '=')
			arguments[key] = value
		}
		DriverManager::gui_action_start(arguments)
	end
end

def gui_action_start(arg)
	g = GuiForUser.new
	g.start(arg)
end

def flashid_test(arg)
	DriverManager::DRIVERS.each{|sc|
		Message::puts sc.name
		if sc.name.include?("AutoDetector")
			next
		end
		DriverManager::action_start({
			"drivername" => sc.name, 
			"action" => DriverManager::ActionCartridgeFlashIDGetSBP, 
			"portname" => "auto", "developer_mode" => "true",
			"usleep" => 11_000_000
		})
	}
end
