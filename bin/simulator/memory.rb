module SimulatorMemory
class Memory
	def address_mask(a)
		a &= @memory.size - 1
	end
	def read(a)
		@memory[address_mask(a)]
	end
	def write(a, d)
	end
	def task
	end
end
class Ram < Memory
	def initialize(size)
		@memory = Array.new(size)
	end
	def write(a, d)
		@memory[address_mask(a)] = d
	end
end
class Rom < Memory
	def initialize(data)
		@memory = data
	end
end
class Flash < Rom
	include FlashMemoryParameters::ParameterFlash
	def initialize(debug_name, data = nil)
		if data == nil
			data = Array.new(memory_capacity, 0)
		end
		super(data)
		srand(Time.now.sec)
		@debug_name = debug_name
		@command = []
		@autoselect = false
		@toggle = 0
		@programming_wait = 0
		@program = {
			:data => nil,
			:status => false, :offset => 0
		}
		@AUTOSELECT_ENTER = [
			{:c => 0x5555, :d => 0xaa}, {:c => 0x2aaa, :d => 0x55},
			{:c => 0x5555, :d => 0x90}
		]
		@PROGRAM = [
			{:c => 0x5555, :d => 0xaa}, {:c => 0x2aaa, :d => 0x55},
			{:c => 0x5555, :d => 0xa0}
		]
		erase = [
			{:c => 0x5555, :d => 0xaa}, {:c => 0x2aaa, :d => 0x55},
			{:c => 0x5555, :d => 0x80}, {:c => 0x5555, :d => 0xaa},
			{:c => 0x2aaa, :d => 0x55}
		]
		@ERASE_SECTOR = erase.dup
		@ERASE_SECTOR << {:c => nil, :d => 0x30}
		@ERASE_CHIP = erase.dup
		@ERASE_CHIP << {:c => 0x5555, :d => 0x10}
		[@AUTOSELECT_ENTER, @PROGRAM, @ERASE_SECTOR, @ERASE_CHIP].each{|a|
		a.each{|b|
			b[:c] &= command_address_mask
		}}
	end
	def cmp(a, b)
		if a.size != b.size
			return false
		end
		a.size.times{|i|
			if a[i][:c] != b[i][:c] || a[i][:d] != b[i][:d]
				return false
			end
			if a[i][:d] != b[i][:d]
				return false
			end
		}
		true
	end
	def address_dump(t)
		str = ''
		t.each{|s|
			str += ("$%05X " % s[:a])
		}
		str.strip
	end
#	def write(a, d)
#	end
	def read(a)
		if @autoselect
			return read_autoselect(a)
		elsif @program[:status] #bit 6 のみサポート
			r = @toggle
			@toggle ^= 1 << 6
			@programming_wait -= 1
			if @programming_wait <= 0
				@program[:status] = false
				@memory[@program[:offset], @program[:data].size] = @program[:data]
				if $dma_puts
					#p @memory[@program[:offset]], @program[:data].size, @program[:data][0]
					puts(@debug_name + "flash toggle done")
				end
			end
			return r
		end
		super(a)
	end
	def read_autoselect(a)
		case a & autoselect_address_mask
		when 0
			return manufacturer_id
		when 1
			return device_id
		else #sector protect verify は未サポート
			return 0xff
		end
	end
end
class AM29F040B < Flash
	include FlashMemoryParameters::ParameterAM29F040B
	def write(a, d)
		@command << {:a => address_mask(a), :c => a & command_address_mask, :d => d}
		if @command.size > 6
			@command.shift
		end
		if (
			@command.size >= 6 && !cmp(@command[-4, 3], @PROGRAM) && (
				((a & 0x03ff) == 0x02aa && (a & 0x3fff) != 0x2aaa) || 
				((a & 0x03ff) == 0x0555 && (a & 0x3fff) != 0x5555)
			)
		)
			printf "%s ?%x ", @debug_name, a
		end
		if cmp(@command, @ERASE_CHIP)
			@program[:offset] = 0
			@program[:data] = Array.new(memory_capacity, 0xff)
			@programming_wait = 50 * 2;
			@program[:status] = true
			if $dma_puts
				puts "#{@debug_name} #{address_dump(@command)} chip erase"
			end
			return
		elsif cmp(@command[0, 5], @ERASE_SECTOR[0, 5]) && @ERASE_SECTOR[-1][:d] == d
			@program[:offset] = address_mask(a) & 0xf_0000
			@program[:data] = Array.new(0x10000, 0xff)
			@programming_wait = 10 * 2
			@program[:status] = true
			if $dma_puts
				puts "#{@debug_name} #{address_dump(@command)} sector erase"
			end
			return
		end
		if @command.size >= 4 && cmp(@command[-4, 3], @PROGRAM)
			@program[:status] = true
			@program[:offset] = address_mask(a)
			@program[:data] = [d]
			#@programming_wait = 4
			@programming_wait = 1 + rand(4)
			if $dma_puts
				puts @debug_name + address_dump(@command[-4, 4]) + "program"
			end
		elsif d == 0xf0
			@autoselect = false
			if $dma_puts
				puts @debug_name + "autoselect off"
			end
		end
		if @command.size >= 3 && cmp(@command[-3, 3], @AUTOSELECT_ENTER)
			@autoselect = true
			if $dma_puts
				puts "#{@debug_name} #{address_dump(@command[-3, 3])} autoselect on"
			end
		end
	end
end
class SST39SF040 < AM29F040B
	include FlashMemoryParameters::ParameterSST39SF040
end
=begin
During the byte-load cycle, the addresses are latched by the falling edge of either #CE or #WE, whichever occurs last. The data are latched by the rising edge of either #CE or #WE, whichever occurs first. 

If the host loads a second byte into the page buffer within a byte-load cycle time (TBLC) of 200 uS after the initial byte-load cycle, the W29C040 will stay in the page load cycle. 
Additional bytes can then be loaded consecutively. The page load cycle will be terminated and the internal write (erase/program) cycle will start if no additional byte is loaded into the page buffer. 
A8 to A18 specify the page address. All bytes that are loaded into the page buffer must have the same page address. A0 to A7 specify the byte address within the page. The bytes may be loaded in any order; sequential loading is not required.
=end
class W29C040 < Flash
	include FlashMemoryParameters::ParameterW29C040
	def initialize(debug_name, data)
		super(debug_name, data)
		@AUTOSELECT_EXIT = [
			{:c => 0x5555, :d => 0xaa}, {:c => 0x2aaa, :d => 0x55},
			{:c => 0x5555, :d => 0xf0}
		]
		@page_programming = 0
	end
	def write(a, d)
		if @page_programming != 0
			if @program[:data].size == 0
				@program[:offset] = address_mask(a)
			elsif (@program[:offset] & 0xffff_ff00) != (a & 0xffff_ff00)
				puts "programming offset is not much"
				Fiber.raise
			end
			@page_programming -= 1
			@program[:data] << d
			if @page_programming == 0
				@program[:status] = true
				@programming_wait = 40 * 2
				if $dma_puts
					puts "page programming is accepted@$%06x" % @program[:offset]
				end
			end
			return
		end
		@command << {:a => address_mask(a), :c => a & 0x7fff, :d => d}
		if @command.size > 6
			@command.shift
		end
		#chip erase のみ, sector erase なし
		if cmp(@command, @ERASE_CHIP)
			@program[:offset] = 0
			@program[:data] = Array.new(memory_capacity , 0xff)
			@programming_wait = 50 * 2;
			@program[:status] = true
			if $dma_puts
				puts address_dump(@command) +  "erase"
			end
			return
		end
		#page programming, 0x100 bytes
		if @command.size >= 3 && cmp(@command[-3, 3], @PROGRAM)
			@page_programming = 0x100
			@program[:data] = []
			if $dma_puts
				puts address_dump(@command[-3, 3]) + "page program start"
			end
			return
		end
		if @command.size >= 3 && cmp(@command[-3, 3], @AUTOSELECT_ENTER)
			@autoselect = true
			if $dma_puts
				puts address_dump(@command[-3, 3]) + "autoselect on"
			end
		end
		if @command.size >= 3 && cmp(@command[-3, 3], @AUTOSELECT_EXIT)
			@autoselect = false
			if $dma_puts
				puts address_dump(@command[-3, 3]) + "autoselect off"
			end
		end
	end
end

def self.flash_get(device_name, debug_name, memory_data = nil)
	r = nil
	case device_name
	when "AM29F040B"
		r = AM29F040B.new(debug_name, memory_data)
	when "W29C040"
		r = W29C040.new(debug_name, memory_data)
	when "SST39SF040"
		r = SST39SF040.new(debug_name, memory_data)
	end
	r
end
end
