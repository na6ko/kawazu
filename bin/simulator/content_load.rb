A = [
	["JF17_control_random", 
		64, 2, 32, 32, 224, 2, 49167, 49231],
	["JF17 fix x1",
		72, 2, 32, 224, 2, 49167, 49231],
	["LZ93D50 no-dummy",
		64, 2, 16, 16, 80, 2, 254, 255, 224, 2, 32768, 32769],
	["LZ93D50 control random",
		72, 1, 0, 72, 1, 16, 72, 1, 0, 72, 1, 16, 80, 4, 58, 254, 58, 255, 116, 1, 112, 1, 116, 1, 112, 1, 224, 4, 0, 32768, 0, 32769],
	#Assertion failed: ad_type == BUS_TYPE_FIXED, file ../dma_manager.c, line 501
	["LZ93D50 assert",
		64, 4, 0, 16, 0, 16, 80, 4, 58, 0, 58, 1, 116, 1, 112, 1, 116, 1, 112, 1, 224, 4, 0, 32768, 0, 32769],
	["LZ93D50 no-dummy",
		72, 2, 16, 80, 2, 254, 255, 224, 2, 32768, 32769]
]
CONTENT_TYPE = %w(buscontrol data-write address data-read)
AD_TYPE = %w(random seq fix)
BUSCONTROL_REGION = %w(CPU_6502 CPU_FLASH PPU VRAM)
BUSCONTROL_RW = %w(read write_data write_open close)
READ_VALID = %w(valid dummy error-2 error-3)
def content_load(data)
	while data.size != 0
		d = data.shift
		buscontrol_flash = d & 0b11
		d >>= 2
		bit3_2 = d & 0b11
		d >>= 2
		content_type = d & 0b11
		d >>= 2
		continue_end = d & 0b11
		length = data.shift
		str =  ("$%x " % length)
		str << CONTENT_TYPE[content_type]
		case content_type
		when 0, 1, 2 #buscontrol, data-write
			str << " " << AD_TYPE[bit3_2]
			times = 1
			if bit3_2 == 0
				times = length
			end
			if content_type < 2
				times.times{
					if content_type == 0
						d = data.shift
						str << " "
						str << BUSCONTROL_REGION[d >> 6]
						str << "-"
						str << BUSCONTROL_RW[(d >> 4) & 0b11]
					else
						str << (" $%02x" % data.shift)
					end
				}
			else
				times.times{
					str << (" $%04x" % data.shift)
				}
			end
		when 3 #data-read
			str << "-" << READ_VALID[bit3_2]
			str << (" $%x" % length)
		end
		puts str
		if continue_end == 0b11
			break
		end
	end
end

begin
	A.each{|t| 
		puts "--" + t.shift + "--"
		content_load(t)
	}
end
