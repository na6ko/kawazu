SIM_FILES = <<EOS
message middlelibrary 
simulator/debug simulator/cartridge
sbp_binary_command sbp_memoryaccess
cartridge_driver programmer
cartridge_drivers/nintendo_mmc1_sxrom
cartridge_drivers/nintendo_mmcs
nesfile
driver_manager
EOS
SIM_FILES.split.each{|t|
	require "#{Hamo::PROGRAM_DIR}/#{t}.rb"
}

def sim_read(arg)
	address = 0x8000
	if arg.size != 0
		address = arg.shift.to_i(0)
	end
	length = 0x100
	if arg.size != 0
		length = arg.shift.to_i(0)
	end
	h = {}
	orgdata = []
	0x2000.times{|i|
		orgdata << (i * 2)
	}
	h[:orgdata] = orgdata.pack('v*').unpack('C*')
	prg = SimulatorMemory::Rom.new(h[:orgdata])
	h[:cartridge] = SimulatorCartridge::PCB_CNROM.new(prg, prg, Nesfile::VRAM_MIRROR_V, 0)
	Hamo::simulator_open h
	f = Fiber.new{
		Hamo::device.port_init h
		(1..0x2001).each{|length|
			h[:reddata] = Memory::read(Command::REGION_CPU_6502, address, length)
			if h[:reddata] == h[:orgdata][address & 0x0fff, h[:reddata].size]
				puts "compare ok"
				r = true
			else
				j = address & 0x0fff
				puts "compare bad"
				h[:reddata].size.times{|i|
					if h[:reddata][i] != h[:orgdata][j + i]
						str = "offset:$%04X" % i
						[h[:orgdata][i], h[:reddata][i]].each{|t|
							str << (" $%02X" % t)
						}
						puts str
						break
					end
				}
				f = File.open("e:/hoge.bin", 'wb')
				f.write(h[:reddata].pack('C*'))
				f.close
				break
			end
		}
	}
	Hamo::device.task(f)
end

def mmc1_reg_reset
	Command.write(Command::REGION_CPU_6502, 1, {:type => Command::TYPE_RANDOM, :value => [0x8000]}, {:type => Command::TYPE_RANDOM, :value => [0x80]})
end

def mmc1_reg_write(address, data, connect_flash_programming_cpu = false, connect_flash_programming_ppu = false)
	dd = []
	5.times{
		dd << data
		data >>= 1
	}
	flag = 0
	if connect_flash_programming_cpu
		flag |= 1 << 6
	end
	if connect_flash_programming_ppu
		flag |= 1 << 5
	end
	
	Command.write(Command::REGION_CPU_6502, dd.length, {:type => Command::TYPE_FIXED, :value => [address]}, {:type => Command::TYPE_RANDOM, :value => dd}, flag)
end

=begin
CPU ROM
address
bit
17:14 MMC1 register / fixed bank $c000-$ffff->4'b1111, fixed bank $8000-$bfff -> 4'b0000
13:0  CPU A13:0

command address SST39F040 A14:0, AM29F040B A10:0
15'h5555[14:13] -> 2'b10, 15'h2aaa[14:13] -> 2'b01
=end
def flash_id_test(arg)
	ar = Array.new(2, 0)
	s = {:cartridge =>PCB_SKROM.new(SimAM29F040B.new(ar), SimAM29F040B.new(ar)), :receive_data => []}
	f = Fiber.new{
		port_init s
		Command.write(Command::REGION_CPU_6502, 2, {:type => Command::TYPE_RANDOM, :value => [0x8a7d, 0x1234]}, {:type => Command::TYPE_RANDOM, :value => [0x80, 0xab]})
		mmc1_reg_write(0x8000, 0)
		mmc1_reg_write(0xe000, 0)
		c_2aaa = 0x8000 | 0x2aaa #1'b0, 4'b0000, 13'h2aaa
		c_5555 = 0xc000 | 0x5555 #1'b0, 4'b1111, 13'h1555
#flash read ID は専用コマンドなく, write, read を組み合わせる
		Command.flash_id_read(Command::REGION_CPU_6502, Command::REGION_CPU_FLASH, 0x8000, c_2aaa, c_5555)
	}
	simulator_exec(f)
end

def sim_program(arg)
	testfile = arg.shift
	cpu = {:debugname => "cpu:"}
	ppu = {:debugname => "ppu:"}
	cpu[:memory] = arg.shift
	ppu[:memory] = arg.shift
	#default_device = "SST39SF040"
	default_device = "AM29F040B"

	r = Nesfile.Load(testfile)
	if r == nil
		puts 'nes file header error'
		return
	end
	#r[:cpu_program][:data] = r[:cpu_program][:data][0x0000, 0x4000]
	#r[:ppu_chara][:data] = r[:ppu_chara][:data][0, 0x4000]
	#r[:ppu_chara][:data] = []
	cpu[:parameter] = {
		:dest_abs_address => 0,
		:erase_request => :erase_type_sector
	}
	ppu[:parameter] = {
		:dest_abs_address => 0,
		:erase_request => :erase_type_sector
	}
	[cpu, ppu].each{|t|
		if t[:memory] == nil
			t[:memory] = default_device
		end
		t[:memory] = SimulatorMemory::flash_get(t[:memory], t[:debugname])
		t[:memory].parameter_get(t[:parameter])
	}
	sim = {:receive_data => []}
	case r[:mappernum]
	when 1
		sim[:cartridge] = SimulatorCartridge::PCB_SKROM.new(cpu[:memory], ppu[:memory])
		cpu[:programmer] = ProgrammerCpuSxROM.new(cpu[:parameter], r[:cpu_rom])
		ppu[:programmer] = ProgrammerPpuSxROM.new(ppu[:parameter], r[:ppu_rom])
		cpu[:programmer].ppu_programmer_set(ppu[:programmer])
		ppu[:programmer].cpu_programmer_set(cpu[:programmer])
	when 4
		d = DriverTKROM.new
		d.flash_program_init_cpu(cpu, r)
		d.flash_program_init_ppu(ppu, r)
		if r[:ppu_chara][:data].size == 0
			sim[:cartridge] = SimulatorCartridge::PCB_TNROM.new(cpu[:memory])
		else
			sim[:cartridge] = SimulatorCartridge::PCB_TKROM.new(cpu[:memory], ppu[:memory])
		end
	end
	[cpu, ppu].each{|t|
		if t[:programmer].erase_type_accepted? == false
			return
		end
	}

	$dma_puts = false
	#$dma_puts = true
	f = Fiber.new{
		port_init sim
		Programmer.flash_programming_main(cpu[:programmer], ppu[:programmer], true, 0x20)
	}
	simulator_exec(f)
end
