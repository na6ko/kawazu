require Hamo::PROGRAM_DIR + './flashmemory_driver.rb'
require Hamo::PROGRAM_DIR + './simulator/bus.rb'
require Hamo::PROGRAM_DIR + './simulator/memory.rb'

module SimulatorCartridge
	class Cartridge
		def initialize
			@prg_rom = nil
			@chrmem = nil
		end
		def bit_get(v, src, dest, xor = 0)
			v >>= src
			v &= 1
			v ^= xor
			v <<= dest
		end
		def task
		end
	end

	class PCB_SxROM < Cartridge
		def initialize
			@shift_count = nil
			@mmc_register = Array.new(4, 0b11111)
			@prg_ram = nil
			@shift_register = 0
		end
		def prg_ram_enabled
			(@mmc_register[3] & 0x10) == 0
		end
		def prg_rom_address(a)
			a14 = a & (1 << 14)
			case (@mmc_register[0] >> 2) & 0b11
			when 0, 1 #switch 32 KB at $8000, ignoring low bit of bank number
				a = nil #TBD
			when 2
				a &= 0x3fff
				if a14 == 0
					a |= 0 << 14
				else
					a |= @mmc_register[3] << 14
				end
			when 3
				a &= 0x3fff
				if a14 == 0
					a |= @mmc_register[3] << 14
				else
					a |= 0b11111 << 14
				end
			end
			a
		end
		def chrmem_address(a)
			register_data = @mmc_register[1 + ((a >> 12) & 1)]
			a &= 0x0fff
			a |= register_data << 12
			a
		end
		def write(region, a, d)
			case region
			when 0
				if a >= 0x8000 && a <= 0xffff && (d & 0x80) != 0
					@shift_count = -1
				end
				case a
				when 0x6000..0x7fff
					if prg_ram_enabled
						@prg_ram.write(a & 0x1fff, d)
					end
				when 0x8000..0xffff
					@shift_register >>= 1
					@shift_register |= (d&1) << 4
					@shift_count += 1
					if @shift_count >= 5
						@shift_count = 0
						register_address = a >> 13
						register_address &= 0b11
						@mmc_register[register_address] = @shift_register
					end
				end
			when 1
				@prg_rom.write(prg_rom_address(a), d)
			when 2
				case a
				when 0x0000..0x1fff
					@chrmem.write(chrmem_address(a), d)
				end
			end
		end
		def read(region, a)
			case region
			when 0
				case(a)
				when 0x6000..0x7fff
					if prg_ram_enabled
						return @prg_ram.read(a & 0x1fff)
					end
				when 0x8000..0xffff
					return @prg_rom.read(prg_rom_address(a))
				else
					return nil
				end
			when 2
				case(a)
				when 0x0000..0x1fff
					return @chrmem.read(chrmem_address(a))
				end
			end
			return nil
		end
	end
	class PCB_SKROM < PCB_SxROM
		def initialize(prg_rom, chrrom)
			super()
			@prg_ram = SimulatorMemory::Ram.new(0x2000)
			@prg_rom = prg_rom
			@chrmem = chrrom
		end
	end

	class PCB_TxROM < Cartridge
		def initialize(chara_reg_initial_value)
			@page_register = Array.new(8, 0)
			@page_address = nil
			@gp_register = Array.new(8, nil)
			if chara_reg_initial_value == nil
				return
			end
			6.times{|i|
				@gp_register[i] = chara_reg_initial_value
			}
		end
		def prg_ram_enabled
			(@gp_register[3] & 0x80) != 0
		end
		def prg_ram_writable
			(@gp_register[3] & 0x40) == 0
		end
		def prg_rom_address(a)
			a14_13 = (a >> 13) & 0b11
			v = nil
			case a14_13 ^ ((((@gp_register[0] >> 6) & 1)) << 1)
			when 0
				v = @page_register[6]
			when 1
				v = @page_register[7]
			when 2
				v = 0x3e
			when 3
				v = 0x3f
			end
			a &= 0x1fff
			a |= v << 13
			a
		end
		def chrmem_address(a)
			a12_a10 = (a >> 10) & 0b111
			a12_a10 ^= (((@gp_register[0] >> 7) & 1)) << 2
			case a12_a10
			when 0..3
				v = @page_register[a12_a10 >> 1]
				v &= 0xfe
				v |= a12_a10 & 1
			when 4..7
				v = @page_register[2 + (a12_a10 & 0b11)]
			else
				xxx
			end
			a &= 0x03ff
			a |= v << 10
			a
		end
		def write(region, a, d)
			case region
			when 0
				case a
				when 0x6000..0x7fff
					if prg_ram_enabled && prg_ram_writable
						@prg_ram.write(a & 0x1fff, d)
					end
				when 0x8000..0xffff
					register_address = a & 1
					register_address |= ((a >> 13) & 0b11) << 1
					@gp_register[register_address] = d
					if register_address == 1
						@page_register[@gp_register[0] & 0b111] = d
					end
				end
			when 1
				@prg_rom.write(prg_rom_address(a), d)
			when 2
				case a
				when 0x0000..0x1fff
					@chrmem.write(chrmem_address(a), d)
				end
			end
		end
		def read(region, a)
			case region
			when 0
				case(a)
				when 0x6000..0x7fff
					if prg_ram_enabled
						return @prg_ram.read(a & 0x1fff)
					end
				when 0x8000..0xffff
					return @prg_rom.read(prg_rom_address(a))
				else
					return nil
				end
			when 2
				case(a)
				when 0x0000..0x1fff
					return @chrmem.read(chrmem_address(a))
				end
			when 3
				return 0 #assert 回避
			end
			return nil
		end
	end
	class PCB_TNROM < PCB_TxROM
		def initialize(prg_rom, chara_reg_initial_value = nil)
			super(chara_reg_initial_value)
			@prg_ram = SimulatorMemory::Ram.new(0x2000)
			@prg_rom = prg_rom
			@chrmem = SimulatorMemory::Ram.new(0x2000)
		end
		def chrmem_address(a) #character RAM address bus is not connected MMC3 banks
			a & 0x1fff
		end
	end
	class PCB_TKROM < PCB_TxROM
		def initialize(prg_rom, chr_rom, chara_reg_initial_value = nil)
			super(chara_reg_initial_value)
			@prg_ram = SimulatorMemory::Ram.new(0x2000)
			@prg_rom = prg_rom
			@chrmem = chr_rom
		end
	end
	class PCB_LZ93D50 < Cartridge
		def initialize(prg_rom, chr_rom)
			@prg_rom = prg_rom
			@prg_reg = 0 #nil
			@chr_rom = chr_rom
			@chr_reg = Array.new(8, 0)
			@nt_reg = nil
			@reg_enable = false
			$dma_puts = true
		end
		def write(region, a, d)
			a &= 0xe00f
			case region
			when 0
				if (a >> 15) == 1 && @reg_enable == false
					Message::puts "warning: @reg_enable == false"
					xx
					return
				end
				#TBD: EEPROM registers
				case a
				when 0x8000..0x8007
					@chr_reg[a & 0b111] = d
				when 0x8008
					@prg_reg = d & 0xf
				when 0x800a
					@nt_reg = d & 0b11
				end
				if (0x6000..0xffff).cover?(a)
					@reg_enable = false
				end
			end
		end
		def read(region, a)
=begin
			if @reg_enable == false
				Message::puts "@reg_enable = true"
			end
=end
			@reg_enable = true
			case region
			when 0
				case a
				when 0x6000..0x7fff
					return (1 << 4) ^ 0xff #dummy ack
				when 0x8000..0xffff
					if @prg_rom == nil
						return 0xff #for EEPROM driver simulation
					end
					r = a & 0x4000 == 0 ? @prg_reg : 0b1111
					r <<= 14
					a &= 0x3fff
					return @prg_rom.read(r | a)
				else
					return 0xdd
				end
			when 2
				case a
				when 0x0000..0x1fff
					r = @chr_reg[a >> 10] << 10
					a &= 0x03ff
					return @chr_rom.read(r | a)
				end
			when 3
				d = 0xf0
				d |= ((a >> 13) & 1) << 1
				case @nt_reg
				when 0
					d |= (a >> 10) & 1
				when 1
					d |= (a >> 11) & 1
				when 2
					d |= 0
				when 3
					d |= 1
				end
				return d
			end
		end
	end
	module Vram_mapper_nametable_mux
		def nt_init(vram_mirror)
			@vram_a10_mux = vram_mirror == Nesfile::VRAM_MIRROR_V ? 10 : 11
		end
		def nt_out(a)
			d = a >> (13 - 1)
			d &= 0b10
			d ^= 0b10 #VRAM CS# = ~PPU A13
			d |= (a >> @vram_a10_mux) & 1 #VRAM A10 = MUX(PPU A10, PPU A11)
			return d ^ 0xff #74240 inverts bit 1:0
		end
	end
	class PCB_UNROM < Cartridge
		include Vram_mapper_nametable_mux
		def initialize(prg_rom, vram_mirror, reg_initial_value)
			super()
			@logic_74161_d = reg_initial_value
			@prg_rom = prg_rom
			@chrmem = SimulatorMemory::Ram.new(0x2000)
			nt_init(vram_mirror)
		end
		def gate(a, b)
			a | b
		end
		def prg_rom_address(a)
			absaddress = a & ((1 << 14) - 1)
			gate_y = gate(@logic_74161_d, (a & (1 << 14)) == 0 ? 0 : 0b1111)
			absaddress |= gate_y << 14
			absaddress
		end
		def read(region, a)
			case region
			when 0
				case(a)
				when 0x8000..0xffff
					return @prg_rom.read(prg_rom_address(a))
				end
			when 2
				case(a)
				when 0x0000..0x1fff
					return @chrmem.read(a)
				end
			when 3
				return nt_out(a)
			end
			return nil
		end
		def write(region, a, d)
			case region
			when 0
				case a
				when 0x8000..0xffff
					@logic_74161_d = read(region, a) & 0xf
				end
			when 2
				case a
				when 0x0000..0x1fff
					@chrmem.write(a, d)
				end
			when 3
			end
		end
	end

	class PCB_CNROM < Cartridge
		include Vram_mapper_nametable_mux
		def initialize(prg_rom, chr_rom, vram_mirror, reg_initial_value)
			super()
			@logic_74161_r = reg_initial_value
			@prg_rom = prg_rom
			@chr_rom = chr_rom
			nt_init(vram_mirror)
		end
		def chrmem_address(a)
			#diode protection の場合は 74161.Q[3:2], diode の方向, A10, A12 の接続を設定する
			(((@logic_74161_r) & 0b11) << 13) | a
		end
		def read(region, a)
			case region
			when 0
				case(a)
				when 0x8000..0xffff
					return @prg_rom.read(a & 0x7fff)
				end
			when 2
				case(a)
				when 0x0000..0x1fff
					return @chr_rom.read(chrmem_address(a))
				end
			when 3
				return nt_out(a)
			end
			return nil
		end
		def write(region, a, d)
			case region
			when 0
				case a
				when 0x8000..0xffff
					d = read(region, a) & 0b0011_0011
					@logic_74161_r = d | (d >> 2)
				end
			when 2
			when 3
			end
		end
	end

=begin
diskdrive の動作は先に実機で動作確認済みで、作成した C レイヤの
ドライバが動くだけの精度とする.
=end
	class PCB_Disksystem < Cartridge
		def initialize(prg_rom, rawdata)
			super()
			@prg_rom = prg_rom
			@prg_ram = SimulatorMemory::Ram.new(0x8000)
			@chrmem = SimulatorMemory::Ram.new(0x2000)
			@drive_control = nil
			@drive_head_ready = 1
			@head_position = -100
			@data_ready = 0
			@disk_data = 0
			@irq_line = 1
			@gap = false
			@disk_image = []
			while rawdata.size != 0
				c = rawdata.shift #$4030.r
				d = rawdata.shift #$4031.r
				rawdata.shift #$4032.r
				if c & 0x80 == 0x80
					@disk_image << d
				end
			end
		end
		def read(region, a)
			case region
			when 0
				case(a)
				when 0x4030 #logical drive status
					return 0x00 | (@data_ready << 7)
				when 0x4031 #disk data
					r = @disk_data
=begin
					if @irq_line == 0
						Message::puts("read disk data $%02X" % r)
					end
=end
					@disk_data = 0
					@irq_line = 1
					@data_ready = 0
					return r
				when 0x4032 #physical drive status
					return 0b100 | (@drive_head_ready << 1)
				when 0x4033 #external connector
					return 0x80
				when 0x6000..0xdfff
					return @prg_ram.read(a - 0x6000)
				when 0xe000..0xffff
					return @prg_rom.read(a & 0x1fff)
				end
			when 2
				case(a)
				when 0x0000..0x1fff
					return @chrmem.read(a)
				end
			when 3
				return nt_out(a)
			end
			return nil
		end
		def write(region, a, d)
			case region
			when 0
				case a
				when 0x4025
					@drive_control = d
					Message::printf("control:$%02x, head:%d\n", d, @head_position)
				when 0x6000..0xdfff
					@prg_ram.write(a - 0x6000, d)
				end
			when 2
				case a
				when 0x0000..0x1fff
					@chrmem.write(a, d)
				end
			end
		end
		def task
			loop{
				drive_task
				Fiber.yield
			}
		end
		def drive_task
			case @drive_control
			when 0x2f
				@drive_head_ready = 1
				@head_position = -100
			when 0x2d, 0x6d, 0xed, 0xfd
				@head_position += 1
				if @head_position == 0
					@drive_head_ready = 0
					@gap = true
					puts "head ready"
=begin
????
				else @head_position >= 20 * 0x0f000
					@drive_head_ready = 1
					@head_position = -100
=end
				end
				if @gap && @drive_control == 0xed
					@gap = false
					@block_byte_left = -1
					@block_byte_offset = 0
					@filesize = 0
				end
				if !@gap && @head_position >= 300 && @head_position % 20 == 0
					if @irq_line == 0
						Message::puts("data discarded...")
						Fiber.raise
					end
					@data_ready = 1
					@disk_data = @disk_image.shift
					if @block_byte_left == -1
						case @disk_data
						when 1
							@block_byte_left = 0x38
						when 2
							@block_byte_left = 2
						when 3
							@block_byte_left = 0x10
						when 4
							@block_byte_left = @filesize
						end
						@block_byte_left += 2
					end
					if @drive_control & 0x80
						@irq_line = 2
					end
					case @block_byte_offset
					when 0x0d
						@filesize = @disk_data
					when 0x0e
						@filesize |= @disk_data << 8
					end
					@block_byte_left -= 1
					@block_byte_offset += 1
					if @block_byte_left == 0
						@gap = true
					end
				end
				if @irq_line == 2
					interrupt_to_mcu
					@irq_line = 0
				end
			end
		end
	end

	class PCB_JF17 < Cartridge
		include Vram_mapper_nametable_mux
		def initialize(prg_rom, chr_rom, vram_mirror, reg_initial_value)
			super()
			@logic_74161_r = reg_initial_value
			@logic_74174_prg_r = 0
			@logic_74174_chr_r = 0
			@prg_rom = prg_rom
			@chr_rom = chr_rom
			nt_init(vram_mirror)
		end
		def read(region, a)
			case region
			when 0
				if (a & 0x8000) != 0
					q = 0b1111
					case(a)
					when 0x8000..0xbfff
						q = @logic_74174_prg_r
					end
					a &= 0x7fff
					a |= q << 14
					return @prg_rom.read(a)
				end
			when 2
				case(a)
				when 0x0000..0x1fff
					a &= 0x1fff
					a |= @logic_74174_chr_r << 13
					return @chr_rom.read(a)
				end
			when 3
				return nt_out(a)
			end
			return nil
		end
		def write(region, a, d)
			case region
			when 0
				case a
				when 0x8000..0xffff
					d = read(region, a)
					trigger = (@logic_74161_r ^ 0xff) & d
					if trigger & (1 << 7) != 0
						@logic_74174_prg_r = d & 0x0f
					end
					if trigger & (1 << 6) != 0
						@logic_74174_chr_r = d & 0x0f
					end
					@logic_74161_r = d & 0xf0
				end
			when 2
			when 3
			end
		end
	end
	
	class PCB_VRC6 < Cartridge
		#mode 1 のみ対応, ROM のみ対応
		def initialize(prgrom, chrrom)
			@prgrom = prgrom
			@chrrom = chrrom
			@prgrom_bank = Array.new(2, 0)
			@chrrom_bank = Array.new(8, 0)
		end
		def prgrom_address(a)
			case a
			when 0x8000...0xc000
				a &= 0x3fff
				a |= @prgrom_bank[0] << 14
			when 0xc000...0xe000
				a &= 0x1fff
				a |= @prgrom_bank[1] << 13
			when 0xe000..0xffff
				a &= 0x1fff
				a |= 0x1f << 13
			end
			a
		end
		def chrrom_address(a)
			if a < 0x2000
				h = a >> 11
				a &= 0x7ff
				a |= (@chrrom_bank[h] & 0xfe) << 10
			else
				h = a >> 10
				h &= 0b011
				h |= 0b100
				a &= 0x3ff
				a |= @chrrom_bank[h] << 10
			end
			a
		end
		def read(region, a)
			case region
			when 0
				if a >= 0x8000
					return @prgrom.read(prgrom_address(a))
				end
			when 2
				return @chrrom.read(chrrom_address(a))
			when 3
				d = bit_get(chrrom_address(a), 10, 0) | 0b10
				return d ^ 0xff
			end
			return nil
		end
		def write(region, a, d)
			case region
			when 0
				case a & 0xf000
				when 0x8000
					@prgrom_bank[0] = d & 0x0f
				when 0xc000
					@prgrom_bank[1] = d & 0x1f
				when 0xd000, 0xe000
					r = bit_get(a, 12, 2, 1)
					r |= bit_get(a, @r0, 0)
					r |= bit_get(a, @r1, 1)
					@chrrom_bank[r] = d
				end
			when 1
				case a
				when 0x8000..0xffff
					@prgrom.write(prgrom_address(a), d)
				end
			when 2
				case a
				when 0x0000..0x1fff
					@chrrom.write(chrrom_address(a), d)
				end
			end
		end
	end
	class PCB_VRC6B < PCB_VRC6
		def initialize(prgrom, chrrom)
			@r1 = 0
			@r0 = 1
			super(prgrom, chrrom)
		end
	end
end
