module SimulatorBus
	REGION = ["CPU_6502", "CPU_FLASH", "PPU", "VRAM"]
	RW = ["READ", "WRITE", "WRITE_OPEN"]
	TYPE = ["RAMDOM", "SEQUENTIAL", "FIXED"]
	DMACH_COMPLEX_CT = 0
	DMACH_COMPLEX_DW = 1
	DMACH_COMPLEX_AL = 2
	DMACH_COMPLEX_AH = 3
	DMACH_COMPLEX_DR = 4
	DMACH_SIMPLE_READ_AL = 0
	DMACH_SIMPLE_READ_AH = 1
	DMACH_SIMPLE_READ_DR = 2
	DMACH_SIMPLE_WRITE_DATA_DW = 0
	DMACH_SIMPLE_WRITE_DATA_AL = 1
	DMACH_SIMPLE_WRITE_DATA_AH = 2
	DMACH_SIMPLE_WRITE_OPEN_AL = 0
	DMACH_SIMPLE_WRITE_OPEN_AH = 1
extend self
	def ad_str(ad, format)
		str = TYPE[ad[:type]]
		ad[:value].each{|t|
			#str << (format % t)
			str += sprintf(format, t)
		}
		str
	end
	def ad_update(ad, d, increment_mask)
		case ad[:type]
		when 0
			d += 1
		when 1
			ad[:value][0] += 1
			ad[:value][0] &= increment_mask
		end
		d
	end
	#重要: 基板設計変更時にファームウェアだけの更新だけでは動かず、ここの各 bit 定義も更新すること
	def control_val_extract(t, ct)
		case ct
		when 0b10_0101 #CPU 6502 READ
			t[:region] = 0; t[:rw] = 0
		when 0b10_0011 #CPU 6502 WRITE WITH DATA
			t[:region] = 0; t[:rw] = 1
		when 0b10_0111 #CPU 6502 WRITE OPEN
			t[:region] = 0; t[:rw] = 2
		when 0b10_0010 #CPU FLASH WRITE
			t[:region] = 1; t[:rw] = 1
		when 0b10_1100 #PPU READ
			t[:region] = 2; t[:rw] = 0
		when 0b11_0000 #PPU WRITE
			t[:region] = 2; t[:rw] = 1
		when 0b01_0100 #VRAM connection
			t[:region] = 3; t[:rw] = 0
		when 0b10_0100 #standby
			t[:region] = 3; t[:rw] = 3
		else
			return
			bin = ''
			8.times{|i|
				bin << (ct & 1).to_s
				if i == 3
					bin << '_'
				end
				ct >>= 1
			}
			Message::puts("unknown control 0b#{bin.reverse}")
			Fiber.raise
		end
	end
	def dma_beat(channels, num)
		r = nil
		if channels[num][:src_label][0,3] == "RAM"
			r = channels[num][:src_value][channels[num][:index]]
			if (channels[num][:btctrl] & (1 << 10)) != 0
				channels[num][:index] += 1
			end
		end
		channels[num][:count] -= 1
		if channels[num][:count] != 0
			return r
		end
		channels[num][:count] = channels[num][:btcnt].dup
		channels[num] = channels[num][:descaddr]
		if channels[num] == nil
			return r
		end
		channels[num][:count] = channels[num][:btcnt].dup
		channels[num][:index] = 0
		return r
	end
	def address_get(t, index)
		a = dma_beat(t[:dma_channel], index + 0)
		a |= dma_beat(t[:dma_channel], index + 1) << 8
	end
	DMAC_BTCTRL_EVOSEL_BLOCK = (1 << 1)
	DMAC_BTCTRL_EVOSEL_BEAT = (3 << 1)
	DMAC_BTCTRL_BLOCKACT_MASK = (0b11 << 3)
	DMAC_BTCTRL_BLOCKACT_NOACT = 0 << 3
	DMAC_BTCTRL_BLOCKACT_INT = 1 << 3
	DMAC_BTCTRL_BLOCKACT_SUSPEND = 2 << 3
	DMAC_BTCTRL_BLOCKACT_BOTH = (3 << 3)
	DMA_UPDATE_INTERRUPT_BY_DMAC = 0
	DMA_UPDATE_INTERRUPT_BY_EVSYS = 1
	def bus_access_simple(t)
		#DMA simulation
		#skip the setup dummy cycles (= dma_channel_number - 1) for the DR channel
		t[:data_read] = []
		t[:dma_channel].size.times{|ch|
			if t[:dma_channel][ch] == nil
				next
			end
			case t[:dma_channel][ch][:src_label]
			when "SPI"
				if t[:dma_channel][ch][:dest_label] != "dummy"
					xx
				end
				#spi から read するときの最初の無効 byte の beat
				(t[:dma_channel].size - 1).times{
					dma_beat(t[:dma_channel], ch)
				}
			when "ADC"
				if (t[:dma_channel][ch][:btctrl] & DMAC_BTCTRL_EVOSEL_BEAT) == 0
					Message::puts ("(t[:dma_channel][ch][:btctrl] & DMAC_BTCTRL_EVOSEL_BEAT) == 0")
					Fiber.raise
				end
				#sample: 8kHz
				#data: saw 500 Hz
				#注: data depth 8 bit のみ
				delta = 8_000 / 500
				v = 0
				t[:dma_channel][ch][:btcnt].times{
					t[:data_read] << v
					v += delta
					if v >= 0x100
						v = 0
					end
				}
				t[:dma_channel][ch] = t[:dma_channel][ch][:descaddr]
				p t[:dma_channel][ch]
				#TBD: 割り込み発生未実装なのでシミュレーションで最後まで動かない
			end
		}

		case t[:rw]
		when 0 #READ
			log = {:prev => nil, :count => 0, :start => nil}
			ch_dr = DMACH_SIMPLE_READ_DR
			read_count = t[:dma_channel][ch_dr][:btcnt]
			a = address_get(t, DMACH_SIMPLE_READ_AL)
			2.times{
				t[:data_read] << nil
			}
			loop{
				if log[:start] == nil
					log[:start] = a
					log[:count] = 1
				elsif (log[:prev] + 1) != a && (a >> 8) != 0x3b && $dma_puts
					if log[:count] == 1
						printf "a:$%04x ", log[:start]
					else
						printf "a:$%04x+ $%x bytes ", log[:start], log[:count]
					end
					log[:start] = a
					log[:count] = 1
				else
					log[:count] += 1
				end
				d = t[:klass].read(t[:region], a)
				if t[:dma_channel][ch_dr][:dest_label] == "RAM"
					t[:data_read] << d
				end
				log[:prev] = a
				btctrl = 0
				if t[:dma_channel][DMACH_SIMPLE_READ_AH]
					btctrl = t[:dma_channel][DMACH_SIMPLE_READ_AH][:btctrl]
					a = address_get(t, 0)
				end
				read_count -= 1
				if read_count == 0
					if t[:dma_channel][ch_dr][:descaddr] == nil
						break
					end
					t[:dma_channel][ch_dr] = t[:dma_channel][ch_dr][:descaddr]
					case btctrl & DMAC_BTCTRL_BLOCKACT_MASK
					when DMAC_BTCTRL_BLOCKACT_BOTH
						dma_update(t, DMA_UPDATE_INTERRUPT_BY_DMAC)
						t[:suspend] = true
						while t[:suspend]
							Fiber.yield
						end
						#p t[:dma_channel][ch_ah]
						#p t[:dma_channel][ch_dr]
					when DMAC_BTCTRL_BLOCKACT_SUSPEND
						xx
					end
					read_count = t[:dma_channel][ch_dr][:btcnt]
				end
			}
		when 1 #WRITE with data
			while t[:dma_channel][0] != nil
				dw = dma_beat(t[:dma_channel], DMACH_SIMPLE_WRITE_DATA_DW)
				if dw == nil
					puts ("dw == nil")
					p t[:dma_channel][DMACH_SIMPLE_WRITE_DATA_DW]
				end
				a = address_get(t, DMACH_SIMPLE_WRITE_DATA_AL)
				if $dma_puts
					printf "*(%04x) = %02x; ", a, dw
				end
				t[:klass].write(t[:region], a, dw)
			end
		when 2 #WRITE without data, ROM outputs data to bank register input
			while t[:dma_channel][0] != nil
				a = address_get(t, DMACH_SIMPLE_WRITE_OPEN_AL)
				t[:klass].write(t[:region], a, t[:klass].read(t[:region], a))
			end
		end
	end
	def cartridge_bus_access(t)
#		if t[:static_mode] == false
#			p t
#		end
		dma_channel_number = 0
		t[:dma_channel].each{|c|
			prefix = "#%d " % dma_channel_number
			length = 0
			str = ""
			descriptor_history = []
			while c != nil
				length += c[:btcnt]
				str += "cnt:$%x" % c[:btcnt]
				c[:count] = c[:btcnt].dup
				c[:index] = 0
				#str += ", ctrl:$%04x" % c[:btctrl]
				str += ", src:%s" % c[:src_label]
				str += ", dest:%s" % c[:dest_label]
				n = c[:descaddr]
				if descriptor_history.include?(n)
					str += "@"
					length = nil
					break
				end
				descriptor_history << c
				c = n
				if c == nil
					str += "/"
				else
					str += " -> "
				end
			end
			if length == nil
				prefix += "infi/"
			else
				prefix += "$%x/ " % length
			end
			if $dma_chain_puts
				puts prefix + str
			end
			dma_channel_number += 1
			
		}
		
		#beat all channels
		if t[:static_mode]
			control_val_extract(t, t[:static_control])
			bus_access_simple(t)
			dma_update(t, DMA_UPDATE_INTERRUPT_BY_DMAC)
			return
		end
		
		str = ''
		ct_prev = nil
		t[:data_read] = []
		4.times{
			if t[:dma_channel].size == 5 && t[:dma_channel][4] != nil
				dma_beat(t[:dma_channel], 4)
			end
			t[:data_read] << nil
		}
		#p t
		debug = t[:dma_channel][4].dup
		while t[:dma_channel][0] != nil
			ct = dma_beat(t[:dma_channel], DMACH_COMPLEX_CT)
			control_val_extract(t, ct)
			if ct_prev != ct
				if $dma_puts
					puts str
				end
				if t[:rw] == 3
					str = 'DUMMY '
				else
					str = sprintf "%s %s ", REGION[t[:region]], RW[t[:rw]]
				end
			else
				str += ';'
			end
			ct_prev = ct
			
			dw = dma_beat(t[:dma_channel], DMACH_COMPLEX_DW)
			a = address_get(t, DMACH_COMPLEX_AL)
			dr = nil
			if t[:rw] == 0
				dr = t[:klass].read(t[:region], a)
			end
			if t[:dma_channel].size == 5 && t[:dma_channel][DMACH_COMPLEX_DR] != nil
				#p t[:dma_channel][DMACH_COMPLEX_DR][:count]
				dummy_read = t[:dma_channel][DMACH_COMPLEX_DR][:dest_label] == "dummy"
				dma_beat(t[:dma_channel], DMACH_COMPLEX_DR)
				if dummy_read
					dr = nil
				elsif t[:rw] == 1
					dr = 0xff
				end
				t[:data_read] << dr
			else
				dr = nil
			end
			case t[:rw]
			when 1
				t[:klass].write(t[:region], a, dw)
			when 2
				t[:klass].write(t[:region], a, dw)
			end
			
			str += sprintf("A:%04X DW:%02X", a, dw)
			if dr != nil
				str += " DR:%02X" % dr
			end
		end
		if $dma_puts && str != ''
			puts str
		end
		#p t[:data_read].size
		#p debug
		#p t[:data_read]
		dma_update(t, DMA_UPDATE_INTERRUPT_BY_DMAC)
	end
	def debug_bool_get(t)
		$dma_puts
	end
	def device_task_init(t)
		t[:delay] = 10
		t[:task] = {}
		t[:task][:cartridge] = Fiber.new{
			t[:klass].task
		}
		t[:task][:dma] = Fiber.new{
			cartridge_bus_access(t)
		}
	end
=begin
	def cartridge_task(t)
		if t == nil
			return
		end
		if t.key? :klass
			t[:klass].task
		end
	end
=end
	def device_task_resume(t)
		if t == nil
			return
		end
		t[:delay] -= 1
		if t[:delay] != 0
			return
		end
		t[:delay] = 10
		t[:task].each{|key, f|
			if f.alive?
				f.resume
			end
		}
	end
	def device_task_stop(t)
		t[:task] = {}
	end
end
