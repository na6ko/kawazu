class Config
	CONFIG = {
		"global" => {
			"portname" => "auto",
			"drivername" => "auto",
			"out:dirname.nes" => ".",
			"out:dirname.sav" => ".",
			"out:dirname.wav" => ".",
			"out:dirname.fds" => ".",
			"out:dirname.raw" => ".",
			"out:filename.format" => "[info_alt_title]", #TBD: OSの言語で初期値を切り替えた方がいい
			"find_word" => "",
			"developer_mode" => "false",
		}, "cartridge_program" => {}
	}
	def initialize
		%w(PRGROM CHRROM).each{|region|
			[
				%w(layout full),
				%w(device auto),
				%w(erase chip)
			].each{|option, value|
				CONFIG["cartridge_program"]["flash:#{region}.#{option}"] = value
			}
		}
	end
	def config_get(action, s)
		config = CONFIG["global"]
		if CONFIG.key? action
			config = CONFIG[action]
		end
		if s[0] == '(' && s[s.size - 1] == ')'
			s = s[1, s.size - 2]
			if config.key? s
				s = config[s]
			else
				s = CONFIG["global"][s]
			end
		end
		s
	end
	FILENAME_OUT_DIRNAME_USAGE = <<EOS
#out:dirname.xxx は拡張子 xxx に応じたパスへファイルを出力します.
#. は kawazu_xxx.exe と同じパスで .. は kawazu_xxx.exe からの相対パスを意味します.
#フォルダの区切りは / または \\ が同じ意味で利用できます. 
EOS
	FILENAME_FORMAT_USAGE = <<EOS
#データベースに一致した場合自動に置換できるキーワードは下記です.
#[info_serial] [title] [info_alt_title]
#info_serial: 型番. 例: HVC-IC
#title: アルファベットでのタイトル. 例: Ice Climber
#info_alt_title: 現地語などのタイトルの別記(ない場合もある) 例: アイスクライマー
#out:filename.format = [info_alt_title] #アイスクライマー
#out:filename.format = [info_serial]_[title] #HVC-IC_Ice Climber
#space_replaceを指定すると置換文字列内の空白文字( )を指定した文字で置換できます.
#ファイル名に利用できない文字 :*?<>|$& は削除または置換されます.
#out:filename.space_replace = _
EOS
	def default_file_write(filename)
		f = File.open(filename, 'w')
		f.puts "#[global] は全機能に有効です. ([dump] [Dump] [read] [write] で機能別に個別に設定可能. 重複した場合は個別設定を優先)"
		%w(global).each{|action|
			f.puts "[#{action}]"
			CONFIG[action].each{|key, value|
				if key == 'out:dirname.nes'
					f.print FILENAME_OUT_DIRNAME_USAGE
				end
				if key == 'out:filename.format'
					f.print FILENAME_FORMAT_USAGE
				end
				f.puts "#{key} = #{value}"
			}
		}
		f.close
	end
	def dircheck(path)
		if File.exist?(path) == false
			path.gsub("\\", '/')
			dirs = path.split('/')
			path = ''
			dirs.each{|dir|
				path << dir
				if path[-1] != ':' && File.exist?(path) == false
					Hamo::File::mkdir(path)
				end
				path << '/'
			}
		elsif File.directory?(path) == false
			Message::puts "error: #{path} is not directory"
			return nil
		end
		path.gsub("\\", '/')
		if path[-1] != '/'
			path << "/"
		end
		path
	end
	def config_load(action_name)
		filename = Hamo::USER_DIR + "/kawazu.cfg"
		if File.exist?(filename) == false
			default_file_write(filename)
			return CONFIG["global"].dup
		end
		f = File.open(filename)
		str = f.read(0x1000)
		f.close
		key = nil
		str.split("\n").each{|t|
			if t[0] == '#'
				next
			end
			if t[0] == '[' && t[-1] == ']'
				key = t.gsub('[', '').gsub(']', '')
				next
			end
			s = t.split('=')
			if s.size != 2
				next
			end
			if key == nil
				next
			end
			if CONFIG.key?(key) == false
				CONFIG[key] = {}
			end
			value = s[1].strip
			if s[0].strip[0, 11]  == "out:dirname"
				value = dircheck(value)
			end
			if value == nil
				return
			end
			CONFIG[key][s[0].strip] = value
		}
		arguments = CONFIG["global"].dup
		if CONFIG.key? action_name
			CONFIG[action_name].each{|key,val|
				arguments[key] = val
			}
		end
		arguments
	end
end
