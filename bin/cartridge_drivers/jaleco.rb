module Jaleco_uPD7756_interface
	include VoiceADC
=begin
	VOICE_RESET = 1 << VOICE_RESET_BIT
	VOICE_STANDBY = VOICE_RESET | VOICE_START
	VOICE_START = 1 << VOICE_START_BIT
	VOICE_DATA_MASK = VOICE_RESET | VOICE_START
	VOICE_NUMBER_MASK =  ((1 << VOICE_NUMBER_WIDTH) - 1) << VOICE_NUMBER_LSB
=end
	def voice_init(reset_bit, start_bit, number_lsb, number_width)
		@VOICE_RESET_POS = 1 << reset_bit
		@VOICE_START_POS = 1 << start_bit
		@VOICE_DATA_MASK = @VOICE_RESET_POS | @VOICE_START_POS
		@VOICE_RESET_VAL = @VOICE_DATA_MASK ^ @VOICE_RESET_POS
		@VOICE_START_VAL = @VOICE_DATA_MASK ^ @VOICE_START_POS
		@VOICE_STANDBY_VAL = @VOICE_DATA_MASK
		@VOICE_NUMBER_LSB = number_lsb
		@VOICE_NUMBER_MASK =  ((1 << number_width) - 1) << number_lsb
		
		c = Command::Content.new
		18.times{
			voice_register_write(c, @VOICE_RESET_VAL, @VOICE_DATA_MASK)
		}
		voice_register_write(c, @VOICE_STANDBY_VAL, @VOICE_DATA_MASK)
		c.send_receive(Command::NUMBER_BUS_ACCESS_START, [0, 0], {:progressbar_receive_index => 0})
		Hamo::usleep(1000)
	end
	def voice_request(i, sample_num)
		c = Command::Content.new
		[@VOICE_START_VAL, @VOICE_STANDBY_VAL].each{|d|
			voice_register_write(c, d, @VOICE_DATA_MASK, i << @VOICE_NUMBER_LSB, @VOICE_NUMBER_MASK)
		}
		c.send_receive(Command::NUMBER_BUS_ACCESS_START, [0, 0], {:progressbar_receive_index => 0})
	end
end

class DriverSS88006 < DriverBase
	include Jaleco_uPD7756_interface
	def description_outline
		"Jaleco SS88006"
	end
	def mapper_number
		18
	end
	def nametable_wirecount
		6 + 2 + 2 + 4
	end
	def bank_assignment
		@bank_register["PRGROM"] = {0x8000 => 0x8000, 0xa000 => 0x8002, 0xc000 => 0x9000}
<<EOS
PRGROM, rrr_rrrA_AAAA_AAAA_AAAA, 0x8000, 0xa000, 0xc000
PRGROM, 111_111A_AAAA_AAAA_AAAA, 0xe000
CHRROM,  rr_rrrr_rrAA_AAAA_AAAA, #{BANK_ASSIGN_PPUBANK_0x0400_8}
#{BANK_ASSIGN_GENERIC_WRAM}
EOS
	end
	def workram_control_set(enable)
		d = 0
		if enable
			d = 0b11
		end
		Memory.write(Command::REGION_CPU_6502, 0x9002, d)
	end
	def irq_disable
		Memory.write(Command::REGION_CPU_6502, 0xf001, 0)
	end
	def nametable_detection
		nt = []
		4.times{|i|
			Memory.write(Command::REGION_CPU_6502, 0xf002, i)
			nt << Memory.vram_assignment
		}
		nt == [
			NAMETABLE_SYSTEMRAM_A11, 
			NAMETABLE_SYSTEMRAM_A10, 
			NAMETABLE_SYSTEMRAM_L,
			NAMETABLE_SYSTEMRAM_L #?
		]
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 13
		Memory.write(Command::REGION_CPU_6502, @bank_register["PRGROM"][processor_address], [d & 0x0f, (d >> 4) & 0b11], c)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 10
		a = 0xa000
		#offset 
		#cpu_a[13:12] = ppu_a[12:11]
		#cpu_a[1] = ppu_a[10]
		aa = processor_address >> 10
		a += ((aa >> 1) & 0b11) << 12
		a |= (aa & 0b1) << 1
		Memory.write(Command::REGION_CPU_6502, a, [d & 0xf, d >> 4], c)
	end
	def workram_read(arg, db)
		if db[:battery_detected] 
			super(arg, db)
		end
		if db[:voice] != true
			return
		end
		voice_init(0, 1, 2, 6)
		workram_control_set(false)
		voice_record(arg, db["info_serial"], 24_000, VoiceADC::BITDEPTH_12)
	end
	def voice_register_write(c, writedata, data_mask, requested_address = 0, address_mask = 0)
		writedata ^= 0b11 #SS88006 inverts data bit 1:0
		Memory.write(Command::REGION_CPU_6502, 0xf003, writedata | requested_address, c)
	end
end
