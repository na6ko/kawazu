=begin
レジスタがないものまたはレジスタに汎用ロジックを使っているもの.
現状、バンクレジスタICは 74161 のみである. (バンク以外のレジスタは別のものもある)
=end
require Hamo::PROGRAM_DIR + '/cartridge_drivers/voice_adc.rb'
require Hamo::PROGRAM_DIR + '/cartridge_drivers/jaleco.rb'

class DriverCNROM < DriverWritebusconflicts
	def description_outline
		"generic NROM/SROM/CNROM"
	end
	def description_number
		"0,3"
	end
	def bank_assignment
#CHRROM は A14 までで、 A16:15 は実在しないがダイオードでのプロテクトとする.
<<EOS
PRGROM,    AAA_AAAA_AAAA_AAAA, 0x8000
CHRROM, rr_rrA_AAAA_AAAA_AAAA, 0x0000
EOS
	end
	def vram_connection_set(h)
		vram_connection_static_detect(h)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
=begin
bank register address CPU $8000-$FFFF
data
bit3:2 diode protection key
bit1:0 character ROM A14:13
=end
		register_data = (abs_address >> 13) & 0b00_0011
		register_data |= (abs_address >> (15 - 4)) & 0b11_0000
		register_write(c, register_data, 0b0011_0011)
	end
	def page_crc_uniq_size(rom, l)
		crc = []
		0.step(rom.size - 1, l){|i|
			crc << Hamo::crc32_calc(rom[i, l])
		}
		crc.uniq.size
	end
	def romimage_save(h, destfilename)
		if page_crc_uniq_size(h[:ppu_chara][:data], h[:ppu_chara][:data].size / 4) == 1
			#Message.puts "no protection"
			h[:mappernum] = 3
			h[:ppu_chara][:data] = h[:ppu_chara][:data][0, h[:ppu_chara][:data].size / 4]
			if page_crc_uniq_size(h[:ppu_chara][:data], 0x2000) == 1
				h[:mappernum] = 0
			end
			return Nesfile.save(h, destfilename)
		end
		#2023年4月24日付: diode がついてるであろうカートリッジを12本試したが検出はなかった.
		Message.puts "diode protection detected"
		Message.puts "please report to SBP devteam"
		Nesfile.save(h, destfilename)
	end
end

#----- GNROM and variants ----
#no fixed CPU bank and bus conflicts
class DriverGNROM < DriverWritebusconflicts
	def description_outline
		"generic GNROM, MHROM"
	end
	def mapper_number
		66
	end
	def bank_assignment
<<EOS
PRGROM, r_rAAA_AAAA_AAAA_AAAA, 0x8000
CHRROM,    rrA_AAAA_AAAA_AAAA, 0x0000
EOS
	end
	def vram_connection_set(h)
		vram_connection_static_detect(h)
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 15
		register_write(c, d << 4, 0b0011_0000)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 13
		d |= 0b0011_0000 #source の変化防止
		register_write(c, d, 0b0011_0011)
	end
end
class DriverAMROM < DriverWritebusconflicts
	def description_outline
		"generic AMROM"
	end
	def mapper_number
		7
	end
	#cpu address $8000-$ffff.w の data bit 4 が VRAM_A10 につながるがバス衝突のため自動判別なし
	def nametable_wirecount
		2 + 2 + 1
	end
	def bank_assignment
		s = <<EOS
PRGROM, rr_rAAA_AAAA_AAAA_AAAA, 0x8000
EOS
		s << BANK_ASSIGN_GENERIC_CHRRAM
		s
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 15
		register_write(c, d, 0b0000_0111)
	end
end
class DriverBNROM < DriverWritebusconflicts
	def description_outline
		"generic BNROM"
	end
	def mapper_number
		34
	end
	def vram_connection_set(h)
		vram_connection_static_detect(h)
	end
	def bank_assignment
		s = <<EOS
PRGROM, r_rAAA_AAAA_AAAA_AAAA, 0x8000
EOS
		s << BANK_ASSIGN_GENERIC_CHRRAM
		s
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 15
		register_write(c, d, 0b0000_0011)
	end
end
class Driver74161IF09 < DriverWritebusconflicts
	def description_outline
		"IREM LROG017-00, 74161x2"
	end
	def mapper_number
		77
	end
	def chararam_detection
		false
	end
	def bank_assignment
#cartridge の PPU memory map は chara (address 0x0800-0x1fff) に ROM と RAM が混在.
#この RAM は nametable としても利用 (address 0x2000-0x27ff).
#nametable 領域は mirror なしで RAM を配置している.
<<EOS
PRGROM, rrr_rAAA_AAAA_AAAA_AAAA, 0x8000
CHRROM,      rrr_rAAA_AAAA_AAAA, 0x0000
CHRRAM,        A_AAAA_AAAA_AAAA, 0x0800
EOS
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 15
		register_write(c, d, 0b0000_1111)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 11
		d <<= 4
		d |= 0x0f #write data source が変わるのを防止
		register_write(c, d, 0b1111_1111)
	end
end

#---- UNROM/UOROM and variants ----
module ModuleUNROM
	def mapper_number
		2
	end
	def bank_assignment
		@bank_register["PRGROM"] = {0x8000 => 0x8000}
		s = <<EOS
PRGROM, rr_rrAA_AAAA_AAAA_AAAA, 0x8000
PRGROM, 11_11AA_AAAA_AAAA_AAAA, 0xc000
EOS
		s << DriverBase::BANK_ASSIGN_GENERIC_CHRRAM
		s
	end
	def vram_connection_set(h)
		vram_connection_static_detect(h)
	end
end

class DriverUNROM < DriverWritebusconflicts
	include ModuleUNROM
	def description_outline
		"generic UNROM/UOROM, bus conflicts on write cycle"
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
=begin
bank register address CPU $8000-$FFFF
data
bit3:0 program ROM A17:14 (UOROM)
bit2:0 program ROM A16:14 (UNROM)
=end
		register_data = abs_address >> 14
		register_write(c, register_data, 0x0f)
	end
end

class DriverUNROMFlash < DriverBase
	include ModuleUNROM
	def description_outline
		"generic UNROM/UOROM, CPU R/W is decoded for ROM, flashable"
	end
	def submapper_number
		2
	end
end

class DriverUN1ROM < DriverWritebusconflicts
	def description_outline
		"Nintendo UN1ROM"
	end
	def mapper_number
		94
	end
	def bank_assignment
		s = <<EOS
PRGROM, r_rrAA_AAAA_AAAA_AAAA, 0x8000
PRGROM, 1_11AA_AAAA_AAAA_AAAA, 0xc000
EOS
		s << DriverBase::BANK_ASSIGN_GENERIC_CHRRAM
		s
	end
	def vram_connection_set(h)
		vram_connection_static_detect(h)
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		register_data = abs_address >> 14
		register_write(c, register_data << 2, 0b0111 << 2)
	end
end

class DriverUNROM7408 < DriverWritebusconflicts
	def description_outline
		"Nintendo UNROM, 7408 on 7432 footprint"
	end
	def mapper_number
		180
	end
	def bank_assignment
		s = <<EOS
PRGROM, 0_00AA_AAAA_AAAA_AAAA, 0x8000
PRGROM, r_rrAA_AAAA_AAAA_AAAA, 0xc000
EOS
		s << DriverBase::BANK_ASSIGN_GENERIC_CHRRAM
		s
	end
	def vram_connection_set(h)
		vram_connection_static_detect(h)
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		register_data = abs_address >> 14
		register_write(c, register_data, 0b0111)
	end
end

#---- 74161 based descrete boards ----
#These cartridges are produced by Bandai, Irem, Jaleco, Sunsoft or Taito

class Driver74161JF05 < DriverBase
	def description_outline
		"Jaleco JF-05, 74161"
	end
	def mapper_number
		87
	end
	def bank_assignment
<<EOS
PRGROM, AAA_AAAA_AAAA_AAAA, 0x8000
CHRROM, rrA_AAAA_AAAA_AAAA, 0x0000
EOS
	end
	def vram_connection_set(h)
		vram_connection_static_detect(h)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 13
		d =
			((d & 0b01) << 1) |
			((d & 0b10) >> 1)
		Memory.write(Command::REGION_CPU_6502, 0x6000, d, c)
	end
end

class Driver74161JF11 < DriverBase
	def description_outline
		"Jaleco JF-11, 74161"
	end
	def mapper_number
		140
	end
	def bank_assignment
<<EOS
PRGROM, r_rAAA_AAAA_AAAA_AAAA, 0x8000
CHRROM, r_rrrA_AAAA_AAAA_AAAA, 0x0000
EOS
	end
	def vram_connection_set(h)
		vram_connection_static_detect(h)
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 15
		d <<= 4
		Memory.write(Command::REGION_CPU_6502, 0x6000, d, c)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 13
		Memory.write(Command::REGION_CPU_6502, 0x6000, d, c)
	end
end

class Driver74161JF16IF12 < DriverWritebusconflicts
	def description_outline
		"Jaleco JF-16 and Irem IF-12, 74161x2"
	end
	def mapper_number
		78
	end
	def description_number
		"#{mapper_number}.1,#{mapper_number}.3"
	end
	def bank_assignment
<<EOS
PRGROM, r_rrAA_AAAA_AAAA_AAAA, 0x8000
PRGROM, 1_11AA_AAAA_AAAA_AAAA, 0xc000
CHRROM, r_rrrA_AAAA_AAAA_AAAA, 0x0000
EOS
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		register_data = abs_address >> 14
		register_write(c, register_data, 0b0111)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		register_data = abs_address >> 13
		register_data <<= 4
		register_write(c, register_data, 0b1111_0000)
	end
	def romimage_save(h, destfilename)
		nt = []
		[0, 1 << 3].each{|d|
			register_write(nil, d, 1 << 3)
			nt << Memory.vram_assignment
		}
		if nt == [NAMETABLE_SYSTEMRAM_L, NAMETABLE_SYSTEMRAM_H]
			h[:submappernum] = 1
		elsif nt == [NAMETABLE_SYSTEMRAM_A11, NAMETABLE_SYSTEMRAM_A10]
			h[:submappernum] = 3
		end
		super(h, destfilename)
	end
	
end

class Driver74161JF13 < DriverBase
	include Jaleco_uPD7756_interface
	def description_outline
		"Jaleco JF-13, 74161x2"
	end
	def mapper_number
		86
	end
	def vram_connection_set(h)
		vram_connection_static_detect(h)
	end
	def bank_assignment
<<EOS
PRGROM, r_rAAA_AAAA_AAAA_AAAA, 0x8000
CHRROM,   rrrA_AAAA_AAAA_AAAA, 0x0000
EOS
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 15
		d <<= 4
		Memory.write(Command::REGION_CPU_6502, 0x6000, d, c)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 13
		d |= (d << 4) & (1 << 6)
		Memory.write(Command::REGION_CPU_6502, 0x6000, d, c)
	end
	def workram_read(arg, db)
		voice_init(5, 4, 0, 4)
		voice_record(arg, "JF-13", 24_000, VoiceADC::BITDEPTH_12)
	end
	def voice_register_write(c, writedata, data_mask, requested_address = 0, address_mask = 0)
		#puts "%02X" % (writedata | requested_address)
		Memory.write(Command::REGION_CPU_6502, 0x7000, writedata | requested_address, c)
	end
end

class Driver74161JF17 < DriverWritebusconflicts
	include Jaleco_uPD7756_interface

	def description_outline
		"Jaleco JF-17, 74161, 74174x2"
	end
	def mapper_number
		72
	end
	def vram_connection_set(h)
		vram_connection_static_detect(h)
	end
	def bank_assignment
<<EOS
PRGROM, r_rrAA_AAAA_AAAA_AAAA, 0x8000
PRGROM, 1_11AA_AAAA_AAAA_AAAA, 0xc000
CHRROM, r_rrrA_AAAA_AAAA_AAAA, 0x0000
EOS
	end
	#連続で書き込みを入れると認識しないらしいので要調査
	def prgrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 14
		d &= 0x0f
		register_write(c, d, 0xff)
		d |= 1 << 7
		register_write(c, d, 0xff)
		true
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 13
		d &= 0x0f
		#register_write(nil, d, 0xff)
		register_write(c, d, 0xff)
		d |= 1 << 6
		register_write(c, d, 0xff)
	end
=begin
uPD7756 assignments
Reset# = 74161.QC <= CPU.D5
ST# = 74161.QD <= CPU.D4
I4:0 = CPU.A4:0 (this is address bus, not databus)
=end
	def workram_read(arg, db)
		if arg[:simulation]
			db[:voice] = true
			db["info_serial"] = "JF-17"
		end
		if !db[:voice]
			return
		end
		if cartridge_initialize == false
			return
		end
		rom_dump_memory("PRGROM", true)
		voice_init(5, 4, 0, 5)
		voice_record(arg, db["info_serial"], 24_000, VoiceADC::BITDEPTH_12)
	end
	def voice_register_write(c, writedata, data_mask, requested_address = 0, address_mask = 0)
		register_write(c, writedata, data_mask, requested_address, address_mask)
	end
end
class Driver74161JF19 < Driver74161JF17
	def description_outline
		"Jaleco JF-19, 74161, 74174x2"
	end
	def mapper_number
		92
	end
	def bank_assignment
<<EOS
PRGROM, 00_00AA_AAAA_AAAA_AAAA, 0x8000
PRGROM, rr_rrAA_AAAA_AAAA_AAAA, 0xc000
CHRROM,  r_rrrA_AAAA_AAAA_AAAA, 0x0000
EOS
	end
end

#基板の印字のワラのワの字は7かケかサかもしれない
#ワラ,ケラ,サラ,7ラでもソフト名とは関係していないようだ
class Driver74161WARA < DriverWritebusconflicts
	def description_outline
		"Bandai ワラ and Taito ARKANOID2, 74161x2"
	end
	def mapper_number
		152
	end
	def bank_assignment
<<EOS
PRGROM, r_rrAA_AAAA_AAAA_AAAA, 0x8000
PRGROM, 1_11AA_AAAA_AAAA_AAAA, 0xc000
CHRROM, r_rrrA_AAAA_AAAA_AAAA, 0x0000
EOS
	end
	#register bit7 is wired to VRAM A10
	def nametable_wirecount
		2 + 2 + 1
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 14
		register_write(c, d << 4, 0b0111_0000)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 13
		register_write(c, d, 0b0000_1111)
	end
end

class Driver74161OEKAKIDS < DriverGNROM
	def description_outline
		"Bandai Oeka Kids, 74161"
	end
	def mapper_number
		96
	end
#	def vram_connection_set(h)
#	end
#PPU memory map が複雑だが PRGROM dump の対応のみとする.
	def bank_assignment
<<EOS
PRGROM, r_rAAA_AAAA_AAAA_AAAA, 0x8000
CHRRAM,   rrrr_AAAA_AAAA_AAAA, 0x0000
EOS
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 15
		register_write(c, d, 0b0000_0011, @prgrom_prev_page)
	end
end
