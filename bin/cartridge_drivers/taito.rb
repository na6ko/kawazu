class DriverX1_005 < DriverBase
	def description_outline
		"Taito X1-005, standard assignment and variant"
	end
	def mapper_number
		nil
	end
	def description_number
		"80,201"
	end
	def nametable_register_mapped_on_6000_to_7fff?
		true
	end
	def bankhash_set(prgbank_increment)
		cpu_a = 0x8000
		reg_a = 0x7efa
		3.times{
			@bank_register["PRGROM"][cpu_a] = reg_a
			cpu_a += 0x2000
			reg_a += prgbank_increment
		}
		reg_a = 0x7ef0
		ppu_a = 0
		2.times{
			@bank_register["CHRROM"][ppu_a] = reg_a
			reg_a += 1
			ppu_a += 0x800
		}
		4.times{
			@bank_register["CHRROM"][ppu_a] = reg_a
			reg_a += 1
			ppu_a += 0x400
		}
	end
	def initialize(a)
		super(a)
		bankhash_set(2)
	end
	ROM_ASSIGN = <<EOS
PRGROM, rr_rrrA_AAAA_AAAA_AAAA, 0x8000, 0xa000, 0xc000
PRGROM, 11_111A_AAAA_AAAA_AAAA, 0xe000
CHRROM, rr_rrrr_rAAA_AAAA_AAAA, 0x0000, 0x0800
CHRROM, rr_rrrr_rrAA_AAAA_AAAA, 0x1000, 0x1400, 0x1800, 0x1c00
EOS
	def bank_assignment
		"WRAM,               AAA_AAAA, 0x7f00\n" + ROM_ASSIGN
	end
	def workram_control_set(enable)
		d = 0
		if enable
			d = 0xa3
		end
		Memory.write(Command::REGION_CPU_6502, 0x7ef8, [d, d])
	end
	def nametable_wirecount
		16 + 1 + #CPU A15:0, D0
		1 + 2
	end
	def bank_detection(a000, e000)
		a000 == e000
	end
	def nametable_variant?
		Memory.write(Command::REGION_CPU_6502, @bank_register["CHRROM"][0], [0, 0x80, 0, 0, 0x80, 0])
		Memory.vram_assignment == [2, 2, 3, 3, 2, 2, 3, 2, 0, 0, 1, 1, 0, 0, 1, 0]
	end
	def nametable_detection
		if nametable_variant?
			return true
		end
		d = []
		2.times{|i|
			Memory.write(Command::REGION_CPU_6502, 0x7ef6, [i])
			d << Memory.vram_assignment
		}
		if d != [NAMETABLE_SYSTEMRAM_A11, NAMETABLE_SYSTEMRAM_A10]
			return false
		end
=begin
X01-005 と X01-017 の nametable controll の仕様は同じなので ROM bank の挙動で判定する. ROM data での比較のため, erase ずみ flash では誤認する.
reg   X1-00 5     X1-017 
$7bfb $8000-$9fff $a000-$bfff
$7bfc $a000-$bfff $c000-$dfff
region はすべて CPU
=end
		Memory.write(Command::REGION_CPU_6502, 0x7efc, [0xff, 0], nil, 0xffff, 0xffff) #$7efc = 0; $7efb = 0xff
		a000 = Memory.read(Command::REGION_CPU_6502, 0xbf00, 0x100)
		e000 = Memory.read(Command::REGION_CPU_6502, 0xff00, 0x100)
		bank_detection(a000, e000)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 10
		Memory.write(Command::REGION_CPU_6502, @bank_register["CHRROM"][processor_address], d, c)
	end
	def romimage_save(h, destfilename)
		if mapper_number != nil
			super(h, destfilename)
			return
		end
		h[:mappernum] = nametable_variant? ? 207 : 80
		Nesfile.save(h, destfilename)
	end
end
class DriverX1_017 < DriverX1_005
	def description_outline
		"Taito X1-017, standrd assignment"
	end
	def mapper_number
		82
	end
	def initialize(a)
		super(a)
		bankhash_set(1)
	end
	def workram_control_set(enable)
		d = [0, 0, 0]
		if enable
			d = [0xca, 0x69, 0x84]
		end
		Memory.write(Command::REGION_CPU_6502, 0x7ef7, d)
	end
	def bank_detection(a000, e000)
		a000 != e000
	end
#市販品の配線のみとする
#note: PRGRAM は CPU address $6000-$73ff の 0x1400 bytes
	def bank_assignment
		"WRAM,       A_AAAA_AAAA_AAAA, 0x6000\n" +  ROM_ASSIGN
	end
	def cartridge_initalize
		#data bit 1: PPU A12 inversion
		Memory.write(Command::REGION_CPU_6502, 0x7ef6, 0)
	end
	def irq_disable
		Memory.write(Command::REGION_CPU_6502, 0x7efe, 0)
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		Memory.write(Command::REGION_CPU_6502, @bank_register["PRGROM"][processor_address], abs_address >> (13 - 2), c)
	end
end
class DriverTC0190 < DriverBase
	def description_outline
		"Taito TC0190 series"
	end
	def initialize(a)
		super(a)
		@chrbank = {
			0x0000 => 0x8002, 0x0800 => 0x8003,
		}
		ppu_a = 0x1000
		4.times{|i|
			@chrbank[ppu_a] = 0xa000 + i
			ppu_a += 0x400
		}
	end
=begin
IRQ 関連のレジスタをいじればそれの有無がわかり、 #33 か #48 か
判別できる予定.
IRQ 機能は TC0190 が載ってる基板に別ICを載せてる場合とそれが統合された
IC の TC0690 があるが、ソフトウェアとしての性能の違いはないらしい.
=end
	def mapper_number
		33
		#48
	end
	def cartridge_initialize
#TC0350 では初期状態で CPU address $c000-$dfff が open.
#CPU address $c000 data bit0 = 0 が必要.
#ドンドコドンのソフトでは $c000 = 0; $c001 = 0 としている.
		Memory.write(Command::REGION_CPU_6502, 0xc000, [0, 0])
		true
	end
	def nametable_wirecount
		3 + 1 + 1 + #CPU A15:13, A1:0 D6
		1 + 2
	end
	def nametable_detection
		d = []
		2.times{|i|
			Memory.write(Command::REGION_CPU_6502, 0x8000, [i << 6])
			d << Memory.vram_assignment
		}
		d == [NAMETABLE_SYSTEMRAM_A10, NAMETABLE_SYSTEMRAM_A11]
	end
	def bank_assignment
		@bank_register["PRGROM"] = {0x8000 => 0x8000, 0xa000 => 0x8001}
		cpu_a = 0x8002
		ppu_a = 0
		2.times{
			@bank_register["CHRROM"][ppu_a] = cpu_a
			cpu_a += 1
			ppu_a += 0x800
		}
		cpu_a = 0xa000
		4.times{
			@bank_register["CHRROM"][ppu_a] = cpu_a
			cpu_a += 1
			ppu_a += 0x400
		}
<<EOS
PRGROM, rr_rrrA_AAAA_AAAA_AAAA, 0x8000, 0xa000
PRGROM, 11_11AA_AAAA_AAAA_AAAA, 0xc000
CHRROM, rr_rrrr_rAAA_AAAA_AAAA, 0x0000, 0x0800
CHRROM, rr_rrrr_rrAA_AAAA_AAAA, 0x1000, 0x1400, 0x1800, 0x1c00
EOS
	end
end
