class DriverTAMS1 < DriverBase
	def description_outline
		"Irem TMAS1"
	end
	def mapper_number
		97
	end
	def nametable_wirecount
		1 + 1 + 1 + 2
	end
	def bank_assignment
		@bank_register["PRGROM"] = {0xc000 => 0x8000}
		s = <<EOS
PRGROM, 11_11AA_AAAA_AAAA_AAAA, 0x8000
PRGROM, rr_rrAA_AAAA_AAAA_AAAA, 0xc000
EOS
		s << DriverBase::BANK_ASSIGN_GENERIC_CHRRAM
		s
	end
	def nametable_detection
		nt = []
		[0, 1 << 7].each{|d|
			Memory.write(Command::REGION_CPU_6502, 0x8000, d)
			nt << Memory.vram_assignment
		}
		nt == [NAMETABLE_SYSTEMRAM_A11, NAMETABLE_SYSTEMRAM_A10]
	end
end

class DriverG101 < DriverBase
	def description_outline
		"Irem G-101"
	end
	def mapper_number
		32
	end
	def submapper_number
		@nametable ? 0 : 1
	end
	def nametable_wirecount
		3 + 1 + 1 + 1
	end
	def bank_assignment
		@bank_register["PRGROM"] = {0x8000 => 0x8000, 0xa000 => 0xa000}
		cpu_a = 0xb000
		ppu_a = 0
		8.times{
			@bank_register["CHRROM"][ppu_a] = cpu_a
			ppu_a += 0x400
			cpu_a += 1
		}
		s = <<EOS
PRGROM, rr_rrrA_AAAA_AAAA_AAAA, 0x8000,0xa000
PRGROM, 11_11AA_AAAA_AAAA_AAAA, 0xc000
CHRROM, rr_rrrr_rrAA_AAAA_AAAA, #{BANK_ASSIGN_PPUBANK_0x0400_8}
EOS
		s
	end
	def cartridge_initialize
		#bit1 = 0, cpu address $c000-$dfff is fixed bank
		Memory.write(Command::REGION_CPU_6502, 0x9000, 0b00)
		if @nametable == nil
			nametable_detection
		end
		true
	end
	def nametable_detection
		#VRC2 と誤認する
		#note: Major League cartridge wires the fixed nametable mode ti G-101
		nt = []
		[0, 1].each{|d|
			Memory.write(Command::REGION_CPU_6502, 0x9000, d)
			nt << Memory.vram_assignment
		}
		@nametable = nt == [NAMETABLE_SYSTEMRAM_A10, NAMETABLE_SYSTEMRAM_A11]
		@nametable
	end
end

class DriverH3001 < DriverG101
	def description_outline
		"Irem H3001"
	end
	def mapper_number
		65
	end
	def submapper_number
		nil
	end
	def nametable_wirecount
		7 + 1 + 2 + 1
	end
	#ROM bank register は G101 と同じ, IRQ と nametable は異なる.
	#ROM bank mode は address $9000 までは同じで bit は 1 から 7 に変更
	def cartridge_initialize
		#bit7 = 0, cpu address $c000-$dfff is fixed bank
		Memory.write(Command::REGION_CPU_6502, 0x9000, 0b00)
		true
	end
	def nametable_detection
		nt = []
		4.times{|i|
			Memory.write(Command::REGION_CPU_6502, 0x9001, i << 6)
			nt << Memory.vram_assignment
		}
		nt == [
			NAMETABLE_SYSTEMRAM_A10, NAMETABLE_SYSTEMRAM_L,
			NAMETABLE_SYSTEMRAM_A11, NAMETABLE_SYSTEMRAM_L
		]
	end
end
