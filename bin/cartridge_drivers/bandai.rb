class DriverFCG2 < DriverBase
	def description_outline
		"Bandai FCG-1, FCG-2"
	end
	def mapper_number
		16
	end
	def submapper_number
		4
	end
	def nametable_register_mapped_on_6000_to_7fff?
		(0x6000...0x8000).cover?(register_address)
	end
	def register_address
		0x6000
	end
	def register_write(register_offset, d, c = nil)
		Memory.write(Command::REGION_CPU_6502, register_address | register_offset, d, c)
	end
=begin
	def irq_disable
		register_write(0x0a, 0)
	end
=end
	def bank_assignment
		@bank_register["PRGROM"] = {0x8000 => register_address | 8}
		cpu_a = register_address
		ppu_a = 0
		8.times{
			@bank_register["CHRROM"][ppu_a] = cpu_a
			ppu_a += 0x400
			cpu_a += 1
		}
		<<EOS
PRGROM, rr_rrAA_AAAA_AAAA_AAAA, 0x8000
PRGROM, 11_11AA_AAAA_AAAA_AAAA, 0xc000
CHRROM, rr_rrrr_rrAA_AAAA_AAAA, #{BANK_ASSIGN_PPUBANK_0x0400_8}
EOS
	end
	def nametable_wirecount
		3 + 2 + 2 + #input CPU A15:13, A1:0, D1:0
		1 + #output PPU A13#
		4 #output PPU A11:10, H, L
	end
	def nametable_detection
		d = []
		4.times{|i|
			register_write(0x09, i)
			d << Memory.vram_assignment
		}
		d == [
			NAMETABLE_SYSTEMRAM_A10,
			NAMETABLE_SYSTEMRAM_A11,
			NAMETABLE_SYSTEMRAM_L,
			NAMETABLE_SYSTEMRAM_H
		]
	end
end

class DriverLZ93D50 < DriverFCG2
	def register_address
		0x8000
	end
	def nametable_wirecount
		1 + 2 + 2 + #input CPU A15, A1:0, D1:0
		1 + #output PPU A13#
		4 #output PPU A11:10, H, L
	end
=begin
連続で write cycle をいれると認識しない.
Program ROM の場合は bank が1つなので dummy read がなくても認識する.
=end
	def content_start(c, progressbar_receive_index = -1)
		c.send_receive(Command::NUMBER_BUS_ACCESS_START, [0, 0], {:progressbar_receive_index => progressbar_receive_index})
	end
	def register_write(register_offset, d, c = nil, dummy = true)
		exec = false
		if c == nil
			c = Command::Content.new
			exec = true
		end
		a = register_address | register_offset
		if dummy
			Memory.read_dummy(Command::REGION_CPU_6502, a, 1, c)
		end
		Memory.write(Command::REGION_CPU_6502, a, d, c)
		if exec
			content_start(c)
		end
	end
end

#FJUMP2 は Datach と区別がしづらく他の要素もかなり異なる. 自動検出不可の別クラスとする.
class DriverLZ93D50FJUMP2 < DriverLZ93D50
	def description_outline
		"Bandai LZ93D50 + FJUMP2"
	end
	def mapper_number
		153
	end
	def submapper_number
		nil
	end
	def autodetect_attribute
		AUTO_DETECTION_NOT_AVAILABLE
	end
	def bank_assignment
		super
		<<EOS
PRGROM, rrr_rrAA_AAAA_AAAA_AAAA, 0x8000
#PRGROM, r11_11AA_AAAA_AAAA_AAAA, 0xc000
#{BANK_ASSIGN_GENERIC_WRAM}
#{BANK_ASSIGN_GENERIC_CHRRAM}
EOS
	end
	def nametable_detection
		false
	end
	def workram_control_set(enable)
		register_write(0x0d, enable ? 1 << 5 : 0)
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		if processor_address == 0x8000
			register_write(8, (abs_address >> 14) & 0b1111, c)
		end
		a18 = abs_address >> 18
		4.times{|i|
			register_write(i, a18, c)
		}
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		xx
	end
end

class EEPROMDriverX24C0x
	RW_W = 0
	RW_R = 1
	DIR_W = 0 << 7
	DIR_R = 1 << 7
	SCL_L = 0 << 5
	SCL_H = 1 << 5
	SDA_W_L = 0 << 6
	SDA_W_H = 1 << 6
	SDA_W_BIT = 6
	SDA_R_H = 1 << 4
	
	DEVICE_LIST = [
		{:name => "X24C01", :address => 0, :i2c => false, :capacity => 0x80},
		{:name => "X24C02", :address => 0b1010 << 3, :i2c => true, :capacity => 0x100},
	]
	def initialize(driver)
		@driver = driver
	end
=begin
タイミングに関して, 1 read + 1 write cycle で 4.5 us 程度の時間がかかるので tHIGH の 4.0 us は満たしている.
=end
	def register_write(c, val, dummy = true)
		@driver.register_write(0xd, val, c, dummy)
	end
	def start(c)
		[
			DIR_W|SCL_L|SDA_W_L, 
			DIR_W|SCL_L|SDA_W_H, 
			DIR_W|SCL_H|SDA_W_H, 
			DIR_W|SCL_H|SDA_W_L, 
			DIR_W|SCL_L|SDA_W_L
		].map{|t|
			register_write(c, t)
		}
	end
	def stop(c)
		[
			DIR_W|SCL_L|SDA_W_L, 
			DIR_W|SCL_H|SDA_W_L, 
			DIR_W|SCL_H|SDA_W_H,
		].map{|t|
			register_write(c, t)
		}
	end
	def bit_write(c, bit)
		bit &= 1
		bit <<= SDA_W_BIT
		[DIR_W|SCL_L, DIR_W|SCL_H, DIR_W|SCL_L].each{|t|
			register_write(c, t | bit)
		}
	end
	def byte_write(c, val)
		8.times{
			val = ((val << 1) | (val >> 7)) & 0xff
			bit_write(c, val & 1)
		}
	end
	def ack(c, ack_read = true)
		[DIR_R|SCL_L, DIR_R|SCL_H].map{|t|
			register_write(c, t)
		}
		dummy = true
		if ack_read
			Memory.read(Command::REGION_CPU_6502, 0x6000, 1, -1, c)
			dummy = false
		end
		register_write(c, DIR_R|SCL_L, dummy)
	end
	def address_write(c, val, rw, ack_read = true)
		val <<= 1
		val |= rw
		byte_write(c, val)
		ack(c, ack_read)
	end
	def byte_read_content_make(c, length)
=begin
TBD: c.data_read_size の廃止
=end
		read_count = 0
		length.times{
			dummy = true
			valid_start = nil
			8.times{|i|
				register_write(c, DIR_R|SCL_L, dummy)
				register_write(c, DIR_R|SCL_H)
				if i == 0
					valid_start = c.data_read_size
				end
				Memory.read(Command::REGION_CPU_6502, 0x6000, 1, -1, c)
				dummy = false
			}
=begin
dummy と valid の切り替えのたびに data_read の descriptor を
消費してしまう対策. EEPROM read の最初から完了までのループ区間の
DR をすべて valid にする.
=end
			read_count += c.data_read_size - valid_start
			#c[:data_read].fill(Command::TYPE_READ_VALID, valid_start...c[:data_read].size)
			c.data_read_fill(valid_start)
			register_write(c, DIR_R | SCL_L, dummy)
			bit_write(c, 0)
		}
		#p c
		read_count
	end
	def byte_read_exec(c, length, memory, read_count)
		draw_index = 0
		Hamo::Progressbar::range_set(draw_index, "EEPROM", read_count)
		Hamo::Progressbar::range_set(1, "", 0)
		Hamo::Progressbar::ready
		r = @driver.content_start(c, draw_index)
		Hamo::Progressbar::done
		
		length.times{
			v = 0
			8.times{|i|
				v <<= 1
				if (r.shift & SDA_R_H) != 0
					v |= 1
				end
				if i != 7
					r.slice!(0, 3)
				end
			}
			memory << v
		}
	end
	def identify
		Command::busaccess_loopcount_set(Command::LOOP_TYPE_MEMORYACCESS, 1)
		#@device = DEVICE_LIST[1]
		#return
		DEVICE_LIST.each{|t|
			c = Command::Content.new
			start(c)
			address_write(c, t[:address], RW_R)
			8.times{|i|
				register_write(c, DIR_R|SCL_L)
				register_write(c, DIR_R|SCL_H)
			}
			stop(c)
			r = @driver.content_start(c)
			if r[0] & SDA_R_H == 0
				Message::puts "EEPROM: " + t[:name]
				@device = t
				return true
			end
		}
		return false
	end
	def capacity
		@device[:capacity]
	end
	def read
		#send address = 0
		c = Command::Content.new
		start(c)
		if @device[:i2c]
			address_write(c, @device[:address], RW_W, false)
			byte_write(c, 0) #memory address
			ack(c, false)
			start(c)
			address_write(c, @device[:address], RW_R, false)
		else
			address_write(c, 0, RW_R, false)
		end
		@driver.content_start(c)
		
		#data
		length = @device[:capacity]
		unit = 4 #実測上 4 でも 5 でも違いは見られなかった (送信待ちの時間が5だと長い)
		c = Command::Content.new
		read_count = byte_read_content_make(c, unit)
		loopcount = length / unit
		if length % unit != 0
			loopcount += 1
		end
		Command.busaccess_loopcount_set(Command::LOOP_TYPE_MEMORYACCESS, loopcount)
		memory = []
		read_count *= loopcount
		byte_read_exec(c, length, memory, read_count)
		
		#最後の read は ack なしで stop をいれる必要がある
		#前段でデータはすべて取っているので dummy の 8 clock -> stop
		c = Command::Content.new
		8.times{|i|
			register_write(c, DIR_R|SCL_L)
			register_write(c, DIR_R|SCL_H)
		}
		stop(c)
		@driver.content_start(c)

		return memory
	end
	def write(data)
		eeprom_address = 0
		unit = 4
		#X24C01, X24C02 ともに1度に書き込めるのは最大 4 bytes.
		(data.size / unit).times{
			c = Command::Content.new
			start(c)
			if @device[:i2c]
				address_write(c, @device[:address], RW_W)
			else
				address_write(c, eeprom_address, RW_W)
			end
			retry_count = 0
			loop{
				r = @driver.content_start(c)
				if r[0] & SDA_R_H == 0
					break
				end
				retry_count += 1
				if retry_count >= 20
					Message::puts "address ack error"
					return
				end
			}
			c = Command::Content.new
			if @device[:i2c]
				byte_write(c, eeprom_address)
				ack(c, false)
			end
			data[eeprom_address, unit].each{|t|
				byte_write(c, t)
				ack(c, false)
			}
			stop(c)
			@driver.content_start(c)
			eeprom_address += unit
		}
	end
end

class DriverLZ93D50Standard < DriverLZ93D50
	def description_outline
		"Bandai LZ93D50 standrd assignments and Datach"
	end
	def description_number
		"16.5, 159, 157"
	end
	def submapper_number
		5 #no EEPROM or 24C02
	end
	def bank_assignment
		super + BANK_ASSIGN_GENERIC_CHRRAM
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		register_write(8, abs_address >> 14, c)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		register_write(processor_address >> 10, abs_address >> 10, c)
	end
	#main cartridge internal EEPROM only
	#Datach sub cartrige external EEPROM is not supported
	def workram_read(arg, db)
		e = EEPROMDriverX24C0x.new(self)
		if e.identify == false
			Message::puts "EEPROM is not found"
			return
		end
		workram_read_file_save(arg, db, e.read)
	end
	def workram_write(arg)
		e = EEPROMDriverX24C0x.new(self)
		e.identify
		f = File.open(arg["in:filename.sav"], "rb")
		data = f.read(e.capacity).unpack('C*')
		f.close
		e.write(data)
	end
	def romimage_save(h, destfilename)
		if Hamo::simulator?
			super(h, destfilename)
			return #simulator does not support EEPROM registers
		end
		
		e = EEPROMDriverX24C0x.new(self)
		if e.identify
			h["bwram"] = e.capacity
		else
			h["bwram"] = 0
		end

		case h["bwram"]
		when 0
		when 0x80
			h[:mappernum] = 159
			return Nesfile.save(h, destfilename)
		when 0x100
			if h[:ppu_chara][:data].size == 0
				h[:mappernum] = 157
				return Nesfile.save(h, destfilename)
			end
		end
		
		super(h, destfilename)
	end
end

def X24C01_bitswap(arg)
	f = File.open(arg[0], "rb")
	src = f.read(0x80).unpack('C*')
	f.close
	swap = Array.new(src.size, 0)
	0x100.times{|i|
		v = 0
		8.times{|j|
			if (i & (1 << j)) != 0
				v |= 1 << (j ^ 0b111)
			end
		}
		swap[i] = v
	}
	dest = Array.new(src.size)
	src.size.times{|i|
		dest[swap[i] >> 1] = swap[src[i]]
	}
	p dest
	f = File.open(arg[1], 'wb')
	f.write(dest.pack('C*'))
	f.close
end
