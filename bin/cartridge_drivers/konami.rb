class DriverVRC3 < DriverBase
	def description_outline
		"Konami VRC3"
	end
	def mapper_number
		73
	end
	def vram_connection_set(h)
		vram_connection_static_detect(h)
	end
	def bank_assignment
		@bank_register["PRGROM"] = {0x8000 => 0xf000}
		str = <<EOS
PRGROM, r_rrAA_AAAA_AAAA_AAAA, 0x8000
#PRGROM, 1_11AA_AAAA_AAAA_AAAA, 0xc000
EOS
		str << BANK_ASSIGN_GENERIC_CHRRAM
		str
	end
	def irq_disable
		Memory.write(Command::REGION_CPU_6502, 0xc000, [0])
	end
end

class DriverVRC1 < DriverBase
	def description_outline
		"Konami VRC1"
	end
	def initialize(a)
		super(a)
		@chrrom_bank = [0, 0]
	end
	def mapper_number
		75
	end
	def nametable_wirecount
		4 + 1 + 1 + 2 #input A15:12, D0
	end
	def cpu_bank_c000_switch_test(c000, e000)
		c000 == e000
	end
	def nametable_detection
		#VRC1 と VRC2 の nametable の仕様は同じでこの方式で区別ができない. CPU address $e000-$ffff の読み出しで区別をする.
		#このため市販品であれば区別はできるが erase 済みの flash であれば区別はできない.
		d = []
		2.times{|i|
			Memory.write(Command::REGION_CPU_6502, 0x9000, i)
			d << Memory.vram_assignment
		}
		if d != [NAMETABLE_SYSTEMRAM_A10, NAMETABLE_SYSTEMRAM_A11]
			return false
		end
		Memory.write(Command::REGION_CPU_6502, 0xc000, 0x1f)
		c000 = Memory.read(Command::REGION_CPU_6502, 0xdf00, 0x100)
		e000 = Memory.read(Command::REGION_CPU_6502, 0xff00, 0x100)
		cpu_bank_c000_switch_test(c000, e000)
	end
	def bank_assignment
<<EOS
PRGROM, r_rrrA_AAAA_AAAA_AAAA, 0x8000, 0xa000, 0xc000
PRGROM, 1_111A_AAAA_AAAA_AAAA, 0xe000
CHRROM, r_rrrr_AAAA_AAAA_AAAA, 0x0000, 0x1000
EOS
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		Memory.write(Command::REGION_CPU_6502, processor_address, [abs_address >> 13], c)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		a = 0xe000 + processor_address
		d = abs_address >> 12
		Memory.write(Command::REGION_CPU_6502, a, [d & 0x0f], c)
		@chrrom_bank[processor_address >> 12] = d
		d = (@chrrom_bank[0] >> 4) << 1
		d |= (@chrrom_bank[1] >> 4) << 2
		Memory.write(Command::REGION_CPU_6502, 0x9000, [d], c)
	end
end

class DriverVRC2 < DriverVRC1
=begin
市販品の基板と VRC2 の接続は下記の3通り.
in   out  in      out
VRC2 CPU  CHRROM  VRC2.CHRROM
A1:0 A1:0 A16:A10 A16:A10 #23.3 (variant b)
A1:0 A0:1 A16:A10 A17:A11 #22.0 (variant a)
A1:0 A1:0 A16:A10 A16:A10 #25.3 (variant c)
=end
	def description_outline
		"Konami VRC2 - all variants"
	end
	def description_number
		"23.3,22.0,25.3"
	end
	def mapper_number
		nil
	end
	def cpu_bank_c000_switch_test(c000, e000)
		c000 != e000
	end
	def bank_assignment_vrc(variant_num)
		@ppu_address_to_register_address = {}
		ppu_a = 0
		0xb000.step(0xe000, 0x1000){|ah|
			0.step(2, 2){|al|
				@ppu_address_to_register_address[ppu_a] = ah | al
				ppu_a += 0x400
			}
		}
		variant_num_set("CHRROM", variant_num)
	end
	def bank_assignment
		bank_assignment_vrc(3)
<<EOS
PRGROM, rr_rrrA_AAAA_AAAA_AAAA, 0x8000, 0xa000
PRGROM, 11_11AA_AAAA_AAAA_AAAA, 0xc000
CHRROM, rr_rrrr_rrAA_AAAA_AAAA, #{BANK_ASSIGN_PPUBANK_0x0400_8}
EOS
	end
	def page_uniq(data)
		d = []
		0.step(data.size - 1, 0x400){|offset|
			d << Hamo::crc32_calc(data[offset, 0x400])
		}
		d.uniq.size
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		swap = [0, 0]
		d = BitTool::bit_get(abs_address, 10, 0, 8)
		case @variant["CHRROM"][:index]
		when 0
		when 1
			swap[0] = 1
			swap[1] = 0
			d <<= 1
		when 2
			swap[0] = 1
			swap[1] = 0
		end
		chrrom_bank_switch_write(c, processor_address, swap, d)
	end
	def chrrom_bank_switch_write(c, processor_address, swap, d)
		register_address = @ppu_address_to_register_address[processor_address]
		2.times{
			Memory.write(Command::REGION_CPU_6502, 
				BitTool::bit_swap(register_address, swap),
				d & 0x1f, c
			)
			d >>= 4
			register_address += 1
		}
	end
	ROMIMAGE_VARIANT = [
		{:m => 23, :s => 3, :suffix => '2b'},
		{:m => 22, :s => 0, :suffix => '2a'},
		{:m => 25, :s => 3, :suffix => '2c'}
	]
	def romimage_save(h, destfilename)
		save(h, destfilename, ROMIMAGE_VARIANT)
	end
	def save(h, destfilename, list)
		chara_data = h[:ppu_chara][:data]
		save_size = chara_data.size / @variant["CHRROM"][:num]
		#全通りファイルに出すと確認が煩雑なので、1番重複データが少ないものを出力する.
		pagenum = {}
		offset = 0
		list.size.times{|i|
			pagenum[i] = page_uniq(chara_data[offset, save_size])
			offset += save_size
		}
		variant = pagenum.sort{|a,b|
			a[1] <=> b[1]
		}
		variant = variant.find_all{|t|
			t[1] == variant.last[1]
		}
		if variant.size >= 2 #VRC2A
			variant.delete_if{|t|
				chara_data[t[0] * save_size + 0, 0x400] == 
				chara_data[t[0] * save_size + 0x400, 0x400]
			}
		end
		variant = variant[0][0]
		t = list[variant]
		offset = variant * save_size
		h[:mappernum] = t[:m]
		if t.key? :s
			h[:submappernum] = t[:s]
		end
		h[:ppu_chara][:data] = chara_data[offset, save_size]
		Nesfile.save(h, destfilename)
	end
end
module VRCNametable
	def vrc_nametable_detection(address)
		d = []
		4.times{|i|
			Memory.write(Command::REGION_CPU_6502, address, i)
			d << Memory.vram_assignment
		}
		d == [
			DriverBase::NAMETABLE_SYSTEMRAM_A10, 
			DriverBase::NAMETABLE_SYSTEMRAM_A11,
			DriverBase::NAMETABLE_SYSTEMRAM_L, 
			DriverBase::NAMETABLE_SYSTEMRAM_H
		]
	end
end
class DriverVRC4 < DriverVRC2
	include VRCNametable
=begin
市販品の基板と VRC4 の接続は下記の5通り.
VRC2 と違って CHRROM の address shift はないらしい.
in   out
VRC4 CPU
A1:0 A2:1 #21.1
A1:0 A0:1 #25.1
A1:0 A7:6 #21.2
A1:0 A2:3 #25.2
A1:0 A3:1 #23.2
=end
	def description_outline
		"Konami VRC4 - all variants"
	end
	def description_number
		"21.1,25.1,21.2,25.2,23.2,23.1"
	end
	def nametable_wirecount
		4 + 2 + 1 + 4 #input A15:12, D1:0
	end
	def nametable_detection
		vrc_nametable_detection(0x9000)
	end
	def bank_assignment
		bank_assignment_vrc(6)
#CPU address 0x8000-0x9fff は電源投入時に固定になっている.
#ここを可変にするには CPU address $9002 への書き込みが必要で全通りの対応が煩雑なためこの領域は使用しない.
#CHRROM A18 は存在するが、市販品には利用実績がないので対応しない.
<<EOS
PRGROM, rr_rrrA_AAAA_AAAA_AAAA, 0xa000
PRGROM, 11_111A_AAAA_AAAA_AAAA, 0xe000
CHRROM, rr_rrrr_rrAA_AAAA_AAAA, #{BANK_ASSIGN_PPUBANK_0x0400_8}
EOS
	end
	def irq_disable
		#TBD:
		#A1:0 = 2'b10 に data = 0 を書けばいいが全通りが煩雑
	end
	def workram_control_set(enable)
		#TBD: A1:0 = 2'b10 に同上
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		a = processor_address
		if a == 0xc000
			a = 0x8000
		end
		Memory.write(Command::REGION_CPU_6502, a, [abs_address >> 13], c)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		swap = [
			[1, 2], [1, 0], [6, 7], [3, 2], [2, 3], [0, 1]
		][@variant["CHRROM"][:index]]
		chrrom_bank_switch_write(c, processor_address, swap, BitTool::bit_get(abs_address, 10, 0, 8))
	end
	ROMIMAGE_VARIANT = [
		{:m => 21, :s => 1, :suffix => '4a'},
		{:m => 25, :s => 1, :suffix => '4b'},
		{:m => 21, :s => 2, :suffix => '4c'},
		{:m => 25, :s => 2, :suffix => '4d'},
		{:m => 23, :s => 2, :suffix => '4e'},
		{:m => 23, :s => 1, :suffix => '4f'}
	]
	def romimage_save(h, destfilename)
		save(h, destfilename, ROMIMAGE_VARIANT)
	end
end

class DriverVRC6 < DriverBase
	include BitTool
	def description_outline
		"Konami VRC6"
	end
	def description_number
		"24,26"
	end
	def initialize(a)
		super(a)
		@swap = nil
	end
	def nametable_wirecount
		6 + 5 + #CPU A15:12, A1:0, D4:0
		2 + #PPU A13#, $b003.bit4
		4 + 4 #PPU A10, PPU A11, H, L, R[7:4]
	end
	def nametable_detection
		Memory.write(Command::REGION_CPU_6502, 0xb003, [0x31])
		d = []
		[[0, 1, 1, 0], [1, 0, 0, 1]].each{|t|
			Memory.write(Command::REGION_CPU_6502, 0xe000, t)
			d << Memory.vram_assignment[8, 4]
		}
		if d != [[2, 3, 3, 2], [3, 2, 2, 3]]
			return false
		end
		#check variant via PPU banking mode #1
		Memory.write(Command::REGION_CPU_6502, 0xe000, [0, 0, 1, 0])
		case Memory.vram_assignment[9, 2]
		when [2, 3]
			@swap = [0, 1]
		when [3, 2]
			@swap = [1, 0]
		else
			return false
		end
		true
	end
	def cartridge_initialize
		nametable_detection
		if @swap == nil
			if @new_by_auto_detector == false
				Message::puts "error: variant detection is failed"
			end
			return false
		end
		@bank_register["PRGROM"] = {0x8000 => 0x8000, 0xc000 => 0xc000}
		ppu_a = 0
		cpu_a = 0xd000
		4.times{
			@bank_register["CHRROM"][ppu_a] = BitTool::bit_swap(cpu_a, @swap)
			cpu_a += 1
			ppu_a += 0x800
		}
		cpu_a = 0xe000
		4.times{
			@bank_register["CHRROM"][ppu_a] = BitTool::bit_swap(cpu_a, @swap)
			cpu_a += 1
			ppu_a += 0x400
		}
		true
	end
	def workram_control_set(enable)
		d = 0x31
		if enable
			d |= 0x80
		end
		Memory.write(Command::REGION_CPU_6502, 0xb003, d)
	end
	def irq_disable
		Memory.write(Command::REGION_CPU_6502, 0xf001, [0, 0])
	end
	def bank_assignment
#nametable mode #1, system VRAM is disabled
<<EOS
PRGROM, rr_rrAA_AAAA_AAAA_AAAA, 0x8000
#PRGROM, rr_rrrA_AAAA_AAAA_AAAA, 0xc000
PRGROM, 11_111A_AAAA_AAAA_AAAA, 0xe000
CHRROM, rr_rrrr_rAAA_AAAA_AAAA, #{BANK_ASSIGN_PPUBANK_0x0800_4}
#CHRROM, rr_rrrr_rrAA_AAAA_AAAA, 0x2000, 0x2400, 0x2800, 0x2c00
EOS
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		shift = 14
		if processor_address == 0xc000
			shift = 13
		end
		Memory.write(Command::REGION_CPU_6502, @bank_register["PRGROM"][processor_address], abs_address >> shift, c)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		#shift 回数固定
		Memory.write(Command::REGION_CPU_6502, @bank_register["CHRROM"][processor_address], abs_address >> 10, c)
	end
	def romimage_save(h, destfilename)
		h[:mappernum] = @swap == [0, 1] ? 24 : 26
		Nesfile.save(h, destfilename)
	end
	def programming_address_assignment(m, memory)
		if memory == "CHRROM"
			return super(m, memory)
		end
		#device ID 取得のためで、 programming 時は随時切り替える
		m[:c_2aaa] = {
			:absolute => 0x2aaa, :processor => [0x8000..0xbfff],
			:combined_bank => 0x8000..0xbfff,
			:switch => true, :uniq_bank => false
		}
		m[:c_5555] = {
			:absolute => 0x5555, :processor => [0xc000..0xdfff],
			:combined_bank => 0xc000..0xdfff,
			:switch => true, :uniq_bank => true
		}
		m[:c_dest] = m[:c_2aaa].dup
		return Programmer::ProgrammerCpuVRC6.new(self, m)
	end
end
module Programmer
=begin
              A14=0    A14=1    16   12
0x8000-0x9fff 14'h0000 14'h5555  r_rr0A <- programming area
0xa000-0xbfff 14'h2aaa n/a       r_rr1A <- programming area
0xc000-0xdfff 14'h5555 14'h2aaa  r_rrrA
0xe000-0xffff n/a      n/a       1_111A

note:
ID 取得のためには CPU address $8000-$bfff に flash address 0x00000-0x03fff を割り当てることが必要.
programming area は 0x4000 bytes のため、管理領域 0x40000 bytes 未満のサイズが来たときに layout=top での末尾の割り当てがおかしくなる. ID 取得とのコード共通化と市販品3つすべてが 0x4000 bytes のため、対応は保留.
=end
	class ProgrammerCpuVRC6 < ProgrammerCpu
		A_02AAA_8000 = 0x8000 | (0x02aaa & 0x3fff)
		A_05555_8000 = 0x8000 | (0x05555 & 0x3fff)
		A_02AAA_C000 = 0xc000 | (0x02aaa & 0x1fff)
		A_05555_C000 = 0xc000 | (0x05555 & 0x1fff)

		def content_memorybank_update(c)
			@dest.bank_switch(@cartridge_driver, c) #CPU address $8000
			v = 0x05555
			if BitTool::bit_get(@dest.abs_offset_get, 14, 0) == 1
				v = 0x02aaa
			end
			@cartridge_driver.prgrom_bank_switch(c, 0xc000, v)
		end
		def abs_to_xpu(src)
			dest = []
			src.each{|t|
				if BitTool::bit_get(@dest.abs_offset_get, 14, 0) == 0
					dest << (t == 0x2aaa ? A_02AAA_8000 : A_05555_C000)
				else
					dest << (t == 0x2aaa ? A_02AAA_C000 : A_05555_8000)
				end
			}
			dest
		end
		def program_content_make_write_address
			address = {:type => Command::TYPE_RANDOM}
			address[:value] = abs_to_xpu([0x5555, 0x2aaa, 0x5555])
			address[:value] << @dest.xpu
			address
		end
		def content_flash_erase_address(chip_erase)
			address = {:type => Command::TYPE_RANDOM}
			address[:value] = abs_to_xpu([0x5555, 0x2aaa, 0x5555, 0x5555, 0x2aaa])
			address[:value] << (chip_erase ? address[:value][0] : @dest.xpu)
			address
		end
		def command_memorybank_update_program_start(d)
			d.flash_program_write(true)
			super(d)
		end
	end
end

class DriverVRC7 < DriverBase
	include BitTool
	include VRCNametable
	def description_outline
		"Konami VRC7"
	end
=begin
	def description_number
		"85.1,85.2"
	end
=end
	def mapper_number
		85
	end
	def initialize(a)
		super(a)
		@swap = nil
	end
	def nametable_wirecount
		4 + 2 + 2 + 1 + 4 #input A15:12, RA1:0(=A5:4, =A5,A3), D1:0
	end
	def nametable_detection
		vrc_nametable_detection(0xe000)
	end
	def workram_control_set(enable)
		d = 0
		if enable
			d = 1 << 7
		end
		Memory.write(Command::REGION_CPU_6502, 0xe000, d)
	end
	def irq_disable
		Memory.write(Command::REGION_CPU_6502, 0xf000, 0)
	end
=begin
variant に関して. VRC7 A1:0 は CPU D5:4 または CPU D5, D3 となっている.
ROM の読み出しに関しては bit 4 と bit 3 が重複してないので両方とも立てる.
VRC7 利用のソフトが2本だけで、 variant の識別は CHARA ROM/RAM でのみ行う. 
将来的に VRC6 のような明確な識別手段がわかれば実装する.
=end
	def bank_assignment
		#$debug_print = true
		#2つの variant 共用のレジスタとする
		@bank_register["PRGROM"] = {0x8000 => 0x8000, 0xa000=>0x8018, 0xc000 => 0x9000}
		@ppu_a_to_cpu_reg_a = {}
		ppu_a = 0
		cpu_a = 0xa000
		4.times{
			2.times{|i|
				@bank_register["CHRROM"][ppu_a] = cpu_a | [0, 0x18][i]
				ppu_a += 0x400
			}
			cpu_a += 0x1000
		}
<<EOS
PRGROM, rrr_rrrA_AAAA_AAAA_AAAA, 0x8000, 0xa000, 0xc000
PRGROM, 111_111A_AAAA_AAAA_AAAA, 0xe000
CHRROM,  rr_rrrr_rrAA_AAAA_AAAA, #{BANK_ASSIGN_PPUBANK_0x0400_8}
EOS
	end
=begin
	def romimage_save(h, destfilename)
		h[:mappernum] = 85
		if h[:ppu_chara][:data].size == 0
			h[:submappernum] =  2
		else
			h[:submappernum] =  1
		end
		Nesfile.save(h, destfilename)
	end
=end
end
