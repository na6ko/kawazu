class DriverSxROMBase < DriverBase
	def mapper_number
		1
	end
	def workram_have_writeprotection
		false #MMC1B はあるが、データを書き換えずに電子的な識別は困難とする.
	end
	def initialize(a)
		super(a)
		@reg_e000_bit4 = 1 << 4
	end
	def shifted_data(data)
		r = []
		5.times{
			r << data
			data >>= 1
		}
		r
	end
	def register_write(address, data)
		Memory.write(Command::REGION_CPU_6502, address, shifted_data(data))
	end
	def cartridge_initialize
		Memory.write(Command::REGION_CPU_6502, 0x8000, [0x80])
		register_write(0x8000, 0b1_0011) #CPU bank 0x8000 x1 mode, PPU bank 0x1000 x2 mode
		true
	end
	def nametable_wirecount
		3 + 1 + 5 + #CPU A15:13, D7, D1 * shift count
		1 + 4
	end
	def nametable_detection
		d = []
		4.times{|nametable_register|
			register_write(0x8000, nametable_register | 0b1_1100)
			d << Memory.vram_assignment
		}
		d[0] == NAMETABLE_SYSTEMRAM_L &&
		d[1] == NAMETABLE_SYSTEMRAM_H &&
		d[2] == NAMETABLE_SYSTEMRAM_A10 &&
		d[3] == NAMETABLE_SYSTEMRAM_A11
	end
	def workram_control_set(enable)
		@reg_e000_bit4 = 1 << 4
		if enable
			@reg_e000_bit4 = 0
		end
		Memory.write(Command::REGION_CPU_6502, 0xe000, shifted_data(@reg_e000_bit4))
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 14
		d &= 0b0_1110
		d |= @reg_e000_bit4
		Memory.write(Command::REGION_CPU_6502, 0xe000, shifted_data(d), c)
	end
	def dummy_write(c)
		Memory.write(Command::REGION_CPU_6502, 0, 0, c)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		a = 0xa000
		if processor_address >= 0x1000
			a = 0xc000
		end
		d = abs_address >> 12
		Memory.write(Command::REGION_CPU_6502, a, shifted_data(d), c)
		#連続で複数のレジスタへ書いてしまうと見落とすのでダミーの cycle が必要
		dummy_write(c)
	end
end

class DriverSKROM < DriverSxROMBase
	include Notice
	def description_outline
		"Nintendo SxROM - MMC1 and generic assignments"
	end
	def bank_assignment
		#$debug_print = true
=begin
CPU address $c000-$ffff と PRG ROM A17 について
電源投入直後は PRG ROM A17 = 1 が保証されているが, MMC1A では bank register #3 (CPU address $e000-$ffff) bit 4 によって A17 の出力が変わってしまう.
さらにこの register bit 4 は MMC1B では W-RAM enable に変わっている.
ROM dump では MMC1A, MMC1B 両方で同じ動作をするために PRG ROM bank mode を 32 KB mode とする.
=end
		<<EOS
PRGROM, rr_rAAA_AAAA_AAAA_AAAA, 0x8000
CHRROM,  r_rrrr_AAAA_AAAA_AAAA, 0x0000, 0x1000
#{BANK_ASSIGN_GENERIC_WRAM}
#{BANK_ASSIGN_GENERIC_CHRRAM}
EOS
	end
	def workram_control_set(enable)
		super(enable)
		#for SNROM
		d = shifted_data(@reg_e000_bit4)
		[0xa000, 0xc000].each{|a|
			Memory.write(Command::REGION_CPU_6502, a, d)
		}
	end
	def workram_read(arg, db)
		pcb = db["feature_pcb"]
		if pcb == nil
			super(arg, db)
		elsif pcb.include?("SZROM")
			cartridge_ram_detection_by_romhash(pcb)
			c = DriverSZROM.new
			c.workram_read(arg, db)
		elsif pcb.include?("SOROM")
			cartridge_ram_detection_by_romhash(pcb)
			c = DriverSOROM.new
			c.workram_read(arg, db)
		else
			super(arg, db)
		end
	end
	def romimage_save(h, arg)
		h[:mappernum] = self.mapper_number
		if submapper_number
			h[:submappernum] = submapper_number
		end
		r = Nesfile.identify(h)
		if r.key?("title") == false
ja = <<EOS
ROM hash が database に見当たりませんでした.
再度 dump をして、ROM hash が安定しない場合は接触不良を疑ってください.
ROM の容量が 512KiB 以上のソフトはドライバ名に SUROM また SXROM を明示してください.
EOS
			print ja
			return Nesfile.save(h, arg)
		end
		pcb = r["feature_pcb"]
		if pcb
			if pcb.include?("SZROM") || pcb.include?("SOROM")
				h[:submappernum] = 0
			end
		end
		Nesfile.save(h, arg)
	end
end

class DriverSUROM < DriverSxROMBase
	def description_outline
		"Nintendo SUROM - MMC1 + extended PRGROM"
	end
	def nametable_detection
		#自動判別不可
		false
	end
	def bank_assignment
		<<EOS
PRGROM, rrr_rAAA_AAAA_AAAA_AAAA, 0x8000
#{BANK_ASSIGN_GENERIC_WRAM}
#{BANK_ASSIGN_GENERIC_CHRRAM}
EOS
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		d = shifted_data(abs_address >> (18 - 4))
		[0xa000, 0xc000].each{|a|
			Memory.write(Command::REGION_CPU_6502, a, d, c)
			dummy_write(c)
		}
		super(c, processor_address, abs_address)
	end
	def workram_read(arg, db)
		pcb = db["feature_pcb"]
		if pcb == nil
			super(arg, db)
		elsif pcb.include?("SXROM")
			cartridge_ram_detection_by_romhash(pcb)
			c = DriverSXROM.new
			c.workram_read(arg, db)
		else
			super(arg, db)
		end
	end
end

=begin
2つ SRAM が Work RAM (W-RAM, PRGRAM など名称が混在しているが目的は同一)
として存在する場合は、電池がついてる WorkRAM のみ read, write の対象と
する. (SZROM, SOROM, ETROM が対象)

増やされた Work RAM への接続
SZROM
	U4.CE1# = CHRROM A16
	U5.CE1# = ~CHRROM A16 (=battery-backuped)
SOROM
	U2.CE1# = CHRROM A15
	U3.CE1# = ~CHRROM A15 (=battery-backuped)
SXROM (U4 = battery-backuped, WRAM は1つ)
	U4.A14 = CHRROM A15
	U4.A15 = CHRROM A16

WorkRAM に対する CHRROM Ax は PPU address bus に変更されている.
CHHROM Ax の出力は PPU Ax への入力で参照バンクが変わる.
CPU として動作を安定させる場合は複数の CHRROM bank register に同じ値を
書き込むことが必要.
=end
class DriverSZROM < DriverSxROMBase
	def description_outline
		"Nintendo SZROM - MMC1 + 8KiB PRGRAM x2 + CHRROM"
	end
	def submapper_number
		0 #2つの WRAM を利用するために必要
	end
	def nametable_detection
		false
	end
	def bank_assignment
		<<EOS
PRGROM, r_rAAA_AAAA_AAAA_AAAA, 0x8000
CHRROM,    rrr_AAAA_AAAA_AAAA, 0x0000, 0x1000
#{BANK_ASSIGN_GENERIC_WRAM}
EOS
	end
	def workram_control_set(enable)
		super(enable)
#WRAM は U4 と U5 の2つ. 2つとも KM6264 (0x2000 bytes).
#backup RAM は U5. U5 は SKROM の配線では CHRROM A16 への出力を 1 にすることで利用できる.
		d = shifted_data(@reg_e000_bit4 ^ (1 << 4))
		[0xa000, 0xc000].each{|a|
			Memory.write(Command::REGION_CPU_6502, a, d)
		}
	end
end

class DriverSOROM < DriverSxROMBase
	def description_outline
		"Nintendo SOROM - MMC1 + 8KiB W-RAM x2 + CHRRAM"
	end
	def submapper_number
		0
	end
	def nametable_detection
		false
	end
	def bank_assignment
		<<EOS
PRGROM, rr_rAAA_AAAA_AAAA_AAAA, 0x8000
#{BANK_ASSIGN_GENERIC_CHRRAM}
#{BANK_ASSIGN_GENERIC_WRAM}
EOS
	end
	def workram_control_set(enable)
		super(enable)
		d = shifted_data(@reg_e000_bit4 ^ (1 << 3))
		[0xa000, 0xc000].each{|a|
			Memory.write(Command::REGION_CPU_6502, a, d)
		}
	end
end

class DriverSXROM < DriverSUROM
	def description_outline
		"Nintendo SXROM - MMC1 + extended PRGROM and W-RAM"
	end
	def submapper_number
		0
	end
	def nametable_detection
		false
	end
	def bank_assignment
		<<EOS
PRGROM, rrr_rAAA_AAAA_AAAA_AAAA, 0x8000
#{BANK_ASSIGN_GENERIC_CHRRAM}
WRAM,        rrA_AAAA_AAAA_AAAA, 0x6000
EOS
	end
	def workram_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 13
		d &= 0b11
		d = shifted_data(d << 2)
		[0xa000, 0xc000].each{|a|
			Memory.write(Command::REGION_CPU_6502, a, d, c)
			dummy_write(c)
		}
	end
end

