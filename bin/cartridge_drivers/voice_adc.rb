module VoiceADC
=begin
Cartridge の Sound Out から ADC 変換を音声データを取得する.
Cartridge 内部に音源がある場合でもカートリッジ内部から音源に clock を供給する必要がある.
この条件を満たすゲームソフトは Jaleco の uPD7756 が載っているものと Konami のラグランジュポイントのみである.
MCU の ADC は音声用ではなく汎用なので音質に期待できない. この機能では uPD7756 が実用できる唯一の音源だと思われる.
=end
	def wave_write(filename, ratio, ch, samplesize, waveform)
		str = "RIFF"
		str << [0x28 + waveform.size * samplesize * ch - 8].pack('V1')
		str << "WAVEfmt "
		str << [16, 1, ch, ratio, ratio * samplesize * ch, ch * samplesize, samplesize * 8].pack('V1v2V2v2')
		str << "data"
		str << [waveform.size * samplesize * ch].pack('V1')
		str << waveform.pack(samplesize == 2 ? "s*" : "C*")
		f = File.open(filename, 'wb')
		f.write(str)
		f.close
	end
=begin
http://www7b.biglobe.ne.jp/~leftyserve/delusion/del_plbl.htm
Moero!! Pro Yakyuu, PCB: JF-13, register: 74174x2, 775x: 146, 148
Moero!! Pro Tennis, PCB: JF-17, register: 74161+74174x2, 775x: 186
Moero!! Pro Yakyuu '88: Kettei Ban, PCB: JF-19, register: 74161+74174x2, 775x: 220
Shin Moero!! Pro Yakuu, PCB: JF-23, register: SS88006, 775x: 220
Moepro! '90 Kandou Hen, PCB: JF-29, register: SS88006, 775x: 220
Moepro! Saikyou Hen, PCB: JF-33 register: SS88006, 775x: 220
Terao no Dosukoi Oozumou, PCB: JF-24A, register: SS88006, 775x: 052
=end

=begin
uPD7756 note:
ST のトリガから busy まで実測 6 us
tRTS min 18.5 us, RESET pulse width
tRS min 12.5 us, ST# set-up time
tSBO typ 6.25 us, max 10 us, Operation mode
tSBS typ 4 ms, max 80 ms, Standby mode

ST のトリガが 3 秒間ないと Standby Mode になり Busy は High-Z になる.
JF-17 の基板は Busy をプルアップしていないので Standby Mode になるとレベルが不定になる.
Busy の出力は配線されていないので、データバスから動的に時間を計測することが出来ない. voice の時間は busy が L の期間を計測したもの. 計測精度は 1 ms 単位.

BA-エアロビ, M50805
=end

	MS_PRO_YAKYUU_220 = [
		271, 296, 292, 287, 289, 288, 369, 443,
		444, 531, 629, 669, 386, 303, 1175, 290,
		254, 287, 316, 3170
	]
	SAMPLE_INDEX_TO_MS = {
		"JF-17" => [ #Pro Tennis
			591, 678, 561, 561, 412, 384, 903, 578, 
			677, 589, 492, 560, 587, 3035, 484, 385, 
			482, 237, 217
		], "JF-13" => [ #Moero!! Pro Yakyuu
			678, 366, 482, 394, 592, 540, 266, 658,
			638, 530, 560, 640, 248, 560, 346, 3000
		], "JF-24" => [ #Terao no Dosukoi Oozumou
			790, 582, 504, 651, 864, 855
		], "JF-19" => MS_PRO_YAKYUU_220,
		"JF-23" => MS_PRO_YAKYUU_220,
		"JF-29" => MS_PRO_YAKYUU_220,
		"JF-33" => MS_PRO_YAKYUU_220
	}
	BITDEPTH_8 = 0
	BITDEPTH_10 = 1
	BITDEPTH_12 = 2
	def voice_record(arg, info_serial, freq, bitdepth)
		h = SAMPLE_INDEX_TO_MS[info_serial]
		sample_par_byte = bitdepth == BITDEPTH_8 ? 1 : 2
		shift = 0
		case bitdepth
		when BITDEPTH_10
			shift = 16 - 10
		when BITDEPTH_12
			shift = 16 - 12
		end
		total_bytes = 0
		h.size.times{|i|
			total_bytes += sample_count(h[i], freq)
		}
		if shift != 0
			total_bytes *= 2
		end
		Hamo::Progressbar::range_set(0, "", total_bytes)
		Hamo::Progressbar::range_set(1, "", 0)
		h.size.times{|i|
			label = sprintf("Voice %2d/%2d", i + 1, h.size)
			Hamo::Progressbar::label_update(0, label)
			if i == 0
				Hamo::Progressbar::ready
			end
			sample_num = sample_count(h[i], freq)
			Command::busaccess_loopcount_set(Command::LOOP_TYPE_ADC, sample_num, 48_000_000 / freq, bitdepth)
			d = voice_request(i, sample_num)
			if d == nil
				next
			end
			if shift != 0
				d = d.pack('C*').unpack('v*').map{|t|
					t <<= shift
					t -= 0x8000
					t
				}
			end
			filename = arg["out:dirname.wav"].dup
			if arg.key? :outprefix
				filename << arg[:outprefix] << "_"
			end
			filename << ("%02d.wav" % i)
			wave_write(filename, freq, 1, sample_par_byte, d)
		}
		Hamo::Progressbar::done
	end
	def sample_count(ms, freq)
		((ms.to_f / 1000) * freq).to_i
	end
	def voice_call(val, ms)
		freq = 24_000
		sample_num = sample_count(ms, freq)
		Command::busaccess_loopcount_set(Command::LOOP_TYPE_ADC, sample_num, 48_000_000 / freq, BITDEPTH_8)
		voice_request(val, sample_num)
	end
end
