=begin
Nintendo MMC 2, 3, 4 and 5
please check 'nintendo_mmc1_sxrom.rb' for MMC1 drivers

TBD: support MMC6
=end

class DriverPxROM < DriverBase
	def description_outline
		"Nintendo PxROM and FxROM - MMC2, MMC4"
	end
	def description_number
		"9,10"
	end
	def mapper_number
		@mapper_number
	end
	def bank_assignment
		@bank_register["PRGROM"] = {0x8000 => 0xa000}
		s = ''
		if @mapper_number == nil || @mapper_number == 9
			s = <<EOS
PRGROM, r_rrrA_AAAA_AAAA_AAAA, 0x8000
#PRGROM, 1_11AA_AAAA_AAAA_AAAA, 0xa000
PRGROM, 1_11AA_AAAA_AAAA_AAAA, 0xc000
CHRROM, r_rrrr_AAAA_AAAA_AAAA, 0x0000,0x1000
EOS
		else
			s = <<EOS
PRGROM, rr_rrAA_AAAA_AAAA_AAAA, 0x8000
PRGROM, 11_11AA_AAAA_AAAA_AAAA, 0xc000
CHRROM,  r_rrrr_AAAA_AAAA_AAAA, 0x0000,0x1000
EOS
		end
		s
	end
	def cartridge_initialize
		ar = []
		2.times{|i|
			Memory.write(Command::REGION_CPU_6502, 0xa000, i)
			ar << Memory.read(Command::REGION_CPU_6502, 0xa000, 0x80, -1)
		}
		if ar[0] == ar[1]
			@mapper_number = 9
		else
			@mapper_number = 10
		end
		@bank_list.clear
		bank_define
		true
	end
	def nametable_wirecount
		4 + 1 + #CPU A15:12 D0
		2 + #VRAM CS# register + PPU A13
		1 #register
	end
	def nametable_detection
		d = []
		2.times{|i|
			Memory.write(Command::REGION_CPU_6502, 0xf000, [i])
			d << Memory.vram_assignment
		}
		d == [NAMETABLE_SYSTEMRAM_A10, NAMETABLE_SYSTEMRAM_A11]
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		latch_address = 0x0fd8 | (processor_address & 0x1000)
		bank_address = 0xb000 + ((processor_address & 0x1000) << 1)
		2.times{
			Memory.read(Command::REGION_PPU, latch_address, 1, -1, c)
			Memory.write(Command::REGION_CPU_6502, bank_address, abs_address >> 12)
			latch_address += 1
			bank_address += 0x1000
		}
	end
end

class DriverTKROM < DriverBase
	def description_outline
		"Nintendo TxROM - MMC3 and variant PCBs"
	end
	def description_number
		"4,118"
	end
	def mapper_number
		@mapper_number
	end
	def bank_assignment
		@prgrom_bank_switch = {0x8000 => 6, 0xa000 => 7}
		@chrrom_bank_switch = {0x0000 => 0, 0x0800 => 1, 0x1000 => 2, 0x1400 =>3, 0x1800 => 4, 0x1c00 => 5}
		s = <<EOS
PRGROM, rrr_rrrA_AAAA_AAAA_AAAA, 0x8000, 0xa000
PRGROM, 111_11AA_AAAA_AAAA_AAAA, 0xc000
CHRROM,  rr_rrrr_rAAA_AAAA_AAAA, 0x0000, 0x0800
CHRROM,  rr_rrrr_rrAA_AAAA_AAAA, 0x1000, 0x1400, 0x1800, 0x1c00
EOS
		s << BANK_ASSIGN_GENERIC_WRAM
		s << BANK_ASSIGN_GENERIC_CHRRAM
		s
	end
	def nametable_wirecount
		[
			3 + 1 + 1 + #CPU A15:13, A0, D0
			1 + 2,

			3 + 1 + 3 + #register address CPU A15:13, A0, D0
			3 + 1 + 1 + #register data CPU A15:13, A0, D0
			1 + 3 #PPU A13#, PPU A12:10
		].min
	end
	def nametable_detection_standard
		d = []
		2.times{|nametable_register|
			Memory.write(Command::REGION_CPU_6502, 0xa000, [nametable_register])
			d << Memory.vram_assignment
		}
		d == [NAMETABLE_SYSTEMRAM_A10, NAMETABLE_SYSTEMRAM_A11]
	end
	def nametable_detection_tksrom
		v = []
		6.times{|i|
			v << i
			v << ((i & 1) << 7)
		}
		Memory.write(Command::REGION_CPU_6502, 0x8000, v)
		if Memory.vram_assignment != [2, 2, 3, 3, 2, 3, 2, 3, 0, 0, 1, 1, 0, 1, 0, 1]
			return false
		end
		Memory.write(Command::REGION_CPU_6502, 0x8000, [0x80])
		return Memory.vram_assignment == [2, 3, 2, 3, 2, 2, 3, 3, 0, 1, 0, 1, 0, 0, 1, 1]
	end
	def nametable_detection
		if nametable_detection_standard
			@mapper_number = 4
			return true
		end
		if nametable_detection_tksrom
			@mapper_number = 118
			return true
		end
		return false
	end
	def workram_control_set(enable)
		d = 0x40
		if enable
			d = 0x80
		end
		Memory.write(Command::REGION_CPU_6502, 0xa001, d)
	end
	def irq_disable
		Memory.write(Command::REGION_CPU_6502, 0xe000, 0)
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		Memory.write(Command::REGION_CPU_6502, 0x8000, [@prgrom_bank_switch[processor_address], abs_address >> 13], c, 0x8001)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		#note: If bank size is 0x800 bytes, register data bit 0 is not used.
		Memory.write(Command::REGION_CPU_6502, 0x8000, [@chrrom_bank_switch[processor_address], abs_address >> 10], c, 0x8001)
	end
	def romimage_save(h, destfilename)
		if @mapper_number == nil
			@mapper_number = nametable_detection_tksrom ? 118 : 4
		end
		super(h, destfilename)
	end
end

#MMC5 は強力な機能があるが driver として必要最小限の実装とする
#(予測) ROM bank 以外のレジスタは強力なバス監視機能により書き込みが無効になってしまっている.
class DriverExROMBase < DriverBase
	def mapper_number
		5
	end
	def submapper_number
		0 #W-RAM の容量で挙動が変わるものが多いので nes 2.0 header 必須とする
	end
	def nametable_wirecount
		16 + 8 + #CPU 全bit繋がっている
		13 + 8 + #PPU
		2 + 8 * 8 #register
	end
	def bank_assignment_base
		@bank_register["PRGROM"] = {}
		cpu_a_w = 0x5114
		cpu_a_r = 0x8000
		4.times{
			@bank_register["PRGROM"][cpu_a_r] = cpu_a_w
			cpu_a_w += 1
			cpu_a_r += 0x2000
		}
		cpu_a_w = 0x5121
		ppu_a_r = 0x0000
		8.times{
			@bank_register["CHRROM"][ppu_a_r] = cpu_a_w
			ppu_a_r += 0x0800
			cpu_a_w += 2
		}
#A19 以降の bit は市販品では使っていないので利用しない.
#CHRROM register $5128-$512b も使用しない.
#(予想) CPU bank $e000-$ffff は MMC5 からの reset 誤検出のため動作していない.
		<<EOS
PRGROM, rrr_rrrA_AAAA_AAAA_AAAA, 0x8000, 0xa000, 0xc000
CHRROM, rrr_rrrr_rAAA_AAAA_AAAA, #{BANK_ASSIGN_PPUBANK_0x0800_4}
EOS
	end
	def bank_assignment
		s = bank_assignment_base
		s << BANK_ASSIGN_GENERIC_WRAM
		s
	end
	def cartridge_initialize
		Memory.write(Command::REGION_CPU_6502, 0x5100, [3, 2])
		true
	end

	def nametable_detection
		d = []
		[0x50, 0x44, 0, 0x55].each{|nametable_register|
			Memory.write(Command::REGION_CPU_6502, 0x5105, nametable_register)
			d << Memory.vram_assignment
		}
		d == [
			[2, 2, 3, 3, 2, 2, 3, 3, 0, 0, 1, 1, 2, 2, 3, 3], 
			[2, 3, 2, 3, 2, 3, 2, 3, 0, 1, 0, 1, 2, 3, 2, 3], 
			[2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 2, 2, 2, 2], 
			[3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 1, 1, 3, 3, 3, 3]
		]
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		a = @bank_register["PRGROM"][processor_address]
		d = abs_address >> 13
		d |= 0x80
		Memory.write(Command::REGION_CPU_6502, a, d, c)
	end
	def workram_control_set(enable)
		d = [0, 0]
		if enable
			d = [2, 1]
		end
		Memory.write(Command::REGION_CPU_6502, 0x5102, d)
		workram_page_set
	end
	def workram_write(arg)
		c = Command::Content.new
		Memory.write(Command::REGION_CPU_6502, 0x5102, [2, 1], c)
		Memory.write(Command::REGION_CPU_6502, 0x7fff, 0xff, c) #連続で write cycle をいれると最初は databus が安定しないようなので、 dummy write が必要のようだ.
		super(arg, {:content_initalizer => c})
	end
end

class DriverEKROM < DriverExROMBase
	def description_outline
		"Nintendo EKROM, ETROM - MMC5, W-RAM size <= 16 KiB"
	end
	def workram_page_set
		#E[TW]ROM では page 0-3 を battery-backup RAM とする (S[ZO]ROM は後半を backup RAM にしている)
		Memory.write(Command::REGION_CPU_6502, 0x5113, 0)
	end
	def workram_read(arg, db)
		pcb = db["feature_pcb"]
		if pcb == nil || pcb.include?("EKROM") || pcb.include?("ETROM")
			super(arg)
		elsif pcb.include?("EWROM")
			cartridge_ram_detection_by_romhash(pcb)
			d = DriverEWROM.new
			d.workram_read(arg, db)
		end
	end
end

class DriverEWROM < DriverExROMBase
	def description_outline
		"Nintendo EWROM - MMC5, W-RAM size 32 KiB"
	end
	def workram_page_set
	end
	def bank_assignment
		s = bank_assignment_base
		s + "WRAM, rrA_AAAA_AAAA_AAAA, 0x6000\n"
	end
	def workram_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 13
		Memory.write(Command::REGION_CPU_6502, 0x5113, d, c)
	end
end

