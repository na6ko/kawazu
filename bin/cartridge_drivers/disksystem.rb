=begin
disk image dumper via HVC-023
special thanks: NESDEV wiki, Brad Taylor, Nori and Enri

CPU address $402x = write, $403x = read
初期化
$4022 = 0 ;timer IRQ disable
$4023 = 0 ;master I/O disable
$4023 = $83 ;master I/O enable
$4025 = $2e
$4026 = $FF ;external connector and battery status

disk 挿入後
lda $4032; and #$01 ;x2
$4025 = $2e ;108.253.7 (frame.scanline(y).beam(x))
$4025 = $2f ;139.194.54
$4025 = $2d ;139.194.74
$4026 = $ff
eol $4033; rol a ; battery status check
$4025 = $2e ;148.199.15 read, transfer reset, stop motor
$4025 = $2f ;148.199.55 read, transfer reset, turn on motor
$4025 = $2d ;148.199.76 read, turn on motor

ヘッドが外周へ移動するまで待つ
lda $4032; lsr a; bcs; lsr a; bcs; wait for disk ready;

外周に移動してから 272 ms 待ってから割り込みを待つ (試した感じ272msには 50ms 早くても遅くても大丈夫だった)
;**important ** disk ready, wait for 267 + 5 ms for first GAP
$4025 = $6d ;165.29.153 head start, read, turn on motor;
$4025 = $ed ;165.29.223 DRQ set, head start, read, turn on motor;
;about 200 ms later, diskdrive found a data block and generate an IRQ
ldx $4031; sta $4024 ;x58 -> 0x38 + 2
$4025 = $fd ;165.87.86 ;set $fd at last data in the block
ldx $4031 ;165.87.241
$4025 = $2d ;165.88.100 ;**important ** first data block is end, reset the head 

;2つ目以降の GAP は 5 ms 待って割り込みを待つ(繰り返し)
;5 ms later, second GAP, same loop
$4025 = $6d ;165.167.218
$4025 = $ed ;165.168.32
ldx $4031; sta $4024 ;165.168.241 x3
$4025 = $fd
$4025 = $2d

;if the block ID is 0 or more than 5, stop the loop
=end
require Hamo::PROGRAM_DIR + '/fdsfile.rb'

class DriverDiskSystem < DriverBase
	def description_outline
		"Nintendo Disk System (HVC-022 + HVC-023)"
	end
	def description_number
		""
	end
	def mapper_number
		nil
	end
	def nametable_wirecount
		16 + 1 + #CPU A15:0, D3
		2 + #VRAM CS#, VRAM A10
		1 #register
	end
	def nametable_detection
		d = []
		2.times{|i|
			Memory.write(Command::REGION_CPU_6502, 0x4025, (1 << 5) | (i << 3) | (1 << 1)) #bit1=1 motor stop
			d << Memory.vram_assignment
		}
		d == [NAMETABLE_SYSTEMRAM_A10, NAMETABLE_SYSTEMRAM_A11]
	end
	def bank_assignment
#CPU address 0x6000-0xdfff の RAM は除外.
		<<EOS
PRGROM, A_AAAA_AAAA_AAAA, 0xe000
#{BANK_ASSIGN_GENERIC_CHRRAM}
EOS
	end

	def receive_data_set(data)
		@receive = data
		@identify = false
	end
	def receive_done
		if @receive.size >= 0x40 * 3 && @identify == false
			s = FDSSide.new
			s.diskimage_set(@receive.dup)
			s.identify_print
			@identify = true
		end
	end

=begin
下記の一括の命令群が送られる前提で MCU 側で実装しているので変更してはいけない. 
この命令群は最初に実行したあと MCU 側でタイミングをとって変更する.
c a     d   (c:cycle, a:address, d:data)
w $4025 $2f ;これと次のwriteに関して,  MCU 側で cycle を write かダミー
w $4025 $2d ;data は $6d, $ed, $fd, $2d に随時替える
r $4030 xx  ;read に関しても得られた値でタイミングを取る
r $4031 xx
r $4032 xx
=end
	def side_dump(side_number)
		Command.busaccess_loopcount_set(Command::LOOP_TYPE_IRQTRIGGER, 0xf000)
		c = Command::Content.new
		Memory.write(Command::REGION_CPU_6502, 0x4025, [0x2f, 0x2d], c, 0xffff, 0)
		Memory.read(Command::REGION_CPU_6502, 0x4030, 3, -1, c)
		option = {:progressbar_receive_index => 0}
		if side_number == 0
			option[:callback] = self
		end
		c.send_receive(Command::NUMBER_BUS_ACCESS_START, [0, 0], option)
	end
	def status_wait(request)
		s = []
		request = Array.new(8, request)
		while s != request
			Hamo::usleep(10_000)
			d = Memory.read(Command::REGION_CPU_6502, 0x4030, 3)
			s << ((d[2] & 1) == 0)
			if s.size > request.size
				s.shift
			end
			#p s
		end
	end
	def disk_data_dump(fds)
		fds.diskimage_set(side_dump(0))
		case fds.gamename_to_side_qty
		when 0
			Message::puts "how many sides?"
			return
		when 1
			return
		end
		(fds.gamename_to_side_qty - 1).times{|i|
			fds.label_update
			Message::puts "eject disk card and insert another side"
			#fds.motor_wait
			status_wait false
			status_wait true
			Hamo::usleep(500_000)
			fds.diskimage_set(side_dump(1 + i))
		}
	end
	def diskdrive_init
		d = Memory.read(Command::REGION_CPU_6502, 0x4030, 3)
		#bit1:0 2'b11: disk not inserted, 2'b01: disk inserted, head 途中, 
		if (d[2] & 1) != 0
			Message::puts("disk is not inserted")
			return false
		end
		Memory.write(Command::REGION_CPU_6502, 0x4022, [0, 0x83, 0, 0x2e, 0xff])
		true
	end
	def workram_read(arg, db)
		Hamo::device.receive_timeout_set(4_000)
		if diskdrive_init
			fds = FDSFile.new
			disk_data_dump fds
			Hamo::Progressbar::done
			fds.save(arg, true)
			fds.motor_wait
		end
	end
=begin
bios ROM の database が見当たらなかった. 下記を参照した. 
ツインファミコン用の BIOS は登録されていない.
https://github.com/mamedev/mame/blob/master/src/devices/bus/nes/disksys.cpp
CRC32    Label
1C7AE5D5 RP2C33A-01
5E607DCF RP2C33A-01A
=end
	BIOS_ROM_CRCS = [0x1C7AE5D5, 0x5E607DCF]
	def romimage_save(h, arg)
		#ROM image は NES header の最小サイズを満たしていないので単純なバイナリデータとする
		path = arg["out:dirname.rom"] + "disksys.rom"
		crc = Hamo::crc32_calc(h[:cpu_program][:data])
		save_rom = true
		if File.exist?(path)
			f = File.open(path, 'rb')
			rom = f.read(0x2000).unpack('C*')
			f.close
			save_rom = crc != Hamo::crc32_calc(rom)
		end
		Message::puts("CRC32 %08X" % crc)
		if save_rom
			f = File.open(path, 'wb')
			f.write(h[:cpu_program][:data].pack('C*'))
			f.close
			Message::puts("saved: #{path}")
			if BIOS_ROM_CRCS.include?(crc) == false
				Message::puts("note: unknown CRC is detected")
			end
		end
		{:disk_dump => BIOS_ROM_CRCS.include?(crc)}
	end
end
