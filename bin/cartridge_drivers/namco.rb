class DriverDRROM < DriverBase
	def description_outline
		"Namco 108 series, DRROM, standard assignment"
	end
	def initialize(a)
		super(a)
		@ppuaddress_to_regaddress = {}
		i = 0
		[0x0000, 0x0800, 0x1000, 0x1400, 0x1800, 0x1c00].each{|a|
			@ppuaddress_to_regaddress[a] = i
			i += 1
		}
	end
	def mapper_number
		206
	end
	def bank_assignment
<<EOS
PRGROM, r_rrrA_AAAA_AAAA_AAAA, 0x8000, 0xa000
PRGROM, 1_11AA_AAAA_AAAA_AAAA, 0xc000
CHRROM,   rrrr_rAAA_AAAA_AAAA, 0x0000, 0x0800
CHRROM,   rrrr_rrAA_AAAA_AAAA, 0x1000, 0x1400, 0x1800, 0x1c00
EOS
	end
	def vram_connection_set(h)
		vram_connection_static_detect(h)
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		r = 6
		if processor_address == 0xa000
			r = 7
		end
		Memory.write(
			Command::REGION_CPU_6502, 0x8000, 
			[r, abs_address >> 13], c, 0x8001
		)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		Memory.write(
			Command::REGION_CPU_6502, 0x8000, 
			[@ppuaddress_to_regaddress[processor_address], abs_address >> 10], c, 0x8001
		)
	end
end

#DS Dragon Sprit
#NM3 Namcot Mahjong III
#QT Quinty
#DM Devil Man
class DriverDRROMCHRROMA16 < DriverDRROM
	def description_outline
		"Namco 108 series, CHRROM A16 = PPU A12, 3433, 3453"
	end
	def mapper_number
		@mapper
	end
	def description_number
		"88,154"
	end
	def vram_connection_set(h)
	end
	def nametable_wirecount
		1 + 1 + 1 + 2 #input CPU A15, D6
	end
	def nametable_detection
		d = []
		[0, 1 << 6].each{|r|
			#devilman のみ 74161 が追加されている
			Memory.write(Command::REGION_CPU_6502, 0x8000, r)
			d << Memory.vram_assignment
		}
		d == [NAMETABLE_SYSTEMRAM_L, NAMETABLE_SYSTEMRAM_H]
	end
	def bank_assignment
<<EOS
PRGROM, r_rrrA_AAAA_AAAA_AAAA, 0x8000, 0xa000
PRGROM, 1_11AA_AAAA_AAAA_AAAA, 0xc000
CHRROM, 0_rrrr_rAAA_AAAA_AAAA, 0x0000, 0x0800
CHRROM, 1_rrrr_rrAA_AAAA_AAAA, 0x1000, 0x1400, 0x1800, 0x1c00
EOS
	end
	def programming_address_assignment(m, memory)
		if memory == "PRGROM"
			return super(m, memory)
		end
		#手動で設定すれば実装可能だと思われるが、実物ができるまで実装は保留
		Message::puts ("Can't assign #{memory} programming banks")
		return nil
	end
	def romimage_save(h, destfilename)
		if nametable_detection
			@mapper = 154
		else
			@mapper = 88
			vram_connection_static_detect(h)
		end
		super(h, destfilename)
	end
end

#DB Dragon Buster
class DriverDRROMNTCONROL < DriverDRROM
	def description_outline
		"Namco 108 series, VRAM A10 = CHRROM A15"
	end
	def mapper_number
		95
	end
	def bank_assignment
<<EOS
PRGROM, r_rrrA_AAAA_AAAA_AAAA, 0x8000, 0xa000
PRGROM, 1_11AA_AAAA_AAAA_AAAA, 0xc000
CHRROM,    rrr_rAAA_AAAA_AAAA, 0x0000, 0x0800
CHRROM,    rrr_rrAA_AAAA_AAAA, 0x1000, 0x1400, 0x1800, 0x1c00
EOS
	end
	def nametable_wirecount
		3 + 1 + 1 + 3 + #register adress CPU A15:13, A0, D5, D2:0
		1 + 3 + 1 #PPU A13#, PPU A12:10, 108.A15
	end
	
	def vram_connection_set(h)
	end
	def nametable_detection
		d = []
		4.times{|i|
			d << (i + 2)
			d << ((i & 1) << 5)
		}
		Memory.write(
			Command::REGION_CPU_6502, 0x8000, d, nil, 0x8001
		)
		d = []
		4.times{|i|
			Memory.write(
				Command::REGION_CPU_6502, 0x8000, 
				[0, i << 5, 1, i << 4], nil, 0x8001
			)
			d << Memory.vram_assignment
		}
		d == [
			[2, 2, 2, 2, 2, 3, 2, 3, 0, 0, 0, 0, 0, 1, 0, 1], 
			[3, 3, 2, 2, 2, 3, 2, 3, 1, 1, 0, 0, 0, 1, 0, 1], 
			[2, 2, 3, 3, 2, 3, 2, 3, 0, 0, 1, 1, 0, 1, 0, 1], 
			[3, 3, 3, 3, 2, 3, 2, 3, 1, 1, 1, 1, 0, 1, 0, 1]
		]
	end
end
class DriverDRROMDDS1 < DriverDRROM
	def description_outline
		"Namco 108 series, 108.PPU_A12 = 1, CHRROM.A16:11 = 108.CHRROM.A15:10, CHRROM.A10 = PPU.A10"
	end
	def initialize(a)
		super(a)
		@ppuaddress_to_regaddress = {}
		i = 2
		[0x0000, 0x0800, 0x1000, 0x1800].each{|a|
			@ppuaddress_to_regaddress[a] = i
			i += 1
		}
		#$debug_print = true
	end
	def mapper_number
		76
	end
	def bank_assignment
<<EOS
PRGROM, r_rrrA_AAAA_AAAA_AAAA, 0x8000, 0xa000
PRGROM, 1_11AA_AAAA_AAAA_AAAA, 0xc000
CHRROM, r_rrrr_rAAA_AAAA_AAAA, 0x0000, 0x0800, 0x1000, 0x1800
EOS
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		Memory.write(
			Command::REGION_CPU_6502, 0x8000, 
			[@ppuaddress_to_regaddress[processor_address], abs_address >> 11], c, 0x8001
		)
	end
end
module NAMCO_163_175_ROMBANK
	def bank_map
		cpu_a = 0x8000
		ppu_a = 0
		8.times{
			@bank_register["CHRROM"][ppu_a] = cpu_a
			cpu_a += 0x0800
			ppu_a += 0x0400
		}
		s = <<EOS
PRGROM, rr_rrrA_AAAA_AAAA_AAAA, 0x8000, 0xa000, 0xc000
PRGROM, 11_111A_AAAA_AAAA_AAAA, 0xe000
CHRROM, rr_rrrr_rrAA_AAAA_AAAA, #{DriverBase::BANK_ASSIGN_PPUBANK_0x0400_8}
EOS
		s += DriverBase::BANK_ASSIGN_GENERIC_WRAM
		s
	end
end
class DriverNAMCO163 < DriverBase
	include NAMCO_163_175_ROMBANK
	def description_outline
		"Namco 163 series"
	end
	def mapper_number
		19
	end
	def nametable_wirecount
		5 + 8 + #CPU A15:11, D7:0
		4 + 12 * 2 #PPU A13:10, registers * (VRAM CS# + VRAM A10)
	end
	def nametable_detection
		Memory.write(Command::REGION_CPU_6502, 0xe800, [0x00])
		d = [0xe0, 0xe1, 2, 3, 0xe4, 5, 0xe6, 0x7, 0xe0, 1, 0xe1, 0]
		Memory.write(Command::REGION_CPU_6502, 0x8000, d, nil, 0xf800, 0x800)
		Memory.vram_assignment == [0, 1, 2, 3, 0, 3, 0, 3, 0, 3, 1, 2, 0, 3, 1, 2]
	end
	def workram_control_set(enable)
		d = 0
		if enable
			d = 0x40
		end
		Memory.write(Command::REGION_CPU_6502, 0xf800, [d])
	end
	def irq_disable
		Memory.write(Command::REGION_CPU_6502, 0x5800, [0])
	end
	def bank_assignment
		bank_map
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 13
		case processor_address
		when 0x8000
			a = 0xe000
			d |= 0x40
		when 0xa000
			a = 0xe800
			d |= 0xc0
		when 0xc000
			a = 0xf000
		end
		Memory.write(Command::REGION_CPU_6502, a, d, c)
	end
end

class DriverNAMCO340 < DriverBase
	include NAMCO_163_175_ROMBANK
	def description_outline
		"Namco 175, 340"
	end
	def mapper_number
		210
	end
	def submapper_number
		@nametable_controller == false ? 1 : 2
	end
	#note: 340 has nametable control. 175 does not.
	def nametable_wirecount
		5 + 2 + #CPU A15:11, D7:6
		3 + 2 #PPU A11:10, VRAM CS# + VRAM A10
	end
	def vram_connection_set(h)
		if @nametable_controller == false
			vram_connection_static_detect(h)
		end
	end
	def bank_assignment
		cpu_w_a = 0xe000
		cpu_r_a = 0x8000
		3.times{
			@bank_register["PRGROM"][cpu_r_a] = cpu_w_a
			cpu_w_a += 0x0800
			cpu_r_a += 0x2000
		}
		bank_map
	end
	def cartridge_initialize
		if @nametable_controller == nil
			nametable_detection
		end
		true
	end
	def nametable_detection
		r = []
		4.times{|i|
			Memory.write(Command::REGION_CPU_6502, 0xe000, i << 6)
			r << Memory.vram_assignment
		}
		@nametable_controller = r == [
			NAMETABLE_SYSTEMRAM_L, NAMETABLE_SYSTEMRAM_A10, 
			NAMETABLE_SYSTEMRAM_H, NAMETABLE_SYSTEMRAM_A11
		]
		@nametable
	end
	def workram_control_set(enable)
		d = 0
		if enable
			d = 1
		end
		Memory.write(Command::REGION_CPU_6502, 0xc000, d)
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		if processor_address == 0xa000
			d = abs_address >> 13
			Memory.write(Command::REGION_CPU_6502, 0xe800, 0xc0 | d, c)
		else
			super(c, processor_address, abs_address)
		end
	end
	def romimage_save(h, arg)
		if @nametable_controller == nil
			nametable_detection
		end
		super(h, arg)
	end
end
