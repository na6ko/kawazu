class DriverSUNSOFT1 < DriverBase
	def description_outline
		"SUNSOFT-1"
	end
	def mapper_number
		184
	end
	def bank_assignment
<<EOS
PRGROM, AAA_AAAA_AAAA_AAAA, 0x8000
CHRROM, rrr_AAAA_AAAA_AAAA, 0x0000,0x1000
EOS
	end
	def vram_connection_set(h)
		vram_connection_static_detect(h)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 12
		d &= 0b0110
		d |= (d | 1) << 4
		Memory.write(Command::REGION_CPU_6502, 0x6000, d, c)
	end
end
class DriverSUNSOFT2 < DriverWritebusconflicts
	def description_outline
		"SUNSOFT-2"
	end
	def mapper_number
		93
	end
	def bank_assignment
		s = <<EOS
PRGROM, r_rrAA_AAAA_AAAA_AAAA, 0x8000
PRGROM, 1_11AA_AAAA_AAAA_AAAA, 0xc000
EOS
		s << BANK_ASSIGN_GENERIC_CHRRAM
		s
	end
	def vram_connection_set(h)
		vram_connection_static_detect(h)
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 14
		d <<= 4
		d |= 1
		register_write(c, d, 0b0111_0001)
	end
end

class DriverSUNSOFT3 < DriverBase
	def description_outline
		"SUNSOFT-3"
	end
	def mapper_number
		67
	end
	def bank_assignment
		@bank_register["PRGROM"] = {0x8000 => 0xf800}
		cpu_a = 0x8800
		ppu_a = 0
		4.times{
			@bank_register["CHRROM"][ppu_a] = cpu_a
			ppu_a += 0x800
			cpu_a += 0x1000
		}
		s = <<EOS
PRGROM, rr_rrAA_AAAA_AAAA_AAAA, 0x8000
PRGROM, 11_11AA_AAAA_AAAA_AAAA, 0xc000
CHRROM, rr_rrrr_rAAA_AAAA_AAAA, #{BANK_ASSIGN_PPUBANK_0x0800_4}
EOS
		s << BANK_ASSIGN_GENERIC_WRAM
		s
	end
	def nametable_wirecount
		5 + 2 + 1 + 2
	end
	def nametable_detection
		d = []
		4.times{|i|
			Memory.write(Command::REGION_CPU_6502, 0xe800, [i])
			d << Memory.vram_assignment
		}
		d == [NAMETABLE_SYSTEMRAM_A10, NAMETABLE_SYSTEMRAM_A11, NAMETABLE_SYSTEMRAM_L, NAMETABLE_SYSTEMRAM_H]
	end
end
class DriverSUNSOFT4 < DriverBase
	def description_outline
		"SUNSOFT-4"
	end
	def mapper_number
		68
	end
	def bank_assignment
		@bank_register["PRGROM"] = {0x8000 => 0xf000}
		cpu_a = 0x8000
		ppu_a = 0
		4.times{
			@bank_register["CHRROM"][ppu_a] = cpu_a
			ppu_a += 0x800
			cpu_a += 0x1000
		}
		2.times{
			@bank_register["CHRROM"][ppu_a] = cpu_a
			ppu_a += 0x400
			cpu_a += 0x1000
		}
		
		s = <<EOS
PRGROM, rr_rrAA_AAAA_AAAA_AAAA, 0x8000
PRGROM, 11_11AA_AAAA_AAAA_AAAA, 0xc000
CHRROM, rr_rrrr_rAAA_AAAA_AAAA, 0x0000,0x0800,0x1000,0x1800
#CHRROM, 1r_rrrr_rrAA_AAAA_AAAA, 0x2000,0x2400
EOS
		s << BANK_ASSIGN_GENERIC_WRAM
		s
	end
=begin
	def cartridge_initialize
		Memory.write(Command::REGION_CPU_6502, 0xe000, [0x10])
		true
	end
=end
	def workram_control_set(enable)
		d = 0
		if enable
			d = 1 << 4
		end
		Memory.write(Command::REGION_CPU_6502, 0xf000, [d])
	end
	def nametable_wirecount
		4 + 3 + #CPU A15:12 D4, D1:0
		2 + #VRAM CS# register + PPU A13
		3 + 4 #VRAM A10 PPU A12:10, registers
	end
	def nametable_detection
		d = []
		[0, 0x10].each{|r|
			Memory.write(Command::REGION_CPU_6502, 0xe000, [r])
			d << Memory.vram_assignment
		}
		if d[0][8, 4] != [0, 1, 0, 1]
			return false
		end
		d[1][8, 4].each{|t|
			if (t & 0b10) == 0
				return false
			end
		}
		true
	end
=begin
	def chrrom_bank_switch(c, processor_address, abs_address)
		a = processor_address << 1
		a |= 0x8000
		if processor_address >= 0x2400
			a = 0xd000
		end
		d = abs_address >> 10
		if processor_address < 0x2000
			d >>= 1
		end
		Memory.write(Command::REGION_CPU_6502, a, d, c)
	end
=end
end

class DriverSUNSOFT5 < DriverBase
	def description_outline
		"SUNSOFT-5 series"
	end
	def mapper_number
		69
	end
	def register_write(c, reg_a, reg_d)
		Memory.write(Command::REGION_CPU_6502, 0x8000, [reg_a, reg_d], c, 0xf000, 0x2000)
	end
	def bank_assignment
		s = <<EOS
PRGROM, rrr_rrrA_AAAA_AAAA_AAAA, 0x6000, 0x8000, 0xa000, 0xc000
PRGROM, 111_111A_AAAA_AAAA_AAAA, 0xe000
CHRROM,  rr_rrrr_rrAA_AAAA_AAAA, #{BANK_ASSIGN_PPUBANK_0x0400_8}
EOS
		s << BANK_ASSIGN_GENERIC_WRAM
		s
	end
	def workram_control_set(enable)
		d = 0
		if enable
			d = 0xc0
		end
		register_write(nil, 8, d)
	end
	def irq_disable
		register_write(nil, 0xd, 0)
	end
	def nametable_wirecount
		3 + 3 + #register address CPU A15:13 D3:0
		1 + 4
	end
	def nametable_detection
		d = []
		4.times{|i|
			register_write(nil, 0x0c, i)
			d << Memory.vram_assignment
		}
		d == [NAMETABLE_SYSTEMRAM_A10, NAMETABLE_SYSTEMRAM_A11, NAMETABLE_SYSTEMRAM_L, NAMETABLE_SYSTEMRAM_H]
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		d = abs_address >> 13
		if processor_address == 0x6000
			d &= 0x3f
		end
		a = processor_address / 0x2000
		a += 5
		register_write(c, a, d)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		a = processor_address >> 10
		d = abs_address >> 10
		register_write(c, a, d)
	end
end
