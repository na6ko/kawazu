module Memory
extend self
	def read(region, address, length, progressbar_index = -1, c = nil)
		if length <= 0 || length >= 0x10000
			xxx
		end
		if $debug_print
			Message::printf("read r:%d a:$%04X l:$%X\n", region, address, length)
		end
		a = {:type => Command::TYPE_SEQUENTIAL, :value=>[address]}
		if c == nil
			Command::read(region, length, a, {:progressbar_receive_index => progressbar_index})
		else
			a[:type] = Command::TYPE_RANDOM
			(length - 1).times{|i|
				a[:value] << (address + 1 + i)
			}
			c.access_set(length, region, Command::RW_READ_WITH_DATA, a)
		end
	end
	def read_dummy(region, address, length, c)
		if length >= 0x10000
			xxx
		end
		a = {:type => Command::TYPE_RANDOM, :value=>[address]}
		c.access_set(length, region, Command::RW_READ_WITHOUT_DATA, a)
	end
	def ad_set(address, data, address_and, address_add)
		a = {:type => Command::TYPE_RANDOM, :value => [address]}
		if address_add == 0 && address_and == 0xffff
			a[:type] = Command::TYPE_FIXED
		elsif address_add == 1 && address_and == 0xffff
			a[:type] = Command::TYPE_SEQUENTIAL
		else
			a[:value] = []
			data.size.times{
				a[:value] << address
				address += address_add
				address &= address_and
			}
		end
		d = nil
		if data != nil
			d = {:type => Command::TYPE_RANDOM, :value => data}
		end
		return a, d
	end
	def write_print(region, address, data)
		str = sprintf("write r:%d a:$%04X d:", region, address)
		if data == nil
			str << "open"
		else
			data.each{|t|
				str << sprintf("%02X ", t)
			}
		end
		Message.puts(str)
	end
	def write(region, address, data, c = nil, address_and = 0xffff, address_add = 1, option = {:progressbar_send_index => -1})
		if data.class.name == "Integer"
			data = [data]
		end
		if $debug_print
			write_print(region, address, data)
		end
		a, d = ad_set(address, data, address_and, address_add)
		rw = data == nil ? Command::RW_WRITE_WITHOUT_DATA : Command::RW_WRITE_WITH_DATA
		length = a[:value].size
		if data != nil
			length = d[:value].size
		end
		if c == nil
			Command::write(region, length, a, d, option)
		else
			c.access_set(length, region, rw, a, d)
		end
		true
	end
	def write_progress(region, address, data, option)
		a, d = ad_set(address, data, 0xffff, 1)
		Command.write_progress(region, a, d, option)
	end
	def chararam_detection
		address = 0x1234
		data = [0, 0xff]
		7.times{|i|
			data << (1 << i)
		}
		write(Command::REGION_PPU, address, data)
		read(Command::REGION_PPU, address, data.size) == data
	end
	def vram_assignment
		a = {:type => Command::TYPE_RANDOM, :value=>[]}
		0.step(0x4000 - 1, 1 << 10){|aa|
			a[:value] << aa
		}
		Command::read(Command::REGION_VRAM_CONNECTION, a[:value].size, a).map{|t|
			xor = 0
			if (t & (1 << 3)) != 0
				xor = 0b11
			end
			t &= 0b11
			t ^= xor
			t
		}
	end
end
