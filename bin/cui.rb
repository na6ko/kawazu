require Hamo::PROGRAM_DIR + '/driver_simulator.rb'
require Hamo::PROGRAM_DIR + '/config.rb'

class CuiForUser < Config
	def usage(action)
		action.each{|action_word, a|
			a = a.new(nil)
			Message::puts action_word + ' ' + a.arguments
			word = a.arguments.split(" ")
			str = "    "
			str << a.description << a.device_name
			Message::puts str
			str = "    (ommited) will be: #{action_word}"
			word.each{|s|
				str << ' ' << config_get(action_word, s)
			}
			Message::puts str
		}
	end

	def cui(src, action_name_to_class)
		if src.size == 0
			usage(action_name_to_class)
			return
		end
		action_name = src.shift
		if action_name_to_class.key?(action_name) == false
			Message::puts "unknown action: " + action_name
			usage(action_name_to_class)
			return
		end
		arguments = config_load(action_name)
		action_class = action_name_to_class[action_name]
		arguments["action"] = action_class
		a = action_class.new(nil)
		designated_argument = []
		a.arguments.split(" ").each{|enclosed_argument|
			value = ''
			opened_argument = enclosed_argument[1...-1]
			parentheses = enclosed_argument[0] == '(' && enclosed_argument[-1] == ')'
			brakets = enclosed_argument[0] == '[' && enclosed_argument[-1] == ']'
			if parentheses == false && brakets == false
				Fiber.raise
			end
			if src.size != 0
				value = src.shift
				if value[0, 2] == '--' && value.include?('=')
					designated_argument << value
					next
				end
			elsif parentheses
				value = arguments[opened_argument]
			end
			
			if brakets && (value == nil || value == "")
				Message::puts "required: #{enclosed_argument}"
				Message::puts action_name + " " + a.arguments
				return
			end
			
			if opened_argument == "drivername"
				if value == "auto"
					value = "DriverManager::DriverAutoDetector"
				elsif value[0, 6] != "Driver"
					value = "Driver" + value
				end
			elsif opened_argument == "portname" || opened_argument == "find_word"
				#nothing to do
			elsif opened_argument[0, 12] == "out:dirname."
				value = dircheck(value)
			elsif opened_argument[0, 12] == "in:filename."
				#nothing to do
			elsif opened_argument[0, 6] == "flash:"
				#nothing to do
			else
				Message::puts "unknown argument #{enclosed_argument}"
				Fiber.raise
			end

			if value == nil
				Message::puts "value == nil"
				Fiber.raise
			end
			arguments[opened_argument] = value
		}
		designated_argument.each{|value|
			t = value.split('=')
			key = t[0][2..-1]
			arguments[key] = t[1]
		}
		DriverManager::action_start(arguments)
	end
=begin
#GUI 用のオプション. -- で始まる. 並べる順番は問わない. ただし同じ名前で違う値の場合は後の値が有効.
--drivername=auto
--out:dirname.nes=xx
=end
	ACTION_SBP = {
		"version" => DriverManager::ActionFirmwareVersionGetSBP,
		"dump" => DriverManager::ActionRomDumpSBP,
		"Dump" => DriverManager::ActionRomRamDumpSBP,
		"read" => DriverManager::ActionRamReadSBP,
		"write" => DriverManager::ActionRamWriteSBP,
		"cartridge_program" => DriverManager::ActionCartridgeProgramSBP,
		"firmware_program" => DriverManager::ActionFirmwareProgramSBP,
		"flashid" => DriverManager::ActionCartridgeFlashIDGetSBP,
		"drivers" => DriverManager::ActionDriverList
	}
	ACTION_SIM = {
		"version" => DriverManager::ActionFirmwareVersionGetSim,
		"dump" => DriverManager::ActionRomDumpSim,
		"read" => DriverManager::ActionRamReadSim,
		"write" => DriverManager::ActionRamWriteSim,
		"cartridge_program" => DriverManager::ActionCartridgeFlashProgramSim,
		"flashid" => DriverManager::ActionCartridgeFlashIdGetSim
	}
end
def cui_for_user(src)
	c = CuiForUser.new
	c.cui(src, CuiForUser::ACTION_SBP)
end
def cui_sim(src)
	#$dma_puts = true
	$simulation = true
	Hamo::simulation_set(true)
	if Hamo::SIMULATOR_LOADED == false
		Message::puts("error: 'mcu_simulator.dll' is not found")
		return
	end
	c = CuiForUser.new
	c.cui(src, CuiForUser::ACTION_SIM)
end
def cui_mou(src)
	$dma_puts = true
	CuiForUser::cui(%w(dump 74161JF17 ../dump/JF-17_Moero!!_Pro_Tennis.nes), CuiForUser::ACTION_SIM, CuiForUser::USAGE_SIM)
	CuiForUser::cui(%w(write 74161JF17 xx), CuiForUser::ACTION_SIM, CuiForUser::USAGE_SIM)
end
