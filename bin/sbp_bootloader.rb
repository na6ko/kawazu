require Hamo::PROGRAM_DIR + './middlelibrary.rb'
require Hamo::PROGRAM_DIR + './message.rb'

module Bootloader
extend self
	ENTRY = "J@!Pn^y;x\r"
	def program_init
		Hamo::device.send([0x20])
		Hamo::device.rx_purge
		command = ENTRY.unpack('C*')
		Hamo::device.send(command)
		reply = []
		while reply != command
			if reply.size >= command.size
				reply.shift
			end
			r = Hamo::device.receive(1)
			if r == []
				Message::puts "communication error"
				return false
			end
			reply += r
		end
		r = Hamo::device.receive(2)
		if r.pack('C*') != "\n3"
			Message::puts "command reply error:" + r.pack('C*')
			return false
		end
		true
	end
	def program_data(buf, address)
		while buf.size > 0
			l = [buf.size, 0x40].min
			d = [0xa5]
			d.concat([address].pack('V1').unpack('C4'))
			Hamo::device.send(d)
			r = Hamo::device.receive(1)
			if r == []
				Message::puts "address reply timeout"
				return false
			end
			if r[0] != 0x40
				Message::puts "address reply error"
				p r.pack('C*')
				return false
			end
			d = Array.new(0x40, 0xff)
			d[0, l] = buf.slice!(0, l)
			Hamo::Progressbar::draw_index_set(0)
			Hamo::device.send(d)
			Hamo::Progressbar::draw_index_set(-1)
			r = Hamo::device.receive(1)
			if r == nil
				Message::puts "data reply timeout"
				return false
			end
			if r[0] != 0x40
				Message::puts "data reply error"
				return
			end
			address += l
			
		end
		true
	end
	def program_main(buf, address)
		if program_init == false
			return
		end
		sendsize = buf.size
		if sendsize % 0x40 != 0
			sendsize &= ~(0x40-1)
			sendsize += 0x40
		end
		Hamo::Progressbar::range_set(0, "firmware", sendsize)
		Hamo::Progressbar::range_set(1, "", 0)
		Hamo::Progressbar::ready
		if program_data(buf, address) == false
			return
		end
		Hamo::Progressbar::done
		Hamo::device.send [0xd7]
		Message::puts "programming is done. please re-connect USB cable."
	end
end
