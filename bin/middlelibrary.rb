module Hamo
=begin
** defined methods and classes by C layer ***
	def self.crc32_calc(array, crc32 = 0)
	end
	module Progressbar
		def self.range_set
		end
		def self.draw_index_set
		end
		def self.read
		end
		def self.done
		end
		def self.label_update
		end
	end
	module Textarea
		def self.message_print
		end
		def self.driverlist_item_add
		end
		def self.actionlist_item_add
		end
	end
	class SimulatorBase
		def initalize(hash)
		end
		def send(array)
		end
		def task
		end
	end
	class Usbcdc
		def open(portname)
		end
		def close
		end
		def send(array)
		end
		def receive
		end
		def usleep(ms)
		end
		def rx_purge
		end
		def receive_timeout_set
		end
	end
=end
	@@simulation = false
	def self.simulation_set(v)
		@@simulation = v
	end
	def self.simulator?
		@@simulation
	end
	def self.device
		@@device
	end
	def self.usbcdc_open(portname, developer_mode = true)
		@@device = Usbsbp.new
		@@device.open(portname, developer_mode)
	end
	def self.usbbootloader_open(portname)
		@@device = Usbbootloader.new
		@@device.open(portname)
	end
	class Usbbootloader < Usbcdc
		SBP_USB_VENDOR = 6666
		SBP_USB_PRODUCT = 8888
		def open(portname)
			if portname == 'auto'
				portname = open_auto(portname)
			end
			if portname == false
				return false
			end
			if super(portname) == false
				Message.puts "cannot open " + portname
				return false
			end
			true
		end
		def open_auto(portname)
			ports = usbdevice_find(SBP_USB_VENDOR, SBP_USB_PRODUCT)
			case ports.size
			when 0
				Message.puts "SBP is not found"
				return false
			when 1
				#nothing to do
			else
				str = "%d SBPs are found. Please select a COMPORT to use: " % port.size
				ports.each{|t|
					str << t << ','
				}
				Message.puts str.chop
				return false
			end
			portname = ports.shift
		end
	end
	class Usbsbp < Usbbootloader
		def open(portname, developer_mode)
			if super(portname) == false
				return false
			end
			@developer_mode = developer_mode
			Message::puts "comm.opened:" + portname, @developer_mode
			#text mode
			key = "^L?j6&B*JJ"
			send(key.unpack('C*'))
			r = receive(10)
			#p r.pack('C*')
			while r != key.unpack('C*')
				t = receive(1)
				if t == []
					p r.pack('C*')
					return false
				end
				r.shift
				r.concat t
			end
			send([0xd])
			
			r = receive(2 + 1)
			r.concat(receive((r[2] & 0x1f) - 1))
			r.shift
			r.shift
			r == [80, 0, 80, 92, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0]
		end
		def close(binarymode_exit)
			if binarymode_exit
				Command.header_send(Command::NUMBER_EXIT, [0, 0], 0, 0)
			end
			super()
			Message::puts "comm.closed", @developer_mode
		end
	end
	class Simulator < SimulatorBase
		def initialize(h)
			@h = h
			@h[:receive_data] = []
			super(@h)
			@retry_count_master = 4000
		end
		def close
		end
		def port_init(s)
			r = receive(1)
			x = r[0]
			if x & 0xe0 != 0x40
				Fiber.raise
			end
			r.concat(receive((x & 0x1f) - 1)) #discard first header, it's needed for text->binary mode
			if x == []
				Fiber.raise
			end
		end
		def receive(length)
			i = 0
			retry_count = @retry_count_master
			receive_data = @h[:receive_data]
			size_prev = receive_data.size
			while receive_data.size < length && i < retry_count
				Fiber.yield
				if size_prev == receive_data.size
					i += 1
				else
					i = 0
				end
				size_prev = receive_data.size
			end
			if receive_data.size < length
				return []
			end
			Hamo::Progressbar::add(length)
			receive_data.slice!(0, length)
		end
		def task(f)
			while f.alive?
				f.resume
				super(@h)
			end
		end
		def usleep(us)
			[1_000, us/1000].min.times{|t|
				Fiber.yield
			}
		end
		def receive_timeout_set(x)
			@retry_count_master = x * 100
		end
	end
	def self.simulator_open(h)
		@@device = Simulator.new(h)
	end
end

class FileSaver
	def tempfilename_get(path, suffix)
		filename = nil
		i = 0
		loop{
			filename = path + ("%04d" % i) + suffix
			if File.exist?(filename) == false
				break
			end
			i += 1
		}
		filename
	end
	def outfilename_get(arg, db)
		str = arg["out:filename.format"].dup
		if db.key?("title") == false
			return "unknown"
		end
		if db.key?("info_alt_title") == false
			db["info_alt_title"] = db["title"]
		end
		["info_serial", "title", "revision", "info_alt_title"].each{|key|
			data = ""
			if db.key?(key)
				data = db[key]
			end
			str.gsub!("[#{key}]", data)
		}
		if arg.key? "out:filename.space_replace"
			str.gsub!(' ', arg["out:filename.space_replace"])
		end
		str.gsub!('/', '_')
		":*?<>|$&".split('').each{|c|
			str.gsub!(c, '')
		}
		str
	end
	def open_read_crc32_get(filename)
		f = File.open(filename, 'rb')
		crc = Hamo::crc32_calc(f.read.unpack('C*'))
		f.close
		crc
	end
	def save
		savefile_directory = @arg["out:dirname." + @suffix].dup
		last = savefile_directory[-1, 1]
		if last != '/' && last != '\\'
			savefile_directory += '/'
		end
		if File.directory?(savefile_directory) == false
			#mruby には mkdir がない....
			Message::puts "please make a directory " + savefile_directory
			Fiber.raise
		end
		@arg[:outprefix] = outfilename_get(@arg, @db)
		savefile_directory_prefix = "#{savefile_directory}#{@arg[:outprefix]}"
		destfilename = "#{savefile_directory_prefix}.#{@suffix}"
		tempfilename = tempfilename_get("#{savefile_directory_prefix}_", "." + @suffix)
		if @db == nil
			xx
		end
		if File.exist?(destfilename)
			sameprefix = {}
			sameprefix[open_read_crc32_get(destfilename)] = destfilename
			#TBD: Hamo::File::find の利用
			1.upto(100){|i|
				filename = "#{savefile_directory_prefix}_%04d.#{@suffix}" % i
				if File.exist?(filename) 
					sameprefix[open_read_crc32_get(filename)] = filename
				end
			}
			f = File.open(tempfilename, 'wb')
			save_file(f)
			f.close
			crc = open_read_crc32_get(tempfilename)
			if sameprefix.key?(crc)
				Message::puts("same file #{sameprefix[crc]} exists, skip saving a new file")
				Hamo::File::unlink(tempfilename)
				return
			end
			destfilename = tempfilename
		else
			f = File.open(destfilename, 'wb')
			save_file(f)
			f.close
		end
		Message::puts("saved: #{destfilename}")
	end
end
