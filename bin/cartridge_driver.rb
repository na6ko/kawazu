require Hamo::PROGRAM_DIR + '/programmer.rb'
module BitTool
	def self.bit_get(data, src, dest, width = 1, xor = 0)
		data >>= src
		data &= (1 << width) - 1
		data << dest
	end
	def self.bit_swap(src, pair)
		bit_0 = pair[0]
		bit_1 = pair[1]
		dest = src
		dest &= ~(1 << bit_0)
		dest &= ~(1 << bit_1)
		dest |= bit_get(src, 0, bit_0)
		dest |= bit_get(src, 1, bit_1)
		dest
	end
end

class DriverBase
	#VRAM_CS# = PPU_A13#, VRAM_A10 = PPU_A10
	NAMETABLE_SYSTEMRAM_A10 = [
		2, 3, 2, 3, 2, 3, 2, 3, 0, 1, 0, 1, 0, 1, 0, 1
	]
	#VRAM_CS# = PPU_A13#, VRAM_A10 = PPU_A11
	NAMETABLE_SYSTEMRAM_A11 = [
		2, 2, 3, 3, 2, 2, 3, 3, 0, 0, 1, 1, 0, 0, 1, 1
	]
	#VRAM_CS# = PPU_A13#, VRAM_A10 = L
	NAMETABLE_SYSTEMRAM_L = [
		2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0
	]
	#VRAM_CS# = PPU_A13#, VRAM_A10 = H
	NAMETABLE_SYSTEMRAM_H = [
		3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1
	]
	#基板の印字や文献で PRGRAM, W-RAM, Work RAM など一致しないが BANK_ASSIGN としては WRAM で統一する. 個別のドライバでは基板の印字などを優先しても良い.
	BANK_ASSIGN_GENERIC_WRAM = 
		"WRAM,          A_AAAA_AAAA_AAAA, 0x6000\n"
	BANK_ASSIGN_GENERIC_CHRRAM = 
		"CHRRAM,        A_AAAA_AAAA_AAAA, 0x0000\n"
	BANK_ASSIGN_PPUBANK_0x0800_4 = 
		"0x0000, 0x0800, 0x1000, 0x1800"
	BANK_ASSIGN_PPUBANK_0x0400_8 = 
		"0x0000, 0x0400, 0x0800, 0x0c00, 0x1000, 0x1400, 0x1800, 0x1c00"
	MEMORY_TO_REGION = {
		"PRGROM" => Command::REGION_CPU_6502,
		"WRAM" => Command::REGION_CPU_6502,
		"CHRROM" => Command::REGION_PPU,
		"CHRRAM" => Command::REGION_PPU
	}
	WORKRAM_PROGRESSBAR_INDEX = 0
	def initialize(new_by_auto_detector)
		@new_by_auto_detector = new_by_auto_detector
		@variant = {}
		@bank_list = {}
		@bank_register = {"PRGROM" => {}, "CHRROM" => {}}
		bank_define()
	end
	def chararam_detection
		true
	end
	def auto_detector
		false
	end
	def submapper_number
		nil
	end
	def description_number
		if mapper_number == nil
			return ""
		end
		r = "#{mapper_number}"
		if submapper_number
			r << ".#{submapper_number}"
		end
		r
	end
	def mapper_digits
		digits = description_number
		if digits != ''
			val = digits.gsub(" ", '').split(",").map{|t|
				t.to_f * 100
			}.min.to_i
			digits = "%06d" % val
		end
		digits
	end
#set to gui list
	def list_set
		item = {
			"name" => self.class.name,
			"description_outline" => description_outline,
			"description_number" => description_number.gsub(",", ", "),
			"auto_detector" => auto_detector.to_s,
			"autodetect_attribute" => autodetect_attribute.to_s,
			"mapper_digits" => mapper_digits
		}
		Hamo::Textarea::driverlist_item_add(item)
	end
	AUTO_DETECTION_NOT_AVAILABLE = 0
	AUTO_DETECTION_NORISK = 1
	AUTO_DETECTION_RISKY_FOR_BACKUPRAM = 2
	def autodetect_attribute
		if nametable_register_has_busconflict? || !nametable_register_available?
			return AUTO_DETECTION_NOT_AVAILABLE
		end
		if nametable_register_mapped_on_6000_to_7fff?
			return AUTO_DETECTION_RISKY_FOR_BACKUPRAM
		end
		AUTO_DETECTION_NORISK
	end
#cartridge depended on memory controllers
	#returns simple number or booleans
	def nametable_wirecount
		2 #PPU A13#, PPU A11 or A10
	end
	def nametable_register_available?
		nametable_wirecount > 2
	end
	def nametable_register_has_busconflict?
		false
	end
	def nametable_register_mapped_on_6000_to_7fff?
		false
	end
	def workram_have_writeprotection
		true
	end
	#send read and write command to MCU
	def nametable_detection
		false
	end
	def workram_control_set(enable)
	end
	def irq_disable
	end
	def vram_connection_set(h)
		true
	end
	def cartridge_initialize
	end
private
	def charrom_bank_defined?
		@bank_list.key?("CHRROM")
	end
	def vram_connection_static_detect(h)
		r = true
		d = Memory.vram_assignment
		case d
		when NAMETABLE_SYSTEMRAM_A10
			h[:vram_mirror] = Nesfile::VRAM_MIRROR_V
		when NAMETABLE_SYSTEMRAM_A11
			h[:vram_mirror] = Nesfile::VRAM_MIRROR_H
		else
			Message.puts("vram connection might error")
			r = false
		end
	end
	#for flash programming mode
	def vram_connection_compare(v)
		h = {}
		vram_connection_set(h)
		if h.key?(:vram_mirror) == false #dynamic, no compare
			return
		end
		if v != h[:vram_mirror]
			Message.puts("vram connections are not matched")
			s = {Nesfile::VRAM_MIRROR_V => "Vertical", Nesfile::VRAM_MIRROR_H => "Horizontal"}
			Message.puts("image: #{s[v]}")
			Message.puts("cartridge: #{s[h[:vram_mirror]]}")
		end
	end
	def variant_num_set(memory, num)
		@variant[memory] = {:num => num, :index => 0}
	end
	def bank_define
		bank_assignment.split("\n").each{|t|
			if t.size == 0
				next
			end
			if t.strip[0] == '#'
				next
			end
			s = t.split(",")
			memory = s.shift
			h = {}
			h[:assign_processor] = 0
			h[:assign_register] = 0
			h[:assign_0] = 0
			h[:assign_1] = 0
			h[:width] = 1
			str = s.shift.strip.gsub('_', '')
			str.split('').each{|a|
				h[:width] <<= 1
				h[:assign_processor] <<= 1
				h[:assign_register] <<= 1
				h[:assign_0] <<= 1
				h[:assign_1] <<= 1
				case a
				when '0'
					h[:assign_0] |= 1
				when '1'
					h[:assign_1] |= 1
				when 'A'
					h[:assign_processor] |= 1
				when 'r'
					h[:assign_register] |= 1
				else
					xxx
				end
			}
			h[:entry] = []
			temp = []
			s.each{|a|
				temp << a.to_i(0)
			}
			temp.sort.each{|e|
				h[:entry] << (e..(e+h[:assign_processor]))
			}
			a = h[:assign_processor]
			h[:bank_shift] = 0
			while a != 0
				h[:bank_shift] += 1
				a >>= 1
			end
			if @bank_list.key?(memory) == false
				@bank_list[memory] = []
			end
			@bank_list[memory] << h
		}
	end
	def rom_dump_memory(memory, fixed_only)
		if @bank_list.key?(memory) == false
			return
		end
		b = @bank_list[memory]
		region = MEMORY_TO_REGION[memory]
		progressbar_index = region == Command::REGION_CPU_6502 ? 0 : 1
		if fixed_only
			progressbar_index = -1
		end
		length = b[0][:width]
		fixedbank = b.find{|t|
			t[:width] == (t[:assign_processor] + 1) ||
			(memory == "PRGROM" && (
				t[:assign_0] != 0 || t[:assign_1] != 0
			))
		}
		if fixedbank != nil && memory == "PRGROM"
			e = fixedbank[:entry][0]
			@prg_rom_fixedpage = {
				:processor_address => e.begin,
				:data => Memory.read(region, e.begin, e.size, progressbar_index)
			}
			@prg_rom_fixedpage[:rom_address] = fixedbank[:assign_1]
			length -= @prg_rom_fixedpage[:data].size
			if length == 0
				return @prg_rom_fixedpage[:data]
			end
		elsif fixedbank != nil && memory == "CHRROM"
			e = fixedbank[:entry][0]
			return Memory.read(region, e.begin, e.size, progressbar_index)
		end
		
		if fixed_only
			return
		end

		data = []
		switchable_bank = b.find_all{|s|
			s[:assign_register] != 0
		}
		rom_abs_address = 0
		if memory == "PRGROM" && @prg_rom_fixedpage != nil && @prg_rom_fixedpage[:rom_address] == 0
			rom_abs_address = @prg_rom_fixedpage[:data].size
			data = @prg_rom_fixedpage[:data].dup
		end
		while length > 0
			c = Command::Content.new
			switchable_total_length = 0
			processor_address = 0
			banks = switchable_bank.find_all{|t|
				if t[:assign_0] == 0 && t[:assign_1] == 0
					next true
				end
				((rom_abs_address & t[:assign_0]) == 0) &&
				((rom_abs_address & t[:assign_1]) == t[:assign_1])
			}
			processor_address = banks[0][:entry][0].begin
			banks.each{|t|
				t[:entry].each{|e|
					if rom_abs_address >= b[0][:width]
						break
					end
					if bank_switch(c, region, e.begin, rom_abs_address) == false
						return nil
					end
					switchable_total_length += e.size
					rom_abs_address += e.size
				}
			}
			c.send_receive(Command::NUMBER_BUS_ACCESS_START, [0, 0])
			l = [switchable_total_length, length].min
			dd = Memory.read(region, processor_address, l, progressbar_index)
			if memory == "PRGROM" && @prgrom_prev_page != nil
				#for GNROM and variants
				@prgrom_prev_page[:data] = dd
			end
			data.concat(dd)
			length -= l
		end
		if memory == "PRGROM" && @prg_rom_fixedpage != nil && @prg_rom_fixedpage[:rom_address] != 0
			data.concat(@prg_rom_fixedpage[:data])
		end
		data
	end
	def pagesize_get(memory)
		b = @bank_list[memory]
		ar = []
		b.each{|t|
			t[:entry].each{|s|
				ar << s.size
			}
		}
		ar.min
	end
	def rom_dump_main(h, memory)
		num = 1
		if @variant.key? memory
			num = @variant[memory][:num]
		end
		h[:data] = []
		num.times{|i|
			if num != 1
				@variant[memory][:index] = i
			end
			h[:pagesize] = pagesize_get(memory)
			r = rom_dump_memory(memory, false)
			if r == nil
				return false
			end
			h[:data].concat(r)
		}
		true
	end
	def programming_address_set(t, e, switch)
		h = {}
		h[:absolute] = t[:assign_0] | t[:assign_1]
		h[:processor] = [e]
		h[:combined_bank] = e
		h[:switch] = switch
		h
	end
	def programming_address_assignment(m, memory)
		bank = @bank_list[memory].map{|src|
			dest = src.dup
			dest[:entry] = src[:entry].dup
			dest
		}
		bank.delete_if{|t|
=begin          15   12
0x2000 bytes 11_111A_AAAA... <- bit14:13 が 2'b01, 2'b10 に設定[不可]
0x4000 bytes 11_11AA_AAAA... <- bit14:13 が 2'b10 に設定可能
=end
			key = nil
			offset = 0
			abs_a_bit_13 = BitTool::bit_get(t[:assign_processor], 13, 0)
			abs_a_bit_13 |= BitTool::bit_get(t[:assign_register], 13, 0) #processor address bus, または register から出ている線なら 0
			if BitTool::bit_get(t[:assign_0], 14, 0) == 1 && abs_a_bit_13 == 1
				key = :c_2aaa
				offset = 0x02aaa
			elsif BitTool::bit_get(t[:assign_1], 14, 0) == 1 && abs_a_bit_13 == 1
				key = :c_5555
				offset = 0x05555
			else
				next false
			end

			r = t[:entry].shift
			m[key] = programming_address_set(t, r, false)
			m[key][:absolute] |= offset #固定値を介するため上位アドレスbitは |= 演算子で設定
			t[:entry].size == 0
		}
		bank.sort!{|a,b|
			a[:entry].size <=> b[:entry].size
		}
		bank.delete_if{|t|
			if m[:c_dest] != nil
				break
			end
			
			if t[:assign_0] != 0 || t[:assign_1] != 0
				next true
			end
			while t[:entry].size != 0 && m[:c_dest] == nil
				if m[:c_2aaa] == nil
					r = t[:entry].shift
					m[:c_2aaa] = programming_address_set(t, r, true)
					m[:c_2aaa][:absolute] = 0x02aaa #register を介するのでレジスタアドレスbitはすべて0なので = 演算子で設定
					m[:c_2aaa][:uniq_bank] = true
				elsif m[:c_5555] == nil
					r = t[:entry].shift
					m[:c_5555] = programming_address_set(t, r, true)
					m[:c_5555][:absolute] = 0x05555
					m[:c_5555][:uniq_bank] = true
				elsif m[:c_dest] == nil
					r = t[:entry].shift
					m[:c_dest] = programming_address_set(t, r, true)
					m[:c_dest][:uniq_bank] = true
				end
			end
			t[:entry].size == 0
		}
		if m.key?(:c_dest) == false
			if m[:c_2aaa][:switch] == true
				m[:c_dest] = m[:c_2aaa]
				m[:c_dest][:uniq_bank] = false
			elsif m[:c_5555][:switch] == true
				m[:c_dest] = m[:c_5555]
			end
			if m.key?(:c_dest) == false
				Message::puts ("Can't assign #{memory} programming banks")
				Fiber.raise
			end
		end
		if m[:c_dest][:combined_bank].size < 0x1000
			bank.each{|t|
				while t[:entry].size != 0
					e = t[:entry].shift
					m[:c_dest][:processor] << e
					m[:c_dest][:combined_bank] = (m[:c_dest][:combined_bank].begin .. e.end)
					if m[:c_dest][:combined_bank].size >= 0x1000
						break
					end
				end
			}
		end
		[:c_2aaa, :c_5555, :c_dest].each{|k|
			if m.key?(k) == false
				Message::puts ("Can't assign #{memory} programming banks, no #{k}")
				return nil
			end
		}

		k = nil
		case memory
		when "PRGROM"
			k = Programmer::ProgrammerCpu
		when "CHRROM"
			k = Programmer::ProgrammerPpu
		end
		return k.new(self, m)
	end
	def bank_switch(c, region, processor_address, abs_address)
		r = false
		case region
		when Command::REGION_CPU_6502, Command::REGION_CPU_FLASH
			r = prgrom_bank_switch(c, processor_address, abs_address)
		when Command::REGION_PPU
			r = chrrom_bank_switch(c, processor_address, abs_address)
		end
		r
	end
	def bank_switch_default(c, memory, processor_address, abs_address)
		h = @bank_list[memory].find{|t|
			t[:entry].find{|s|
				s.include? processor_address
			}
		}
		Memory.write(Command::REGION_CPU_6502, @bank_register[memory][processor_address], abs_address >> h[:bank_shift], c)
	end
	def prgrom_bank_switch(c, processor_address, abs_address)
		bank_switch_default(c, "PRGROM", processor_address, abs_address)
	end
	def chrrom_bank_switch(c, processor_address, abs_address)
		bank_switch_default(c, "CHRROM", processor_address, abs_address)
	end
	
	def variant_num(key)
		mul = 1
		if @variant.key? key
			mul = @variant[key][:num]
		end
		mul
	end
public
	def rom_dump(arg)
		h = Nesfile.header_new
		
		#bank_define(bank_assignment)
		if cartridge_initialize == false
			return
		end
		workram_control_set(false)
		irq_disable
		if vram_connection_set(h) == false
			return
		end
		chararam = false
		if chararam_detection
			chararam = Memory.chararam_detection
		end

		Hamo::Progressbar::range_set(0, "Program ROM", @bank_list["PRGROM"][0][:width] * variant_num("PRGROM"))
		if chararam
			Hamo::Progressbar::range_set(1, "Character RAM", 0)
		elsif @bank_list.key? "CHRROM"
			Hamo::Progressbar::range_set(1, "Character ROM", @bank_list["CHRROM"][0][:width] * variant_num("CHRROM"))
		else
			Message.puts("Character ROM is found, the driver is not assigned for it")
			return
		end
		
		if $debug_print == false or $debug_print == nil
			Hamo::Progressbar::ready
		end
		if rom_dump_main(h[:cpu_program], "PRGROM") == false
			return
		end
		if chararam == false
			h[:ppu_chara][:name] = "Character ROM"
			if rom_dump_main(h[:ppu_chara], "CHRROM") == false
				return
			end
		end
		Hamo::Progressbar::done
		romimage_save(h, arg)
	end
	def romimage_save(h, arg)
		h[:mappernum] = self.mapper_number
		if submapper_number
			h[:submappernum] = submapper_number
		end
		Nesfile.save(h, arg)
	end
	def workram_mode_init
		#bank_define(bank_assignment)
		if @bank_list.key?("WRAM") == false
			Message.puts("This driver is not supported for Work RAM")
			return nil
		end
		if cartridge_initialize == false
			return
		end
		workram_control_set(true)
		irq_disable
		list = @bank_list["WRAM"]
		if list.size != 1
			Message.puts(list.size.to_s + " WRAMs are assigned")
			return nil
		end
		list = list.shift
=begin
		if list[:assign_register] != 0
			Message.puts("This driver is not supported WRAM bank switching")
			p list
			return
		end
=end
		list
	end
	def workram_read_main(length, list)
		d = []
		if list[:assign_register] == 0
			d = Memory.read(Command::REGION_CPU_6502, list[:entry][0].begin, length, WORKRAM_PROGRESSBAR_INDEX)
		else
			cpu_a = list[:entry][0].begin
			ram_a = 0
			l = list[:entry][0].size
			while(length > 0)
				c = Command::Content.new
				workram_bank_switch(c, cpu_a, ram_a)
				c.send_receive(Command::NUMBER_BUS_ACCESS_START, [0, 0])
				d.concat(
					Memory.read(Command::REGION_CPU_6502, cpu_a, l, WORKRAM_PROGRESSBAR_INDEX)
				)
				ram_a += l
				length -= l
			end
		end
		d
	end
	class RamImageSaver < FileSaver
		def initialize(arg, db, ramimage)
			@suffix = "sav"
			@arg = arg
			@db = db
			@ramimage = ramimage
		end
		def save_file(f)
			f.write(@ramimage.pack('C*'))
		end
	end
	def workram_read_file_save(arg, db, data)
		t = RamImageSaver.new(arg, db, data)
		t.save
	end
	def workram_read(arg, db)
		list = workram_mode_init
		if list == nil
			return
		end
		length = list[:width]
		Hamo::Progressbar::range_set(0, "Backup RAM", length)
		Hamo::Progressbar::range_set(1, "", 0)
		Hamo::Progressbar::ready
		data = workram_read_main(length, list)
		workram_control_set(false)
		Hamo::Progressbar::done
		workram_read_file_save(arg, db, data)
	end
	def workram_write(srcfilename, option = {})
		list = workram_mode_init
		if list == nil
			return
		end
		length = list[:width]
		f = File.open(srcfilename["in:filename.sav"], 'rb')
		src = f.read(length).unpack('C*')
		f.close
		Hamo::Progressbar::range_set(0, "Backup RAM", length * 2)
		Hamo::Progressbar::range_set(1, "", 0)
		Hamo::Progressbar::ready
		option[:progressbar_send_index] = WORKRAM_PROGRESSBAR_INDEX
		if list[:assign_register] == 0
			Memory.write_progress(Command::REGION_CPU_6502, list[:entry][0].begin, src, option)
		else
			cpu_a = list[:entry][0].begin
			ram_a = 0
			l = list[:entry][0].size
			while(length > 0)
				c = Command::Content.new
				workram_bank_switch(c, cpu_a, ram_a)
				c.send_receive(Command::NUMBER_BUS_ACCESS_START, [0, 0])
				Memory.write_progress(Command::REGION_CPU_6502, cpu_a, src[ram_a, l], option)
				ram_a += l
				length -= l
			end
		end
		d = workram_read_main(list[:width], list)
		workram_control_set(false)
		Hamo::Progressbar::done
		if src == d
			Message.puts("memory compare ok")
		else
			Message.puts("memory compare error")
		end
	end
	def flash_data_layout(rv, region_name, layout)
		romdata = rv[:romdata]
		if romdata.length == 0
			rv[:enable] = false
			return
		end
		rv[:enable] = layout != "disable"
		rom_total_length = @bank_list[region_name][0][:width]
		if romdata.length == rom_total_length
			return
		end
		if romdata.length > rom_total_length
			Message::puts "#{region_name} size overs controllable memory size"
			Fiber.raise
		elsif rom_total_length % romdata.length != 0
			Message::puts "#{region_name} size is not divide number for controller memory size"
			Fiber.raise
		end
		
		fixed_1 = nil

		if region_name == "PRGROM"
			fixed_0 = @bank_list[region_name].find{|t|
				t[:assign_0] != 0
			}
			if fixed_0 != nil
				#TBD
				xxx
			end
			fixed_1 = @bank_list[region_name].find{|t|
				t[:assign_1] != 0
			}
			if fixed_1 == nil
				Message::puts "non-fixed bank is not supported"
				Fiber.raise
			end
		end

		ar = Array.new(rom_total_length, 0xff)
		
		case layout
		when "top"
			l = romdata.length
			if region_name == "PRGROM"
				fixed_bank_size = fixed_1[:entry][0].size
				l -= fixed_bank_size
				ar[0, l] = romdata[0, l]
				l = fixed_bank_size
				ar[
					rom_total_length - l, l
				] = romdata[-l, l]
			else
				ar[0, l] = romdata
			end
		when "bottom"
			l = romdata.length
			ar[-l, l] = romdata
		when "disable"
			romdata.clear
			rv[:enable] = false
			return
		else
			ar = []
			while ar.size < rom_total_length
				ar.concat(romdata)
			end
		end
		romdata.clear
		romdata.concat(ar)
	end
end

class DriverWritebusconflicts < DriverBase 
	def nametable_register_has_busconflict?
		true
	end
	def cartridge_initialize
		fixed_bank = @bank_list["PRGROM"].find{|s|
			s[:assign_0] != 0 || s[:assign_1] != 0
		}
		if fixed_bank != nil
			return false
		end
		e = @bank_list["PRGROM"][0][:entry][0]
		#初期バンクレジスタが不定だがバス衝突回避のため現在の page から ROM data を取得して書き込み源とする
		@prgrom_prev_page = {}
		@prgrom_prev_page[:processor_address] = e.begin
		@prgrom_prev_page[:data] = Memory.read(Command::REGION_CPU_6502, e.begin, e.size)
		true
	end
	def register_write(c, writedata, data_mask, requested_address = 0, address_mask = 0)
		src = @prg_rom_fixedpage
		if @prgrom_prev_page != nil
			src = @prgrom_prev_page
		end
		single = false
		if c == nil
			c = Command::Content.new
			single = true
		end
		address = nil
		src[:data].size.times{|a|
			d = src[:data][a]
			if (d & data_mask) != writedata
				next
			end
			a += src[:processor_address]
			if (a & address_mask) != requested_address
				next
			end
			address = a
			break
		}
		if address == nil
			Message.puts("write data is not found in readbuffer")
			return false
		end
		Memory.write(Command::REGION_CPU_6502, address, nil, c)
		if single
			c.send_receive(Command::NUMBER_BUS_ACCESS_START, [0, 0])
		end
		true
	end
	#can't program
	def flash_program_init_cpu(t, nesfile)
		xx
	end
	def flash_program_init_ppu(t, nesfile)
		xx
	end
end

module Notice
	include Message
	def cartridge_ram_detection_by_romhash(pcbname)
		pcbname = pcbname.dup
		pcbname.sub!("HVC-", '')
		pcbname.sub!("NES-", '')
ja = <<EOS
ROM hash よりカートリッジ基板を #{pcbname} と推定しました. backup RAM は #{pcbname} として読み出します. 
今後 backup RAM の読み出しのみを行う場合は #{pcbname} を指定してください. 推定が間違っている場合は手動で適切なドライバを選択してやりなおしてください.
EOS
		print(ja)
	end
end
