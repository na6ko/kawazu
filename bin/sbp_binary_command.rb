module DebugOut
	ES_BACKGROUND_GRAY = "\x1b[48;2;0;80;96m"
	ES_BACKGROUND_RESTORE = "\x1b[49m"
	ES_PAIR = ["", ES_BACKGROUND_GRAY]
	def self.dump(str, data)
		i = 0
		pair = 0
		line = 0
		if data.size >= 0x10
			str << "\n"
		end
		data.each{|t|
			case i
			when 0, 4, 8, 12
				str += ES_PAIR[pair]
				pair ^= 1
			end
			
			str += "%02X" % t
			
			case i
			when 3, 7, 11
				if pair == 0
					str += ES_BACKGROUND_RESTORE
				end
				str += '-'
			when 15
				str += ES_BACKGROUND_RESTORE
				str += "\n"
				line += 1
				if line >= 4
					pair ^= 1
					line = 0
				end
			else
				str += ' '
			end
			i += 1
			if i >= 0x10
				i = 0
			end
		}
		puts str + ES_BACKGROUND_RESTORE
	end
	CONTENT_TYPE = %w(buscontrol data-write address data-read)
	AD_TYPE = %w(random seq fix fifo)
	def self.array_shift_byte_set_to_str(str, data, times)
		times.times{
			str << ("$%02x " % data.shift)
		}
	end
	def self.content_view(data)
		while data.size != 0
			d = data.shift
			buscontrol_flash = d & 0b11
			d >>= 2
			bit3_2 = d & 0b11
			d >>= 2
			content_type = d & 0b11
			d >>= 2
			continue_end = d & 0b11
			length = data.shift
			length |= data.shift << 8
			str = CONTENT_TYPE[content_type].dup
			case content_type
			when 0, 1, 2 #buscontrol, data-write
				str << " " << AD_TYPE[bit3_2] + ' '
				times = 1
				if bit3_2 == 0
					times = length
				end
				if content_type < 2
					if times <= 0x20
						array_shift_byte_set_to_str(str, data, times)
					else
						array_shift_byte_set_to_str(str, data, 0x10)
						times -= 0x10
						str << ("(snip $%x bytes) " % (times - 0x10))
						data.slice!(0, times - 0x10)
						times = 0x10
						array_shift_byte_set_to_str(str, data, times)
					end
				else
					times.times{
						d = data.shift
						d |= data.shift << 8
						str << ("$%04x " % d)
					}
				end
				if bit3_2 >= 1
					str.rstrip!
					str << (", $%x times" % length)
				end
			when 3 #data-read
				str << (bit3_2 == 0 ? "-dummy" : "-valid")
				str << (" $%x" % length)
			end
			puts str
			if continue_end == 0b11
				break
			end
		end
	end
end

module Command
	NUMBER_EXIT = 2
	NUMBER_VERSION_GET = 3
	NUMBER_BUS_ACCESS_LOOPCOUNT_SET = 0x10
	NUMBER_BUS_ACCESS_START = 0x11
	NUMBER_FLASH_MODE_ALLOCATE_FREE = 0x12
	NUMBER_FLASH_FIFO_DATA_SEND = 0x13
	NUMBER_BUS_ACCESS_PROGRAM = 0x14
	NUMBER_FLASH_NOTIFY_CONTROL = 0x15
	NUMBER_FLASH_STATUS_NOTIFY = 0x16
	
	REGION_CPU_6502 = 0; REGION_CPU_FLASH = 1
	REGION_PPU = 2; REGION_VRAM_CONNECTION = 3

	CONTENT_CONTINUE = 0b01; CONTENT_END = 0b11
	CONTENT_TYPE_BUSCONTROL = 0; CONTENT_TYPE_DATA_WRITE = 1;
	CONTENT_TYPE_ADDRESS = 2; CONTENT_TYPE_DATA_READ = 3

	RW_READ_WITH_DATA = 0; RW_READ_WITHOUT_DATA = 4
	RW_WRITE_WITH_DATA = 1; RW_WRITE_WITHOUT_DATA = 2

	TYPE_RANDOM = 0; TYPE_SEQUENTIAL = 1; TYPE_FIXED = 2; TYPE_FIFO_PAGE = 3
	TYPE_READ_DUMMY = 0; TYPE_READ_VALID = 1

	STATUS_NEXT_COMMAND_READY = 1 << 6
	STATUS_DIR_PC_TO_MCU = 0 << 5; STATUS_DIR_MCU_TO_PC = 1 << 5
	STATUS_TYPE_COMMAND = 0 << 4; STATUS_TYPE_DATA = 1 << 4
	STATUS_ERROR = 1 << 3
	STATUS_STATUS_MASK = 0b111
	STATUS_READY = 0; STATUS_REQUEST = 1; STATUS_BUSY = 2
	STATUS_CONTINUE = 3; STATUS_DONE = 4; STATUS_ABORT = 5

	FLASH_COMMAND_NONE = 0; FLASH_COMMAND_ERASE = 1
	FLASH_COMMAND_POLLING = 2; FLASH_COMMAND_PROGRAMMING = 3
	
	ORGNAIZING_DATA_FLAG_FIFO_DATA = 0x10000
extend self
	def repack(ar, packstr)
		ar.pack(packstr).unpack('C*')
	end

	class Content
		def initialize
			@c = {:packet_data=>nil, :programming => false}
			@organize_data_write = true
			@macro_limit = 8
			[:buscontrol, :data_write, :address, :data_read].each{|k|
				@c[k] = []
			}
		end
		def organize_data_write_disable
			@organize_data_write = false
		end
		def macro_limit_set(v)
			@macro_limit = v
		end
		def data_read_size
			@c[:data_read].size
		end
		def data_read_fill(offset)
			#p @c[:data_read], offset
			#xx
			@c[:data_read].fill(Command::TYPE_READ_VALID, offset...@c[:data_read].size)
		end
		def member_set(c, length, dest_member, spi_data)
			d = nil
			case spi_data[:type]
			when TYPE_FIXED, TYPE_FIFO_PAGE
				#p spi_data[:value]
				v = spi_data[:value][0]
				if v != nil && spi_data[:type] == TYPE_FIFO_PAGE
					v |= ORGNAIZING_DATA_FLAG_FIFO_DATA
				end
				d = Array.new(length, v)
			when TYPE_RANDOM
				d = spi_data[:value]
			when TYPE_SEQUENTIAL
				v = spi_data[:value][0]
				d = []
				length.times{
					d << v
					v += 1
				}
			else
				p spi_data[:type]
				xxx
			end
			c[dest_member].concat(d)
		end
		def access_set(length, region, rw, address, spi_data = nil, flash_command = FLASH_COMMAND_NONE)
			if @c[:pack_data] != nil
				Message::puts 'error: content is already finalized'
				Fiber.raise
			end
			#bus control
			bt = {:type => TYPE_FIXED, :value => [(region << 6) | ((rw & 0b11) << 4) | flash_command]}
			if length == nil
				xx
			end
			member_set(@c, length, :buscontrol, bt)
			#data write
			if spi_data == nil
				spi_data = {:value => [nil], :type => TYPE_FIXED}
			end
			member_set(@c, length, :data_write, spi_data)
			#address
			member_set(@c, length, :address, address)
			#data read
			data_read = {:type => TYPE_FIXED, :value => [TYPE_READ_DUMMY]}
			case rw
			when RW_READ_WITH_DATA
				data_read[:value] = [TYPE_READ_VALID]
			end
			member_set(@c, length, :data_read, data_read)
			true
		end

		def content_copy(s)
			d = {}
			[:buscontrol, :data_write, :address, :data_read].each{|k|
				d[k] = s[k].dup
			}
			d
		end
		def content_fixing(c)
			if c[:content].find{|t|
				t[:spi_data][:type] == TYPE_SEQUENTIAL || t[:spi_data][:type] == TYPE_FIFO_PAGE
			}
				return false
			end
			val = c[:content][0][:spi_data][:value][0]
			if c[:content].find{|t|
				t[:spi_data][:value].find{|s|
					s  != val
				}
			}
				return false
			end
			l = 0
			c[:content].each{|t|
				l += t[:length]
			}
			c[:content][0][:length] = l
			c[:content][0][:spi_data][:type] = TYPE_FIXED
			c[:content][0][:spi_data][:value] = [val]
			c[:content] = c[:content][0, 1]
			return true
		end

		def content_print(s)
			puts '---'
			s[:content].each{|t|
				print t[:length]
			}
		end
		def content_packet_set(c, key, type, data, macro_count = nil)
			d = []
			data_pack = 'C'
			v = 0b01 << 6
			case key
			when :buscontrol
				v |= 0 << 4
			when :data_write
				v |= 1 << 4
			when :address
				data_pack = 'v'
				v |= 2 << 4
			when :data_read
				if type != TYPE_FIXED
					Message::puts "data_read channel allowed only type = FIXED"
					xxx
				end
				v |= 3 << 4
				vv = 0
				if macro_count == nil
					vv = data[0]
				else
					vv = data
				end
				v |= vv << 2
			end
			if key != :data_read
				v |= type << 2
			end
			#TBD: flash command
			pack = 'Cv'
			d << v
			if macro_count == nil
				pack += data_pack + data.size.to_s
				d << data.size
				if key != :data_read
					d.concat(data)
				end
			else
				pack += data_pack + macro_count.to_s
				d << macro_count
				if key != :data_read
					d << data
				end
			end
			c[:last_header_offset] = c[:packet_data].size
			c[:packet_data].concat(Command::repack(d, pack))
		end
		ORGANIZE = {
			:buscontrol => {:sequential => false, :macro_limit => 8},
			:data_write => {:sequential => true, :macro_limit => 0x80, :data_mask => 0x00ff},
			:data_read => {:sequential => false, :macro_limit => 2},
			:address => {:sequential => true, :macro_limit => 8, :data_mask => 0xffff}
		}
		def content_organize(c, key, macro_use)
			h = ORGANIZE[key]
			if key == :buscontrol
				h[:macro_limit] = @macro_limit
			end
			d = c[key].dup
			random_data = []
			while d.size != 0
				first_data = d.shift
				macro_count = 0
				type = nil
				if d.size == 0
					random_data << first_data
					break
				elsif macro_use && first_data == d[0]
					macro_count = 1
					type = TYPE_FIXED
					d.each{|t|
						if first_data != t
							break
						end
						macro_count += 1
					}
				elsif macro_use && h[:sequential] && first_data + 1 == d[0]
					macro_count = 1
					v = first_data + 1
					type = TYPE_SEQUENTIAL
					d.each{|t|
						if v != t
							break
						end
						macro_count += 1
						v += 1
						v &= h[:data_mask]
					}
				end
				if macro_count < h[:macro_limit]
					random_data << first_data
					next
				end
				if key == :data_read
					random_data.each{|t|
						content_packet_set(c, key, TYPE_FIXED, t, 1)
						c[:descriptor_count] += 1
					}
					random_data.clear
				elsif random_data.size != 0
					content_packet_set(c, key, TYPE_RANDOM, random_data)
					random_data.clear
					c[:descriptor_count] += 1
				end

				if type == TYPE_FIXED && first_data >= ORGNAIZING_DATA_FLAG_FIFO_DATA
					type = TYPE_FIFO_PAGE
					first_data &= 0xff
				end
				content_packet_set(c, key, type, first_data, macro_count)
				c[:descriptor_count] += 1
				d.slice!(0, macro_count - 1)
			end
			if random_data.size != 0
				if random_data.uniq.size == 1 && random_data.size == c[key].size
					content_packet_set(c, key, TYPE_FIXED, random_data[0], random_data.size)
					c[:descriptor_count] += 1
				elsif key == :data_read
					random_data.each{|t|
						content_packet_set(c, key, TYPE_FIXED, t, 1)
					}
					c[:descriptor_count] += random_data.size
				else
					content_packet_set(c, key, TYPE_RANDOM, random_data)
					c[:descriptor_count] += 1
				end
			end
		end
		def close
			c = @c
			if c[:packet_data] == nil
				c[:descriptor_count] = 0
				c[:packet_data] = []

				content_organize(c, :buscontrol, true)
				content_organize(c, :address, true)
				if c[:data_write].uniq != [nil]
					c[:data_write].map!{|t|
						if t == nil
							t = 0x3a
						end
						t
					}
					content_organize(c, :data_write, @organize_data_write)
				end
				u = c[:data_read].uniq
				#p ":data_read #{c[:data_read]} #{u}"
				if u.size == 0
					xxx
				end
				if u.size == 2 || (u.size == 1 && u[0] == TYPE_READ_VALID)
					content_organize(c, :data_read, true)
				end
				c[:packet_data][c[:last_header_offset]] |= 0b11 << 6
			end
			str = 'descriptor counts: '
			str += ' -> '
			str += c[:descriptor_count].to_s
			r = c[:packet_data]
			#$dma_puts = false
=begin
			if $dma_puts
				p r.size
				str = ''
				r.each{|t|
					str << ("0x%02x " % t)
				}
				Message::puts('[' + str.strip.gsub(" ", ", ") + ']')
			end
			if @c[:data_read].size == 64
				DebugOut::content_view(r.dup)
			end
=end
			if $dma_puts
				DebugOut::content_view(r.dup)
			end
			if c[:descriptor_count] >= 31
				Message.puts "error:" + str
				if $dma_puts == false
					p r
				end
				Fiber.raise
			end
			if r.size == 0x40
				#***kuldge***
				r << 0x80
			end
			r
		end
		def send_receive(command_number, command_arg, option = {})
			content = self.close()
			h = Command::header_send(command_number, command_arg, content.size, 0)
			if h == nil
				return
			end
			if option[:dodump]
				p h
				DebugOut.dump('command 0x%02x>' % command_number, content)
			end
			Hamo::device.send(content)
			data = []
			if option.key?(:callback)
				option[:callback].receive_data_set(data)
			end
			loop{
				h = Command::header_receive
				#p h
				if h[:status] & STATUS_NEXT_COMMAND_READY != 0
					if h[:mcu_to_pc] != 0
						Fiber.raise
					end
					return h #要修正: return が header か data になり紛らわしい
				elsif h[:mcu_to_pc] == 0
					break
				end
				if option.key?(:progressbar_receive_index)
					Hamo::Progressbar::draw_index_set(option[:progressbar_receive_index])
				end
				#p h
				d = Hamo::device.receive(h[:mcu_to_pc])
				if d.size == 0
					Message::puts "receive data timeout"
					Fiber.raise
				end
				Hamo::Progressbar::draw_index_set(-1)
				if option.key?(:callback)
					option[:callback].receive_done
				end
				data.concat(d)
				if (h[:status] & STATUS_STATUS_MASK) == STATUS_DONE
					break
				end
			}
			h = Command::header_receive
			if option[:dodump]
				p h
			end
			#p h
			data
		end
	end
	def header_receive(timeout_return_nil = false)
		data = Hamo::device.receive(1)
		if data == []
			if timeout_return_nil
				return
			end
			Message::puts('header_receive timeout')
			Fiber.raise
		end

		if (data[0] & 0xe0) != 0x40
			data += Hamo::device.receive((10 + 2) * 4 - 1)
			DebugOut.dump('<bad header ', data)
			Message::puts $increment.to_s
			puts "header index error:(data[0] & 0xe0) != 0x40"
			Fiber.raise
		end

		data.concat(Hamo::device.receive((data[0] & 0x1f) - 1))
		sum = 0
		data.each{|t|
			sum += t
		}
		if (sum & 0xff) != 0
			puts "header checksum error:(sum & 0xff) != 0"
			DebugOut.dump('<bad header ', data)
			Fiber.raise
		end
		h = {}
		data = data.pack("C16").unpack('C4V2vC2')
		[
			:header_length, :command_number, :status, :checksum,
			:pc_to_mcu, :mcu_to_pc, :increment
		].each{|key|
			h[key] = data.shift
		}
		h[:flash_status] = data.dup
		h[:status_error] = (h[:status] & STATUS_ERROR) != 0
		$increment = h[:increment]

		if h[:status_error]
			printf "status error line:%d\n", h[:increment]
			str = ['COMMAND', 'DATA'][(h[:status] >> 4) & 1] + ' '
			str += ['READY', 'REQUEST', 'BUSY', 'CONTINUE', 'DONE', 'ABORT'][h[:status] & 0x7]
			p h
			Message.puts str
			Fiber.raise
		end
		#p h
		h
	end
	def checksum_get(d)
		sum = 0xff
		d.each{|t|
			sum += t
		}
		sum &= 0xff
		sum ^= 0xff
		sum
	end
	def header_send(number, arg, pc_length, mcu_length)
=begin
		d = Array.new(9, 0)
		d[0] = 0x4a
		d[1] = number
		d[2, 2] = le16_pack pc_length
		d[4, 2] = le16_pack mcu_length
		d[6, 2] = arg
		d[6] |= 0x80
		d[8] = STATUS_TYPE_COMMAND | STATUS_REQUEST
		checksum_set(d)
=end
		d = repack([
			0x40 | 0x10, number, 
			STATUS_TYPE_COMMAND | STATUS_REQUEST, 0, 
			pc_length, mcu_length, 
		], 'C4V2')
		arg [0] |= 0x80
		d.concat(arg)
		if d.size < 16
			d.concat(Array.new(16 - d.size, 0))
		end
		d[3] = checksum_get(d)
		Hamo::device.send(d)
		if number == NUMBER_EXIT
			return
		end
		h = nil
		loop{
			h = header_receive
			if h[:command_number] == number
				break
			else
				Message::puts "unmatched command number #{h[:command_number]}"
				if h[:command_number] == 22
					Message::puts "status #{__LINE__} #{@status}"
				end
				xxx
			end
		}
		h
	end

	def read(region, length, address, option = {})
		c = Content.new
		c.access_set(length, region, RW_READ_WITH_DATA, address)
		c.send_receive(NUMBER_BUS_ACCESS_START, [0, 0], option)
	end

	def write_xx(region, length, address, data, rw, option)
		c = Content.new
		c.access_set(length, region, rw, address, data)
		c.send_receive(NUMBER_BUS_ACCESS_START, [0, 0], option)
	end
	def write(region, length, address, data, option)
		write_xx(region, length, address, data, RW_WRITE_WITH_DATA, option)
	end
	def write_progress(region, address, data, option)
		data[:value] = data[:value].dup
		while data[:value].size > 0
			c = Content.new
			c.organize_data_write_disable
			if option.key?(:content_initalizer)
				c = content_copy(option[:content_initalizer]) #for processor reset detector in MMC5
			end
			c.access_set(data[:value].size, region, RW_WRITE_WITH_DATA, address, data)
			content = c.close
			content_non_data_size = content.size - data[:value].size
			h = header_send(NUMBER_BUS_ACCESS_START, [0, 0], content.size, 0)
			data_size = data[:value].size
			if h[:pc_to_mcu] < content_non_data_size + data[:value].size
				data_size = h[:pc_to_mcu] - content_non_data_size
				loop{
					d = {:type => data[:type], :value => data[:value][0, data_size]}
					c = Content.new
					if option.key?(:content_initalizer)
						c = content_copy(option[:content_initalizer])
					end
					c.access_set(d[:value].size, region, RW_WRITE_WITH_DATA, address, d)
					content = c.close
					if content.size <= h[:pc_to_mcu]
						break
					end
					puts("d#{data_size}, c#{content.size}")
					data_size -= 0x40
				}
				content.concat(Array.new(h[:pc_to_mcu] - content.size, 0x80))
			end
			#p data_size, content.size

			Hamo::device.send(content)
=begin
OS の serial 送受信の callback では純粋な data だけを計測することができないため、mruby レイヤで progressbar 更新を呼び出す.
USB 経由での ROM read, backup RAM read, flash memory programming では OS の callback を利用可能なので Hamo::Progressbar::add は使わないこと.
=end
			if option.key?(:progressbar_send_index)
				Hamo::Progressbar::draw_index_set(option[:progressbar_send_index])
				Hamo::Progressbar::add(data_size)
				Hamo::Progressbar::draw_index_set(-1)
			end
			h = header_receive
			data[:value].slice!(0, data_size)
			address[:value][0] += data_size
		end
		#xx
	end
=begin
	def write_open(region, length, address)
		write_xx(region, length, address, nil, RW_WRITE_WITHOUT_DATA,  -1)
	end
=end
=begin
		autoselect = []
		autoselect << {:a => c_5555, :d => 0xaa}
		autoselect << {:a => c_2aaa, :d => 0x55}
		autoselect << {:a => c_5555, :d => 0x90}
		id = []
		id << {:a => 0x8000, :d => 0}
		id << {:a => 0x8001, :d => 0}
		reset = [{:a=> 0x8000, :d => 0xf0}]
		flash_id_read(REGION_CPU_6502, autoselect, id, reset)
=end
	class FlashDriver
		include Command
		def initialize(r)
			@status = [0, 0]
			@program_content_size = nil
			@programmers = r
		end
		def command_send(c, number, flag)
			@status = c.send_receive(number, flag)[:flash_status]
			if $dma_puts
				puts "status #{__LINE__} #{@status}"
			end
		end
		def flash_program_write(send_to_mcu)
			c = Content.new
			c.macro_limit_set(2)
			flag = [0, 0]
			#region 0: program -> region 1: program
			@programmers.each{|key, t|
				t.program_content_make_write(c)
			}
			#region 0: polling read -> region 1: polling read
			@programmers.each{|key, t|
				t.program_content_make_read(c, flag)
			}
			if send_to_mcu == false
				@program_content_size = c.close.size
				return
			end
			command_send(c, NUMBER_BUS_ACCESS_PROGRAM, flag)
		end
		def flash_program_start(c, region)
			flag = [0, 0]
			if region == Command::REGION_CPU_FLASH
				flag[0] |= 1 << 6
			else
				flag[1] |= 1 << 6
			end
			#puts "start flag #{flag}"
			command_send(c, NUMBER_BUS_ACCESS_START, flag)
		end
		
		def content_size_get
			@program_content_size
		end

		def status_get(index)
			@status[index]
		end

		def fifo_set(action, region, src)
			if src.size == 0
				case action
				when 1
					return
				when 2
					xxx
				end
			end
			h = Command::header_send(Command::NUMBER_FLASH_FIFO_DATA_SEND, [region | (action << 2), 0], [0xff00, src.size].min, 0)
			@status = h[:flash_status]
			if h == nil
				return
			end
			if h[:pc_to_mcu] == 0
				return h[:pc_to_mcu]
			end
			Hamo::device.send(src.slice!(0, h[:pc_to_mcu]))
			if $dma_puts
				printf("region %d, append:%d, size:%d\n", region, h[:pc_to_mcu], src.size)
			end
			j = Command::header_receive
			if j[:increment] == h[:increment]
				puts("same header...")
				j = Command::header_receive
				if j[:command_number] != Command::NUMBER_FLASH_FIFO_DATA_SEND
					puts("strange command number $%02x", j)
					xxx
				end
			end
			@status = j[:flash_status]
			if $dma_puts
				puts "status #{__LINE__} #{@status}"
			end
			return h[:pc_to_mcu]
		end

		def fifo_status_get
			h = self.header_send(NUMBER_FLASH_NOTIFY_CONTROL, [1, 0], 0, 0)
			@status = h[:flash_status]
		end
		def fifo_notify_wait(fifo_empty)
			#puts "send acknowledge"
			h = self.header_send(NUMBER_FLASH_NOTIFY_CONTROL, [fifo_empty ? 1 : 0, 0], 0, 0)
			@status = h[:flash_status]
			if $dma_puts
				puts "status #{__LINE__} #{@status}"
			end
			if @status == [0, 0]
				return
			end
			if fifo_empty == false
				cpu = @status[0] & (Programmer::FLASH_STATUS_FIFO_BUSY | Programmer::FLASH_STATUS_PROGRAMMING_BUSY)
				ppu = @status[1] & (Programmer::FLASH_STATUS_FIFO_BUSY | Programmer::FLASH_STATUS_PROGRAMMING_BUSY)
				if cpu == Programmer::FLASH_STATUS_PROGRAMMING_BUSY || ppu == Programmer::FLASH_STATUS_PROGRAMMING_BUSY
					return
				end
			end

			h = nil
			retry_count = 0
			loop{
				h = header_receive(Hamo::simulator?)
				if h == nil
					if retry_count >= 100
						Message::puts "notify waiting error: #{@status}"
						Fiber.raise
					end
					10000.times{
						Fiber.yield
					}
					retry_count += 1
				elsif h[:command_number] == NUMBER_FLASH_STATUS_NOTIFY
					@status = h[:flash_status]
					break
				end
			}
			if $dma_puts
				printf "notify [$%02x, $%02x]\n", @status[0], @status[1]
			end
			@notify_acknowledge = false
		end
		def flash_buffer_free
			h = header_send(NUMBER_FLASH_MODE_ALLOCATE_FREE, [0, 0], 0, 0)
			r = Hamo::device.receive(h[:mcu_to_pc])
			r.pack('C4').unpack('V1')[0]
		end
		
		def flash_buffer_allocate(size)
			if $dma_puts
				str = ''
				size.each{|t|
					str << (" %#x" % t)
				}
				str.strip!
				str.gsub!(" ", ", ")
				Message::puts "buffersizes: [#{str}]"
			end
			d = repack(size, 'V4')
			h = header_send(NUMBER_FLASH_MODE_ALLOCATE_FREE, [1, 0], d.size, 0)
			if h == nil
				Fiber.raise
			end
			Hamo::device.send(d)
			h = header_receive
		end
	end

	def version_get
		h = self.header_send(NUMBER_VERSION_GET, [0, 0], 0, 0)
		r = Hamo::device.receive(h[:mcu_to_pc])
		r
	end
	
	LOOP_TYPE_MEMORYACCESS = 0
	LOOP_TYPE_ADC = 1
	LOOP_TYPE_IRQTRIGGER = 2
	def busaccess_loopcount_set(type, count, sample_cc = 0, bit_depth = 0)
		if count == 0 || count >= 0x80000
			Fiber.raise
		end
		d = repack([type, bit_depth, sample_cc, count], 'C2v1V1')
		h = self.header_send(NUMBER_BUS_ACCESS_LOOPCOUNT_SET, [0, 0], d.size, 0)
		if h == nil
			Fiber.raise
		end
		Hamo::device.send(d)
		h = header_receive
	end
end
