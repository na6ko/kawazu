require Hamo::PROGRAM_DIR + '/driver_manager.rb'
require Hamo::PROGRAM_DIR + '/simulator/cartridge.rb'

module DriverManager
	#communicate to simulator
	module DeviceSimulator
		def device_open
			#simulator の場合は引数 hash が必要なためここではなにもせず別途 Hamo::simulator_open を呼ぶ
		end
		def device_close(binrymode_exit)
		end
		def simulation_start(s, f)
			Hamo::simulator_open(s)
			Hamo::device.task(f)
		end
	end
	class ActionFirmwareVersionGetSim < ActionFirmwareVersionGet
		include DeviceSimulator
		def device_name
			"@simulator"
		end
		def arguments
			""
		end
		def action
			s = {}
			f = Fiber.new{
				Hamo::device.port_init(s)
				super()
			}
			simulation_start(s, f)
		end
	end
	class ActionRomDumpSim < ActionRomDump
		include DeviceSimulator
		def device_name
			"@simulator"
		end
		def arguments
			"[drivername] [in:filename.nes] (out:dirname.nes)"
		end
		def panel
			"etc"
		end
		def action
			h = Nesfile.Load(@arg["in:filename.nes"])
			if @driver.auto_detector
				@driver = @driver.new_by_mappernum(h[:mappernum])
			end
			prg = SimulatorMemory::Rom.new(h[:cpu_program][:data])
			if h[:ppu_chara][:data].size != 0
				chr = SimulatorMemory::Rom.new(h[:ppu_chara][:data])
			end
			s = {}
			if @driver.class.name == "DriverDiskSystem"
				r = SimulatorMemory::Rom.new([0])
				s[:cartridge] = SimulatorCartridge::PCB_Disksystem.new(r, Array.new(0x1000, 0))
			else
				case h[:mappernum]
				when 1
					s[:cartridge] = SimulatorCartridge::PCB_SKROM.new(prg, chr)
					#TBD: SNROM
				when 2
					s[:cartridge] = SimulatorCartridge::PCB_UNROM.new(prg, h[:vram_mirror], 0)
				when 3
					s[:cartridge] = SimulatorCartridge::PCB_CNROM.new(prg, chr, h[:vram_mirror], 0)
				when 4
					if h[:ppu_chara][:data].size == 0
						s[:cartridge] = SimulatorCartridge::PCB_TNROM.new(prg, 0)
					else
						s[:cartridge] = SimulatorCartridge::PCB_TKROM.new(prg, chr, 0)
					end
				when 16
					s[:cartridge] = SimulatorCartridge::PCB_LZ93D50.new(prg, chr)
				when 26
					s[:cartridge] = SimulatorCartridge::PCB_VRC6B.new(prg, chr)
				when 72
					s[:cartridge] = SimulatorCartridge::PCB_JF17.new(prg, chr, h[:vram_mirror], 0)
				else
					Message.puts "simulator does not support mapper #%d" % h[:mappernum]
					xx
				end
			end
			f = Fiber.new{
				Hamo::device.port_init(s)
				@driver.rom_dump(@arg)
			}
			simulation_start(s, f)
		end
	end
	class ActionCartridgeFlashProgramSim < ActionProgram
		include DeviceSimulator
		def action_init(s, nesfile)
			prg = SimulatorMemory::AM29F040B.new("prg_flash", Array.new(0x80000, 0xff))
			#prg = SimulatorMemory::W29C040.new("prg_flash", Array.new(0x80000, 0xff))
			case @driver.class.name
			when "DriverTKROM"
				if nesfile[:ppu_chara][:data].size == 0
					s[:cartridge] = SimulatorCartridge::PCB_TNROM.new(prg, 0)
					start = 0x7c000
					nesfile[:cpu_program][:data][0x00000...start] = Array.new(start - 0x0000, 0xff)#test code
				else
					s[:cartridge] = SimulatorCartridge::PCB_TKROM.new(
						prg, 
						SimulatorMemory::AM29F040B.new("chr_flash", Array.new(0x40000, 0xff)),
						0
					)
					#return
					start = 0x1c000
					nesfile[:cpu_program][:data][0x00000...start] = Array.new(start - 0x0000, 0xff)
					nesfile[:ppu_chara][:data][0x00000...start] = Array.new(start - 0x0000, 0xff)
					#nesfile[:ppu_chara][:data][0x00000...0xf800] = Array.new(0xf800, 0xff)
				end
			when "DriverVRC6"
				prg = SimulatorMemory::SST39SF040.new("prg_flash", Array.new(0x80000, 0xff))
				chr = SimulatorMemory::W29C040.new("chr_flash", Array.new(0x80000, 0xff))
				start = 0x18000
				#nesfile[:cpu_program][:data][0x00000...start] = Array.new(start - 0x0000, 0xff)
				s[:cartridge] = SimulatorCartridge::PCB_VRC6B.new(prg, chr)
			when "DriverUNROM"
				#nametable 設定の確認の確認のみで flash への program は未実装
				s[:cartridge] = SimulatorCartridge::PCB_UNROM.new(prg, Nesfile::VRAM_MIRROR_H, 0)
			else
				Message::puts("unsupported hardware #{@driver.class.name}")
				Fiber.raise
			end
		end
		def action_start(s, region_values)
			t = Programmer::ProgrammerBoth.new
			f = Fiber.new{
				Hamo::device.port_init(s)
				t.flash_programming_main(@driver, region_values, @arg)
			}
			simulation_start(s, f)
		end
	end
	class ActionCartridgeFlashIdGetSim < ActionFlashIDGet
		include DeviceSimulator
		def arguments
			"[drivername]"
		end
		def action_init(s)
			case @driver.class.name
			when "DriverTKROM"
				prg = SimulatorMemory::AM29F040B.new("prg_flash", Array.new(0x08000, 0xff))
				s[:cartridge] = SimulatorCartridge::PCB_TNROM.new(prg, 0)
			when "DriverVRC6"
				prg = SimulatorMemory::SST39SF040.new("prg_flash", Array.new(0x08000, 0xff))
				chr = SimulatorMemory::W29C040.new("chr_flash", Array.new(0x08000, 0xff))
				s[:cartridge] = SimulatorCartridge::PCB_VRC6B.new(prg, chr)
			end
		end
		def action_start(s, region_values)
			@arg[:simulation] = true
			t = Programmer::ProgrammerBoth.new
			f = Fiber.new{
				Hamo::device.port_init(s)
				t.flash_id_get(@driver, region_values)
			}
			simulation_start(s, f)
		end
	end
	
	module SimReadWrite
		def s_get
			if @driver.auto_detector
				xxx
			end
			s = {}
			case @driver.class.name
			when "DriverTKROM", "DriverEKROM"
				s[:cartridge] = SimulatorCartridge::PCB_TNROM.new(nil, 0)
			when "DriverLZ93D50Standard"
				s[:cartridge] = SimulatorCartridge::PCB_LZ93D50.new(nil, nil)
			when "Driver74161JF17"
				rom = []
				0x100.times{|i|
					rom << i
				}
				[0, 0x10, 0x20, 0x30].each{|d|
					0x20.times{
						rom << d
					}
				}
				while rom.size < 0x200
					rom << 0
				end
				s[:cartridge] = SimulatorCartridge::PCB_JF17.new(SimulatorMemory::Rom.new(rom), nil,  Nesfile::VRAM_MIRROR_V, 0)
			when "DriverDiskSystem"
				f = File.open("../dump/ZAN.raw", "rb")
				if f.read(4) != "RAW1"
					Message.puts "header error"
					Fiber.raise
				end
				length = f.read(4).unpack("V1")[0]
				if length > 0x30000
					Message.puts "image size is too large"
					Fiber.raise
				end
				image = f.read(length).unpack("C*")
				f.close
				r = SimulatorMemory::Rom.new([0])
				s[:cartridge] = SimulatorCartridge::PCB_Disksystem.new(r, image)
			else
				Message.puts @driver.class.name + " is not supproted for simulation"
				Fiber.raise
			end
			s
		end
	end
	class ActionRamWriteSim < ActionRamWrite
		include DeviceSimulator
		include SimReadWrite
		def device_name
			"@simulator"
		end
		def arguments
			"[drivername] [in:filename.sav]"
		end
		def action
			s = s_get
			f = Fiber.new{
				Hamo::device.port_init(s)
				super
			}
			simulation_start(s, f)
		end
	end
	class ActionRamReadSim < ActionRamRead
		include DeviceSimulator
		include SimReadWrite
		def device_name
			"@simulator"
		end
		def arguments
			"[drivername] [in:filename.sav] (out:dirname.sav)"
		end
		def action
			s = s_get
			f = Fiber.new{
				Hamo::device.port_init(s)
				super
			}
			simulation_start(s, f)
		end
	end
end
