#pragma once
#ifndef _HAMO_INCLUDED_
#error "please #include <hamo.h>"
#endif

struct progressbar_callback{
	void (*range_set)(void *t, int32_t index, const char *name, int32_t length);
	void (*add)(void *t, int32_t index, int32_t length);
	void (*ready)(void *t);
	void (*done)(void *t);
	void (*label_update)(void *t, int32_t index, const char *name);
};

//called by CUI or GUI manager
struct mrb_state;
DLLFUNC void progressbar_init(struct mrb_state *m, void *klass, const struct progressbar_callback *callback);

