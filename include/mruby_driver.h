#pragma once

struct mrb_value;
struct script_function_list{
	const char *name;
	mrb_value (*func)(struct mrb_state *m, mrb_value self);
	uint32_t argc;
};
struct RClass;
DLLFUNC void hamo_module_define(struct mrb_state *m, struct RClass *mm, const char *modulename, const struct script_function_list *l);
DLLFUNC void hamo_class_define(struct mrb_state *m, struct RClass *mm, const char *classname, const struct script_function_list *l);

struct script_array{
	mrb_value mrb_array;
	mrb_int size;
	void *parent;
	void (*callback)(void *parent, const uint8_t *data, mrb_int length);
};
DLLFUNC int mruby_array_into_uint8_t(struct mrb_state *m, struct script_array *t);
DLLFUNC void uint8_t_into_mruby_array(struct mrb_state *m, mrb_value mrb_array, const uint8_t *src, int length);
