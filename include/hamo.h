#pragma once
#define _HAMO_INCLUDED_
#ifdef _HAMO_BUILD_
#define DLLFUNC __declspec(dllexport) 
#endif 
#ifdef _HAMO_LOAD_
#define DLLFUNC __declspec(dllimport) 
#endif
#ifdef _HAMO_STATICLINK_
#define DLLFUNC 
#endif
#include <stdint.h>
//mruby interface
DLLFUNC struct mrb_state *hamo_mrb_open(void);
DLLFUNC int32_t hamo_main(struct mrb_state *m, int32_t c, char **v);
DLLFUNC void hamo_mrb_close(struct mrb_state *m);

//string encoding interface
DLLFUNC int program_dir_concat(const char *suffix, const char dir, char *dest, int dest_length);
DLLFUNC int my_wmain(int (*callback)(int c, char **v));

#include <klass_manager.h>
#include <textarea.h>
#include <progressbar.h>
