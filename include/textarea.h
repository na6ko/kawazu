#pragma once
#ifndef _HAMO_INCLUDED_
#error "please #include <hamo.h>"
#endif
struct text_item{
	const char *key, *value;
};
typedef void (*hamo_callback_item_add)(void *t, int32_t c, const struct text_item *items);

struct mrb_state;
//message
struct errorlog_callback{
	void *(*open)(void *klass);
	void (*puts)(void *t, const char *str);
	void (*close)(void *t);
};
DLLFUNC void message_new(struct mrb_state *m, void *klass, void (*print_callback)(void *t, const char *str), const char *logfilename, const char *mode, const struct errorlog_callback *errorlog_callback);
DLLFUNC void message_delete(struct mrb_state *m);
//cartridge drivers
DLLFUNC void list_driver_init(struct mrb_state *m, void *klass, hamo_callback_item_add callback);
DLLFUNC void list_driver_clear(struct mrb_state *m);
//actions
DLLFUNC void list_action_init(struct mrb_state *m, void *klass, hamo_callback_item_add callback);
DLLFUNC void list_action_clear(struct mrb_state *m);
//flashdevices
DLLFUNC void list_flashdevice_init(struct mrb_state *m, void *klass, hamo_callback_item_add callback);
DLLFUNC void list_flashdevice_clear(struct mrb_state *m);
//config
DLLFUNC void list_config_init(struct mrb_state *m, void *klass, hamo_callback_item_add config_send);
//void (*config_set)(void *t, const char *key, const char *value));
DLLFUNC void list_config_clear(struct mrb_state *m);
//title to drivers
DLLFUNC int32_t hamo_title_to_driver_load(struct mrb_state *m, void *klass, hamo_callback_item_add callback);
DLLFUNC int32_t hamo_title_to_driver_call(struct mrb_state *m, const char *word);
DLLFUNC void hamo_title_to_driver_clear(struct mrb_state *m);
