/* 
THIS FILE IS AUTO-GENERATED
by klass_manager.rb
*/

#pragma once
struct mrb_state;
struct simulator_usb_driver;
DLLFUNC void klass_simulator_usb_driver_register(struct mrb_state *m, struct simulator_usb_driver *t);
DLLFUNC struct simulator_usb_driver *klass_simulator_usb_driver_ref(struct mrb_state *m);

struct simulator_cartridge_bus;
DLLFUNC void klass_simulator_cartridge_bus_register(struct mrb_state *m, struct simulator_cartridge_bus *t);
DLLFUNC struct simulator_cartridge_bus *klass_simulator_cartridge_bus_ref(struct mrb_state *m);

