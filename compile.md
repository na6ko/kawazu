# 動作環境
Msys2 でのみ確認. 現状 Windows のみで OS 依存のソースコードが一部含まれる.

# ビルド
現状 CUI に配布用 (`source_core/hamo_cui.mak`), デバッグ用 (`source_core/hamo_debug.mak`) の2種類. 配布用の DLL (`source_core/hamo_dll.mak`) の合計 3 つのビルドがある. 

`source_core/hamo_cui.mak` では `source_core/hamo.dll` があればリンクして実行できる. しかし残りは mruby とシミュレータのための MCU 側のソースコードが必要で、このレポジトリのみではビルドはできない. 

将来的にこの問題に対応するが、暫定処置として `source_core/hamo.dll` はコンパイル済みのバイナリファイルをレポジトリに置く.

## ライブラリと path
`source_core/path.mk` は現状非公開の MCU 用のソースコードを配置する. ユーザー個別の絶対パスを含める前提なのでレポジトリにはいれていない. 下記は筆者の `source_core/path.mk` の内容. `SBP_DIR` の内容はこれに合わせる必要はない.
```
SBP_DIR = d:/work/hvc/sam_bus_programmer/firmware
SBP_LIB = $(SBP_DIR)/lib/sbp.a
```

mruby はこのディレクトリに展開する前提で `source_core/base.mk` に記述してある. レポジトリに含めているファイルは require が最新版 (mruby-3.2.0) 向けでコンパイルが通らなかったのでその対策のみ含めている.

## bin/Makefile
3つの .mak を続けてビルドするので現状原作者のみ利用可能.

## CUI 配布用
```
make -C kawazu/source_core -f hamo_cui.mak
```

このビルドは3つのソースファイルだけでコールバック関数を実装している. GUI 版作成の参考にしてほしい.

## GUI
GUI 版では bin/hamo.dll をリンクして利用できる. source_gui にソースを配置すること. 

